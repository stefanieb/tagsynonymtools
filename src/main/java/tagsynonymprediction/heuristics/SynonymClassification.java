package tagsynonymprediction.heuristics;

import java.util.ArrayList;
import java.util.List;

import tagsynonymprediction.categories.maincat.AbbreviationDot;
import tagsynonymprediction.categories.maincat.AbbreviationMinus;
import tagsynonymprediction.categories.maincat.AbstrTagSynonymClass;
import tagsynonymprediction.categories.maincat.Dots;
import tagsynonymprediction.categories.maincat.MetaphoneClass;
import tagsynonymprediction.categories.maincat.Minus;
import tagsynonymprediction.categories.maincat.Plus;
import tagsynonymprediction.categories.maincat.Sharp;
import tagsynonymprediction.categories.maincat.Stem;
import tagsynonymprediction.categories.maincat.SynonymInTag;
import tagsynonymprediction.categories.maincat.SynonymInWord;
import tagsynonymprediction.categories.maincat.Version;
import tagsynonymprediction.categories.similarity.JaccardSimilarity;
import tagsynonymprediction.categories.similarity.LevenstheinSimilarity;
import tagsynonymprediction.categories.similarity.NGramSimilarity;
import tagsynonymprediction.models.SynonymCategory;
import tagsynonymprediction.utility.HelperFunctions;
import tagsynonymprediction.utility.Utilities;

public class SynonymClassification {

	private Stem stem;
	private Version version;
	private Dots dots;

	private List<String> others;

	private HelperFunctions hf;
	private ArrayList<AbstrTagSynonymClass> typeList;
	private SynonymInTag synonymInTag;
	private Plus plus;
	private Sharp sharp;
	private JaccardSimilarity jaccard;
	private MetaphoneClass metaphone;
	private LevenstheinSimilarity levensthein;
	private NGramSimilarity ngram;
	private SynonymInWord synonymInWord;
	private AbbreviationMinus abbreviationMinus;
	private AbbreviationDot abbreviationDot;
	private Minus minus;

	private List<String> notFoundString;

	private int notFound = 0;

	public int getNotFound() {
		return notFound;
	}

	public void printNotFound() {
		for (String s : notFoundString) {
			System.out.println(s);
		}
		notFoundString = new ArrayList<>();
	}

	public SynonymClassification() {
		stem = new Stem();
		version = new Version();
		dots = new Dots();
		sharp = new Sharp();
		minus = new Minus();
		plus = new Plus();
		synonymInTag = new SynonymInTag();
		abbreviationDot = new AbbreviationDot();
		abbreviationMinus = new AbbreviationMinus();
		synonymInWord = new SynonymInWord();
		ngram = new NGramSimilarity();
		levensthein = new LevenstheinSimilarity();
		jaccard = new JaccardSimilarity();
		metaphone = new MetaphoneClass();
		notFoundString = new ArrayList<>();
	}

	public void classifySynonymNew(String source, String target, boolean isSpecialTag) {

		// stem
		if (stem.isSynonymClassic(source, target, isSpecialTag)) {
			return;
		}
		// version
		else if (version.isSynonymClassic(source, target, isSpecialTag)) {
			return;
		}
		// dots
		else if (dots.isSynonymClassic(source, target, isSpecialTag)) {
			return;
		}
		// sharp
		else if (sharp.isSynonymClassic(source, target, isSpecialTag)) {
			return;
		}
		// minus
		else if (minus.isSynonymClassic(source, target, isSpecialTag)) {
			return;
		}
		// plus
		else if (plus.isSynonymClassic(source, target, isSpecialTag)) {
			return;
		}
		// synonym In Tag
		else if (synonymInTag.isSynonymClassic(source, target, isSpecialTag)) {
			return;
		}
		// abbreviationDot
		else if (abbreviationDot.isSynonymClassic(source, target, isSpecialTag)) {
			return;
		}
		// abbreviationMinus
		else if (abbreviationMinus.isSynonymClassic(source, target, isSpecialTag)) {
			return;
		}
		// synonymInWord
		else if (synonymInWord.isSynonymClassic(source, target, isSpecialTag)) {
			return;
		}
		// ngram
		else if (ngram.isSynonymClassic(source, target, isSpecialTag)) {
			return;
		}
		// levensthein
		else if (levensthein.isSynonymClassic(source, target, isSpecialTag)) {
			return;
		}
		// jaccard
		else if (jaccard.isSynonymClassic(source, target, isSpecialTag)) {
			return;
		}
		// methaphone
		else if (metaphone.isSynonymClassic(source, target, isSpecialTag)) {
			return;
		}
		notFound++;
		notFoundString.add(source + " -- " + target);

	}

	public void classifySynonym(String source, String target) {

		if (stem.isSynonymClass(source, target)) {
			stem.add(source + " -- " + target);
			return;
		} else // einkommentieren wenn ausschließend
			if (version.isSynonymClass(source, target)) {
			version.add(source + " -- " + target);
			return;
		}

		// stemming porter, stemming english, number replacement
		String[] stem2AndNumberSource = new String[2];
		String[] stem2AndNumberTarget = new String[2];

		stem2AndNumberSource[0] = source;
		stem2AndNumberSource[1] = Utilities.getStemmedTag(source);

		// stem2AndNumberSource[2] = version.getNumbersInStrings(source);
		stem2AndNumberTarget[0] = target;
		stem2AndNumberTarget[1] = Utilities.getStemmedTag(target);
		// stem2AndNumberTarget[2] = version.getNumbersInStrings(target);

		boolean check = false;
		outerloop: for (int j = 2; j < hf.getListOfSynonymCreationTypes().size(); j++) {
			AbstrTagSynonymClass atsc = hf.getListOfSynonymCreationTypes().get(j);

			innerloop: for (int i = 0; i < stem2AndNumberSource.length; i++) {
				if (atsc.isSynonymClass(stem2AndNumberSource[i], stem2AndNumberTarget[i])) {
					atsc.add(source + " -- " + target);
					check = true;
					// break innerloop;
					break outerloop; // einkommentieren wenn ausschließend
				}
			}

		}

		if (!check) {
			others.add(source + " -- " + target);
		}

	}

	public void printPairsForClass() {
		// ?? msas -- ssas, xsrf -- csrf (dot?)
		// clojure.core.logic -- clojure-core.logic abbreviation?,
		// statistical-analysis -- statistics
		// hf.getListOfSynonymCreationTypes().get(4).printSynonymPairs();

		sanityCheck();

		System.out.println("OTHERS: " + others.size());

		for (String s : others) {
			System.out.println(s);
		}

		System.out.println("alle paare: " + (SynonymCategory.getCountOfAll() + others.size()));
	}

	private void sanityCheck() {
		// sanity check

		hf.getListOfSynonymCreationTypes().get(2).getSynonymPairsAsList();

		List<String> stemms = stem.getSynonymPairsAsList();
		List<String> versions = version.getSynonymPairsAsList();
		List<String> dotsharpminuspluss = new ArrayList<>();
		dotsharpminuspluss.addAll(hf.getListOfSynonymCreationTypes().get(2).getSynonymPairsAsList());
		dotsharpminuspluss.addAll(hf.getListOfSynonymCreationTypes().get(3).getSynonymPairsAsList());
		dotsharpminuspluss.addAll(hf.getListOfSynonymCreationTypes().get(4).getSynonymPairsAsList());
		dotsharpminuspluss.addAll(hf.getListOfSynonymCreationTypes().get(5).getSynonymPairsAsList());
		List<String> synInTag = hf.getListOfSynonymCreationTypes().get(6).getSynonymPairsAsList();
		List<String> abbreviation = hf.getListOfSynonymCreationTypes().get(7).getSynonymPairsAsList();
		abbreviation.addAll(hf.getListOfSynonymCreationTypes().get(8).getSynonymPairsAsList());

		List<String> synInWord = hf.getListOfSynonymCreationTypes().get(9).getSynonymPairsAsList();
		synInWord.size();

		List<String> similarity = hf.getListOfSynonymCreationTypes().get(10).getSynonymPairsAsList();
		similarity.addAll(hf.getListOfSynonymCreationTypes().get(11).getSynonymPairsAsList());
		similarity.addAll(hf.getListOfSynonymCreationTypes().get(12).getSynonymPairsAsList());

		List<String> metaphone = hf.getListOfSynonymCreationTypes().get(13).getSynonymPairsAsList();

		// for (String s : others) {
		// if (stemms.contains(s) || versions.contains(s) ||
		// metaphone.contains(s) || dotsharpminuspluss.contains(s) ||
		// synInTag.contains(s) || synInWord.contains(s)
		// || abbreviation.contains(s) || similarity.contains(s)) {
		// System.out.println("ERROROROROR" + s);
		// }
		// }

	}
}
//