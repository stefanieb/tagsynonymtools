
package tagsynonymprediction.heuristics;

import java.util.HashMap;
import java.util.Map;

public class SynonymCoverage {

	private String newSource;
	private String newTarget;

	public void calculateCoverage() {

		// HelperFunctions hf = HelperFunctions.getInstance();

		SynonymClassification synClassification = new SynonymClassification();

		// Map<String, String> synonymPairs = hf.getAllSynonymPairs();

		// Map<String, String> synonymPairs = DBHandler.getAllTagsAndSynonymsFromDb(0, 5000);
		Map<String, String> synonymPairs = getOwnSynonymPairs();
		System.out.println("SIZE: " + synonymPairs.size());
		int count = 0;
		for (String source : synonymPairs.keySet()) {
			String target = synonymPairs.get(source);

			// boolean special = isSpecialTag(source, target);
			boolean special = false;
			newSource = source;
			newTarget = target;
			synClassification.classifySynonymNew(newSource, newTarget, special);
			count++;
			if (count % 100 == 0) {
				System.out.println("count: " + count);
				System.out.println("not found: " + synClassification.getNotFound());
				synClassification.printNotFound();
			}
		}

		System.out.println("==========");
		System.out.println("NOT FOUND: " + synClassification.getNotFound());
		System.out.println((synonymPairs.size() - synClassification.getNotFound()) + " synonym pairs of " + synonymPairs.size() + " found");
		System.out.println("percentage: " + (100 - (synClassification.getNotFound() / 2765)));
		synClassification.printNotFound();
		// synClassification.printPairsForClass();
		// SynonymCategory.printHeuristics();

	}

	private Map<String, String> getOwnSynonymPairs() {
		Map<String, String> synonymPairs = new HashMap<>();
		synonymPairs.put("jws", "java-web-start");
		// synonymPairs.put("time", "timetable");
		// synonymPairs.put("elisp", "lisp");
		// synonymPairs.put("bigint", "biginteger");
		// synonymPairs.put("msie7", "internet-explorer-7");
		// synonymPairs.put("yml", "yaml");
		// synonymPairs.put("rewriterule", "mod-rewrite");
		// synonymPairs.put("scalar-udf", "user-defined-functions");
		// synonymPairs.put("webspiders", "web-crawler");
		// synonymPairs.put("rewritecond", "mod-rewrite");
		// synonymPairs.put("msie8", "internet-explorer-8");

		return synonymPairs;
	}

	private boolean isSpecialTag(String source, String target) {

		String tagToParse1 = "";
		String tagToParse = "";
		if (source.equals("android") || target.equals("java")) {
			return false;
		}

		newSource = source;
		newTarget = target;

		// handle source

		if (source.startsWith("android-")) {
			tagToParse1 = source.replace("android-", "");
			tagToParse1 = tagToParse1.replaceAll("\\d+(\\.?\\d+)", "");
			tagToParse1 = tagToParse1.replaceAll("\\d+", "");
			tagToParse1 = tagToParse1.trim();
			newSource = tagToParse1;
			if (tagToParse1.equals("")) {
				newSource = source;
			}

		} else if (source.startsWith("java-")) {
			tagToParse1 = source.replace("java-", "");
			tagToParse1 = tagToParse1.replaceAll("\\d+(\\.?\\d+)", "");
			tagToParse1 = tagToParse1.replaceAll("\\d+", "");
			newSource = tagToParse1;
			if (tagToParse1.equals("")) {
				newSource = source;
			}
		}

		// handle target

		if (target.startsWith("android-")) {
			tagToParse = target.replace("android-", "");
			tagToParse = tagToParse.replaceAll("\\d+(\\.?\\d+)", "");
			tagToParse = tagToParse.replaceAll("\\d+", "");
			tagToParse = tagToParse.trim();
			newTarget = tagToParse;
			if (tagToParse.equals("")) {
				newTarget = target;
			}

		} else if (target.startsWith("java-")) {
			tagToParse = target.replace("java-", "");
			tagToParse = tagToParse.replaceAll("\\d+(\\.?\\d+)", "");
			tagToParse = tagToParse.replaceAll("\\d+", "");
			newTarget = tagToParse;
			if (tagToParse.equals("")) {
				newTarget = target;
			}
		}

		if (!newSource.equals(source) || !newTarget.equals(target)) {
			return true;
		}

		return false;
	}

	public static void main(String args[]) {

		SynonymCoverage sc = new SynonymCoverage();
		sc.calculateCoverage();

	}

}
