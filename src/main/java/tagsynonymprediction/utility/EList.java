package tagsynonymprediction.utility;

import java.util.List;

public interface EList<E> extends List<E> {
	public EList<E> getTopOfList(int top);

	public boolean addAllElements(E... elements);
}
