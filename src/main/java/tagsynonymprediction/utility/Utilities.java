package tagsynonymprediction.utility;

import org.apache.lucene.search.spell.LevensteinDistance;
import org.tartarus.snowball.ext.PorterStemmer;

import tagsynonymprediction.main.TagSynonymPrediction;

public class Utilities {

	private static PorterStemmer p2;

	static {
		p2 = new PorterStemmer();
	}

	public static String getStemmedTag(String tag) {
		if (tag.equals("ios") || tag.startsWith("ios-") || tag.endsWith("-ios") || tag.contains("-ios-")
				|| tag.length() < 2) {
			return tag;
		}

		p2.setCurrent(tag);
		p2.stem();
		return p2.getCurrent();

	}

	public static Float getSimilarity(String s1, String s2) {
		LevensteinDistance ld = new LevensteinDistance();
		return ld.getDistance(s1, s2);
	}

	public static void main(String[] args) {
		TagSynonymPrediction tsp = new TagSynonymPrediction();
		tsp.saveIntoDB(true);
		tsp.setPrintResults(false);
		tsp.setUseTestTags(false);
		tsp.startTagPrediction();
	}
}
