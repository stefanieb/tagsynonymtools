package tagsynonymprediction.utility;

import java.util.ArrayList;

import com.google.common.collect.Sets.SetView;

public class EArrayList<E> extends ArrayList<E> implements EList<E> {

	private static final long serialVersionUID = 1L;

	public EArrayList() {
	}

	public EArrayList(SetView<E> intersection) {
		this.addAll(intersection);

	}

	/**
	 * returns top top elements of the list
	 * 
	 * @param top
	 * @return
	 */
	public EList<E> getTopOfList(int top) {
		EList<E> returnList = new EArrayList<E>();
		for (int i = 0; i < this.size() && i < top; i++) {
			returnList.add(this.get(i));
		}
		return returnList;
	}

	@Override
	public boolean addAllElements(@SuppressWarnings("unchecked") E... elements) {
		for (E e : elements) {
			this.add(e);
		}

		return true;
	}

}
