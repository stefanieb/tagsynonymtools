package tagsynonymprediction.utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import tagsynonymprediction.categories.maincat.AbbreviationDot;
import tagsynonymprediction.categories.maincat.AbbreviationMinus;
import tagsynonymprediction.categories.maincat.AbstrTagSynonymClass;
import tagsynonymprediction.categories.maincat.Dots;
import tagsynonymprediction.categories.maincat.MetaphoneClass;
import tagsynonymprediction.categories.maincat.Minus;
import tagsynonymprediction.categories.maincat.Plus;
import tagsynonymprediction.categories.maincat.Sharp;
import tagsynonymprediction.categories.maincat.Stem;
import tagsynonymprediction.categories.maincat.SynonymInTag;
import tagsynonymprediction.categories.maincat.SynonymInWord;
import tagsynonymprediction.categories.maincat.Version;
import tagsynonymprediction.categories.similarity.JaccardSimilarity;
import tagsynonymprediction.categories.similarity.LevenstheinSimilarity;
import tagsynonymprediction.categories.similarity.NGramSimilarity;
import tagsynonymprediction.db.DBHandler;
import tagsynonymprediction.models.SynonymCategory;
import tagsynonymprediction.models.Tag;

public class HelperFunctions {

	private static HelperFunctions singleton = new HelperFunctions();

	private Map<String, Tag> ORIGINAL_TAGS;
	private Map<String, String> allSynonymPairs;
	private List<AbstrTagSynonymClass> typeList;

	/*
	 * A private Constructor prevents any other class from instantiating.
	 */
	private HelperFunctions() {
		System.out.println("start init helper fuctions");
		// this.ORIGINAL_TAGS = DBHandler.getAllTagsFromDb();

		// comment out when only using for tests
		this.ORIGINAL_TAGS = initOriginalTags();
		this.allSynonymPairs = DBHandler.getAllTagsAndSynonymsFromDb(0, 5000);
		System.out.println("end init helper fuctions");

	}

	private Map<String, Tag> initOriginalTags() {
		Set<Tag> allTags = DBHandler.getAllTagsFromDb();
		allTags.addAll(DBHandler.getAllTagsOfReferenceSetDisregardingTagTable());

		Map<String, Tag> returnMap = new HashMap<String, Tag>();
		for (Tag tag : allTags) {
			returnMap.put(tag.getTagName(), tag);
		}
		return returnMap;
	}

	/**
	 * returns a set of all tags
	 * 
	 * @return
	 */
	public Set<Tag> getAllOriginalTags() {
		Set<Tag> returnSet = new HashSet<>();
		// returnSet.addAll(ORIGINAL_TAGS.values());
		returnSet.addAll(getAllAndroidTags());
		return returnSet;
	}

	public Set<Tag> getAllAndroidTags() {
		return DBHandler.getAllAndroidTags();
	}

	/**
	 * initializes the list of SynonymClassificationAbstr
	 */
	public void initTypeList() {
		this.typeList = new ArrayList<>();
		typeList.add(new Stem());
		// 1
		typeList.add(new Version());
		// 2
		typeList.add(new Dots());

		typeList.add(new Sharp());

		typeList.add(new Minus());

		typeList.add(new Plus());
		// // 3
		typeList.add(new SynonymInTag());

		typeList.add(new AbbreviationDot());

		typeList.add(new AbbreviationMinus());

		typeList.add(new SynonymInWord());

		typeList.add(new NGramSimilarity());
		// 9
		typeList.add(new LevenstheinSimilarity());
		// 10
		typeList.add(new JaccardSimilarity());
		//
		typeList.add(new MetaphoneClass());
	}

	/**
	 * returns a list of all synonym pairs disregarding of the existence of the tags
	 * 
	 * @return
	 */
	public Map<String, String> getAllSynonymPairs() {
		return allSynonymPairs;
	}

	/**
	 * singleton - returniert 1 instanz
	 * 
	 * @return
	 */
	public static HelperFunctions getInstance() {
		return singleton;
	}

	/**
	 * returns if the tagSuggestion is contained in original tags
	 * 
	 * @param tag
	 * @return
	 */
	public boolean isContainedInOriginalTags(String tagSynonymSuggestion) {
		return ORIGINAL_TAGS.containsKey(tagSynonymSuggestion);
	}

	public List<AbstrTagSynonymClass> getListOfSynonymCreationTypes() {

		return typeList;
	}

	/**
	 * returns true if source tag is subtag of target
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public boolean isSourceTargetRelation(String source, String target, SynonymCategory cat) {
		Integer countSource = getCountOfTag(source);
		Integer countTarget = getCountOfTag(target);

		// was mehr subtags hat, ist meisst spezieller
		if (cat.equals(SynonymCategory.SYNONYM_IN_TAG_LIKE) || cat.equals(SynonymCategory.SYNONYM_IN_TAG_SUBTAG)) {
			int sourceCount = getCountOfSymbol('-', source);
			int targetCount = getCountOfSymbol('-', target);

			if (sourceCount != targetCount) {
				return sourceCount > targetCount;
			}

		} else if (cat.equals(SynonymCategory.SYNONYM_IN_TAG)) {
			if (getCountOfSymbol('-', source) != getCountOfSymbol('-', target)) {
				return source.length() > target.length();
			}
		}

		return countSource < countTarget;
	}

	public int getCountOfSymbol(char c, String s) {
		int counter = 0;
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == c) {
				counter++;
			}
		}
		return counter;
	}

	public int getCountOfTag(String tagName) {
		if (ORIGINAL_TAGS.containsKey(tagName)) {
			return ORIGINAL_TAGS.get(tagName).getCount();
		}
		return -1;
	}

	public Double getAvgTagPositionOfTag(String tagName) {
		if (ORIGINAL_TAGS.containsKey(tagName)) {
			return ORIGINAL_TAGS.get(tagName).getAvgTagPosition();
		}
		return 2.5;
	}

}
