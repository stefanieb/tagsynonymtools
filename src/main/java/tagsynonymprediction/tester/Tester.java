package tagsynonymprediction.tester;

import java.util.HashMap;
import java.util.List;
import java.util.Map;



import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Table;
import tagsynonymprediction.db.DBHandler;
import tagsynonymprediction.models.SynonymCandidate;
import tagsynonymprediction.models.SynonymCategory;
import tagsynonymprediction.models.TagSynonymCandidates;


public class Tester {

	private Map<String, String> detectedSynonyms;
	private Map<String, String> synonyms;
	private int countMatches = 0;
	private int top10Matches = 0;
	private int topMatches = 0;
	private int top3Matches = 0;
	private int top5Matches = 0;
	private int topXMatches = 0;
	private TagSynonymCandidates tsc;
	private Table<String, String, SynonymCandidate> candidateTable;

	public Tester() {
		detectedSynonyms = new HashMap<>();
		// synonyms = DBHandler.getTagsAndSynonymsFromDb(0, 5000);
		// synonyms = DBHandler.getAndroidTagsAndSynonymsFromDb(0, 13000);
		synonyms = DBHandler.getAllTagsAndSynonymsFromDb(0, 5000);
		tsc = TagSynonymCandidates.getInstance();
		candidateTable = tsc.getCandidateTable();
	}

	// private Map<String, String> getAllPossibleCombinationsForCandidates(List<String> listOfCandidates) {
	//
	// Map<String, String> returnMap = new HashMap<String, String>();
	//
	// for (int i = 0; i < listOfCandidates.size(); i++) {
	// for (int j = i + 1; j < listOfCandidates.size(); j++) {
	//
	// returnMap.put(listOfCandidates.get(i), listOfCandidates.get(j));
	// }
	// }
	//
	// return returnMap;
	//
	// }

	/**
	 * checks how many tagsynonyms are predicted correctly
	 * 
	 * @return
	 */
	public Integer testSynonymPredictionTable() {

		for (Table.Cell<String, String, SynonymCandidate> cell : candidateTable.cellSet()) {

			String keyToCompare = cell.getRowKey();
			String valueToCompare = cell.getColumnKey();
			SynonymCategory cat = cell.getValue().getCat();

			if (synonyms.containsKey(keyToCompare) && synonyms.get(keyToCompare).equals(valueToCompare)) {
				// if (detectedSynonyms.containsKey(keyToCompare) && detectedSynonyms.get(keyToCompare).equals(valueToCompare)) {
				countMatches++;
				// }

				cat.addCount();

				detectedSynonyms.put(keyToCompare, valueToCompare);
			} else if (synonyms.containsKey(valueToCompare) && synonyms.get(valueToCompare).equals(keyToCompare)) {
				countMatches++;
				cat.addCount();
				detectedSynonyms.put(valueToCompare, keyToCompare);
			}

		}

		System.out.println("Combination Candidates: " + candidateTable.size());
		System.out.println("count Matches: " + countMatches);
		System.out.println("found in top10 " + (top10Matches));
		System.out.println("found in top15 " + (topXMatches));

		return countMatches;
	}

	public void printNotDetectedSynonyms() {
		System.out.println("============= NOT FOUND =============");
		for (String synonymDetectedKey : detectedSynonyms.keySet()) {
			synonyms.remove(synonymDetectedKey);
		}

		for (String key : synonyms.keySet()) {
			System.out.println(key + "--> " + synonyms.get(key));
		}

		System.out.println("SIZE: " + synonyms.size());
		System.out.println("count Matches: " + countMatches);

	}

	private Multimap<String, String> mmAllResults = ArrayListMultimap.create();

	/**
	 * berechnet und speichert die anzahl der gefundenen synonympairs in den topX, top15, top 10, top 5 und top
	 * 
	 * @param tag
	 * @param topX
	 */
	public void calculateCorrectSynonymPairsOfTopResults(String tag, int topX) {

		List<String> toCheck = tsc.getRankedListOfSynonymCandidatesForTagOnlyList(tag, false, topX);
		// TODO ranking von fund

		for (int i = 0; i < topX && i < toCheck.size(); i++) {

			String listElement = toCheck.get(i);
			if (listElement != null) {
				String source = "";
				String target = "";

				if ((synonyms.containsKey(tag) && synonyms.get(tag).equals(listElement)) || (synonyms.containsKey(listElement) && synonyms.get(listElement).equals(tag))) {
					source = tag;
					target = synonyms.get(tag);

					if (!mmAllResults.containsEntry(source, target) && !mmAllResults.containsEntry(target, source)) {
						// hinzufügen
						mmAllResults.put(source, target);
						if (i < 1) {
							topMatches++;
						}

						if (i < 3) {
							top3Matches++;
						}
						if (i < 5) {
							top5Matches++;
						}
						if (i < 10) {
							top10Matches++;
						}
						topXMatches++;
					}
					return;
				}

			}
		}
		// System.err.println("nicht gefunden in top " + topX);

	}

	public void getHeuristics() {
		for (SynonymCategory cat : SynonymCategory.values()) {
			System.out.println(cat.toString() + ": " + cat.getCount());
		}

	}

	public int getTopMatches() {
		return topMatches;
	}

	public int getTop3Matches() {
		return top3Matches;
	}

	public int getTop5Matches() {
		return top5Matches;
	}

	public int getTop10Matches() {
		return top10Matches;
	}

	public int getTop15Matches() {
		return topXMatches;
	}

	public void setTop15Matches(int topXMatches) {
		this.topXMatches = topXMatches;
	}

}
