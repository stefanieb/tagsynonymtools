package tagsynonymprediction.main;


import tagsynonymprediction.log.Logger;

public class Main {

	private boolean JAR = false;

	public Main(String tag, int noOfSuggestions) {
		Logger.log("TagSynonymPrediction STARTED");

		/**
		 * TODO heuristic + ranking tag-tag, lernen (existierende tags)
		 * 
		 */

		TagSynonymPrediction tsp = new TagSynonymPrediction();
		// tsp.calculateHeuristics();

		tsp.startTagPrediction(tag, noOfSuggestions, JAR);
	}

	public static void main(String args[]) {

		// HelperFunctions hf = HelperFunctions.getInstance();

		String tag = null;
		int noOfSuggestions = 0;
		if (args.length > 0) {
			tag = args[0];
			noOfSuggestions = Integer.parseInt(args[1]);
		}
		// starts the tag synonym prediction
		new Main(tag, noOfSuggestions);
		Logger.log("TagSynonymPrediction FINISHED");

	}
}
