package tagsynonymprediction.main;

import java.util.List;
import java.util.Set;

import tagsynonymprediction.categories.maincat.AbstrTagSynonymClass;
import tagsynonymprediction.models.Tag;
import tagsynonymprediction.models.TagSynonymCandidates;
import tagsynonymprediction.tester.Tester;
import tagsynonymprediction.utility.EArrayList;
import tagsynonymprediction.utility.EList;
import tagsynonymprediction.utility.HelperFunctions;

public class TagSynonymPrediction {

	private HelperFunctions hf;
	private List<AbstrTagSynonymClass> typeList;
	private Boolean SAVE_INTO_DB = true;
	private Boolean useTestTags = false;
	private Boolean printResults = false;

	public void setUseTestTags(Boolean useTestTags) {
		this.useTestTags = useTestTags;
	}

	public void setPrintResults(Boolean printResults) {
		this.printResults = printResults;
	}

	public TagSynonymPrediction() {
		hf = HelperFunctions.getInstance();

		hf.initTypeList();
		typeList = hf.getListOfSynonymCreationTypes();
	}

	/**
	 * to start without arguments
	 */
	public void startTagPrediction() {

		startTagPrediction(null, 0, false);
	}

	/**
	 * tag in tag mit heuristik über vorkommen von teiltags
	 * 
	 */
	public void startTagPrediction(String tagToCheck, int noOfSuggestions, boolean jar) {

		int top = 10;
		boolean transitivity = false;
		EList<Tag> tagList = new EArrayList<>();

		// get tags
		tagList = getTagList();

		// apply settings for jar
		if (jar) {
			if (noOfSuggestions > 0) {
				top = noOfSuggestions;
			}
			if (tagToCheck != null) {
				tagList = new EArrayList<>();
				tagList.add(new Tag(-1, tagToCheck, -1, -1.0));
			}
		}

		// search for tags and add synonym candidates for each category in Tag
		// Synonym Candidates
		for (Tag tag : tagList) {
			String originalTag = tag.getTagName();

			// iteration over all synonym-categories
			for (AbstrTagSynonymClass cat : typeList) {

				cat.createAndAddSynonyms(originalTag);

			}
		}

		// TEST
		TagSynonymCandidates tsc = TagSynonymCandidates.getInstance();
		Tester tester = new Tester();

		// SAVE IN DB

		if (SAVE_INTO_DB) {
			tsc.saveSynonymCandidates(top);
		}

		if (printResults) {
			for (Tag tag : tagList) {
				String tagName = tag.getTagName();

				tester.calculateCorrectSynonymPairsOfTopResults(tagName, top);
				tsc.printRankedListOfSynonyms(top, tagName, transitivity);

			}
		}

		// result found
		// tester.testSynonymPredictionTable();

		// result not found
		// tester.printNotDetectedSynonyms();

		// result statistics
		// System.out.println("Ergebnis: Top15:" + tester.getTop15Matches());
		// System.out.println("Ergebnis: Top10:" + tester.getTop10Matches());
		// System.out.println("Ergebnis: Top5:" + tester.getTop5Matches());
		// System.out.println("Ergebnis: Top3:" + tester.getTop3Matches());
		// System.out.println("Ergebnis: Top1:" + tester.getTopMatches());
		System.out.println("ENDE");

	}

	private EList<Tag> getTagList() {
		EList<Tag> tagList = new EArrayList<>();
		if (useTestTags) {

			// tags zum TESTEN
			tagList.add(new Tag(-1, "jws", -1, -1.0));
			tagList.add(new Tag(-1, "java-web-start", -1, -1.0));
			tagList.add(new Tag(-1, "timetable", -1, -1.0));
			tagList.add(new Tag(-1, "time", -1, -1.0));
			tagList.add(new Tag(-1, "elisp", -1, -1.0));
			tagList.add(new Tag(-1, "lisp", -1, -1.0));
			tagList.add(new Tag(-1, "bigint", -1, -1.0));
			tagList.add(new Tag(-1, "biginteger", -1, -1.0));

			// auskommentieren wenn nur bestimmte tags testen

			tagList = tagList.getTopOfList(10);
		} else {
			Set<Tag> tagset = hf.getAllOriginalTags();
			tagList.addAll(tagset);
		}
		return tagList;
	}

	public void saveIntoDB(boolean save) {
		SAVE_INTO_DB = save;
	}

}
