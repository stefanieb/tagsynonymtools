package tagsynonymprediction.models;

import org.apache.lucene.search.spell.NGramDistance;

import tagsynonymprediction.utility.HelperFunctions;

public class SynonymCandidate implements Comparable<SynonymCandidate> {

	private Integer id;

	private SynonymCategory cat;

	private Integer count;
	// SOURCE
	private String tag;

	// TARGET
	private String synonym;

	private Float similarity;

	private boolean isTagSource;

	private NGramDistance ngram = new NGramDistance(2);

	private static HelperFunctions hf;

	private Double ranking;

	private Float jac_similarity;

	public SynonymCandidate(SynonymCategory cat, String sourceTag, String targetTag, boolean isTagSource) {
		super();
		this.cat = cat;
		this.count = 1;
		this.tag = sourceTag;
		this.synonym = targetTag;
		this.isTagSource = isTagSource;
		this.similarity = ngram.getDistance(sourceTag, targetTag);
		this.jac_similarity = jaccard(sourceTag.toCharArray(), targetTag.toCharArray());
	}

	public SynonymCandidate(String sourceTag, String targetTag, boolean isTagSource) {
		super();
		this.count = 1;
		this.tag = sourceTag;
		this.synonym = targetTag;
		this.isTagSource = isTagSource;
		this.similarity = ngram.getDistance(sourceTag, targetTag);
		// fallback für approved synonym pairs
		this.cat = SynonymCategory.APPROVED;
		this.jac_similarity = jaccard(sourceTag.toCharArray(), targetTag.toCharArray());
	}

	public SynonymCandidate(SynonymCandidate dto) {
		super();
		this.cat = dto.getCat();
		this.count = dto.getCount();
		this.tag = dto.getTag();
		this.synonym = dto.getSynonymTag();
		this.similarity = ngram.getDistance(tag, synonym);
		this.isTagSource = dto.isTagSource();
		this.jac_similarity = jaccard(dto.getTag().toCharArray(), dto.getSynonymTag().toCharArray());
	}

	public String getTag() {
		return tag;
	}

	public String getSynonymTag() {
		return synonym;
	}

	public void setCat(SynonymCategory cat) {
		this.cat = cat;
	}

	public SynonymCategory getCat() {
		return cat;
	}

	public SynonymCandidate switchTagAndSynonym() {
		String helper = tag;
		tag = synonym;
		synonym = helper;

		switchIsTagSource();

		return this;

	}

	public Integer getCount() {
		return count;
	}

	public void increaseCount() {
		count++;
	}

	public void doubleCount() {
		count = count * 2;
	}

	public void setStoredRanking(Double ranking) {
		this.ranking = ranking;
	}

	public Double getStoredRanking() {
		return ranking;
	}

	public Double getRanking() {

		if (ranking == null) {
			ranking = calculateRanking();
		}
		return ranking;
	}

	private Double calculateRanking() {
		// ranking setzt sich zusammen aus
		// count, category,
		Double ranking = count + 0.0;
		Double categoryWeight = 0.7;
		Double countWeight = 0.25;
		Double tagPositionWeight = 0.05;
		// wenn stemming dann stimmt das ziemlich sicher als synonym

		ranking = 0.0 // category
				+ getCategoryValue() * categoryWeight
				// count
				+ getCountValue() * countWeight
				// position
				+ getTagPositionValue() * tagPositionWeight;
		if (ranking >= 1) {

			ranking = 1.0;
		}
		return round(ranking, 2);
	}

	private Double getTagPositionValue() {
		// TODO consider count of tags for position?
		if (hf == null) {
			hf = HelperFunctions.getInstance();
		}
		return hf.getAvgTagPositionOfTag(synonym) / 5.0;
	}

	private Double getCountValue() {
		// maximal mit 8 bzw. 14 spezial categorien gefunden -
		if (count > 50) {
			System.out.println("count = " + count);
			count = 50;
		}
		return count / 50.00;
	}

	private Double getCategoryValue() {
		Double categoryFactor = -1.0;
		if (cat == SynonymCategory.STEM) {
			categoryFactor = 12.5;
		} else if (cat == SynonymCategory.VERSION) {
			categoryFactor = 10.0;
		} else if (cat == SynonymCategory.DOTS || cat == SynonymCategory.SHARP || cat == SynonymCategory.MINUS || cat == SynonymCategory.PLUS) {
			categoryFactor = 10.0;
		} else if (cat == SynonymCategory.SYNONYM_IN_TAG) {

			if (similarity > 0.85) {
				categoryFactor = 12.5;
			} else {
				categoryFactor = 9.0;
			}
		} else if (cat == SynonymCategory.SYNONYM_IN_TAG_SUBTAG) {
			if (jac_similarity > 0.85 && tag.length() > 3 && synonym.length() > 3) {
				categoryFactor = 8.0;
			} else {
				categoryFactor = 6.0;
			}
		} else if (cat == SynonymCategory.SYNONYM_IN_WORD) {
			categoryFactor = 5.5;
		} else if (cat == SynonymCategory.ABBREVIATION) {
			categoryFactor = 5.0;
		} else if (cat == SynonymCategory.METAPHONE) {
			if (similarity > 0.8) {
				categoryFactor = 8.0;
			}
			categoryFactor = 4.0;
		} else if (cat == SynonymCategory.SIMILARITY) {
			if (similarity > 0.8 && tag.length() > 3 && synonym.length() > 3) {
				categoryFactor = 10.0;
			} else {
				categoryFactor = 4.0;
			}
		} else {
			if (cat != null) {
				categoryFactor = Math.min((17.00 - cat.ordinal()) / 2.0, 2.5);
			}
		}

		categoryFactor = categoryFactor / 10;

		return categoryFactor;
	}

	private void switchIsTagSource() {
		if (isTagSource) {
			isTagSource = false;
		}
		isTagSource = true;
	}

	public double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public int compareTo(SynonymCandidate o) {
		if (o.getRanking().equals(this.getRanking())) {
			return 0;
		} else if (o.getRanking() > this.getRanking()) {
			return 1;
		} else if (o.getRanking() < this.getRanking()) {
			return -1;
		} else
			return 0;

	}

	public boolean isTagSource() {
		return isTagSource;
	}

	public void setTagSource(boolean isTagSource) {
		this.isTagSource = isTagSource;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String toString() {
		return tag + " --> " + synonym + (ranking != null ? "(" + ranking + ")" : "");
	}

	// Jaccard INDEX COPY

	// copy taming text
	public static float jaccard(char[] s, char[] t) {
		int intersection = 0;
		int union = s.length + t.length;
		boolean[] sdup = new boolean[s.length];
		union -= findDuplicates(s, sdup); // <co id="co_fuzzy_jaccard_dups1"/>
		boolean[] tdup = new boolean[t.length];
		union -= findDuplicates(t, tdup);
		for (int si = 0; si < s.length; si++) {
			if (!sdup[si]) { // <co id="co_fuzzy_jaccard_skip1"/>
				for (int ti = 0; ti < t.length; ti++) {
					if (!tdup[ti]) {
						if (s[si] == t[ti]) { // <co
												// id="co_fuzzy_jaccard_intersection"
												// />
							intersection++;
							break;
						}
					}
				}
			}
		}
		union -= intersection;
		return (float) intersection / union; // <co
												// id="co_fuzzy_jaccard_return"/>
	}

	public static int findDuplicates(char[] s, boolean[] sdup) {
		int ndup = 0;
		for (int si = 0; si < s.length; si++) {
			if (sdup[si]) {
				ndup++;
			} else {
				for (int si2 = si + 1; si2 < s.length; si2++) {
					if (!sdup[si2]) {
						sdup[si2] = s[si] == s[si2];
					}
				}
			}
		}
		return ndup;
	}
}
