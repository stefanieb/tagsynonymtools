package tagsynonymprediction.models;


public class Tag {

	private Integer id;
	private String tagName;
	private Integer count;
	private Integer excerptPostId;
	private Integer wikiPostId;
	private Double avgTagPosition;

	public Tag(Integer id, String tagName, Integer count, Double avgTagPosition) {
		super();
		this.id = id;
		this.tagName = tagName;
		this.count = count;
		this.avgTagPosition = avgTagPosition;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public void setExcerptPostId(Integer excerptPostId) {
		this.excerptPostId = excerptPostId;
	}

	public Integer getWikiPostId() {
		return wikiPostId;
	}

	public void setWikiPostId(Integer wikiPostId) {
		this.wikiPostId = wikiPostId;
	}

	public Integer getId() {
		return id;
	}

	public String getTagName() {
		return tagName;
	}

	public Integer getCount() {
		return count;
	}

	public Integer getExcerptPostId() {
		return excerptPostId;
	}

	public Double getAvgTagPosition() {
		return avgTagPosition;
	}

	public void setAvgTagPosition(Double avgTagPosition) {
		this.avgTagPosition = avgTagPosition;
	}

}
