package tagsynonymprediction.models;

import java.util.List;

public class ResultDTO {

	private boolean _isValid;

	private List<String> _listOfSynonyms;

	public ResultDTO(boolean isValid, List<String> synonyms) {
		this._isValid = isValid();
		this._listOfSynonyms = synonyms;
	}

	public boolean isValid() {
		return _isValid;
	}

	public List<String> getListOfSynonyms() {
		return _listOfSynonyms;
	}

	public void setListOfSynonyms(List<String> _listOfSynonyms) {
		this._listOfSynonyms = _listOfSynonyms;
	}

}
