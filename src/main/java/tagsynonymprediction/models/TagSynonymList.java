package tagsynonymprediction.models;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TagSynonymList {

	private String _tagName;

	private Map<String, SynonymCategory> _synonymList;

	public TagSynonymList(String tagName) {
		this._tagName = tagName;
		this._synonymList = new HashMap<String, SynonymCategory>();
	}

	public void addToList(List<String> synonyms, SynonymCategory cat) {
		for (String synonym : synonyms) {
			_synonymList.put(synonym, cat);
		}

	}

	public List<String> getRankedListOfSynonyms() {
		// TOOD implement ranking

		List<String> returnlist = new ArrayList();
		returnlist.addAll(_synonymList.keySet());
		return returnlist;
	}

	public void printRankedListOfSynonyms(int top) {
		List<String> rankedListOfSynonyms = getRankedListOfSynonyms();
		System.out.println("TOP " + top + " SYNONYMS FOR " + _tagName);
		for (int i = 0; i < top; i++) {
			System.out.println("- " + (i + 1) + ": " + rankedListOfSynonyms.get(i));
		}

	}
}
