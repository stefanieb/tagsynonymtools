package tagsynonymprediction.models;

import java.util.Collections;
import java.util.Map.Entry;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Multimap;
import com.google.common.collect.Table;
import com.google.common.collect.Table.Cell;

import helper.HelperDBHandler;
import tagsynonymprediction.db.DBHandler;
import tagsynonymprediction.utility.EArrayList;
import tagsynonymprediction.utility.EList;
import tagsynonymprediction.utility.HelperFunctions;
import tagsynonymprediction.utility.Utilities;

public class TagSynonymCandidates {

	private ArrayListMultimap<String, String> mm;

	private Table<String, String, SynonymCandidate> table;

	private HelperFunctions hf;

	private static TagSynonymCandidates singleton = new TagSynonymCandidates();

	private TagSynonymCandidates() {
		mm = ArrayListMultimap.create();
		table = HashBasedTable.create();
		hf = HelperFunctions.getInstance();
	}

	public static TagSynonymCandidates getInstance() {
		return singleton;
	}

	private boolean addToTagSynonymCandidates(String tag, String synonym, SynonymCategory cat) {

		boolean isTagSource = hf.isSourceTargetRelation(tag, synonym, cat);

		if (tag.startsWith("android-") && synonym.startsWith("android-")) {

			String tagWithoutAndroid = tag.replace("android-", "");
			String synonymWithoutAndroid = synonym.replace("android-", "");
			if (Utilities.getSimilarity(tagWithoutAndroid, synonymWithoutAndroid) < 0.7) {
				return false;
			}

		}

		// System.out.println("synonym: " + synonym + " tag: " + tag);
		// not contained in map or table
		if (!mm.containsEntry(synonym, tag) && !mm.containsEntry(tag, synonym)) {
			mm.put(tag, synonym);
			table.put(tag, synonym, new SynonymCandidate(cat, tag, synonym, isTagSource));
		} else {
			SynonymCandidate dto;

			if (mm.containsEntry(synonym, tag)) {
				dto = table.get(synonym, tag);
				dto.increaseCount();

				// niedrigere cat nehmen
				if (cat.ordinal() < dto.getCat().ordinal()) {
					dto.setCat(cat);
				}
				table.put(synonym, tag, dto);
			} else if (mm.containsEntry(tag, synonym)) {
				// update
				dto = table.get(tag, synonym);
				dto.increaseCount();

				// niedrigere cat nehmen
				if (cat.ordinal() < dto.getCat().ordinal()) {
					dto.setCat(cat);
				}
				table.put(tag, synonym, dto);
			}

		}
		return true;

	}

	public ArrayListMultimap<String, String> getCandidateList() {
		return mm;
	}

	public Table<String, String, SynonymCandidate> getCandidateTable() {

		return table;
	}

	/**
	 * adds synonym candidate pair and type to list if both are in taglist
	 * 
	 * @param tagFrom
	 * @param tagTo
	 * @param cat
	 */
	private boolean checkPairIsValidAndAddToCandidates(String tagFrom, String tagTo, SynonymCategory cat) {

		boolean isValid = false;

		if (isValidPair(tagFrom, tagTo)) {
			isValid = addToTagSynonymCandidates(tagFrom, tagTo, cat);

		}

		return isValid;
	}

	private boolean isValidPair(String tagFrom, String tagTo) {

		// if (!tagFrom.equals(tagTo) && hf.isContainedInOriginalTags(tagTo)) {
		if (!tagFrom.trim().equals(tagTo.trim()) && hf.isContainedInOriginalTags(tagTo) && tagFrom.length() > 1 && tagTo.length() > 1) {

			return true;
		}
		return false;
	}

	// /**
	// * filters non valid Synonym Candidates
	// *
	// * @param listToCheck
	// * @return list of validate candidates
	// */
	// public List<String> returnListOfValidCandidates(String tagFrom,
	// List<String> listToCheck) {
	// List<String> returnList = new ArrayList<>();
	// for (String tagTo : listToCheck) {
	// if (isValidPair(tagFrom, tagTo)) {
	//
	// returnList.add(tagTo);
	// }
	// }
	// return returnList;
	//
	// }

	/**
	 * calls checkPairIsvalidAndAddToCandidates for each pair of candidates
	 *
	 */
	public boolean checkListIsValidAndAddToCandidates(String tagFrom, Multimap<SynonymCategory, String> candidates) {
		boolean isValid = false;

		for (Entry<SynonymCategory, String> entry : candidates.entries()) {

			String tagTo = entry.getValue();

			if (checkPairIsValidAndAddToCandidates(tagFrom, tagTo, entry.getKey()) && !isValid) {

				isValid = true;
			}
		}
		return isValid;
	}

	public ArrayListMultimap<String, String> returnCandidateList() {
		return getCandidateList();
	}

	public Table<String, String, SynonymCandidate> returnCandidateTable() {
		return getCandidateTable();
	}

	/**
	 * returns a list of possible synonym candidates without ranking
	 * 
	 * @param tag
	 * @return
	 */
	public EList<EList<SynonymCandidate>> getRankedListOfSynonymCandidatesForTag(String tag, boolean transitive, int top) {

		// TODO ranking
		EList<EList<SynonymCandidate>> returnList = new EArrayList<>();

		EList<SynonymCandidate> firstList = new EArrayList<>();

		for (Cell<String, String, SynonymCandidate> entry : table.cellSet()) {

			if (entry.getRowKey().equals(tag) || entry.getColumnKey().equals(tag)) {
				SynonymCandidate dto = new SynonymCandidate(entry.getValue());
				if (!tag.equals(dto.getTag())) {
					// switch tag und synonym
					dto = dto.switchTagAndSynonym();
				}
				firstList.add(dto);
			}

		}
		firstList = groupCandidates(firstList);
		Collections.sort(firstList);
		firstList = firstList.getTopOfList(top);

		returnList.add(firstList);

		// transitivity
		if (transitive) {

			for (SynonymCandidate candidate : firstList) {

				EList<SynonymCandidate> candidateList = getRankedListOfSynonymCandidatesForTag(candidate.getSynonymTag(), false, top).get(0);
				if (candidateList.size() > 1) {
					candidateList = groupCandidates(candidateList);
					Collections.sort(candidateList);
					returnList.add(candidateList.getTopOfList(top));
				}
			}
		}

		return returnList;
	}

	/**
	 * gruppiert die resultate zb. method und methods werden zu method
	 * 
	 * @param returnList
	 * @return
	 */
	private EList<SynonymCandidate> groupCandidates(EList<SynonymCandidate> returnList) {
		EList<SynonymCandidate> groupedReturnList = new EArrayList<>();
		groupedReturnList.addAll(returnList);
		for (int i = 0; i < returnList.size(); i++) {
			for (int j = 0; j <= i; j++) {
				if (i != j) {
					String tagDto1 = returnList.get(i).getSynonymTag();
					String tagDto2 = returnList.get(j).getSynonymTag();
					if (Utilities.getStemmedTag(tagDto1).equals(tagDto2) || tagDto1.substring(0, tagDto1.length() - 1).equals(tagDto2)) {

						groupedReturnList.remove(getInfoDtoWhereCandidateLike(tagDto1, returnList));
						SynonymCandidate dto = getInfoDtoWhereCandidateLike(tagDto2, returnList);

						groupedReturnList.remove(dto);
						dto.doubleCount();
						groupedReturnList.add(dto);

					} else if (Utilities.getStemmedTag(tagDto2).equals(tagDto1) || tagDto2.substring(0, tagDto2.length() - 1).equals(tagDto1)) {
						groupedReturnList.remove(getInfoDtoWhereCandidateLike(tagDto2, returnList));

						SynonymCandidate dto = getInfoDtoWhereCandidateLike(tagDto1, returnList);

						groupedReturnList.remove(dto);
						dto.doubleCount();
						groupedReturnList.add(dto);
					}
				}

			}
		}
		return groupedReturnList;
	}

	private SynonymCandidate getInfoDtoWhereCandidateLike(String candidate, EList<SynonymCandidate> searchList) {
		for (SynonymCandidate dto : searchList) {
			if (dto.getTag().equals(candidate) || dto.getSynonymTag().equals(candidate)) {
				return dto;
			}
		}
		return null;

	}

	/**
	 * returns a list of synonym candidates with ranking as Stringlist
	 * 
	 * @param tag
	 * @return
	 */
	public EList<String> getRankedListOfSynonymCandidatesForTagOnlyList(String tag, boolean transitive, int top) {

		EList<String> returnList = new EArrayList<>();

		if (transitive) {
			for (EList<SynonymCandidate> list : getRankedListOfSynonymCandidatesForTag(tag, transitive, top)) {
				for (SynonymCandidate entry : list) {
					if (entry.getTag().equals(tag)) {

						returnList.add(entry.getSynonymTag());
					} else if (entry.getSynonymTag().equals(tag)) {
						returnList.add(entry.getTag());
					}
				}
			}
		} else {

			for (SynonymCandidate entry : getRankedListOfSynonymCandidatesForTag(tag, transitive, top).get(0)) {

				// if more than 1000 synonym pairs are collected they are stored

				if (entry.getTag().equals(tag)) {

					returnList.add(entry.getSynonymTag());

				} else if (entry.getSynonymTag().equals(tag)) {

					returnList.add(entry.getTag());

				}
			}
		}

		return returnList;
	}

	public void printRankedListOfSynonyms(int top, String tagName, boolean transitivity) {

		EList<EList<SynonymCandidate>> infodtos = getRankedListOfSynonymCandidatesForTag(tagName, transitivity, top);

		printRankedListOfSynonyms(top, infodtos.get(0));

		if (transitivity) {
			for (int i = 1; i < infodtos.size(); i++) {
				printRankedListOfSynonyms(top, infodtos.get(i));
			}

		}

	}

	private void printRankedListOfSynonyms(int top, EList<SynonymCandidate> rankedListOfSynonyms) {
		if (rankedListOfSynonyms.size() > 0) {
			System.out.println("TOP " + top + " SYNONYMS FOR " + rankedListOfSynonyms.get(0).getTag());

			int i = 1;
			for (SynonymCandidate element : rankedListOfSynonyms) {

				System.out.println("-" + i + "(" + element.getRanking() + (element.isTagSource() ? ",target" : ",source") + // ",
																															// "
																															// +
																															// "source"
				"): " + element.getSynonymTag() + " --> " + element.getCat());
				i++;
				if (i > top) {
					break;
				}
			}
		}
	}

	public void saveSynonymCandidates(int top) {
		// TODO implementieren dass nur top x genommen werden

		HelperDBHandler.deleteFromDB("synonym_candidates");
		// synonym collection in database
		EList<SynonymCandidate> dtosForDB = new EArrayList<>();

		for (SynonymCandidate entry : table.values()) {

			if (dtosForDB.size() > 1000) {
				System.out.println("insert +1000");
				DBHandler.insertSynonymCandidates(dtosForDB);
				dtosForDB = new EArrayList<>();
			}

			// add to db
			// if (entry.getRanking() > 0.3) {
			dtosForDB.add(entry);
			// }
		}
		DBHandler.insertSynonymCandidates(dtosForDB);

	}

}
