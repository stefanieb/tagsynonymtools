package tagsynonymprediction.models;


public enum SynonymCategory {

	STEM, VERSION, DOTS, SHARP, MINUS, PLUS, SYNONYM_IN_TAG, SYNONYM_IN_TAG_SUBTAG, ABBREVIATION, SYNONYM, SYNONYM_IN_WORD, SIMILARITY, SYNONYM_IN_TAG_LIKE, ABBREVIATION_START_OR_END, ABBREVIATION_COMPLEX, SYNONYM_IN_WORD_LIKE, METAPHONE, APPROVED

	// TODO ORDER
	;

	private int count = 0;

	public void addCount() {
		count++;
	}

	public int getCount() {
		return count;
	}

	public static int getCountOfAll() {
		int ret = 0;
		for (SynonymCategory cat : SynonymCategory.values()) {
			ret += cat.getCount();
		}
		return ret;
	}

	public static void printHeuristics() {
		int ret = 0;
		for (SynonymCategory cat : SynonymCategory.values()) {
			System.out.println(cat.toString() + ": " + cat.getCount());
			ret += cat.getCount();
		}
		System.out.println("ALL: " + ret);
	}

	public static SynonymCategory getCategoryByName(String name) {
		switch (name) {
		case "AbbreviationDot":
			return SynonymCategory.ABBREVIATION;
		case "AbbreviationMinus":
			return SynonymCategory.ABBREVIATION;
		case "Dots":
			return SynonymCategory.DOTS;
		case "Minus":
			return SynonymCategory.MINUS;
		case "Plus":
			return SynonymCategory.PLUS;
		case "main.categories.main.categories.maincat.Sharp":
			return SynonymCategory.SHARP;
		case "LevenstheinSimilarity":
			return SynonymCategory.SIMILARITY;
		case "JaccardSimilarity":
			return SynonymCategory.SIMILARITY;
		case "NGramSimilarity":
			return SynonymCategory.SIMILARITY;
		case "Stem":
			return SynonymCategory.STEM;
		case "Synonym":
			return SynonymCategory.SYNONYM;
		case "SynonymInTag":
			return SynonymCategory.SYNONYM_IN_TAG;
		case "SynonymInWord":
			return SynonymCategory.SYNONYM_IN_WORD;
		case "Version":
			return SynonymCategory.VERSION;
		case "MetaphoneClass":
			return SynonymCategory.METAPHONE;
		default:
			return null;

		}

	}

}
