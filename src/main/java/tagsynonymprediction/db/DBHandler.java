package tagsynonymprediction.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import tagsynonymprediction.models.SynonymCandidate;
import tagsynonymprediction.models.Tag;

public class DBHandler {

	private static String url = "jdbc:mysql://localhost:3306/";
	// private static String dbName = "AndroidOnly";
	private static String dbName = "SO_Nov2014";
	private static String driver = "com.mysql.jdbc.Driver";
	private static String userName = "root";
	private static String password = "";

	/**
	 * returns a list of all tags in db
	 * 
	 * @return
	 */
	public static Set<Tag> getAllTagsFromDb() {

		Set<Tag> returnList = new HashSet<Tag>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select distinct tags.Id as 'id', tags.TagName as 'tagname', tags.Count as 'count'   from Tags limit 0, 40000;"); // tagNeuSynonyms

			while (tags_db.next()) {

				String tagName = tags_db.getString("tagname");
				Tag tag = new Tag(tags_db.getInt("id"), tagName, tags_db.getInt("count"), getAveragePositionOfTag(tagName));
				returnList.add(tag);

			}

			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	/**
	 * returns a list of all tagsynonyms in db with existing tags
	 * 
	 * @param limit_start
	 * @param limit_end
	 * @return
	 */
	public static Map<String, String> getTagsAndSynonymsFromDb(int limit_start, int limit_end) {

		Map<String, String> returnList = new HashMap<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select * from synonymsInTags;"); // synonymsTagsNeu

			while (tags_db.next()) {

				String sourceTag = tags_db.getString("source");
				String targetTag = tags_db.getString("target");
				returnList.put(sourceTag, targetTag);

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	/**
	 * returns a list of all tagsynonyms in db with existing tags
	 * 
	 * @param limit_start
	 * @param limit_end
	 * @return
	 */
	public static Map<String, String> getAndroidTagsAndSynonymsFromDb(int limit_start, int limit_end) {

		Map<String, String> returnList = new HashMap<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select  * from tagsynonyms where SourceTagName in( select TagName from tags join "
					+ "post2tag on tags.Id = tagId) and TargetTagName in( select TagName from tags join" + " post2tag on tags.Id = tagId)"); // synonymsTagsNeu

			while (tags_db.next()) {

				String sourceTag = tags_db.getString("SourceTagName");
				String targetTag = tags_db.getString("TargetTagName");
				returnList.put(sourceTag, targetTag);

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	/**
	 * returns all tagsynonyms in db, disregarding if the tags still exist
	 * 
	 * @param limit_start
	 * @param limit_end
	 * @return
	 */
	public static Map<String, String> getAllTagsAndSynonymsFromDb(int limit_start, int limit_end) {

		Map<String, String> returnList = new HashMap<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select * from TagSynonyms order by SourceTagName asc;");

			while (tags_db.next()) {

				String sourceTag = tags_db.getString("SourceTagName");
				String targetTag = tags_db.getString("TargetTagName");
				returnList.put(sourceTag, targetTag);

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	public static List<String> getTagsWhereSubTagIsXXX(String xxx) {
		List<String> returnList = new ArrayList<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select * from tagsplit where part1 = '" + xxx + "' or part2 = '" + xxx + "' or part3 = '" + xxx + "' or part4 =" + "'" + xxx
					+ "' or part5 = '" + xxx + "'	 ;");

			while (tags_db.next()) {

				String tagName = tags_db.getString("fulltag");
				returnList.add(tagName);

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	public static List<String> getTagsLikeXXX(String xxx) {
		ArrayList<String> alist = new ArrayList<>();
		Set<String> returnList = new HashSet<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select TagName from Tags where TagName like '%" + xxx + "%';");

			while (tags_db.next()) {

				String tagName = tags_db.getString("TagName");
				returnList.add(tagName);

			}

			// toggle if old tags hsould be used
			boolean oldTagsAllowed = true;
			if (oldTagsAllowed) {
				tags_db = st.executeQuery("select old_tagname from old_tags_target where old_tagname like '%" + xxx + "%';");

				while (tags_db.next()) {

					String tagName = tags_db.getString("old_tagname");
					returnList.add(tagName);

				}

				tags_db = st.executeQuery("select old_tagname from old_tags_source where old_tagname like '%" + xxx + "%';");

				while (tags_db.next()) {

					String tagName = tags_db.getString("old_tagname");
					returnList.add(tagName);

				}
			}

			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		alist.addAll(returnList);
		return alist;

	}

	public static List<String> getTagsLikeXXXOLDTS(String xxx) {
		ArrayList<String> alist = new ArrayList<>();
		Set<String> returnList = new HashSet<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select targettagname from Tagsynonyms where targettagname like '%" + xxx + "%';");

			while (tags_db.next()) {

				String tagName = tags_db.getString("targettagname");
				returnList.add(tagName);

			}

			// toggle if old tags hsould be used

			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select sourcetagname from Tagsynonyms where sourcetagname like '%" + xxx + "%';");

			while (tags_db.next()) {

				String tagName = tags_db.getString("sourcetagname");
				returnList.add(tagName);

			}

			// toggle if old tags hsould be used

			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		alist.addAll(returnList);
		return alist;

	}

	public static List<String> getTagsStartOrEndWithXXX(String xxx) {
		ArrayList<String> alist = new ArrayList<>();
		Set<String> returnList = new HashSet<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select TagName from Tags where TagName like '%" + xxx + "' or TagName like '" + xxx + "%';");

			while (tags_db.next()) {

				String tagName = tags_db.getString("TagName");
				returnList.add(tagName);

			}

			boolean oldTagsAllowed = true;
			if (oldTagsAllowed) {
				tags_db = st.executeQuery("select old_tagname from old_tags_target where old_tagname like '%" + xxx + "' or old_tagname like '" + xxx + "%';");

				while (tags_db.next()) {

					String tagName = tags_db.getString("old_tagname");
					returnList.add(tagName);

				}

				tags_db = st.executeQuery("select old_tagname from old_tags_source where old_tagname like '%" + xxx + "' or old_tagname like '" + xxx + "%';");

				while (tags_db.next()) {

					String tagName = tags_db.getString("old_tagname");
					returnList.add(tagName);

				}
			}

			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		alist.addAll(returnList);
		return alist;

	}

	public static List<String> getTagsStartOrEndWithXXXOLDTS(String xxx) {
		ArrayList<String> alist = new ArrayList<>();
		Set<String> returnList = new HashSet<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select sourcetagname from Tagsynonyms  where sourcetagname like '%" + xxx + "' or sourcetagname like '" + xxx + "%';");

			while (tags_db.next()) {

				String tagName = tags_db.getString("sourcetagname");
				returnList.add(tagName);

			}

			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select targettagname from Tagsynonyms  where targettagname like '%" + xxx + "' or targettagname like '" + xxx + "%';");

			while (tags_db.next()) {

				String tagName = tags_db.getString("targettagname");
				returnList.add(tagName);

			}

			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		alist.addAll(returnList);

		return alist;

	}

	public static List<String> getTagsStartWithXXXSkipMinusDot(String xxx, String stemXXX) {
		ArrayList<String> alist = new ArrayList<>();
		Set<String> returnList = new HashSet<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select fulltag from tagsplit where skipminusanddot like '%" + xxx + "' or skipminusanddot like '" + stemXXX + "%';");

			while (tags_db.next()) {

				String tagName = tags_db.getString("fulltag");
				returnList.add(tagName);

			}

			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		alist.addAll(returnList);
		return alist;

	}

	/**
	 * returns all tags that are similar to
	 * 
	 * @return
	 */
	public static List<String> getSimilarTags(String tag) {

		List<String> returnList = new ArrayList<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			double limit_levensthein = 0.7;
			double limit_jaccard = 0.75;
			double limit_ngram2 = 0.6;
			double limit_ngram3 = 0.6;
			double limit_ngram4 = 0.6;
			// #or ts.tag2Id = t.Id

			String query = "select distinct tagname1, tagname2 from tag_similarity " + "where (jaccardSim > " + limit_jaccard + " or levenstheinDist > " + limit_levensthein
					+ " or ngram2 > " + limit_ngram2 + " or ngram3 > " + limit_ngram3 + " or ngram4 >" + limit_ngram4 + ")" + " and tagname1 = '" + tag + "' xor tagname2 = '" + tag
					+ "';";
			// System.out.println(query);
			ResultSet tags_db = st.executeQuery(query);

			while (tags_db.next()) {

				String tagname1 = tags_db.getString("tagname1");
				String tagname2 = tags_db.getString("tagname2");

				returnList.add(tagname1.equals(tag) ? tagname2 : tagname1);

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	public static Set<Tag> getAllAndroidTags() {

		Set<Tag> returnList = new HashSet<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery(
					"select distinct tags.Id as 'id', tags.TagName as 'tagname', tags.Count as 'count' from tags  join post2tag  on tags.Id = post2tag.tagId order by tags.count desc limit 0, 15000;");

			while (tags_db.next()) {

				Tag tag = new Tag(tags_db.getInt("id"), tags_db.getString("tagname"), tags_db.getInt("count"), getAveragePositionOfTag("tagname"));
				returnList.add(tag);

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	public static Set<String> getTagsWhereStemmedTagIsXXX(String XXX) {

		Set<String> returnList = new HashSet<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select * from tagsplit where stemmedtag ='" + XXX + "';");

			while (tags_db.next()) {

				String tag = tags_db.getString("fulltag");
				returnList.add(tag);

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	public static Set<String> getTagsWhereTagWithoutNumbersIsXXX(String XXX) {

		Set<String> returnList = new HashSet<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select * from tagsplit where skipnumber ='" + XXX + "';");

			while (tags_db.next()) {

				String tag = tags_db.getString("fulltag");
				returnList.add(tag);

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	public static Set<String> getTagsByTagWithoutNumbersButExistingNumbers(String tag) {

		Set<String> returnList = new HashSet<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select * from tagsplit where skipnumber ='" + tag + "';");

			while (tags_db.next()) {

				String fulltag = tags_db.getString("fulltag");
				if (fulltag.matches(".*\\d\\.?\\d*.*")) {
					returnList.add(fulltag);
				}

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	public static Set<Tag> getAllTagsOfReferenceSetDisregardingTagTable() {

		Set<Tag> returnList = new HashSet<>();
		Set<String> tagNames = new HashSet<String>();
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select SourceTagName as 'source', TargetTagName as 'target' from tagsynonyms order by rand()");

			while (tags_db.next()) {

				tagNames.add(tags_db.getString("source"));
				tagNames.add(tags_db.getString("target"));
			}

			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		for (String tagName : tagNames) {

			returnList.add(getTagByName(tagName));

		}

		return returnList;

	}

	public static Tag getTagByName(String tagName) {

		Tag tag = new Tag(-1, tagName, -1, -1.0);

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select distinct tags.Id as 'id', tags.TagName as 'tagname', tags.Count as 'count'   from Tags where tagName = '" + tagName + "'");

			if (tags_db.isBeforeFirst()) {
				while (tags_db.next()) {

					tag.setId(tags_db.getInt("id"));
					tag.setCount(tags_db.getInt("count"));
					tag.setAvgTagPosition(getAveragePositionOfTag(tagName));

				}
			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tag;

	}

	public static Set<String> getAllMetaphoneTagsWithCodeLength(String metaphone, int codelength) {

		Set<String> returnList = new HashSet<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select fulltag  from tagmetaphone where metaphone" + codelength + " ='" + metaphone + "';");

			while (tags_db.next()) {

				returnList.add(tags_db.getString("fulltag"));

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	public static Double getAveragePositionOfTag(String tagName) {

		Double average = -1.0;
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select avg(position) as 'avg' from post2tag join tags on post2tag.tagId = tags.Id and tags.tagName = '" + tagName + "';");

			while (tags_db.next()) {

				average = tags_db.getDouble("avg");

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return average;

	}

	public static void insertSynonymCandidates(List<SynonymCandidate> dtoList) {

		String values;

		StringBuilder sb = new StringBuilder();
		int count = 0;
		for (SynonymCandidate dto : dtoList) {
			if (count > 0) {
				sb.append(",");
			}

			if (dto.isTagSource()) {
				sb.append("('" + dto.getTag() + "', '" + dto.getSynonymTag() + "', " + dto.getRanking() + ", '" + dto.getCat().name() + "', " + dto.getCount() + ")");
			} else {
				sb.append("('" + dto.getSynonymTag() + "', '" + dto.getTag() + "', " + dto.getRanking() + ", '" + dto.getCat().name() + "', " + dto.getCount() + ")");
			}

			count++;

		}
		sb.append(";");

		values = sb.toString();

		Connection conn = null;
		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			String query = "insert into synonym_candidates (source, target, ranking, category, count) values " + values;
			st.executeUpdate(query);
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static List<String> getTagsWhereSubTagOldXXX(String xxx) {
		List<String> returnList = new ArrayList<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select sourcetagname from tagsynonyms where sourcetagname like  '%-" + xxx + "%' or sourcetagname like '" + xxx
					+ "-%' or sourcetagname like '%-" + xxx + "-%' ;");

			while (tags_db.next()) {

				String tagName = tags_db.getString("sourcetagname");
				returnList.add(tagName);

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select targettagname from tagsynonyms where targettagname like  '%-" + xxx + "%' or targettagname like '" + xxx
					+ "-%' or targettagname like '%-" + xxx + "-%' ;");

			while (tags_db.next()) {

				String tagName2 = tags_db.getString("targettagname");
				returnList.add(tagName2);

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnList;

	}
}
