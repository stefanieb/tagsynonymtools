package tagsynonymprediction.categories.similarity;

import tagsynonymprediction.models.SynonymCategory;

public class JaccardSimilarity extends Similarity {

	@Override
	protected boolean isSynonymClassWithoutCount(String source, String target) {

		if (getSimilarityDistance(source, target) > 0.75) {
			SynonymCategory.SIMILARITY.addCount();
			return true;
		}
		return false;

		// List<String> possibleSynonyms = createSynonyms(source);
		// for (String synonym : possibleSynonyms) {
		// if (synonym.equals(target)) {
		// SynonymCategory.SIMILARITY.addCount();
		// return true;
		// }
		// }
		//
		// possibleSynonyms = createSynonyms(target);
		// for (String synonym : possibleSynonyms) {
		// if (synonym.equals(source)) {
		// SynonymCategory.SIMILARITY.addCount();
		// return true;
		// }
		// }
		//
		// return false;

	}

	@Override
	public float getSimilarityDistance(String tag, String tag2compare) {
		return jaccard(tag.toCharArray(), tag2compare.toCharArray());
	}

	// copy taming text
	private float jaccard(char[] s, char[] t) {
		int intersection = 0;
		int union = s.length + t.length;
		boolean[] sdup = new boolean[s.length];
		union -= findDuplicates(s, sdup); // <co id="co_fuzzy_jaccard_dups1"/>
		boolean[] tdup = new boolean[t.length];
		union -= findDuplicates(t, tdup);
		for (int si = 0; si < s.length; si++) {
			if (!sdup[si]) { // <co id="co_fuzzy_jaccard_skip1"/>
				for (int ti = 0; ti < t.length; ti++) {
					if (!tdup[ti]) {
						if (s[si] == t[ti]) { // <co
												// id="co_fuzzy_jaccard_intersection"
												// />
							intersection++;
							break;
						}
					}
				}
			}
		}
		union -= intersection;
		return (float) intersection / union; // <co
												// id="co_fuzzy_jaccard_return"/>
	}

	private int findDuplicates(char[] s, boolean[] sdup) {
		int ndup = 0;
		for (int si = 0; si < s.length; si++) {
			if (sdup[si]) {
				ndup++;
			} else {
				for (int si2 = si + 1; si2 < s.length; si2++) {
					if (!sdup[si2]) {
						sdup[si2] = s[si] == s[si2];
					}
				}
			}
		}
		return ndup;
	}

	@Override
	protected void increaseCounter() {
		SynonymCategory.SIMILARITY.addCount();

	}

}
