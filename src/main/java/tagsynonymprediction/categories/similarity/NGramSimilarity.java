package tagsynonymprediction.categories.similarity;

import org.apache.lucene.search.spell.NGramDistance;

import tagsynonymprediction.models.SynonymCategory;

public class NGramSimilarity extends Similarity {

	@Override
	protected boolean isSynonymClassWithoutCount(String source, String target) {

		if (getSimilarityDistance(source, target, 2) > 0.6 || getSimilarityDistance(source, target, 3) > 0.6 || getSimilarityDistance(source, target, 4) > 0.6) {
			SynonymCategory.SIMILARITY.addCount();
			return true;
		}
		return false;

		// List<String> possibleSynonyms = createSynonyms(source);
		// for (String synonym : possibleSynonyms) {
		// if (synonym.equals(target)) {
		// SynonymCategory.SIMILARITY.addCount();
		// return true;
		// }
		// }
		//
		// possibleSynonyms = createSynonyms(target);
		// for (String synonym : possibleSynonyms) {
		// if (synonym.equals(source)) {
		// SynonymCategory.SIMILARITY.addCount();
		// return true;
		// }
		// }

		// return false;
	}

	@Override
	public float getSimilarityDistance(String tag, String tag2compare) {

		return getSimilarityDistance(tag, tag2compare, 2);
	}

	public float getSimilarityDistance(String tag, String tag2compare, Integer n) {
		NGramDistance ngram = new NGramDistance(n);
		return ngram.getDistance(tag, tag2compare);
	}

	@Override
	protected void increaseCounter() {
		SynonymCategory.SIMILARITY.addCount();

	}

}
