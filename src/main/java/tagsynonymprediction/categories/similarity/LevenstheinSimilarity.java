package tagsynonymprediction.categories.similarity;

import org.apache.lucene.search.spell.LuceneLevenshteinDistance;

import tagsynonymprediction.models.SynonymCategory;

public class LevenstheinSimilarity extends Similarity {

	@Override
	protected boolean isSynonymClassWithoutCount(String source, String target) {

		if (getSimilarityDistance(source, target) > 0.7) {
			SynonymCategory.SIMILARITY.addCount();
			return true;
		}
		return false;
		// List<String> possibleSynonyms = createSynonyms(source);
		// for (String synonym : possibleSynonyms) {
		// if (synonym.equals(target)) {
		// SynonymCategory.SIMILARITY.addCount();
		// return true;
		// }
		// }
		//
		// possibleSynonyms = createSynonyms(target);
		// for (String synonym : possibleSynonyms) {
		// if (synonym.equals(source)) {
		// SynonymCategory.SIMILARITY.addCount();
		// return true;
		// }
		// }

		// return false;
	}

	@Override
	public float getSimilarityDistance(String tag, String tag2compare) {
		LuceneLevenshteinDistance lld = new LuceneLevenshteinDistance();
		return lld.getDistance(tag, tag2compare);
	}

	@Override
	protected void increaseCounter() {
		SynonymCategory.SIMILARITY.addCount();

	}

}
