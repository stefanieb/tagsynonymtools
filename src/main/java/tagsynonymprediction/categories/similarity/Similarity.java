package tagsynonymprediction.categories.similarity;


import tagsynonymprediction.categories.maincat.AbstrTagSynonymClass;
import tagsynonymprediction.db.DBHandler;
import tagsynonymprediction.models.SynonymCategory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;


public abstract class Similarity extends AbstrTagSynonymClass {

	@Override
	protected Multimap<SynonymCategory, String> createSynonyms(String originalTag) {

		Multimap<SynonymCategory, String> returnList = ArrayListMultimap.create();

		// jaccard
		// levensthein
		// ngram n = 3,2,4
		returnList.putAll(SynonymCategory.SIMILARITY, DBHandler.getSimilarTags(originalTag));

		return returnList;
	}

	public boolean isSynonymClass(String source, String target) {

		if (isSynonymClassWithoutCount(source, target)) {
			// System.out.println(source + "--" + target);
			SynonymCategory.SIMILARITY.addCount();
			return true;
		}
		return false;

	}

	protected abstract boolean isSynonymClassWithoutCount(String source, String target);

	public abstract float getSimilarityDistance(String tag, String tag2compare);

	@Override
	public SynonymCategory getCategory() {
		return SynonymCategory.SIMILARITY;
	}

}
