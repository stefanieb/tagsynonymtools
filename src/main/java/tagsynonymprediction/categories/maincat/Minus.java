package tagsynonymprediction.categories.maincat;

import java.util.Collection;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import tagsynonymprediction.db.DBHandler;
import tagsynonymprediction.models.SynonymCategory;
import tagsynonymprediction.utility.EArrayList;
import tagsynonymprediction.utility.EList;
import tagsynonymprediction.utility.Utilities;

public class Minus extends AbstrTagSynonymClass {

	@Override
	public SynonymCategory getCategory() {
		return SynonymCategory.MINUS;
	}

	@Override
	protected Multimap<SynonymCategory, String> createSynonyms(String originalTag) {

		Multimap<SynonymCategory, String> returnList = ArrayListMultimap.create();
		if (originalTag.contains("-")) {
			returnList.putAll(SynonymCategory.MINUS, addMinus(originalTag));
		}

		return returnList;
	}

	private EList<String> addMinus(String originalTag) {
		EList<String> returnList = new EArrayList<>();
		if (originalTag.contains("-")) {
			returnList.addAll(DBHandler.getTagsWhereStemmedTagIsXXX(Utilities.getStemmedTag(originalTag.replaceAll("-", ""))));
			returnList.add(originalTag.replaceAll("-", "."));
			returnList.add(originalTag.replaceFirst("-", ""));
		}

		return returnList;
	}

	@Override
	public boolean isSynonymClass(String source, String target) {
		if (source.contains("-") || target.contains("-")) {
			Collection<String> possibleSynonyms = createSynonyms(source).values();
			for (String synonym : possibleSynonyms) {
				if (synonym.equals(target)) {
					SynonymCategory.MINUS.addCount();
					return true;
				}
			}

			possibleSynonyms = createSynonyms(target).values();
			for (String synonym : possibleSynonyms) {
				if (synonym.equals(source)) {
					SynonymCategory.MINUS.addCount();
					return true;
				}
			}
		}

		return false;

		// String sourceNeu = source;
		// String targetNeu = target;
		// if (source.replace("-", "").equals(target) || target.replace("-", "").equals(source)) {
		// SynonymCategory.MINUS.addCount();
		// return true;
		// }
		// }

	}

	@Override
	protected void increaseCounter() {
		SynonymCategory.MINUS.addCount();

	}

}
