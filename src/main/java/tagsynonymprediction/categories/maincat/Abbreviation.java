package tagsynonymprediction.categories.maincat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import tagsynonymprediction.db.DBHandler;
import tagsynonymprediction.models.SynonymCategory;
import tagsynonymprediction.utility.EArrayList;
import tagsynonymprediction.utility.EList;

public abstract class Abbreviation extends AbstrTagSynonymClass {

	// @Override
	// public List<String> createSynonyms(String originalTag) {
	// List<String> returnList = new ArrayList<>();
	//
	// // returnList.addAll(getSimpleAbbreviations(originalTag, "."));
	// // returnList.addAll(getSimpleAbbreviations(originalTag, "-"));
	// // returnList.addAll(getComplexAbbreviations(originalTag, "."));
	// // returnList.addAll(getComplexAbbreviations(originalTag, "-"));
	// // handleSimpleAbbreviation(originalTag, separator);
	// // handleComplexAbbreviation(originalTag, separator);
	// return returnList;
	// }

	public void handleAbbreviation(String originalTag, String separator) {

	}

	/**
	 * returns a list of simple abbreviations, splitted by separator
	 * 
	 * @param originalTag
	 * @param separator
	 * @return
	 */
	protected EList<String> getSimpleAbbreviations(String originalTag, String separator) {

		if (separator.contains(".")) {
			separator = "\\.";
		}

		String[] splitSource = originalTag.split(separator);
		String abbreviation = "";
		String abbreviation2 = "";
		String abbreviation3 = "";
		String abbreviation4 = "";

		if (splitSource.length > 1) {
			for (String split : splitSource) {
				if (split.length() != 0) {
					if (split.matches(".*\\d\\.?\\d*.*")) {
						abbreviation += split;
						abbreviation2 += split;
						abbreviation4 += "-" + split;

					} else if (split.equals("and")) {
						abbreviation2 = abbreviation2 + "n";
						abbreviation += split.charAt(0);
						abbreviation4 += split.charAt(0);
					} else if (split.equals("to")) {
						abbreviation2 = abbreviation2 + "2";
						abbreviation += split.charAt(0);
						abbreviation4 += split.charAt(0);
					} else if (split.startsWith("cross")) {
						abbreviation2 = abbreviation2 + "x";
						abbreviation += split.charAt(0);
						abbreviation4 += split.charAt(0);
					} else if (split.contains("x")) {
						abbreviation2 = abbreviation2 + "x";
						abbreviation += split.charAt(0);
						abbreviation4 += split.charAt(0);
					} else if (split.equals("javascript")) {
						abbreviation += "js-";
						abbreviation2 += "js";
						abbreviation4 += split.charAt(0);
					} else {
						abbreviation += split.charAt(0);
						abbreviation2 += split.charAt(0);
						abbreviation4 += split.charAt(0);
					}
				}
			}

			// if (target.equals(abbreviation) || target.endsWith(abbreviation))
			// {
			// return true;
			// } else if (abbreviation.length() > 1 &&
			// (target.startsWith(abbreviation) ||
			// abbreviation.contains(target))) {
			// return true;
			// } else if (target.endsWith(abbreviation2) ||
			// target.equals(abbreviation2)) {
			// return true;
			// } else if (abbreviation2.length() > 1 &&
			// (target.startsWith(abbreviation2) ||
			// abbreviation.contains(target))) {
			// return true;
			// }
		}
		abbreviation3 = originalTag.replaceAll(separator, "").replace("to", "2");

		EList<String> returnList = new EArrayList<String>();

		returnList.addAllElements(abbreviation, abbreviation2, abbreviation3, abbreviation4, abbreviation.length() > 1 ? abbreviation.substring(1) : "---------------");
		return returnList;
	}

	private void handleComplexAbbreviation(String originalTag, String separator) {
		List<String> candidates = getComplexAbbreviations(originalTag, separator);

		// addListToCandidateList(originalTag, candidates,
		// EnumMainSynonymCategories.ABBREVIATION);

	}

	/**
	 * returns a list of complex abbreviations, splitted by separator
	 * 
	 * @param separator
	 * @return
	 */
	protected List<String> getComplexAbbreviations(String tag, String separator) {
		List<String> hList = new ArrayList<>();
		if (tag.contains(separator)) {
			if (separator.equals(".")) {
				separator = "\\.";
			}
			List<List<String>> lll = new ArrayList<>();
			// TODO sanity
			String[] splitTag = tag.split(separator);

			for (int i = 0; i < splitTag.length; i++) {
				hList = new ArrayList<>();
				String subTag = splitTag[i];
				for (int j = 1; j < subTag.length() + 1; j++) {
					hList.add(subTag.substring(0, j));
				}
				lll.add(hList);
			}

			hList = new ArrayList<>();

			if (splitTag.length == 2) {
				for (String s : lll.get(0)) {

					for (String t : lll.get(1)) {
						hList.add(s + t);
					}

				}
			} else if (splitTag.length == 3) {
				for (String s : lll.get(0)) {

					for (String t : lll.get(1)) {
						for (String u : lll.get(2)) {
							hList.add(s + t + u);
						}
					}

				}
			}

		}
		return hList;

	}

	/**
	 * returns if source and target are related in an abbreviation with separator
	 * 
	 * @param source
	 * @param target
	 * @param separator
	 * @return
	 */
	public boolean isAbbreviation(String source, String target, String separator) {
		Collection<String> possibleSynonyms = createSynonyms(source).values();
		for (String synonym : possibleSynonyms) {
			if (synonym.equals(target)) {
				SynonymCategory.ABBREVIATION.addCount();
				return true;
			}
		}

		possibleSynonyms = createSynonyms(target).values();
		for (String synonym : possibleSynonyms) {
			if (synonym.equals(source)) {
				SynonymCategory.ABBREVIATION.addCount();
				return true;
			}
		}

		return false;

		// if (checkAbbreviation(source, target, separator) || checkAbbreviation(target, source, separator)) {
		// SynonymCategory.ABBREVIATION.addCount();
		// return true;
		// }
		// return false;

	}

	/**
	 * helpermethod to avoid duplicate code for the relations of source and target
	 * 
	 * @param tag
	 * @param separator
	 * @return
	 */
	private boolean checkAbbreviation(String tag, String target, String separator) {

		List<String> abbreviations = new ArrayList<String>();
		abbreviations.addAll(getSimpleAbbreviations(tag, separator));
		abbreviations.addAll(getComplexAbbreviations(tag, separator));

		if (isSimpleAbbreviation(target, abbreviations)) {
			return true;
		}
		if (isSynonymInTagRelation(target, abbreviations)) {

			return true;
		}
		return false;
	}

	/**
	 * returns true, if the tag starts or ends with the abbreviation
	 * 
	 * @param source
	 * @param abbreviations
	 * @return
	 */
	private boolean isSynonymInTagRelation(String source, List<String> abbreviations) {

		for (String abbreviation : abbreviations) {
			if (abbreviation.length() > 1) {
				if (!source.contains("-") && (source.startsWith(abbreviation) || source.endsWith(abbreviation))) {
					return true;
				}

			}

		}
		return false;
	}

	/**
	 * returns true if the tag equals an abbreviation of the abbreviations-list
	 * 
	 * @param source
	 * @param abbreviations
	 * @return
	 */
	private boolean isSimpleAbbreviation(String source, List<String> abbreviations) {

		for (String abbreviation : abbreviations) {
			if (source.equals(abbreviation)) {
				return true;
			}
			if (source.length() > 1 && abbreviation.length() > 2) {
				if (source.startsWith(abbreviation) || abbreviation.startsWith(source)) {
					return true;
				} else if (source.endsWith(abbreviation) || abbreviation.endsWith(source)) {
					return true;
				}
			}
		}

		return false;
	}

	@Override
	public abstract boolean isSynonymClass(String source, String target);

	public List<String> getStartAndAndTags(Collection<String> returnList) {
		List<String> startOrEndTags = new ArrayList<>();
		for (String abbreviation : returnList) {
			if (abbreviation.length() >= 2) {
				startOrEndTags.addAll(DBHandler.getTagsStartOrEndWithXXX(abbreviation));
				if (useOld) {
					startOrEndTags.addAll(DBHandler.getTagsStartOrEndWithXXXOLDTS(abbreviation));
				}
			}

		}
		return startOrEndTags;
	}

}
