package tagsynonymprediction.categories.maincat;

import java.util.Collection;

import org.apache.commons.codec.language.Metaphone;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import tagsynonymprediction.db.DBHandler;
import tagsynonymprediction.models.SynonymCategory;

public class MetaphoneClass extends AbstrTagSynonymClass {

	@Override
	protected Multimap<SynonymCategory, String> createSynonyms(String originalTag) {
		Multimap<SynonymCategory, String> returnList = ArrayListMultimap.create();
		int size = Math.min(Math.max(originalTag.length() / 2, 2), 7);
		String metaphoneSize = getMetaphoneTag(originalTag, size);
		returnList.putAll(SynonymCategory.METAPHONE, DBHandler.getAllMetaphoneTagsWithCodeLength(metaphoneSize, size));

		// TODO ranking? je höher der code mit übereinstimmungen desto besser?
		if (size < 7) {
			metaphoneSize = getMetaphoneTag(originalTag, size + 1);
			returnList.putAll(SynonymCategory.METAPHONE, DBHandler.getAllMetaphoneTagsWithCodeLength(metaphoneSize, size + 1));
		}

		if (size > 2) {
			metaphoneSize = getMetaphoneTag(originalTag, size - 1);
			returnList.putAll(SynonymCategory.METAPHONE, DBHandler.getAllMetaphoneTagsWithCodeLength(metaphoneSize, size - 1));
		}

		return returnList;
	}

	/**
	 * returns true if metaphonecodes of stemmed source and target are equal
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public boolean isSynonymClass(String source, String target) {

		Collection<String> possibleSynonyms = createSynonyms(source).values();
		for (String synonym : possibleSynonyms) {
			if (synonym.equals(target)) {
				SynonymCategory.VERSION.addCount();
				return true;
			}
		}

		possibleSynonyms = createSynonyms(target).values();
		for (String synonym : possibleSynonyms) {
			if (synonym.equals(source)) {
				SynonymCategory.VERSION.addCount();
				return true;
			}
		}

		// int codeLength = Math.min(Math.max(Math.min(source.length(), target.length()) / 2, 2), 7);
		//
		// String sourcePhone = getMetaphoneTag(hf.getStemmedTag(source), codeLength);
		// String targetPhone = getMetaphoneTag(hf.getStemmedTag(target), codeLength);
		//
		// if (sourcePhone.equals(targetPhone)) {
		// // System.out.println(source + ": " + sourcePhone + " --> " + target
		// // + ": " + targetPhone);
		// SynonymCategory.METAPHONE.addCount();
		// return true;
		// }

		return false;
	}

	public String getMetaphoneTag(String tag, int codeLength) {
		Metaphone mp = new Metaphone();
		mp.setMaxCodeLen(codeLength);
		return mp.encode(tag);
	}

	@Override
	public SynonymCategory getCategory() {
		return SynonymCategory.METAPHONE;
	}

	@Override
	protected void increaseCounter() {
		SynonymCategory.METAPHONE.addCount();

	}

}
