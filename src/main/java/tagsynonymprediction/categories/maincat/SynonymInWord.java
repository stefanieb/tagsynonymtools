package tagsynonymprediction.categories.maincat;

import java.util.Collection;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import tagsynonymprediction.db.DBHandler;
import tagsynonymprediction.models.SynonymCategory;
import tagsynonymprediction.utility.HelperFunctions;
import tagsynonymprediction.utility.Utilities;

public class SynonymInWord extends AbstrTagSynonymClass {

	HelperFunctions hf;

	public SynonymInWord() {
		hf = HelperFunctions.getInstance();
	}

	@Override
	protected Multimap<SynonymCategory, String> createSynonyms(String originalTag) {

		Multimap<SynonymCategory, String> returnList = ArrayListMultimap.create();

		String stemmedTag = Utilities.getStemmedTag(originalTag);
		if (stemmedTag.length() > 2) {
			returnList.putAll(SynonymCategory.SYNONYM_IN_WORD, DBHandler.getTagsStartOrEndWithXXX(Utilities.getStemmedTag(originalTag)));
			returnList.putAll(SynonymCategory.SYNONYM_IN_WORD_LIKE, DBHandler.getTagsLikeXXX(Utilities.getStemmedTag(originalTag)));
		} else {
			returnList.putAll(SynonymCategory.SYNONYM_IN_WORD, DBHandler.getTagsStartOrEndWithXXX(originalTag));
			returnList.putAll(SynonymCategory.SYNONYM_IN_WORD_LIKE, DBHandler.getTagsLikeXXX(originalTag));
		}

		if (useOld) {
			returnList.putAll(SynonymCategory.SYNONYM_IN_WORD, DBHandler.getTagsStartOrEndWithXXXOLDTS(Utilities.getStemmedTag(originalTag)));
			returnList.putAll(SynonymCategory.SYNONYM_IN_WORD_LIKE, DBHandler.getTagsLikeXXXOLDTS(Utilities.getStemmedTag(originalTag)));
		}
		return returnList;

	}

	public boolean isSynonymClass(String source, String target) {

		Collection<String> possibleSynonyms = createSynonyms(source).values();
		for (String synonym : possibleSynonyms) {
			if (synonym.equals(target)) {
				SynonymCategory.SYNONYM_IN_WORD.addCount();
				return true;
			}
		}

		possibleSynonyms = createSynonyms(target).values();
		for (String synonym : possibleSynonyms) {
			if (synonym.equals(source)) {
				SynonymCategory.SYNONYM_IN_WORD.addCount();
				return true;
			}
		}

		// if (source.startsWith(target) || source.endsWith(target) || target.startsWith(source) || target.endsWith(source)) {
		// SynonymCategory.SYNONYM_IN_WORD.addCount();
		// return true;
		// } else if (source.contains(target) || target.contains(source)) {
		// SynonymCategory.SYNONYM_IN_WORD.addCount();
		// return true;
		// }
		// // else if (hf.getStemmedTag(source).contains(hf.getStemmedTag(target))
		// // || hf.getStemmedTag(target).contains(hf.getStemmedTag(source))) {
		// // EnumMainSynonymCategories.SYNONYM_IN_WORD.addCount();
		// // return true;
		// // }
		//
		// String[] splitSource = source.split("-");
		// String[] splitTarget = target.split("-");
		//
		// for (String split : splitSource) {
		// if (target.contains(split)) {
		// SynonymCategory.SYNONYM_IN_WORD.addCount();
		// return true;
		// }
		// }
		//
		// for (String split : splitTarget) {
		// if (source.contains(split)) {
		// SynonymCategory.SYNONYM_IN_WORD.addCount();
		// return true;
		// }
		// }

		return false;
	}

	@Override
	public SynonymCategory getCategory() {
		return SynonymCategory.SYNONYM_IN_WORD;
	}

	@Override
	protected void increaseCounter() {
		SynonymCategory.SYNONYM_IN_WORD.addCount();

	}

}
