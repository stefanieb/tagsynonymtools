package tagsynonymprediction.categories.maincat;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import tagsynonymprediction.models.SynonymCategory;
import tagsynonymprediction.models.TagSynonymCandidates;
import tagsynonymprediction.utility.EArrayList;
import tagsynonymprediction.utility.EList;

public abstract class AbstrTagSynonymClass {

	private TagSynonymCandidates tsc;
	protected EList<String> list;
	protected Boolean isSpecialTag;
	protected boolean useOld;

	// protected HelperFunctions hf;

	public abstract SynonymCategory getCategory();

	public AbstrTagSynonymClass() {
		tsc = TagSynonymCandidates.getInstance();
		// hf = HelperFunctions.getInstance();
		list = new EArrayList<>();
	}

	private void setSpecialTag(Boolean isSpecialTag) {
		this.isSpecialTag = isSpecialTag;
	}

	public boolean createAndAddSynonyms(String originalTag) {
		setSpecialTag(false);
		String tagToParse = originalTag;

		if (originalTag.equals("android") || originalTag.equals("java")) {
			return false;
		}

		if (originalTag.startsWith("android-")) {
			tagToParse = originalTag.replace("android-", "");
			tagToParse = tagToParse.replaceAll("\\d+(\\.?\\d+)", "");
			tagToParse = tagToParse.replaceAll("\\d+", "");
			tagToParse = tagToParse.trim();
			if (tagToParse.equals("")) {
				tagToParse = originalTag;
			}

		} else if (originalTag.startsWith("java-")) {
			tagToParse = originalTag.replace("java-", "");
			tagToParse = tagToParse.replaceAll("\\d+(\\.?\\d+)", "");
			tagToParse = tagToParse.replaceAll("\\d+", "");
			if (tagToParse.equals("")) {
				tagToParse = originalTag;
			}
		} else if (originalTag.matches("\\d+\\.?\\d+")) {
			String tag = originalTag.replaceAll("\\d+(\\.?\\d+)", "");
			tag = tag.replaceAll("\\d+", "");
			tag = tag.trim();
			if (tag.equals("") && !this.getClass().getSimpleName().equals(("Version"))) {
				return false;
			}
		}

		if (!tagToParse.equals(originalTag)) {
			setSpecialTag(true);
		}
		Multimap<SynonymCategory, String> candidates = ArrayListMultimap.create();
		if (this.getClass().getSimpleName().equals("AbbreviationMinus") || this.getClass().getSimpleName().equals("AbbreviationDot")) {
			candidates = createSynonyms(originalTag);
			tsc.checkListIsValidAndAddToCandidates(originalTag, candidates);
		}

		candidates = createSynonyms(tagToParse);
		// tsc.returnListOfValidCandidates(originalTag, candidates);

		return tsc.checkListIsValidAndAddToCandidates(originalTag, candidates);

	}

	public boolean isSynonymClassic(String source, String target, boolean isSpecialTag) {

		this.setSpecialTag(isSpecialTag);
		this.setUseOld(true);
		Multimap<SynonymCategory, String> createSynonyms = createSynonyms(source);
		if (createSynonyms.containsValue(target)) {
			increaseCounter();
			return true;
		} else {
			createSynonyms = createSynonyms(target);
			if (createSynonyms.containsValue(source)) {
				increaseCounter();
				return true;
			}
		}
		return false;

	}

	private void setUseOld(boolean useOld) {
		this.useOld = useOld;

	}

	protected abstract void increaseCounter();

	/**
	 * adds the string s to the list
	 * 
	 * @param s
	 */
	public void add(String s) {
		list.add(s);
	}

	protected abstract Multimap<SynonymCategory, String> createSynonyms(String originalTag);

	/**
	 * returns true if tag may be classified with this method for heuristics
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public abstract boolean isSynonymClass(String source, String target);

	/**
	 * returns the list of synonympairs
	 * 
	 * @return
	 */
	public EList<String> getSynonymPairsAsList() {
		return list;
	}

	/**
	 * prints the list of synonympairs
	 */
	public void printSynonymPairs() {
		for (String pair : list) {
			System.out.println(pair);
		}
	}

}
