package tagsynonymprediction.categories.maincat;

import java.util.Collection;

import org.tartarus.snowball.ext.PorterStemmer;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import tagsynonymprediction.categories.similarity.JaccardSimilarity;
import tagsynonymprediction.categories.similarity.LevenstheinSimilarity;
import tagsynonymprediction.categories.similarity.NGramSimilarity;
import tagsynonymprediction.db.DBHandler;
import tagsynonymprediction.models.SynonymCategory;
import tagsynonymprediction.utility.Utilities;

public class SynonymInTag extends AbstrTagSynonymClass {

	@Override
	protected Multimap<SynonymCategory, String> createSynonyms(String originalTag) {
		Multimap<SynonymCategory, String> returnList = ArrayListMultimap.create();

		String tagToSearch = originalTag.replaceAll("-", "");
		tagToSearch = tagToSearch.replaceAll("\\.", "");
		String tagToSearchNumber = tagToSearch.replaceAll("\\d+(\\.?\\d+)", "");
		tagToSearchNumber = tagToSearchNumber.replaceAll("\\d+", "");
		String stemmed = Utilities.getStemmedTag(originalTag);

		if ((originalTag.contains("-") || originalTag.contains(".")) && tagToSearchNumber.length() > 1) {

			// tagInTag
			if (originalTag.contains("-")) {
				returnList.putAll(splitAndGo(originalTag, "-"));
			}
			if (originalTag.contains(".")) {
				returnList.putAll(splitAndGo(originalTag, "\\."));
			}

			// tagInTag macht das überhaupt sinn?
			// returnList.putAll(splitAndGo(originalTag, "-"));
			// returnList.putAll(splitAndGo(originalTag, "\\."));

			// TODO other stuff?
			if (!isSpecialTag) {
				returnList.putAll(SynonymCategory.SYNONYM_IN_TAG, DBHandler.getTagsStartOrEndWithXXX(tagToSearchNumber));
			}

			if (useOld) {
				returnList.putAll(SynonymCategory.SYNONYM_IN_TAG, DBHandler.getTagsStartOrEndWithXXXOLDTS(tagToSearchNumber));
			}

			returnList.putAll(SynonymCategory.SYNONYM_IN_TAG, DBHandler.getTagsStartWithXXXSkipMinusDot(tagToSearch, Utilities.getStemmedTag(tagToSearch)));

		} else {
			// höher einstufen als andere?
			returnList.putAll(SynonymCategory.SYNONYM_IN_TAG, DBHandler.getTagsWhereSubTagIsXXX(originalTag));
			if (stemmed.length() > 2) {
				returnList.putAll(SynonymCategory.SYNONYM_IN_TAG, DBHandler.getTagsWhereSubTagIsXXX(stemmed));
			}
			String modifiedTag = originalTag.replaceAll("\\d+(\\.?\\d+)", "");
			modifiedTag = modifiedTag.replaceAll("\\d+", "");
			modifiedTag = modifiedTag.trim();
			if (modifiedTag.length() > 2) {
				returnList.putAll(SynonymCategory.SYNONYM_IN_TAG, DBHandler.getTagsWhereSubTagIsXXX(modifiedTag));
				// returnList.putAll(SynonymCategory.SYNONYM_IN_TAG, DBHandler.getTagsStartWithXXXSkipMinusDot(modifiedTag, Utilities.getStemmedTag(modifiedTag)));
			}

		}
		return returnList;
	}

	private Multimap<SynonymCategory, String> splitAndGo(String originalTag, String separator) {
		Multimap<SynonymCategory, String> returnList = ArrayListMultimap.create();
		String[] split = originalTag.split(separator);
		for (String s : split) {
			if (s.length() > 1) {
				String stemmed = getStemmedTag(s);
				// nummern entfernen
				stemmed = stemmed.replaceAll("\\d+(\\.?\\d+)", "");
				stemmed = stemmed.replaceAll("\\d+", "");
				stemmed = stemmed.trim();

				if (!stemmed.equals("")) {

					SynonymCategory sc;
					if (s.length() < 3) {
						sc = SynonymCategory.SYNONYM_IN_TAG_LIKE;
					} else {
						sc = SynonymCategory.SYNONYM_IN_TAG_SUBTAG;
					}

					if (!stemmed.equals(getStemmedTag(s))) {
						returnList.putAll(SynonymCategory.SYNONYM_IN_TAG_SUBTAG, DBHandler.getTagsWhereSubTagIsXXX(stemmed));
					}
					// check dass nicht zu kurz

					returnList.putAll(sc, DBHandler.getTagsWhereSubTagIsXXX(s));
					returnList.putAll(sc, DBHandler.getTagsWhereSubTagIsXXX(s));

					if (super.useOld) {
						returnList.putAll(SynonymCategory.SYNONYM_IN_TAG_SUBTAG, DBHandler.getTagsWhereSubTagOldXXX(s));
						if (!stemmed.equals("")) {
							returnList.putAll(SynonymCategory.SYNONYM_IN_TAG_SUBTAG, DBHandler.getTagsWhereSubTagOldXXX(stemmed));
							returnList.putAll(SynonymCategory.SYNONYM_IN_TAG_LIKE, DBHandler.getTagsLikeXXXOLDTS(stemmed));
						}
						returnList.putAll(SynonymCategory.SYNONYM_IN_TAG_LIKE, DBHandler.getTagsLikeXXXOLDTS(s));
					}

					returnList.putAll(SynonymCategory.SYNONYM_IN_TAG_LIKE, DBHandler.getTagsLikeXXX(s));
					returnList.putAll(SynonymCategory.SYNONYM_IN_TAG_LIKE, DBHandler.getTagsLikeXXX(stemmed));
					returnList.putAll(SynonymCategory.SYNONYM_IN_TAG, DBHandler.getTagsWhereStemmedTagIsXXX(getStemmedTag(s)));

				}
			}
		}
		return returnList;
	}

	public String getStemmedTag(String tag) {

		PorterStemmer p2 = new PorterStemmer();
		p2.setCurrent(tag);
		p2.stem();
		return p2.getCurrent();

	}

	public boolean isSynonymClass(String source, String target) {

		if (source.contains("-") || target.contains("-")) {
			Collection<String> possibleSynonyms = createSynonyms(source).values();
			for (String synonym : possibleSynonyms) {
				if (synonym.equals(target)) {
					SynonymCategory.SYNONYM_IN_TAG.addCount();
					return true;
				}
			}

			possibleSynonyms = createSynonyms(target).values();
			for (String synonym : possibleSynonyms) {
				if (synonym.equals(source)) {
					SynonymCategory.SYNONYM_IN_TAG.addCount();
					return true;
				}
			}
		}

		return false;
	}

	private boolean isTagInTag(String source, String target, String separator) {
		String[] splitSource = source.split("-");

		for (String split : splitSource) {
			if (split.equals(target)) {
				return true;
			}
		}
		return false;
	}

	private boolean isTagsSwitched(String[] splitSource, String[] splitTarget) {

		if (splitSource.length > 1 || splitTarget.length > 1) {
			for (String splitS : splitSource) {

				// TODO getstemmed not implemented
				for (String splitT : splitTarget) {
					if (splitS.equals(splitT)) {
						return true;
					}
					if (isSimilar(splitS, splitT)) {
						return true;
					}
					// else if
					// (hf.getStemmedTag(splitS).equals(hf.getStemmedTag(splitT)))
					// {
					// return true;
					// }

				}

			}
		}
		return false;
	}

	public boolean isSimilar(String source, String target) {

		// ngram

		NGramSimilarity ng = new NGramSimilarity();
		if (ng.isSynonymClass(source, target)) {
			return true;
		}
		// levensthein
		LevenstheinSimilarity ls = new LevenstheinSimilarity();
		if (ls.isSynonymClass(source, target)) {
			return true;
		}
		// jaccard
		JaccardSimilarity js = new JaccardSimilarity();
		if (js.isSynonymClass(source, target)) {
			return true;
		}

		return false;
	}

	@Override
	public SynonymCategory getCategory() {
		return SynonymCategory.SYNONYM_IN_TAG;
	}

	@Override
	protected void increaseCounter() {
		SynonymCategory.SYNONYM_IN_TAG.addCount();

	}

}
