package tagsynonymprediction.categories.maincat;

import java.util.Collection;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import tagsynonymprediction.models.SynonymCategory;
import tagsynonymprediction.utility.EArrayList;
import tagsynonymprediction.utility.EList;

public class Plus extends AbstrTagSynonymClass {

	@Override
	protected Multimap<SynonymCategory, String> createSynonyms(String originalTag) {

		Multimap<SynonymCategory, String> returnList = ArrayListMultimap.create();

		if (originalTag.contains("pp") || originalTag.contains("+") || originalTag.contains("plus")) {
			returnList.putAll(SynonymCategory.PLUS, addPlus(originalTag));
		}

		return returnList;
	}

	// google+--> google-plus
	private EList<String> addPlus(String originalTag) {
		EList<String> returnList = new EArrayList<>();

		returnList.add(originalTag.replace("pp", "++"));
		returnList.add(originalTag.replace("++", "pp"));
		returnList.add(originalTag.replace("plus", "+"));

		return returnList;
	}

	/**
	 * returns true if dot shar or minus plus relation
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public boolean isSynonymClass(String source, String target) {
		if (source.contains("+") || target.contains("+")) {
			Collection<String> possibleSynonyms = createSynonyms(source).values();
			for (String synonym : possibleSynonyms) {
				if (synonym.equals(target)) {
					SynonymCategory.PLUS.addCount();
					return true;
				}
			}

			possibleSynonyms = createSynonyms(target).values();
			for (String synonym : possibleSynonyms) {
				if (synonym.equals(source)) {
					SynonymCategory.PLUS.addCount();
					return true;
				}
			}
		}

		return false;

	}

	@Override
	public SynonymCategory getCategory() {
		return SynonymCategory.PLUS;
	}

	@Override
	protected void increaseCounter() {
		SynonymCategory.PLUS.addCount();

	}

}
