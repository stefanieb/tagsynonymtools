package tagsynonymprediction.categories.maincat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import tagsynonymprediction.models.SynonymCategory;

public class Sharp extends AbstrTagSynonymClass {

	@Override
	public SynonymCategory getCategory() {
		return SynonymCategory.SHARP;
	}

	@Override
	protected Multimap<SynonymCategory, String> createSynonyms(String originalTag) {

		Multimap<SynonymCategory, String> returnList = ArrayListMultimap.create();
		if (originalTag.contains("#") || originalTag.contains("sharp")) {
			returnList.putAll(SynonymCategory.SHARP, addSharp(originalTag));
		}

		return returnList;
	}

	private List<String> addSharp(String originalTag) {
		List<String> returnList = new ArrayList<>();
		returnList.add(originalTag.replace("#", "sharp"));
		returnList.add(originalTag.replace("#", "-sharp"));
		returnList.add(originalTag.replace("#", ""));
		returnList.add(originalTag.replace("sharp", "#"));
		returnList.add(originalTag.replace("sharp", "-#"));
		returnList.add(originalTag.replace("sharp", "#").replace(".", "-"));

		return returnList;
	}

	@Override
	public boolean isSynonymClass(String source, String target) {

		if (!source.contains("#") && !target.contains("#")) {
			return false;
		}

		Collection<String> possibleSynonyms = createSynonyms(source).values();
		for (String synonym : possibleSynonyms) {
			if (synonym.equals(target)) {
				SynonymCategory.SHARP.addCount();
				return true;
			}
		}

		possibleSynonyms = createSynonyms(target).values();
		for (String synonym : possibleSynonyms) {
			if (synonym.equals(source)) {
				SynonymCategory.SHARP.addCount();
				return true;
			}
		}

		return false;

		// if (!source.contains("#") && !target.contains("#")) {
		// return false;
		// }
		// String sourceNeu = source.replace("#", "sharp");
		// String targetNeu = target.replace("#", "sharp");
		// if (sourceNeu.equals(targetNeu)) {
		// SynonymCategory.SHARP.addCount();
		// return true;
		// }
		// sourceNeu = source.replace("#", "");
		// targetNeu = target.replace("#", "");
		// if (sourceNeu.equals(targetNeu)) {
		// SynonymCategory.SHARP.addCount();
		// return true;
		// }
		//
		// return false;
	}

	@Override
	protected void increaseCounter() {
		SynonymCategory.SHARP.addCount();

	}

}
