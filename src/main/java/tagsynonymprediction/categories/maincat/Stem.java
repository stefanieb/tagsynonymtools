package tagsynonymprediction.categories.maincat;

import org.tartarus.snowball.ext.EnglishStemmer;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import tagsynonymprediction.db.DBHandler;
import tagsynonymprediction.models.SynonymCategory;
import tagsynonymprediction.utility.Utilities;

public class Stem extends AbstrTagSynonymClass {

	/**
	 * returns the stemmed string, operated with english stemmer
	 * 
	 * @param tagToStem
	 * @return
	 */
	public String getStemmedTagEnglish(String tagToStem) {

		// if (tagToStem.startsWith("de")) {
		// tagToStem = tagToStem.replace("de", "");
		// }
		// if (tagToStem.startsWith("en")) {
		// tagToStem = tagToStem.replace("en", "");
		// }

		if (tagToStem.length() > 1) {
			EnglishStemmer english = new EnglishStemmer();

			english.setCurrent(tagToStem);
			english.stem();
			return english.getCurrent().trim();
		}
		return tagToStem;

	}

	/**
	 * returns the stemmed string, operated with porter stemmer
	 * 
	 * @param tagToStem
	 * @return
	 */
	public String getStemmedTagPorter(String tagToStem) {

		return Utilities.getStemmedTag(tagToStem);

	}

	/**
	 * @Overwrite
	 */
	public Multimap<SynonymCategory, String> createSynonyms(String originalTag) {
		Multimap<SynonymCategory, String> returnMap = ArrayListMultimap.create();

		// if
		// (getStemmedTagEnglish(originalTag).equals(getStemmedTagPorter(originalTag)))
		// {
		// returnList.add(getStemmedTagPorter(originalTag));
		// returnList.add(getStemmedTagEnglish(originalTag));
		// } else
		// returnList.add(getStemmedTagPorter(originalTag));
		returnMap.putAll(getCategory(), DBHandler.getTagsWhereStemmedTagIsXXX(getStemmedTagPorter(originalTag)));

		return returnMap;
	}

	public boolean isSynonymClass(String source, String target) {

		if (getStemmedTagPorter(source).equals(target) || getStemmedTagPorter(target).equals(source)) {
			SynonymCategory.STEM.addCount();
			return true;
		}

		// List<String> possibleSynonyms = createSynonyms(source);
		// for (String synonym : possibleSynonyms) {
		// if (synonym.equals(target)) {
		// SynonymCategory.STEM.addCount();
		// return true;
		// }
		// }
		//
		// possibleSynonyms = createSynonyms(target);
		// for (String synonym : possibleSynonyms) {
		// if (synonym.equals(source)) {
		// SynonymCategory.STEM.addCount();
		// return true;
		// }
		// }
		return false;
	}

	@Override
	public SynonymCategory getCategory() {
		return SynonymCategory.STEM;
	}

	@Override
	protected void increaseCounter() {
		SynonymCategory.STEM.addCount();

	}

}
