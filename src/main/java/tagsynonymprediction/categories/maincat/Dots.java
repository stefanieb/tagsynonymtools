package tagsynonymprediction.categories.maincat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import tagsynonymprediction.models.SynonymCategory;

public class Dots extends AbstrTagSynonymClass {

	@Override
	public SynonymCategory getCategory() {
		return SynonymCategory.DOTS;
	}

	@Override
	protected Multimap<SynonymCategory, String> createSynonyms(String originalTag) {

		Multimap<SynonymCategory, String> returnList = ArrayListMultimap.create();
		returnList.putAll(SynonymCategory.DOTS, addDot(originalTag));

		return returnList;
	}

	private List<String> addDot(String originalTag) {
		List<String> returnList = new ArrayList<>();
		if (originalTag.contains(".") || originalTag.contains("dot")) {
			if (originalTag.startsWith(".")) {
				returnList.add(originalTag.replaceFirst(".", ""));
				returnList.add(originalTag.replaceFirst(".", "dot"));
				returnList.add(originalTag.replaceFirst(".", "-dot"));
				returnList.add(originalTag.replaceFirst(".", "-"));
			}

			returnList.add(originalTag.replaceAll(".", "-"));
			returnList.add(originalTag.replaceAll(".", ""));
			returnList.add(originalTag.replaceAll("dot", "."));
			returnList.add(originalTag.replaceAll("dot-", "."));
		}
		return returnList;
	}

	@Override
	public boolean isSynonymClass(String source, String target) {
		if (source.contains(".") || source.contains("dot") || target.contains(".") || target.contains("dot")) {
			Collection<String> possibleSynonyms = createSynonyms(source).values();
			for (String synonym : possibleSynonyms) {
				if (synonym.equals(target)) {
					SynonymCategory.DOTS.addCount();
					return true;
				}
			}

			possibleSynonyms = createSynonyms(target).values();
			for (String synonym : possibleSynonyms) {
				if (synonym.equals(source)) {
					SynonymCategory.DOTS.addCount();
					return true;
				}
			}
		}

		return false;

	}

	@Override
	protected void increaseCounter() {
		SynonymCategory.DOTS.addCount();

	}

}
