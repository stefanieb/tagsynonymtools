package tagsynonymprediction.categories.maincat;

import java.text.DecimalFormat;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import tagsynonymprediction.db.DBHandler;
import tagsynonymprediction.models.SynonymCategory;
import tagsynonymprediction.utility.EArrayList;
import tagsynonymprediction.utility.EList;

public class Version extends AbstrTagSynonymClass {

	@Override
	protected Multimap<SynonymCategory, String> createSynonyms(String originalTag) {

		Multimap<SynonymCategory, String> returnList = ArrayListMultimap.create();

		if (originalTag.matches(".*\\d\\.?\\d*.*")) {
			returnList.put(SynonymCategory.VERSION, removeAllNumbers(originalTag));
			returnList.put(SynonymCategory.VERSION, getNumbersInStrings(originalTag).trim());
			returnList.putAll(SynonymCategory.VERSION, DBHandler.getTagsWhereTagWithoutNumbersIsXXX(removeAllNumbers(originalTag)));

			// TODO implement
			// returnList.add(separateNumbersFromTag(originalTag));
		} else {

			if (!getStringInNumbers(originalTag, "").trim().equals(originalTag)) {
				returnList.put(SynonymCategory.VERSION, getStringInNumbers(originalTag, ""));
			}
			if (!getStringInNumbers(originalTag, "-").trim().equals(originalTag)) {
				returnList.put(SynonymCategory.VERSION, getStringInNumbers(originalTag, "-"));
			}
			if (!getNumbersInStrings(originalTag).trim().equals(originalTag)) {
				returnList.put(SynonymCategory.VERSION, getStringInNumbers(originalTag, ""));
			}
			returnList.putAll(SynonymCategory.VERSION, DBHandler.getTagsByTagWithoutNumbersButExistingNumbers(originalTag));

		}

		return returnList;
	}

	public EList<String> getCreatedSynonymsForTag(String tag) {
		EList<String> returnList = new EArrayList<>();

		if (tag.matches(".*\\d\\.?\\d*.*")) {
			returnList.add(removeAllNumbers(tag));
			returnList.add(getNumbersInStrings(tag).trim());
			returnList.addAll(DBHandler.getTagsWhereTagWithoutNumbersIsXXX(removeAllNumbers(tag)));
		}
		return returnList;
	}

	private String getStringInNumbers(String originalTag, String separator) {

		// TODO evtl einbauen...
		String newTag = originalTag;
		if (originalTag.contains("one")) {
			newTag = newTag.replaceAll("one", "1");
		}
		if (originalTag.contains("two")) {
			newTag = newTag.replaceAll("two", "2");
		}
		if (originalTag.contains("three")) {
			newTag = newTag.replaceAll("three", "3");
		}
		if (originalTag.contains("four")) {
			newTag = newTag.replaceAll("four", "4");
		}
		if (originalTag.contains("five")) {
			newTag = newTag.replaceAll("five", "5");
		}
		if (originalTag.contains("six")) {
			newTag = newTag.replaceAll("one", "6");
		}
		if (originalTag.contains("seven")) {
			newTag = newTag.replaceAll("seven", "7");
		}
		if (originalTag.contains("eight")) {
			newTag = newTag.replaceAll("eight", "8");
		}
		if (originalTag.contains("nine")) {
			newTag = newTag.replaceAll("nine", "9");
		}
		if (originalTag.contains("zero")) {
			newTag = newTag.replaceAll("zero", "0");
		}
		return newTag;
	}

	private String separateNumbersFromTag(String originalTag) {
		String modifiedTag = originalTag;
		if (originalTag.matches(".*\\d\\.?\\d*")) {
			originalTag.split("\\d");
		} else if (originalTag.matches(".*\\d\\.?\\d*.*")) {
			//
		}
		return modifiedTag;
	}

	public boolean isSynonymClass(String source, String target) {
		// neu START
		Multimap<SynonymCategory, String> createSynonyms = createSynonyms(source);
		if (createSynonyms.containsValue(target)) {
			return true;
		} else {
			createSynonyms = createSynonyms(target);
			if (createSynonyms.containsValue(source)) {
				return true;
			}
		}
		return false;

		// neu ENDE

		// Collection<String> possibleSynonyms = createSynonyms(source).values();
		// for (String synonym : possibleSynonyms) {
		// if (synonym.equals(target)) {
		// SynonymCategory.VERSION.addCount();
		// return true;
		// }
		// }
		//
		// possibleSynonyms = createSynonyms(target).values();
		// for (String synonym : possibleSynonyms) {
		// if (synonym.equals(source)) {
		// SynonymCategory.VERSION.addCount();
		// return true;
		// }
		// }

		// if (hf.isNumeric(source) || hf.isNumeric(target)) {
		// if (removeAllNumbers(source).equals(removeAllNumbers(target))) {
		// SynonymCategory.VERSION.addCount();
		// return true;
		// } else if (getNumbersInStrings(source).equals(getNumbersInStrings(target))) {
		// SynonymCategory.VERSION.addCount();
		// return true;
		// } else if (getStringInNumbers(source, "-").equals(target) || getStringInNumbers(target, "-").equals(source)) {
		// SynonymCategory.VERSION.addCount();
		// return true;
		// } else if (getStringInNumbers(source, ".").equals(target) || getStringInNumbers(target, ".").equals(source)) {
		// SynonymCategory.VERSION.addCount();
		// return true;
		// }
		// }

		// return false;
	}

	public String removeAllNumbers(String tag) {
		tag = tag.replaceAll("\\d+(\\.?\\d*)", "");
		if (tag.endsWith("-")) {
			tag = tag.substring(0, tag.length() - 1);
		}
		return tag;
	}

	private static final String[] tensNames = { "", " ten", " twenty", " thirty", " forty", " fifty", " sixty", " seventy", " eighty", " ninety" };

	private static final String[] numNames = { "", " one", " two", " three", " four", " five", " six", " seven", " eight", " nine", " ten", " eleven", " twelve", " thirteen",
			" fourteen", " fifteen", " sixteen", " seventeen", " eighteen", " nineteen" };

	private String convertLessThanOneThousand(int number) {
		String soFar;

		if (number % 100 < 20) {
			soFar = numNames[number % 100];
			number /= 100;
		} else {
			soFar = numNames[number % 10];
			number /= 10;

			soFar = tensNames[number % 10] + soFar;
			number /= 10;
		}
		if (number == 0)
			return soFar;
		return numNames[number] + " hundred" + soFar;
	}

	public String getNumbersInStrings(String toParse) {

		if (toParse.matches(".*\\d\\.?\\d*.*")) {

			String numberOnly = toParse.replaceAll("[^0-9]", "");
			String convertToNumber = convertToNumber(Long.parseLong(numberOnly));

			return toParse.replaceAll("\\d\\.?\\d*", convertToNumber.trim());
		}
		return toParse;

	}

	// copied from http://www.rgagnon.com/javadetails/java-0426.html
	public String convertToNumber(long number) {
		// 0 to 999 999 999 999
		if (number == 0) {
			return "zero";
		}

		String snumber = Long.toString(number);

		// pad with "0"
		String mask = "000000000000";
		DecimalFormat df = new DecimalFormat(mask);
		snumber = df.format(number);

		// XXXnnnnnnnnn
		int billions = Integer.parseInt(snumber.substring(0, 3));
		// nnnXXXnnnnnn
		int millions = Integer.parseInt(snumber.substring(3, 6));
		// nnnnnnXXXnnn
		int hundredThousands = Integer.parseInt(snumber.substring(6, 9));
		// nnnnnnnnnXXX
		int thousands = Integer.parseInt(snumber.substring(9, 12));

		String tradBillions;
		switch (billions) {
		case 0:
			tradBillions = "";
			break;
		case 1:
			tradBillions = convertLessThanOneThousand(billions) + " billion ";
			break;
		default:
			tradBillions = convertLessThanOneThousand(billions) + " billion ";
		}
		String result = tradBillions;

		String tradMillions;
		switch (millions) {
		case 0:
			tradMillions = "";
			break;
		case 1:
			tradMillions = convertLessThanOneThousand(millions) + " million ";
			break;
		default:
			tradMillions = convertLessThanOneThousand(millions) + " million ";
		}
		result = result + tradMillions;

		String tradHundredThousands;
		switch (hundredThousands) {
		case 0:
			tradHundredThousands = "";
			break;
		case 1:
			tradHundredThousands = "one thousand ";
			break;
		default:
			tradHundredThousands = convertLessThanOneThousand(hundredThousands) + " thousand ";
		}
		result = result + tradHundredThousands;

		String tradThousand;
		tradThousand = convertLessThanOneThousand(thousands);
		result = result + tradThousand;

		// remove extra spaces!
		return result.replaceAll("^\\s+", "").replaceAll("\\b\\s{2,}\\b", " ");
	}

	@Override
	public SynonymCategory getCategory() {
		return SynonymCategory.VERSION;
	}

	@Override
	protected void increaseCounter() {
		SynonymCategory.VERSION.addCount();

	}

}
