package tagsynonymprediction.categories.maincat;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import tagsynonymprediction.models.SynonymCategory;

public class AbbreviationMinus extends Abbreviation {

	private final String SEPARATOR = "-";

	@Override
	public boolean isSynonymClass(String source, String target) {
		return super.isAbbreviation(source, target, SEPARATOR);
	}

	@Override
	public Multimap<SynonymCategory, String> createSynonyms(String originalTag) {

		Multimap<SynonymCategory, String> returnList = ArrayListMultimap.create();

		if (originalTag.contains(SEPARATOR)) {
			returnList.putAll(SynonymCategory.ABBREVIATION, super.getSimpleAbbreviations(originalTag, SEPARATOR));
			returnList.putAll(SynonymCategory.ABBREVIATION_COMPLEX, super.getComplexAbbreviations(originalTag, SEPARATOR));
			if (originalTag.contains("javascript")) {
				returnList.putAll(SynonymCategory.ABBREVIATION, super.getSimpleAbbreviations(originalTag.replace("javascript", "js"), SEPARATOR));
				returnList.putAll(SynonymCategory.ABBREVIATION_COMPLEX, super.getComplexAbbreviations(originalTag.replace("javascript", "js"), SEPARATOR));
			} else if (originalTag.contains("java")) {
				returnList.putAll(SynonymCategory.ABBREVIATION, super.getSimpleAbbreviations(originalTag.replace("java", "j"), SEPARATOR));
				returnList.putAll(SynonymCategory.ABBREVIATION_COMPLEX, super.getComplexAbbreviations(originalTag.replace("java", "j"), SEPARATOR));
			}
		}
		// special abbreviations
		else {
			if (originalTag.contains("javascript")) {
				returnList.put(SynonymCategory.ABBREVIATION, originalTag.replace("javascript", "js"));
			} else if (originalTag.contains("java")) {
				returnList.put(SynonymCategory.ABBREVIATION, originalTag.replace("java", "j"));
			}
		}
		returnList.putAll(SynonymCategory.ABBREVIATION_START_OR_END, super.getStartAndAndTags(returnList.values()));

		return returnList;
	}

	@Override
	public SynonymCategory getCategory() {
		return SynonymCategory.ABBREVIATION;
	}

	@Override
	protected void increaseCounter() {
		SynonymCategory.ABBREVIATION.addCount();

	}

}
