package helper;

import java.util.Date;

public class TrendDto {

	private Date _date;
	private Integer postCount;
	private Integer answerCountAvg;
	private Integer answerCountSum;
	private Integer commentCountSum;
	private Integer commentCountAvg;
	private Integer acceptedAnswerCountSum;
	// private Integer acceptedAnswerCountAvg;
	private Integer openAnswerCountSum;
	private Integer openAnswerCountAvg;
	private Integer favouriteCountSum;
	private Integer favouriteCountAvg;
	private Integer scoreCountSum;
	private Integer scoreCountPos;
	private Integer scoreCountNeg;
	private Integer viewCountAvg;
	private Integer viewCountSum;

	public Integer getViewCountSum() {
		return viewCountSum;
	}

	public void setViewCountSum(Integer viewCountSum) {
		this.viewCountSum = viewCountSum;
	}

	public Integer getAnswerCountAvg() {
		return answerCountAvg;
	}

	public void setAnswerCountAvg(Integer answerCountAvg) {
		this.answerCountAvg = answerCountAvg;
	}

	public Integer getAnswerCountSum() {
		return answerCountSum;
	}

	public void setAnswerCountSum(Integer answerCountSum) {
		this.answerCountSum = answerCountSum;
	}

	public Integer getCommentCountSum() {
		return commentCountSum;
	}

	public void setCommentCountSum(Integer commentCountSum) {
		this.commentCountSum = commentCountSum;
	}

	public Integer getCommentCountAvg() {
		return commentCountAvg;
	}

	public void setCommentCountAvg(Integer commentCountAvg) {
		this.commentCountAvg = commentCountAvg;
	}

	public Integer getAcceptedAnswerCountSum() {
		return acceptedAnswerCountSum;
	}

	public void setAcceptedAnswerCountSum(Integer acceptedAnswerCountSum) {
		this.acceptedAnswerCountSum = acceptedAnswerCountSum;
	}

	// public Integer getAcceptedAnswerCountAvg() {
	// return acceptedAnswerCountAvg;
	// }
	//
	// public void setAcceptedAnswerCountAvg(Integer acceptedAnswerCountAvg) {
	// this.acceptedAnswerCountAvg = acceptedAnswerCountAvg;
	// }

	public Integer getOpenAnswerCountSum() {
		return openAnswerCountSum;
	}

	public void setOpenAnswerCountSum(Integer openAnswerCountSum) {
		this.openAnswerCountSum = openAnswerCountSum;
	}

	public Integer getOpenAnswerCountAvg() {
		return openAnswerCountAvg;
	}

	public void setOpenAnswerCountAvg(Integer openAnswerCountAvg) {
		this.openAnswerCountAvg = openAnswerCountAvg;
	}

	public Integer getFavouriteCountSum() {
		return favouriteCountSum;
	}

	public void setFavouriteCountSum(Integer favouriteCountSum) {
		this.favouriteCountSum = favouriteCountSum;
	}

	public Integer getFavouriteCountAvg() {
		return favouriteCountAvg;
	}

	public void setFavouriteCountAvg(Integer favouriteCountAvg) {
		this.favouriteCountAvg = favouriteCountAvg;
	}

	public Integer getScoreCountSum() {
		return scoreCountSum;
	}

	public void setScoreCountSum(Integer scoreCountSum) {
		this.scoreCountSum = scoreCountSum;
	}

	public Integer getScoreCountPos() {
		return scoreCountPos;
	}

	public void setScoreCountPos(Integer scoreCountPos) {
		this.scoreCountPos = scoreCountPos;
	}

	public Integer getScoreCountNeg() {
		return scoreCountNeg;
	}

	public void setScoreCountNeg(Integer scoreCountNeg) {
		this.scoreCountNeg = scoreCountNeg;
	}

	public Date getDate() {
		return _date;
	}

	public void setDate(Date _date) {
		this._date = _date;
	}

	public Integer getPostCount() {
		return postCount;
	}

	public void setPostCount(Integer postCount) {
		this.postCount = postCount;
	}

	public Integer getViewCountAvg() {
		return viewCountAvg;
	}

	public void setViewCountAvg(Integer viewCountAvg) {
		this.viewCountAvg = viewCountAvg;
	}

}
