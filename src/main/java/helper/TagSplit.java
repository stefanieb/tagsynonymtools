package helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import tagsynonymprediction.categories.maincat.MetaphoneClass;
import tagsynonymprediction.utility.Utilities;

public class TagSplit {

	private List<String> tagList;
	private Map<String, Integer> tags;

	public static void main(String args[]) {

		TagSplit ts = new TagSplit();
		ts.initSplitAndStemmedTags();
		System.out.println("SplitStem finished");
		// ts.calculateSimilarityForEachTag();
		// System.out.println("similarity finished");
		// ts.calculateMetaphone();
		// System.out.println("metaphone finished");

	}

	public TagSplit() {
		// this.tags = HelperDBHandler.getAllTagsAndId();
		// this.tags = HelperDBHandler.getAllAndroidTagsAndId();
		this.tags = HelperDBHandler.getAllTagOfTagsAndSynonymTable();
		tagList = new ArrayList<String>();
		tagList.addAll(tags.keySet());
		// this.tags = HelperDBHandler.getAllTagsOfReferenceSetDisregardingTagTable();
	}

	public void calculateSimilarityForEachTag() {

		HelperDBHandler.deleteFromDB("tag_similarity");

		int POOL_SIZE = 5000;
		ExecutorService executor = Executors.newFixedThreadPool(POOL_SIZE);
		int TOTAL_TAG_COUNT = tags.size();

		List<Runnable> threadList = new ArrayList<Runnable>();

		// Collections.sort(allTagNames);

		for (int i = 0; i < TOTAL_TAG_COUNT; i += POOL_SIZE) {
			List<String> pooltags = new ArrayList<>();
			int subset = i;
			for (int j = i; j < i + POOL_SIZE && j < TOTAL_TAG_COUNT; j++) {
				// for (int j = i; j < i + POOL_SIZE && i + POOL_SIZE < TOTAL_TAG_COUNT; j++) {
				// i + POOL_SIZE < TOTAL_TAG_COUNT
				// System.out.println(allTagNames.get(j));
				pooltags.add(tagList.get(j));
			}
			Runnable worker = new TagDataMain(pooltags, tagList, subset);
			threadList.add(worker);
			// System.out.println("worker");
		}

		for (Runnable r : threadList) {
			executor.execute(r);
		}

		executor.shutdown();
		while (!executor.isTerminated()) {
			// System.out.println("Waiting for Threadpool to finish");
		}
		System.out.println("Finished all threads");
	}

	public void initSplitAndStemmedTags() {
		HelperDBHandler.deleteFromDB("tagsplit");
		for (String tag : tagList) {

			String skipNumber = tag.replaceAll("-?\\d+(\\.?\\d*)", "");

			String stemmed = Utilities.getStemmedTag(tag);

			String[] split = tag.split("-");

			String parts[] = new String[5];
			Arrays.fill(parts, "");
			String stem = Utilities.getStemmedTag(split[0]);
			if (stem.length() > 2) {
				parts[0] = stem;
			} else {
				parts[0] = split[0];
			}
			if (split.length > 1) {
				stem = Utilities.getStemmedTag(split[1]);
				if (stem.length() > 2) {
					parts[1] = stem;
				} else {
					parts[1] = split[1];
				}

				if (split.length > 2) {
					stem = Utilities.getStemmedTag(split[2]);
					if (stem.length() > 2) {
						parts[2] = stem;
					} else {
						parts[2] = split[2];
					}

					if (split.length > 3) {
						stem = Utilities.getStemmedTag(split[3]);
						if (stem.length() > 2) {
							parts[3] = stem;
						} else {
							parts[3] = split[3];
						}

						if (split.length > 4) {
							stem = Utilities.getStemmedTag(split[4]);
							if (stem.length() > 2) {
								parts[4] = stem;
							} else {
								parts[4] = split[1];
							}
						}
					}
				}
			}

			String skipMinusOrDot = tag.replaceAll("\\.", "").replaceAll("-", "");
			HelperDBHandler.updateDBwithTagSplitting(tags.get(tag), tag, stemmed, skipNumber, skipMinusOrDot, parts);
		}
	}

	public void calculateMetaphone() {
		HelperDBHandler.deleteFromDB("tagmetaphone");
		MetaphoneClass mc = new MetaphoneClass();
		for (String tag : tagList) {
			String[] metaphone = new String[7];
			metaphone[0] = tag;
			for (int i = 2; i <= 7; i++) {
				metaphone[i - 1] = mc.getMetaphoneTag(tag, i);
			}

			HelperDBHandler.updateDBwithMetaphoneTags(metaphone);

		}

	}
}
