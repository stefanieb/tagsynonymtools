package helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.lucene.search.spell.LuceneLevenshteinDistance;
import org.apache.lucene.search.spell.NGramDistance;
import org.tartarus.snowball.ext.PorterStemmer;

public class TagDataMain implements Runnable {

	// private Stem stem;
	// private JaccardSimilarity jacSim;
	// private LevenstheinSimilarity levSim;
	// private NGramSimilarity ngramSim;

	private List<String> allTags;
	private List<String> tags;
	private int subset;

	public TagDataMain(List<String> tags, List<String> allTags, int subset) {
		this.tags = tags;
		// this.stem = new Stem();
		// this.jacSim = new JaccardSimilarity();
		// this.levSim = new LevenstheinSimilarity();
		// this.ngramSim = new NGramSimilarity();
		this.allTags = allTags;
		this.subset = subset;
	}

	@Override
	public void run() {
		List<String> tagNames = new ArrayList<>();
		tagNames.addAll(allTags);
		int counter = 0;
		for (String tag : tags) {
			int i = subset;
			for (i = subset; i < allTags.size(); i++) {
				// for (int i = 0; i < tagNames.size(); i++) {

				String tagFromAllTags = tagNames.get(i);
				// System.out.println(tag + " --- " + tagFromAllTags);
				calculateAndInsertData(tag, tagFromAllTags);
				counter++;
				if (counter == 1000) {

					if (!tag1List.isEmpty()) {
						HelperDBHandler.insertSimilarityValuesAsList(tag1List, tag2List, jaccList, levenstheinList, n2List, n3List, n4List);
					}
					tag1List = new ArrayList<>();
					tag2List = new ArrayList<>();
					jaccList = new ArrayList<>();
					levenstheinList = new ArrayList<>();
					n2List = new ArrayList<>();
					n3List = new ArrayList<>();
					n4List = new ArrayList<>();
					counter = 0;
				}
			}
			subset++;

		}

		if (!tag1List.isEmpty()) {
			HelperDBHandler.insertSimilarityValuesAsList(tag1List, tag2List, jaccList, levenstheinList, n2List, n3List, n4List);
		}
		System.out.println("complete");

	}

	private String calculateStem(String tagToStem) {
		PorterStemmer p2 = new PorterStemmer();
		p2.setCurrent(tagToStem);
		p2.stem();
		return p2.getCurrent();
	}

	private float calculateJaccard(char[] s, char[] t) {
		int intersection = 0;
		int union = s.length + t.length;
		boolean[] sdup = new boolean[s.length];
		union -= findDuplicates(s, sdup); // <co id="co_fuzzy_jaccard_dups1"/>
		boolean[] tdup = new boolean[t.length];
		union -= findDuplicates(t, tdup);
		for (int si = 0; si < s.length; si++) {
			if (!sdup[si]) { // <co id="co_fuzzy_jaccard_skip1"/>
				for (int ti = 0; ti < t.length; ti++) {
					if (!tdup[ti]) {
						if (s[si] == t[ti]) { // <co
												// id="co_fuzzy_jaccard_intersection"
												// />
							intersection++;
							break;
						}
					}
				}
			}
		}
		union -= intersection;
		return (float) intersection / union; // <co
												// id="co_fuzzy_jaccard_return"/>
	}

	private int findDuplicates(char[] s, boolean[] sdup) {
		int ndup = 0;
		for (int si = 0; si < s.length; si++) {
			if (sdup[si]) {
				ndup++;
			} else {
				for (int si2 = si + 1; si2 < s.length; si2++) {
					if (!sdup[si2]) {
						sdup[si2] = s[si] == s[si2];
					}
				}
			}
		}
		return ndup;
	}

	private float getNgramDistance(String tag, String tag2compare, Integer n) {
		NGramDistance ngram = new NGramDistance(n);
		return ngram.getDistance(tag, tag2compare);
	}

	public float getLevenstheinDistance(String tag, String tag2compare) {
		LuceneLevenshteinDistance lld = new LuceneLevenshteinDistance();
		return lld.getDistance(tag, tag2compare);
	}

	List<String> tag1List = new ArrayList<>();
	List<String> tag2List = new ArrayList<>();
	List<Float> jaccList = new ArrayList<>();
	List<Float> levenstheinList = new ArrayList<>();
	List<Float> n2List = new ArrayList<>();
	List<Float> n3List = new ArrayList<>();
	List<Float> n4List = new ArrayList<>();

	private void calculateAndInsertData(String tag1, String tag2) {
		if (!tag1.equals(tag2)) {

			float jaccardSimilarity = 0.0f;
			float levenstheinDistance = 0.0f;
			float nGramSimilarity2 = 0.0f;
			float nGramSimilarity3 = 0.0f;
			float nGramSimilarity4 = 0.0f;

			String tag1Stemmed = calculateStem(tag1);
			String tag2Stemmed = calculateStem(tag2);

			jaccardSimilarity = calculateJaccard(tag1Stemmed.toCharArray(), tag2Stemmed.toCharArray());
			levenstheinDistance = getLevenstheinDistance(tag1Stemmed, tag2Stemmed);
			nGramSimilarity2 = getNgramDistance(tag1Stemmed, tag2Stemmed, 2);
			nGramSimilarity3 = getNgramDistance(tag1Stemmed, tag2Stemmed, 3);
			nGramSimilarity4 = getNgramDistance(tag1Stemmed, tag2Stemmed, 4);

			if (jaccardSimilarity >= 0.5 && levenstheinDistance >= 0.5 && nGramSimilarity2 >= 0.5 && nGramSimilarity3 >= 0.5 && nGramSimilarity4 >= 0.5) {

				tag1List.add(tag1);
				tag2List.add(tag2);
				jaccList.add(jaccardSimilarity);
				levenstheinList.add(levenstheinDistance);
				n2List.add(nGramSimilarity2);
				n3List.add(nGramSimilarity3);
				n4List.add(nGramSimilarity4);
			}

			// System.out.println("-- " + tag1 + " " + tag2);
			// HelperDBHandler.insertSimilarityValues(tag1, tag2, jaccardSimilarity, levenstheinDistance, nGramSimilarity2, nGramSimilarity3, nGramSimilarity4);

		}
	}

}
