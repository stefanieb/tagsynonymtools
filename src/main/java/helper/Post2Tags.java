package helper;

import java.util.Map;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

public class Post2Tags {

	private Table<Integer, Integer, Integer> post2Tag;

	public static void main(String args[]) {
		new Post2Tags().startPost2Tags();
		// DBHandler.getAllTagsOfReferenceSetDisregardingTagTable();

	}

	public void startPost2Tags() {
		System.out.println("start: insert post2tag");
		HelperDBHandler.deleteFromDBwithPost2Tag();

		// postId and tagId

		// TODO performance - mehr inserts und evtl threads?
		post2Tag = HashBasedTable.create();
		// android
		Map<Integer, String> postIdAndTags = HelperDBHandler.getPostIdAndTags();
		Map<String, Integer> tagAndTagId = HelperDBHandler.getAllTagsAndId();

		for (Integer i : postIdAndTags.keySet()) {

			if (post2Tag.size() > 1000) {
				HelperDBHandler.updateDBwithPost2TagRelationTable(post2Tag);
				post2Tag = HashBasedTable.create();
			}
			String tags = postIdAndTags.get(i);
			if (tags.contains("><")) {
				String[] split = tags.split("><");
				for (int j = 0; j < split.length; j++) {
					String s = split[j];
					int position = j + 1;
					s = s.replace("<", "");
					s = s.replace(">", "");
					if (tagAndTagId.containsKey(s)) {
						// HelperDBHandler.updateDBwithPost2TagRelationSingle(i,
						// tagAndTagId.get(s), position);
						post2Tag.put(i, tagAndTagId.get(s), position);

					}
				}
			} else {
				tags = tags.replace("<", "");
				tags = tags.replace(">", "");
				if (tagAndTagId.containsKey(tags)) {
					// HelperDBHandler.updateDBwithPost2TagRelationSingle(i,
					// tagAndTagId.get(tags), 1);
					post2Tag.put(i, tagAndTagId.get(tags), 1);

				}
			}

		}
		HelperDBHandler.updateDBwithPost2TagRelationTable(post2Tag);
		post2Tag = HashBasedTable.create();

		System.out.println("stop: insert post2tag ");

	}

	public Post2Tags() {

	}

	//
	// int POOL_SIZE = 1000;
	// ExecutorService executor = Executors.newFixedThreadPool(POOL_SIZE);
	// int TOTAL_TAG_COUNT = postIdAndTags.size();
	//
	// List<Runnable> threadList = new ArrayList<Runnable>();
	//
	// List<String> allTagNames = new ArrayList<String>();
	// allTagNames.addAll(tags.keySet());
	// // Collections.sort(allTagNames);
	//
	// for (int i = 0; i < TOTAL_TAG_COUNT; i += POOL_SIZE) {
	// List<String> pooltags = new ArrayList<>();
	// int subset = i;
	// for (int j = i; j < i + POOL_SIZE && j < TOTAL_TAG_COUNT; j++) {
	//
	// pooltags.add(allTagNames.get(j));
	// }
	// Runnable worker = new TagDataMain(pooltags, tags, subset);
	// threadList.add(worker);
	// // System.out.println("worker");
	// }
	//
	// for (Runnable r : threadList) {
	// executor.execute(r);
	// }
	//
	// executor.shutdown();
	// while (!executor.isTerminated()) {
	// // System.out.println("Waiting for Threadpool to finish");
	// }
	// System.out.println("Finished all threads");

}
