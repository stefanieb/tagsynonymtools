package helper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Table;
import com.google.common.collect.Table.Cell;

import tagsynonymprediction.models.SynonymCandidate;
import tagsynonymprediction.models.SynonymCategory;
import tagsynonymprediction.models.Tag;
import tagsynonymprediction.utility.EArrayList;
import tagsynonymprediction.utility.EList;

public class HelperDBHandler {

	private static String url = "jdbc:mysql://localhost:3306/";
	private static String dbName = "SO_Nov2014";
	// private static String dbName = "AndroidOnly";
	private static String driver = "com.mysql.jdbc.Driver";
	private static String userName = "root";
	private static String password = "";

	public static Map<Integer, String> getPostIdAndTags() {

		Map<Integer, String> returnList = new HashMap<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select Id, Tags from all_posts where Tags is not null and Tags like '%android%'");
			// ResultSet tags_db = st.executeQuery(
			// "select Id, Tags from all_posts where Id in
			// (14693145,4968751,4968512,9937681,21620553,20870853,17639040,21620505,19875372,8819400,25470073,13853279,13853285,9937798,6926551,20871044)");
			while (tags_db.next()) {

				Integer id = tags_db.getInt("Id");
				String tags = tags_db.getString("Tags");
				returnList.put(id, tags);

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	public static Map<String, Integer> getAllTagsAndId() {

		Map<String, Integer> returnList = new HashMap<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select Id, TagName from Tags;");

			while (tags_db.next()) {

				Integer id = tags_db.getInt("Id");
				String tags = tags_db.getString("TagName");
				returnList.put(tags, id);

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	public static Map<String, Integer> getAllAndroidTagsAndId(Integer limit) {

		if (limit == null) {
			limit = 15000;
		}
		Map<String, Integer> returnList = new HashMap<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			String where = "";// where tags.TagName like 'g%'";// //or
								// tags.TagName = 'droidex' or tags.TagName =
								// 'droid-fu' or tags.TagName = 'freetype' or
								// tags.TagName =
								// 'droidgap'
								// or
								// tags.TagName = 'color' ";

			ResultSet tags_db = st.executeQuery(
					"select distinct tags.Id, tags.TagName  from tags  join post2tag  on tags.Id = post2tag.tagId " + where + " order by tags.count desc limit 0," + limit + " ;");

			while (tags_db.next()) {

				Integer id = tags_db.getInt("Id");
				String tags = tags_db.getString("TagName");
				returnList.put(tags, id);

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	public static boolean deleteFromDBwithPost2Tag() {
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			st.executeUpdate("delete from post2tag");

			conn.close();
			return true;

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public static boolean updateDBwithPost2TagRelationSingle(Integer postId, Integer tagId, int position) {
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			st.executeUpdate("insert into post2tag (postId, tagId, position) values (" + postId + ", " + tagId + ", " + position + ")");
			conn.close();
			return true;

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public static void updateDBwithPost2TagRelationTable(Table<Integer, Integer, Integer> post2Tag) {
		String values;

		StringBuilder sb = new StringBuilder();
		int count = 0;
		for (Cell<Integer, Integer, Integer> cell : post2Tag.cellSet()) {
			if (count > 0) {
				sb.append(",");
			}
			sb.append("('" + cell.getRowKey() + "', '" + cell.getColumnKey() + "', " + cell.getValue() + ")");
			count++;

		}
		sb.append(";");

		values = sb.toString();

		Connection conn = null;
		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			String query = "insert into post2tag (postId, tagId, position) values " + values;
			st.executeUpdate(query);
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static boolean updateDBwithTagSplitting(Integer tagId, String tag, String stemmedTag, String skipNumber, String skipMinusAndDot, String[] parts) {
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);

			Statement st = conn.createStatement();
			st.executeUpdate("insert into tagsplit (tagId, fulltag, stemmedtag, skipNumber,skipMinusAndDot,  part1, part2, part3, part4, part5) " + "values (" + tagId + ", '" + tag
					+ "', '" + stemmedTag + "', '" + skipNumber + "', '" + skipMinusAndDot + "', '" + parts[0] + "', '" + parts[1] + "', '" + parts[2] + "', '" + parts[3] + "', '"
					+ parts[4] + "')");
			conn.close();
			return true;

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public static boolean deleteFromDB(String tableName) {
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			st.executeUpdate("delete from " + tableName);

			conn.close();
			return true;

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public static void insertSimilarityValues(String tagName1, String tagName2, float jaccardSimilarity, float levenstheinDistance, float nGramSimilarity2, float nGramSimilarity3,
			float nGramSimilarity4) {
		Connection conn = null;
		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			st.executeUpdate("insert into tag_similarity ( tagName1, tagName2, jaccardSim, levenstheinDist,  nGram2, nGram3, nGram4) values ('" + tagName1 + "', '" + tagName2
					+ "', " + jaccardSimilarity + ", " + levenstheinDistance + ", " + nGramSimilarity2 + ", " + nGramSimilarity3 + ", " + nGramSimilarity4 + ")");
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void insertSimilarityValuesAsList(List<String> tagNames1, List<String> tagNames2, List<Float> jaccardSimilarity, List<Float> levenstheinDistance,
			List<Float> nGramSimilarity2, List<Float> nGramSimilarity3, List<Float> nGramSimilarity4) {

		StringBuilder sb = new StringBuilder();
		sb.append("('" + tagNames1.get(0) + "', '" + tagNames2.get(0) + "', " + jaccardSimilarity.get(0) + ", " + levenstheinDistance.get(0) + ", " + nGramSimilarity2.get(0) + ", "
				+ nGramSimilarity3.get(0) + ", " + nGramSimilarity4.get(0) + ")");
		for (int i = 1; i < tagNames1.size(); i++) {

			sb.append(", ");
			sb.append("('" + tagNames1.get(i) + "', '" + tagNames2.get(i) + "', " + jaccardSimilarity.get(i) + ", " + levenstheinDistance.get(i) + ", " + nGramSimilarity2.get(i)
					+ ", " + nGramSimilarity3.get(i) + ", " + nGramSimilarity4.get(i) + ")");

		}
		sb.append(";");
		insertSimilarityList(sb.toString());
	}

	private static void insertSimilarityList(String values) {
		Connection conn = null;
		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			String query = "insert into tag_similarity (tagName1, tagName2, jaccardSim, levenstheinDist,  nGram2, nGram3, nGram4) values " + values;
			st.executeUpdate(query);
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// System.out.println("update");
	}

	public static Set<Tag> getAllTagsOfReferenceSetDisregardingTagTable() {

		Set<Tag> returnList = new HashSet<>();
		List<String> tagNames = new ArrayList<String>();
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select SourceTagName as 'source', TargetTagName as 'target' from tagsynonyms");

			while (tags_db.next()) {

				tagNames.add(tags_db.getString("source"));
				tagNames.add(tags_db.getString("target"));
			}

			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		for (String tagName : tagNames) {

			returnList.add(getTagByName(tagName));

		}

		return returnList;

	}

	public static Map<String, Integer> getAllTagOfTagsAndSynonymTable() {

		Map<String, Integer> returnList = new HashMap<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select SourceTagName as 'source', TargetTagName as 'target' from tagsynonyms");

			while (tags_db.next()) {

				returnList.put(tags_db.getString("source"), -1);
				returnList.put(tags_db.getString("target"), -1);

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		returnList.putAll(getAllTagsAndId());
		System.out.println("size: " + returnList.size());
		return returnList;

	}

	public static void updateDBwithMetaphoneTags(String[] metaphone) {
		Connection conn = null;
		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url + dbName, userName, password);

			Statement st = conn.createStatement();

			st.executeUpdate("insert into tagmetaphone (fulltag, metaphone2, metaphone3, metaphone4, metaphone5, metaphone6, metaphone7) " + "values ('" + metaphone[0] + "', '"
					+ metaphone[1] + "', '" + metaphone[2] + "', '" + metaphone[3] + "', '" + metaphone[4] + "', '" + metaphone[5] + "', '" + metaphone[6] + "')");
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	public static Tag getTagByName(String tagName) {

		Tag tag = new Tag(-1, tagName, -1, -1.0);

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select distinct tags.Id as 'id', tags.TagName as 'tagname', tags.Count as 'count'   from Tags where tagName = '" + tagName + "'");

			if (tags_db.isBeforeFirst()) {
				while (tags_db.next()) {

					tag.setId(tags_db.getInt("id"));
					tag.setCount(tags_db.getInt("count"));

				}
			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tag;

	}

	/**
	 * returns all tagsynonyms in db, disregarding if the tags still exist
	 * 
	 * @param limit_start
	 * @param limit_end
	 * @return
	 */
	public static EList<SynonymCandidate> getAllApprovedSynonymPairs(int limit_start, int limit_end) {

		EList<SynonymCandidate> returnList = new EArrayList<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select * from TagSynonyms  order by SourceTagName asc limit 0, 3000;");

			while (tags_db.next()) {

				String sourceTag = tags_db.getString("SourceTagName");
				String targetTag = tags_db.getString("TargetTagName");
				Integer id = tags_db.getInt("id");
				SynonymCandidate dto = new SynonymCandidate(SynonymCategory.APPROVED, sourceTag, targetTag, true);
				dto.setId(id);
				returnList.add(dto);

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	/**
	 * returns all tagsynonyms in db, disregarding if the tags still exist
	 * 
	 * @param limit_start
	 * @param limit_end
	 * @return
	 */
	public static EList<SynonymCandidate> getAllApprovedSynonymPairsForAndroid(int limit_end) {

		EList<SynonymCandidate> returnList = new EArrayList<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery(
					"select distinct SourceTagName, TargetTagName, TagSynonyms.id from TagSynonyms join androidtags on SourceTagName = tagname or TargetTagName = tagname "
							+ " order by SourceTagName asc, TargetTagName asc limit 0, " + limit_end + ";");

			while (tags_db.next()) {

				String sourceTag = tags_db.getString("SourceTagName");
				String targetTag = tags_db.getString("TargetTagName");
				Integer id = tags_db.getInt("id");
				SynonymCandidate dto = new SynonymCandidate(SynonymCategory.APPROVED, sourceTag, targetTag, true);
				dto.setId(id);
				dto.setStoredRanking(1.0);

				// TODO remove
				// if ( sourceTag.contains("xcode") ||
				// targetTag.contains("xcode") || sourceTag.contains("win") ||
				// targetTag.contains("win") || sourceTag.contains("zend")
				// || targetTag.contains("zend")) {

				returnList.add(dto);
				// }

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	/**
	 * returns all tagsynonyms in db, disregarding if the tags still exist
	 * 
	 * 
	 * @return
	 */
	public static EList<String> getAllDistinctTargetsofApprovedSynonyms() {

		EList<String> returnList = new EArrayList<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select distinct TargetTagName from TagSynonyms asc limit 0, 5000;");

			while (tags_db.next()) {

				String targetTag = tags_db.getString("TargetTagName");

				returnList.add(targetTag);

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	/**
	 * 
	 * param tag für den die kanidaten gefunden werden sollen
	 * 
	 * @return
	 */
	public static EList<Set<String>> getSelectedSynonymCandidatesForTag(String tag) {

		Set<String> sourceList = new HashSet<>();
		Set<String> targetList = new HashSet<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			// String query = "select source, target from synonym_candidates
			// where " + "(source like '" + tag + "' or target like '" + tag +
			// "' ) and ((ranking > 0.4) XOR "
			// + "( (select count(*) from synonym_candidates sc where (sc.source
			// like '" + tag + "' or sc.target like '" + tag
			// + "' ) and sc.ranking >0.4 ) = 0 AND ranking < 0.4)) order by
			// ranking desc, count desc limit 0, 5;";
			// System.out.println(query);

			String query = "select source, target from synonym_candidates where (source  like  '" + tag + "' or target like '" + tag
					+ "' ) and ranking >= 0.5 order by ranking desc, count desc limit 0, 15;";

			ResultSet tags_db = st.executeQuery(query);

			while (tags_db.next()) {

				if (tags_db.getString("target").equals(tag)) {
					sourceList.add(tags_db.getString("source"));
				} else if (tags_db.getString("source").equals(tag)) {
					targetList.add(tags_db.getString("target"));
				}

			}
			conn.close();

			if ((sourceList.size() + targetList.size()) < 1) {
				Class.forName(driver).newInstance();
				conn = DriverManager.getConnection(url + dbName, userName, password);
				st = conn.createStatement();

				query = "select source, target from synonym_candidates where (source  like  '" + tag + "' or target like '" + tag + "' ) and "
						+ "(select count(*)  from synonym_candidates sc where (sc.source  like  '" + tag + "' or sc.target like '" + tag + "' )  "
						+ " and sc.ranking >=0.5 ) = 0  and  (		ranking < 0.5  and 0.35 <= ranking ) order by ranking desc, count desc ";
				// System.out.println(query);

				tags_db = st.executeQuery(query);

				while (tags_db.next()) {

					if (tags_db.getString("target").equals(tag)) {
						sourceList.add(tags_db.getString("source"));
					} else if (tags_db.getString("source").equals(tag)) {
						targetList.add(tags_db.getString("target"));
					}

				}
				conn.close();
			}

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		EList<Set<String>> returnList = new EArrayList<>();
		returnList.add(sourceList);
		returnList.add(targetList);
		return returnList;

	}

	/**
	 * 
	 * param tag für den die kanidaten gefunden werden sollen
	 * 
	 * @return
	 */
	public static Map<Integer, SynonymCandidate> getAllSynonymCandidates() {
		Map<Integer, SynonymCandidate> returnMap = new HashMap<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			// String query = "select source, target from synonym_candidates
			// where " + "(source like '" + tag + "' or target like '" + tag +
			// "' ) and ((ranking > 0.4) XOR "
			// + "( (select count(*) from synonym_candidates sc where (sc.source
			// like '" + tag + "' or sc.target like '" + tag
			// + "' ) and sc.ranking >0.4 ) = 0 AND ranking < 0.4)) order by
			// ranking desc, count desc limit 0, 5;";
			// System.out.println(query);

			String query = "select source, target,  category, id from synonym_candidates where ranking >= 0.5 order by target asc limit 0, 500000;";

			ResultSet tags_db = st.executeQuery(query);

			while (tags_db.next()) {

				String sourceTag = tags_db.getString("source");
				String targetTag = tags_db.getString("target");
				String category = tags_db.getString("category");
				Integer id = tags_db.getInt("id");
				SynonymCategory cat = SynonymCategory.getCategoryByName(category);
				SynonymCandidate dto = new SynonymCandidate(cat, sourceTag, targetTag, true);
				dto.setId(id);

				returnMap.put(id, dto);
			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return returnMap;

	}

	/**
	 * 
	 * param tag für den die kanidaten gefunden werden sollen
	 * 
	 * @return
	 */
	public static EList<SynonymCandidate> getSynonymCandidatesAndroidByMinimumRanking(double limit) {
		EList<SynonymCandidate> returnList = new EArrayList<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			String query = "select source, target,  category, synonym_candidates.id as 'iid' , ranking  from synonym_candidates where ranking >=  " + limit
					+ " and (  source in (select tagname from tagnodes)  and target in (select tagname from tagnodes)) order by target asc limit 0, 500000;";

			ResultSet tags_db = st.executeQuery(query);

			while (tags_db.next()) {

				String sourceTag = tags_db.getString("source");
				String targetTag = tags_db.getString("target");
				String category = tags_db.getString("category");
				Integer id = tags_db.getInt("iid");
				Double ranking = tags_db.getDouble("ranking");
				SynonymCategory cat = SynonymCategory.getCategoryByName(category);
				SynonymCandidate dto = new SynonymCandidate(cat, sourceTag, targetTag, true);
				dto.setStoredRanking(ranking);
				dto.setId(id);

				returnList.add(dto);
			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return returnList;

	}

	public static Map<Integer, SynonymCandidate> getAllSynonymCandidatesByTarget(String target) {
		Map<Integer, SynonymCandidate> returnMap = new HashMap<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			String query = "select source, target,  category, id from synonym_candidates where ranking >= 0.5  and target = '" + target + "' order by target asc ;";

			ResultSet tags_db = st.executeQuery(query);

			while (tags_db.next()) {

				String sourceTag = tags_db.getString("source");
				String targetTag = tags_db.getString("target");
				String category = tags_db.getString("category");
				Integer id = tags_db.getInt("id");
				SynonymCategory cat = SynonymCategory.getCategoryByName(category);
				SynonymCandidate dto = new SynonymCandidate(cat, sourceTag, targetTag, true);
				dto.setId(id);

				returnMap.put(id, dto);
			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return returnMap;

	}

	/**
	 * 
	 * param tag für den die kanidaten gefunden werden sollen
	 * 
	 * @return
	 */
	public static EList<Set<SynonymCandidate>> getSelectedSynonymCandidatesAsInfoDtosForTag(String tag) {

		Set<SynonymCandidate> sourceList = new HashSet<>();
		Set<SynonymCandidate> targetList = new HashSet<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			// String query = "select source, target from synonym_candidates
			// where " + "(source like '" + tag + "' or target like '" + tag +
			// "' ) and ((ranking > 0.4) XOR "
			// + "( (select count(*) from synonym_candidates sc where (sc.source
			// like '" + tag + "' or sc.target like '" + tag
			// + "' ) and sc.ranking >0.4 ) = 0 AND ranking < 0.4)) order by
			// ranking desc, count desc limit 0, 5;";
			// System.out.println(query);

			String query = "select source, target,  category from synonym_candidates where (source  like  '" + tag + "' or target like '" + tag
					+ "' ) and ranking >= 0.5 order by ranking desc, count desc limit 0, 15;";

			ResultSet tags_db = st.executeQuery(query);

			while (tags_db.next()) {

				String sourceTag = tags_db.getString("source");
				String targetTag = tags_db.getString("target");
				String category = tags_db.getString("category");
				SynonymCategory cat = SynonymCategory.getCategoryByName(category);
				SynonymCandidate dto = new SynonymCandidate(cat, sourceTag, targetTag, true);

				if (tags_db.getString("target").equals(tag)) {
					sourceList.add(dto);
				} else if (tags_db.getString("source").equals(tag)) {
					targetList.add(dto);
				}

			}
			conn.close();

			if ((sourceList.size() + targetList.size()) < 1) {
				Class.forName(driver).newInstance();
				conn = DriverManager.getConnection(url + dbName, userName, password);
				st = conn.createStatement();

				query = "select source, target, category from synonym_candidates where (source  like  '" + tag + "' or target like '" + tag + "' ) and "
						+ "(select count(*)  from synonym_candidates sc where (sc.source  like  '" + tag + "' or sc.target like '" + tag + "' )  "
						+ " and sc.ranking >=0.5 ) = 0  and  (		ranking < 0.5  and 0.35 <= ranking ) order by ranking desc, count desc ";
				// System.out.println(query);

				tags_db = st.executeQuery(query);

				while (tags_db.next()) {

					String sourceTag = tags_db.getString("source");
					String targetTag = tags_db.getString("target");
					String category = tags_db.getString("category");
					SynonymCategory cat = SynonymCategory.getCategoryByName(category);
					SynonymCandidate dto = new SynonymCandidate(cat, sourceTag, targetTag, true);

					if (tags_db.getString("target").equals(tag)) {
						sourceList.add(dto);
					} else if (tags_db.getString("source").equals(tag)) {
						targetList.add(dto);
					}

				}
				conn.close();
			}

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		EList<Set<SynonymCandidate>> returnList = new EArrayList<>();
		returnList.add(sourceList);
		returnList.add(targetList);
		return returnList;

	}

	/**
	 * 
	 * param tag für den die kanidaten gefunden werden sollen
	 * 
	 * @return
	 */
	public static Set<SynonymCandidate> getSourceTagsAsInfoDtosForTag(String tag) {

		Set<SynonymCandidate> sourceList = new HashSet<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			// String query = "select source, target from synonym_candidates
			// where " + "(source like '" + tag + "' or target like '" + tag +
			// "' ) and ((ranking > 0.4) XOR "
			// + "( (select count(*) from synonym_candidates sc where (sc.source
			// like '" + tag + "' or sc.target like '" + tag
			// + "' ) and sc.ranking >0.4 ) = 0 AND ranking < 0.4)) order by
			// ranking desc, count desc limit 0, 5;";
			// System.out.println(query);

			String query = "select source, target,  category from synonym_candidates where (source  =  '" + tag
					+ "' ) and ranking >= 0.5 order by ranking desc, count desc limit 0, 15;";

			ResultSet tags_db = st.executeQuery(query);

			while (tags_db.next()) {

				String sourceTag = tags_db.getString("source");
				String targetTag = tags_db.getString("target");
				String category = tags_db.getString("category");
				SynonymCategory cat = SynonymCategory.getCategoryByName(category);
				SynonymCandidate dto = new SynonymCandidate(cat, sourceTag, targetTag, true);

				if (tags_db.getString("target").equals(tag)) {
					sourceList.add(dto);
				}

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return sourceList;

	}

	public static SynonymCandidate getSynonymCandidateBySourceAndTag(String source, String target) {

		SynonymCandidate sc = null;
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			// String query = "select source, target from synonym_candidates
			// where " + "(source like '" + tag + "' or target like '" + tag +
			// "' ) and ((ranking > 0.4) XOR "
			// + "( (select count(*) from synonym_candidates sc where (sc.source
			// like '" + tag + "' or sc.target like '" + tag
			// + "' ) and sc.ranking >0.4 ) = 0 AND ranking < 0.4)) order by
			// ranking desc, count desc limit 0, 5;";
			// System.out.println(query);

			// StringBuilder sb = new StringBuilder();
			// for (String target : targetList) {
			// sb.append(", '" + target + "'");
			// }

			// String targets = sb.substring(1);

			String query = "select source, target,  category, ranking from synonym_candidates where ranking >= 0.75 and (source  =  '" + source + "' ) and (target =  '" + target
					+ "') limit 0, 15;";

			ResultSet tags_db = st.executeQuery(query);

			while (tags_db.next()) {
				String sourceTag = tags_db.getString("source");
				String targetTag = tags_db.getString("target");
				String category = tags_db.getString("category");
				SynonymCategory cat = SynonymCategory.getCategoryByName(category);
				sc = new SynonymCandidate(cat, sourceTag, targetTag, true);

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return sc;

	}

	public static EList<TrendDto> getTrendsForGroupedSynonyms(Set<String> tags) {

		EList<String> taglist = new EArrayList<>();
		taglist.addAll(tags);

		EList<TrendDto> returnList = new EArrayList<>();

		StringBuilder where = new StringBuilder();

		for (int i = 0; i < taglist.size(); i++) {
			if (i > 0) {
				where.append(" or ");
			}
			where.append("tags.TagName = '" + taglist.get(i) + "' ");

		}

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery(
					"select YEAR(creationDate) AS 'year', MONTH(creationDate) AS 'month' ,count(*) as 'postCount', sum(AnswerCount) as 'answerCountSum',avg(AnswerCount) as 'answerCountAvg', sum(CommentCount)  as 'commentCountSum', avg(CommentCount)  as 'commentCountAvg', count(acceptedAnswerId)  as 'acceptedAnswersSum',"
							+ " sum(FavoriteCount) as 'favoriteCountSum', avg(FavoriteCount) as 'favoriteCountAvg', sum(score) as 'scoreCountSum', sum(case when score > 0 then score else 0 end ) as 'scoreCountPos', sum(case when score < 0 then score else 0 end )  as 'scoreCountNeg',  sum(viewCount) as 'viewCountSum',avg(viewCount) as 'viewCountAvg' from all_posts p join post2tag p2t on p.Id = p2t.postId join tags on p2t.tagId = tags.Id"
							+ " where " + where.toString() + "	group by date(creationdate) limit 0,50000000;");

			while (tags_db.next()) {

				TrendDto dto = new TrendDto();
				Calendar c = Calendar.getInstance();
				c.set(tags_db.getInt("year"), tags_db.getInt("month"), 01);

				dto.setDate(c.getTime());
				dto.setPostCount(tags_db.getInt("postCount"));
				dto.setAnswerCountSum(tags_db.getInt("answerCountSum"));
				dto.setAnswerCountAvg(tags_db.getInt("answerCountAvg"));
				dto.setCommentCountSum(tags_db.getInt("commentCountSum"));
				dto.setCommentCountAvg(tags_db.getInt("commentCountAvg"));
				dto.setAcceptedAnswerCountSum(tags_db.getInt("acceptedAnswersSum"));
				// dto.setAcceptedAnswerCountAvg(tags_db.getInt("acceptedAnswers"));
				dto.setOpenAnswerCountSum(dto.getPostCount() - dto.getAcceptedAnswerCountSum());
				dto.setFavouriteCountSum(tags_db.getInt("favoriteCountSum"));
				dto.setFavouriteCountAvg(tags_db.getInt("favoriteCountAvg"));
				dto.setScoreCountSum(tags_db.getInt("scoreCountSum"));
				dto.setScoreCountPos(tags_db.getInt("scoreCountPos"));
				dto.setScoreCountNeg(tags_db.getInt("scoreCountNeg"));
				dto.setViewCountSum(tags_db.getInt("viewCountSum"));
				dto.setViewCountAvg(tags_db.getInt("viewCountAvg"));

				returnList.add(dto);
			}
			conn.close();

			// TODO add notansweredcount

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return returnList;

	}

	public static EList<TrendDto> getTrendsForGroupedSynonyms(String synonym) {

		EList<TrendDto> returnList = new EArrayList<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery(
					"select  DATE_FORMAT(creationDate, '01-%m-%Y') as 'dd' ,count(*) as 'postCount', sum(AnswerCount) as 'answerCountSum', avg(AnswerCount) as 'answerCountAvg',  sum(CommentCount)  as 'commentCountSum', avg(CommentCount)  as 'commentCountAvg', count(acceptedAnswerId)  as 'acceptedAnswersSum',"
							+ " sum(FavoriteCount) as 'favoriteCountSum',avg(FavoriteCount) as 'favoriteCountAvg',   sum(score) as 'scoreCountSum', sum(case when score > 0 then score else 0 end ) as 'scoreCountPos', sum(case when score < 0 then score else 0 end )  as 'scoreCountNeg', sum(viewCount) as 'viewCountSum',avg(viewCount) as 'viewCountAvg'  "
							+ "from all_posts p join post2synonyms p2t on p.Id = p2t.postId " // join
			// tags
			// on
			// p2t.synonym =
			// tags.tagname"
							+ " where synonym = '" + synonym + "'	group by dd order by DATE_FORMAT(creationDate, '%Y-%m') limit 0,50000000;");

			while (tags_db.next()) {

				TrendDto dto = new TrendDto();
				SimpleDateFormat mydat = new SimpleDateFormat("dd-MM-yyyy");
				Date parse;
				try {
					parse = mydat.parse(tags_db.getString("dd"));
					dto.setDate(parse);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				dto.setPostCount(tags_db.getInt("postCount"));
				dto.setAnswerCountSum(tags_db.getInt("answerCountSum"));
				dto.setAnswerCountAvg(tags_db.getInt("answerCountAvg"));
				dto.setCommentCountSum(tags_db.getInt("commentCountSum"));
				dto.setCommentCountAvg(tags_db.getInt("commentCountAvg"));
				dto.setAcceptedAnswerCountSum(tags_db.getInt("acceptedAnswersSum"));
				// dto.setAcceptedAnswerCountAvg(tags_db.getInt("acceptedAnswers"));
				dto.setOpenAnswerCountSum(dto.getPostCount() - dto.getAcceptedAnswerCountSum());
				dto.setFavouriteCountSum(tags_db.getInt("favoriteCountSum"));
				dto.setFavouriteCountAvg(tags_db.getInt("favoriteCountAvg"));
				dto.setScoreCountSum(tags_db.getInt("scoreCountSum"));
				dto.setScoreCountPos(tags_db.getInt("scoreCountPos"));
				dto.setScoreCountNeg(tags_db.getInt("scoreCountNeg"));
				dto.setViewCountSum(tags_db.getInt("viewCountSum"));
				dto.setViewCountAvg(tags_db.getInt("viewCountAvg"));

				returnList.add(dto);
			}
			conn.close();

			// TODO add notansweredcount

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return returnList;

	}

	public static EList<TrendDto> getTrendsForTags(String tag) {

		EList<TrendDto> returnList = new EArrayList<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery(
					"select  DATE_FORMAT(creationDate, '01-%m-%Y') as 'dd' ,count(*) as 'postCount', sum(AnswerCount) as 'answerCountSum', avg(AnswerCount) as 'answerCountAvg',  sum(CommentCount)  as 'commentCountSum', avg(CommentCount)  as 'commentCountAvg', count(acceptedAnswerId)  as 'acceptedAnswersSum',"
							+ " sum(FavoriteCount) as 'favoriteCountSum',avg(FavoriteCount) as 'favoriteCountAvg',   sum(score) as 'scoreCountSum', sum(case when score > 0 then score else 0 end ) as 'scoreCountPos', sum(case when score < 0 then score else 0 end )  as 'scoreCountNeg', sum(viewCount) as 'viewCountSum',avg(viewCount) as 'viewCountAvg'  "
							+ "from all_posts p join post2tag p2t on p.Id = p2t.postId join tags on p2t.tagId = tags.Id" + " where tagname = '" + tag
							+ "'	group by dd order by DATE_FORMAT(creationDate, '%Y-%m') limit 0,50000000;");

			while (tags_db.next()) {

				TrendDto dto = new TrendDto();
				SimpleDateFormat mydat = new SimpleDateFormat("dd-MM-yyyy");
				Date parse;
				try {
					parse = mydat.parse(tags_db.getString("dd"));
					dto.setDate(parse);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				dto.setPostCount(tags_db.getInt("postCount"));
				dto.setAnswerCountSum(tags_db.getInt("answerCountSum"));
				dto.setAnswerCountAvg(tags_db.getInt("answerCountAvg"));
				dto.setCommentCountSum(tags_db.getInt("commentCountSum"));
				dto.setCommentCountAvg(tags_db.getInt("commentCountAvg"));
				dto.setAcceptedAnswerCountSum(tags_db.getInt("acceptedAnswersSum"));
				// dto.setAcceptedAnswerCountAvg(tags_db.getInt("acceptedAnswers"));
				dto.setOpenAnswerCountSum(dto.getPostCount() - dto.getAcceptedAnswerCountSum());
				dto.setFavouriteCountSum(tags_db.getInt("favoriteCountSum"));
				dto.setFavouriteCountAvg(tags_db.getInt("favoriteCountAvg"));
				dto.setScoreCountSum(tags_db.getInt("scoreCountSum"));
				dto.setScoreCountPos(tags_db.getInt("scoreCountPos"));
				dto.setScoreCountNeg(tags_db.getInt("scoreCountNeg"));
				dto.setViewCountSum(tags_db.getInt("viewCountSum"));
				dto.setViewCountAvg(tags_db.getInt("viewCountAvg"));

				returnList.add(dto);
			}
			conn.close();

			// TODO add notansweredcount

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return returnList;

	}

	public static EList<TrendDto> getImportanceOfTopic(Set<String> tags) {

		EList<String> taglist = new EArrayList<>();
		taglist.addAll(tags);

		EList<TrendDto> returnList = new EArrayList<>();

		StringBuilder where = new StringBuilder();

		for (int i = 0; i < taglist.size(); i++) {
			if (i > 0) {
				where.append(" or ");
			}
			where.append("tags.TagName = '" + taglist.get(i) + "' ");

		}

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery(
					"select count(*) as 'postCount', sum(AnswerCount) as 'answerCountSum', avg(AnswerCount) as 'answerCountAvg', sum(CommentCount)  as 'commentCountSum', avg(CommentCount)  as 'commentCountAvg', count(acceptedAnswerId)  as 'acceptedAnswersSum',"
							+ " sum(FavoriteCount) as 'favoriteCount', avg(FavoriteCount) as 'favoriteCountAvg', sum(score) as 'scoreCountSum', sum(case when score > 0 then score else 0 end ) as 'scoreCountPos', sum(case when score < 0 then score else 0 end )  as 'scoreCountNeg',  sum(viewCount) as 'viewCountSum',avg(viewCount) as 'viewCountAvg'"
							+ " from all_posts p join post2tag p2t on p.Id = p2t.postId join tags on p2t.tagId = tags.Id" + " where " + where.toString()
							+ "	 limit 0,50000000;");

			while (tags_db.next()) {

				TrendDto dto = new TrendDto();

				dto.setPostCount(tags_db.getInt("postCount"));
				dto.setAnswerCountSum(tags_db.getInt("answerCountSum"));
				dto.setAnswerCountAvg(tags_db.getInt("answerCountAvg"));
				dto.setCommentCountSum(tags_db.getInt("commentCountSum"));
				dto.setCommentCountAvg(tags_db.getInt("commentCountAvg"));
				dto.setAcceptedAnswerCountSum(tags_db.getInt("acceptedAnswersSum"));
				// dto.setAcceptedAnswerCountAvg(tags_db.getInt("acceptedAnswers"));
				dto.setOpenAnswerCountSum(dto.getPostCount() - dto.getAcceptedAnswerCountSum());
				dto.setFavouriteCountSum(tags_db.getInt("favoriteCountSum"));
				dto.setFavouriteCountAvg(tags_db.getInt("favoriteCountAvg"));
				dto.setScoreCountSum(tags_db.getInt("scoreCountSum"));
				dto.setScoreCountPos(tags_db.getInt("scoreCountPos"));
				dto.setScoreCountNeg(tags_db.getInt("scoreCountNeg"));
				dto.setViewCountSum(tags_db.getInt("viewCountSum"));
				dto.setViewCountAvg(tags_db.getInt("viewCountAvg"));

				returnList.add(dto);
			}
			conn.close();

			// TODO add notansweredcount

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return returnList;

	}

	public static Set<Integer> getChainsForTopCategories() {

		Set<Integer> returnMap = new HashSet<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			String query = "select t1.id as 'id1',t2.id as 'id2' from synonym_candidates t1 join synonym_candidates t2  on t1.target = t2.source " + "where t1.Id != t2.Id and"
					+ "( t1.category = 'STEM' or t1.category = 'VERSION' or t1.category = 'DOT' or t1.category = 'MINUS' or t1.category = 'SHARP' or t1.category = 'PLUS' )"
					+ "			and (t2.category = 'STEM' or t2.category = 'VERSION' or t2.category = 'DOT' or t2.category = 'MINUS' or t2.category = 'SHARP' or t2.category = 'PLUS' ) limit 0, 15000";

			// System.out.println(query);

			// String query = "select id from synonym_candidates t1 "
			// + " where ( t1.category = 'STEM' or t1.category = 'VERSION' or
			// t1.category = 'DOT' or t1.category = 'MINUS' or t1.category =
			// 'SHARP' or t1.category = 'PLUS' );";

			ResultSet tags_db = st.executeQuery(query);

			while (tags_db.next()) {

				Integer id1 = tags_db.getInt("id1");
				Integer id2 = tags_db.getInt("id2");

				returnMap.add(id1);
				returnMap.add(id2);
			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		// try {
		// Class.forName(driver).newInstance();
		// Connection conn = DriverManager.getConnection(url + dbName, userName,
		// password);
		// Statement st = conn.createStatement();
		//
		// // String query = "select t1.id as 'id1',t2.id as 'id2' from
		// synonym_candidates t1 join synonym_candidates t2 on t1.target =
		// t2.source "
		// // + "where t1.Id != t2.Id and"
		// // + "( t1.category = 'STEM' or t1.category = 'VERSION' or
		// t1.category = 'DOT' or t1.category = 'MINUS' or t1.category = 'SHARP'
		// or t1.category = 'PLUS' )"
		// // +
		// //
		// " and (t2.category = 'STEM' or t2.category = 'VERSION' or t2.category
		// = 'DOT' or t2.category = 'MINUS' or t2.category = 'SHARP' or
		// t2.category = 'PLUS' ) limit 0, 15000";
		//
		// // System.out.println(query);
		//
		// String query = "select id from synonym_candidates t1 "
		// + " where ( t1.category = 'STEM' or t1.category = 'VERSION' or
		// t1.category = 'DOT' or t1.category = 'MINUS' or t1.category = 'SHARP'
		// or t1.category = 'PLUS' );";
		//
		// ResultSet tags_db = st.executeQuery(query);
		//
		// while (tags_db.next()) {
		//
		// Integer id1 = tags_db.getInt("id");
		//
		// returnMap.add(id1);
		// }
		// conn.close();
		//
		// } catch (InstantiationException | IllegalAccessException |
		// ClassNotFoundException e) {
		// e.printStackTrace();
		// } catch (SQLException e) {
		// e.printStackTrace();
		// }

		return returnMap;

	}

	public static EList<SynonymCandidate> getSynonymCandidatesByMinimumRanking(double rankingLimit) {

		EList<SynonymCandidate> returnList = new EArrayList<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			String query = "select * from synonym_candidates t1 where  t1.ranking >= " + rankingLimit + "order by t1.ranking desc, source asc limit 0, 50000";

			// System.out.println(query);

			ResultSet tags_db = st.executeQuery(query);

			while (tags_db.next()) {

				Integer dtoId = tags_db.getInt("id");
				String source = tags_db.getString("source");
				String target = tags_db.getString("target");
				String cat = tags_db.getString("category");
				Integer count = tags_db.getInt("count");
				Double ranking = tags_db.getDouble("ranking");

				SynonymCategory categoryByName = SynonymCategory.valueOf(cat);

				SynonymCandidate sc = new SynonymCandidate(categoryByName, source, target, true);
				sc.setCount(count);
				sc.setId(dtoId);
				sc.setStoredRanking(ranking);

				returnList.add(sc);

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return returnList;

	}

	public static Set<Integer> getTopRankingSynonymCandidates() {

		Set<Integer> returnMap = new HashSet<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			String query = "select t1.id as 'id1' from synonym_candidates t1 "
					+ "where ( t1.category != 'STEM' and t1.category != 'VERSION' and t1.category != 'DOT' and t1.category != 'MINUS' and t1.category != 'SHARP' and t1.category != 'PLUS' )"
					+ "	 and ranking >= 0.65 limit 0, 50000";

			// System.out.println(query);

			// String query = "select id from synonym_candidates t1 "
			// + " where ( t1.category = 'STEM' or t1.category = 'VERSION' or
			// t1.category = 'DOT' or t1.category = 'MINUS' or t1.category =
			// 'SHARP' or t1.category = 'PLUS' );";

			ResultSet tags_db = st.executeQuery(query);

			while (tags_db.next()) {

				Integer id1 = tags_db.getInt("id1");

				returnMap.add(id1);
			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return returnMap;
	}

	public static Set<Integer> getTopRankingSynonymCandidatesAndroid() {

		Set<Integer> returnMap = new HashSet<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			String query = "select t1.id as 'id1' from synonym_candidates t1 join androidtags on tagname = t1.source or tagname = t1.target "
					+ "where ( t1.category != 'STEM' and t1.category != 'VERSION' and t1.category != 'DOT' and t1.category != 'MINUS' and t1.category != 'SHARP' and t1.category != 'PLUS' )"
					// delphi test
					// + " and t1.source like '%delphi%' and t1.target like
					// '%delphi%' "
					+ "                             	 and ranking >= 0.65 limit 0, 50000";

			// System.out.println(query);

			// String query = "select id from synonym_candidates t1 "
			// + " where ( t1.category = 'STEM' or t1.category = 'VERSION' or
			// t1.category = 'DOT' or t1.category = 'MINUS' or t1.category =
			// 'SHARP' or t1.category = 'PLUS' );";

			ResultSet tags_db = st.executeQuery(query);

			while (tags_db.next()) {

				Integer id1 = tags_db.getInt("id1");

				returnMap.add(id1);
			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return returnMap;
	}

	public static SynonymCandidate getInfoDtoForSourceTarget(String source, String target, double limit) {

		SynonymCandidate sc = null;
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			String query = "select  id, source, target, ranking, category from synonym_candidates where source = '" + source + "' and target = '" + target + "'and  ranking >= "
					+ limit + " order by ranking desc limit 0, 50000";

			ResultSet tags_db = st.executeQuery(query);

			while (tags_db.next()) {

				String sourceTag = tags_db.getString("source");
				String targetTag = tags_db.getString("target");
				String category = tags_db.getString("category");
				SynonymCategory cat = SynonymCategory.getCategoryByName(category);
				sc = new SynonymCandidate(cat, sourceTag, targetTag, true);
				sc.setId(tags_db.getInt("id"));
				sc.setStoredRanking(tags_db.getDouble("ranking"));

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return sc;
	}

	public static Set<String> getAllTagsForAndroid() {

		Set<String> returnList = new HashSet<>();
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			String query = "select tagname from tagnodes ";

			// System.out.println(query);

			// String query = "select id from synonym_candidates t1 "
			// + " where ( t1.category = 'STEM' or t1.category = 'VERSION' or
			// t1.category = 'DOT' or t1.category = 'MINUS' or t1.category =
			// 'SHARP' or t1.category = 'PLUS' );";

			ResultSet tags_db = st.executeQuery(query);

			while (tags_db.next()) {

				returnList.add(tags_db.getString("tagname"));

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return returnList;
	}

	public static Set<SynonymCandidate> getSynonymCandidatesForUnusedTags(Set<String> unusedTags) {
		Set<SynonymCandidate> returnSet = new HashSet<SynonymCandidate>();
		// int count = 0;
		for (String unusedTagName : unusedTags) {
			// count++;
			returnSet.addAll(getSynonymCandidateBySourceOrTargetName(unusedTagName));
			// if (count == 500) {
			// return returnSet;
			// }
		}

		return returnSet;
	}

	public static Set<SynonymCandidate> getSynonymCandidateBySourceOrTargetName(String tagname) {
		Set<SynonymCandidate> dtoSet = new HashSet<>();
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			String query = "select synonym_candidates.* from synonym_candidates  where( source = '" + tagname + "' or target = '" + tagname
					+ "' ) and ranking > 0.45 order by ranking desc limit 0,5";

			// System.out.println(query);

			// String query = "select id from synonym_candidates t1 "
			// + " where ( t1.category = 'STEM' or t1.category = 'VERSION' or
			// t1.category = 'DOT' or t1.category = 'MINUS' or t1.category =
			// 'SHARP' or t1.category = 'PLUS' );";

			ResultSet tags_db = st.executeQuery(query);

			while (tags_db.next()) {

				Integer dtoId = tags_db.getInt("id");
				String source = tags_db.getString("source");
				String target = tags_db.getString("target");
				String cat = tags_db.getString("category");
				Integer count = tags_db.getInt("count");
				Double ranking = tags_db.getDouble("ranking");

				SynonymCandidate dto = new SynonymCandidate(SynonymCategory.getCategoryByName(cat), source, target, true);
				dto.setCount(count);
				dto.setId(dtoId);
				dto.setStoredRanking(ranking);
				dtoSet.add(dto);

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (dtoSet.size() < 1) {
			try {
				Class.forName(driver).newInstance();
				Connection conn = DriverManager.getConnection(url + dbName, userName, password);
				Statement st = conn.createStatement();

				String query = "select synonym_candidates.id as 'scid', synonym_candidates.* from synonym_candidates where source =  '" + tagname + "' or target = '" + tagname
						+ "' order by ranking desc limit 0,1";

				// System.out.println(query);

				// String query = "select id from synonym_candidates t1 "
				// + " where ( t1.category = 'STEM' or t1.category = 'VERSION'
				// or t1.category = 'DOT' or t1.category = 'MINUS' or
				// t1.category = 'SHARP' or t1.category = 'PLUS' );";

				ResultSet tags_db = st.executeQuery(query);

				while (tags_db.next()) {

					Integer dtoId = tags_db.getInt("scid");
					String source = tags_db.getString("source");
					String target = tags_db.getString("target");
					String cat = tags_db.getString("category");
					Integer count = tags_db.getInt("count");
					Double ranking = tags_db.getDouble("ranking");

					// SynonymCandidate dto = new
					// SynonymCandidate(SynonymCategory.getCategoryByName(cat),
					// target, source, true);
					SynonymCandidate dto = new SynonymCandidate(SynonymCategory.getCategoryByName(cat), source, target, true);
					dto.setCount(count);
					dto.setId(dtoId);
					dto.setStoredRanking(ranking);
					dtoSet.add(dto);

				}
				conn.close();

			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return dtoSet;
	}
}
