package helper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Post2TagsInARow {

	private Map<Integer, String> post2Tag;

	public static void main(String args[]) {
		Post2TagsInARow p = new Post2TagsInARow();

		p.startPost2TagInARow();
		// DBHandler.getAllTagsOfReferenceSetDisregardingTagTable();
		System.out.println("Done.");

	}

	public void startPost2TagInARow() {

		deleteFromDBwithPost2TagInARow();
		System.out.println("post2tagsInARow deleted");
		// postId and tagId

		// TODO performance - mehr inserts und evtl threads?
		post2Tag = new HashMap<>();
		// android
		Map<Integer, String> postIdAndTags = HelperDBHandler.getPostIdAndTags();
		System.out.println("post2tagsInARow start insert");
		for (Integer postId : postIdAndTags.keySet()) {

			if (post2Tag.size() > 1000) {
				updateDBwithPost2TagRelationTable(post2Tag);
				post2Tag = new HashMap<>();
			}

			String tags = postIdAndTags.get(postId);

			if (tags.contains("><")) {

				String[] tagSplit = tags.split("><");

				tags = tags.replace("><", "', '");

				for (int i = tagSplit.length; i < 5; i++) {
					tags = tags + ", null";
				}

			} else {
				tags = tags + ", null, null, null, null";
			}
			tags = tags.replace("<", "'");
			tags = tags.replace(">", "'");

			post2Tag.put(postId, tags);

		}
		updateDBwithPost2TagRelationTable(post2Tag);
		post2Tag = new HashMap<>();

		System.out.println("post2tagsInARow finished");
	}

	public boolean deleteFromDBwithPost2TagInARow() {

		String url = "jdbc:mysql://localhost:3306/";
		String dbName = "SO_Nov2014";
		// private static String dbName = "AndroidOnly";
		String driver = "com.mysql.jdbc.Driver";
		String userName = "root";
		String password = "";
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			st.executeUpdate("delete from post2tagInARow");

			conn.close();
			return true;

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public void updateDBwithPost2TagRelationTable(Map<Integer, String> post2tag) {
		String url = "jdbc:mysql://localhost:3306/";
		String dbName = "SO_Nov2014";
		// private static String dbName = "AndroidOnly";
		String driver = "com.mysql.jdbc.Driver";
		String userName = "root";
		String password = "";

		String values;

		StringBuilder sb = new StringBuilder();
		int count = 0;
		for (Entry<Integer, String> entry : post2tag.entrySet()) {
			if (count > 0) {
				sb.append(",");
			}
			sb.append("(" + entry.getKey() + ", " + entry.getValue() + ")");
			count++;

		}
		sb.append(";");

		values = sb.toString();

		Connection conn = null;
		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			String query = "insert into post2tagInARow (postId, tag1, tag2, tag3, tag4, tag5) values " + values;
			st.executeUpdate(query);
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	//
	// int POOL_SIZE = 1000;
	// ExecutorService executor = Executors.newFixedThreadPool(POOL_SIZE);
	// int TOTAL_TAG_COUNT = postIdAndTags.size();
	//
	// List<Runnable> threadList = new ArrayList<Runnable>();
	//
	// List<String> allTagNames = new ArrayList<String>();
	// allTagNames.addAll(tags.keySet());
	// // Collections.sort(allTagNames);
	//
	// for (int i = 0; i < TOTAL_TAG_COUNT; i += POOL_SIZE) {
	// List<String> pooltags = new ArrayList<>();
	// int subset = i;
	// for (int j = i; j < i + POOL_SIZE && j < TOTAL_TAG_COUNT; j++) {
	//
	// pooltags.add(allTagNames.get(j));
	// }
	// Runnable worker = new TagDataMain(pooltags, tags, subset);
	// threadList.add(worker);
	// // System.out.println("worker");
	// }
	//
	// for (Runnable r : threadList) {
	// executor.execute(r);
	// }
	//
	// executor.shutdown();
	// while (!executor.isTerminated()) {
	// // System.out.println("Waiting for Threadpool to finish");
	// }
	// System.out.println("Finished all threads");

}
