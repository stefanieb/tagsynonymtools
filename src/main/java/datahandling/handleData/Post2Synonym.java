package datahandling.handleData;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import tagsynonymprediction.utility.EArrayList;
import tagsynonymprediction.utility.EList;

public class Post2Synonym {
	private String url = "jdbc:mysql://localhost:3306/";
	private String dbName = "SO_Nov2014";
	// private static String dbName = "AndroidOnly";
	private String driver = "com.mysql.jdbc.Driver";
	private String userName = "root";
	private String password = "";

	private int count;
	private List<String> valuesList;

	public static void main(String[] args) {
		Post2Synonym p2s = new Post2Synonym();
		p2s.startPost2Synonym();
	}

	public void startPost2Synonym() {
		deletePost2Synonyms();
		findSynonymsToTags();
	}

	public void findSynonymsToTags() {
		count = 0;
		valuesList = new ArrayList<>();

		for (StackOverflowPost post : getPosts()) {

			Integer postId = post.getId();
			String[] tags = post.getTags();
			// pos position

			Set<String> tagsForPost = new HashSet<>();

			for (int pos = 0; pos < tags.length; pos++) {
				Map<Integer, String> tagsPerPosition = new HashMap<>();

				String tag = tags[pos];
				// android-tag
				if (!tag.equals("android")) {

					Map<Integer, String> synonymsForTag = getSynonymsForTag(tag);

					// neu

					if (synonymsForTag.size() == 0) {
						tagsForPost.add(tag);
						tagsPerPosition.put(getIdforTag(tag), tag);
					} else {
						tagsForPost.addAll(synonymsForTag.values());
						int negId = getIdforTag(tag);
						for (String value : synonymsForTag.values()) {
							tagsPerPosition.put(negId, value);
						}

						// noch mal checken!
						// for (Entry<Integer, String> synonymEntry : synonymsForTag.entrySet()) {
						// Map<Integer, String> synonymsForTag2 = getSynonymsForTag(synonymEntry.getValue());
						//
						// if (synonymsForTag2.size() == 0) {
						// tagsForPost.add(synonymEntry.getValue());
						// tagsPerPosition.put(synonymEntry.getKey(), synonymEntry.getValue());
						// } else {
						// tagsForPost.addAll(synonymsForTag2.values());
						// tagsPerPosition.putAll(synonymsForTag2);
						// }
						//
						// }

					}
					if (count == 5000) {
						addToSavePost2Synonyms(postId, pos + 1, tagsPerPosition);
						savePost2SynonymsPerformance();
						count = 0;
						valuesList = new ArrayList<>();
					} else {
						addToSavePost2Synonyms(postId, pos + 1, tagsPerPosition);
						count++;
					}

					// savePost2Synonyms(postId, pos + 1, tagsPerPosition);
				}

			}
			// TODO Einkommentieren
			// savePost2SynonymsInARow(postId, tagsForPost);
		}
	}

	/**
	 * triggers the valuesList into post2synonyms
	 */
	private void savePost2SynonymsPerformance() {

		StringBuilder sb = new StringBuilder();
		for (String value : valuesList) {
			sb.append(value);
		}
		String values = sb.substring(0, sb.length() - 1);
		saveValuesIntoPost2Synonyms(values);

	}

	/**
	 * addes posts2synonyms to insertlist
	 * 
	 * @param postId
	 * @param pos
	 * @param synonyms
	 */
	private void addToSavePost2Synonyms(Integer postId, int pos, Map<Integer, String> synonyms) {
		StringBuilder sb = new StringBuilder();
		for (Entry<Integer, String> synonym : synonyms.entrySet()) {
			sb.append("(");
			sb.append(postId);
			sb.append(", ");
			sb.append(pos);
			sb.append(", '");
			sb.append(synonym.getValue());
			sb.append("', ");
			sb.append(synonym.getKey());
			sb.append("),");

		}

		valuesList.add(sb.toString());

	}

	private void savePost2Synonyms(Integer postId, int pos, Map<Integer, String> synonyms) {

		StringBuilder sb = new StringBuilder();
		for (Entry<Integer, String> synonym : synonyms.entrySet()) {
			sb.append("(");
			sb.append(postId);
			sb.append(", ");
			sb.append(pos);
			sb.append(", '");
			sb.append(synonym.getValue());
			sb.append(", '");
			sb.append(synonym.getKey());
			sb.append("'),");

		}

		String values = sb.substring(0, sb.length() - 1);

		saveValuesIntoPost2Synonyms(values);

	}

	private void saveValuesIntoPost2Synonyms(String values) {
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);

			Statement st = conn.createStatement();
			String query = "insert into post2synonyms (postId, position, synonym, groupId) " + "values " + values + "";
			st.executeUpdate(query);
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("saved into db + 5000");
	}

	private void savePost2SynonymsInARow(Integer postId, Set<String> synonymSet) {

		EList<String> synonyms = new EArrayList<>();
		synonyms.addAll(synonymSet);

		StringBuilder synonymBuilder = new StringBuilder();

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < synonyms.size() && i < 50; i++) {

			synonymBuilder.append(" tag" + (i + 1) + ",");

			sb.append("'");
			sb.append(synonyms.get(i));
			sb.append("',");

		}
		if (synonyms.size() > 50) {
			System.out.println("zu klein! postId: " + postId);
		}

		String values = sb.substring(0, sb.length() - 1);
		String synonymValues = synonymBuilder.substring(0, synonymBuilder.length() - 1);

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);

			Statement st = conn.createStatement();
			String query = "insert into post2synonymsInARow (postId, " + synonymValues + ") " + "values (" + postId + ",  " + values + ")";
			st.executeUpdate(query);
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * retourniert die passenden synonyme für gegebenes tag,
	 * 
	 * @param tag
	 * @return
	 */
	private Map<Integer, String> getSynonymsForTag(String tag) {
		Map<Integer, String> returnList = new HashMap<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select groupId, target from tagcommunities where source = '" + tag + "' and target!= source");

			while (tags_db.next()) {

				String synonym = tags_db.getString("target");
				Integer groupId = tags_db.getInt("groupId");
				returnList.put(groupId, synonym);

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	public EList<StackOverflowPost> getPosts() {

		EList<StackOverflowPost> returnList = new EArrayList<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select Id, Tags from qposts");

			while (tags_db.next()) {

				Integer id = tags_db.getInt("Id");
				String tags = tags_db.getString("Tags");

				String[] splitTags = tags.split("><");
				splitTags[0] = splitTags[0].replace("<", "");
				splitTags[splitTags.length - 1] = splitTags[splitTags.length - 1].replace(">", "");

				StackOverflowPost post = new StackOverflowPost();
				post.setId(id);
				post.setTags(splitTags);

				returnList.add(post);

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnList;

	}

	/**
	 * returns the id * (-1) of the given tag *(-1) to differ between group and tagids
	 * 
	 * @param tag
	 * @return
	 */
	private Integer getIdforTag(String tag) {
		Integer id = 0;
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select Id from tags where tagname = '" + tag + "'");

			while (tags_db.next()) {

				id = tags_db.getInt("Id");

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -id;
	}

	private void deletePost2Synonyms() {
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			st.executeUpdate("delete from post2synonyms");
			st.executeUpdate("delete from post2synonymsInARow");

			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		System.out.println("post2synonyms & post2snynonymsInARow deleted");
	}
}
