package datahandling.handleData;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import helper.HelperDBHandler;
import helper.TrendDto;
import tagsynonymprediction.models.SynonymCandidate;
import tagsynonymprediction.utility.EList;

public class GroupTags {

	public static void main(String args[]) {
		new GroupTags();
	}

	// liste aller bereits groupierten tags
	private Set<String> allGroupedTagNames;
	private Map<String, Set<String>> synonymGroups;

	public GroupTags() {

		// collect all synonym pairs for each tag and put them into one table

		allGroupedTagNames = new HashSet<>();
		synonymGroups = new HashMap<>();

		// add all approved synonym pairs
		// addApprovedSynonyms();

		// add selected synonym candidates
		addSelectedSynonymCandidates();

		String time = System.currentTimeMillis() + "";
		String filename = "/Users/stefanie/Documents/workspace/TagSynonymPrediction/generatedData/";
		getTrendsPerGroup(filename, time);

	}

	private void getTrendsPerGroup(String path, String time) {

		// save groups
		try {

			FileWriter writer = new FileWriter(path + time + "groups.txt", true);

			setHeaderOfTrendsFile(path, time);

			// get trends
			for (Entry<String, Set<String>> entry : synonymGroups.entrySet()) {

				Set<String> tags = entry.getValue();
				tags.add(entry.getKey());

				System.out.println(collectionItemsToString(tags));
				writer.append(collectionItemsToString(tags));
				writer.append("\n");

				EList<TrendDto> trends = HelperDBHandler.getTrendsForGroupedSynonyms(tags);

				exportTrendsPerGroupToCsv(trends, entry.getKey(), path, time);

			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void setHeaderOfTrendsFile(String path, String time) throws IOException {
		FileWriter writer2 = new FileWriter(path + time + "trends.csv", true);

		writer2.append("category");
		writer2.append(",");
		writer2.append("date");
		writer2.append(",");
		writer2.append("postCount");
		writer2.append(",");
		writer2.append("answerCountSum");
		writer2.append(",");
		writer2.append("answerCountAvg");
		writer2.append(",");
		writer2.append("commentCountSum");
		writer2.append(",");
		writer2.append("commentCountAvg");
		writer2.append(",");
		writer2.append("acceptedAnswersCountSum");
		writer2.append(",");
		writer2.append("acceptedAnswersCountAvg");
		writer2.append(",");
		writer2.append("openAnswerCountSum");
		writer2.append(",");
		writer2.append("openAnswerCountAvg");
		writer2.append(",");
		writer2.append("favoriteCountSum");
		writer2.append(",");
		writer2.append("favoriteCountAvg");
		writer2.append(",");
		writer2.append("scoreCountSum");
		writer2.append(",");
		writer2.append("scoreCountPos");
		writer2.append(",");
		writer2.append("scoreCountNeg");
		writer2.append("\n");
		writer2.flush();
		writer2.close();
	}

	private <E> String collectionItemsToString(Collection<E> coll) {
		StringBuilder sb = new StringBuilder();
		for (Object oo : coll) {
			sb.append((oo.toString() + " "));
		}
		return sb.toString();
	}

	private void exportTrendsPerGroupToCsv(EList<TrendDto> trends, String label, String path, String time) {

		try {
			FileWriter writer = new FileWriter(path + time + "trends.csv", true);

			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

			for (TrendDto trend : trends) {
				writer.append(label);
				writer.append(',');
				String date_ = sdf.format(trend.getDate());
				writer.append(date_);
				writer.append(',');
				writer.append(trend.getPostCount().toString());
				writer.append(',');
				writer.append(trend.getAnswerCountSum().toString());
				writer.append(',');
				writer.append(trend.getAnswerCountAvg().toString());
				writer.append(',');
				writer.append(trend.getCommentCountSum().toString());
				writer.append(',');
				writer.append(trend.getCommentCountAvg().toString());
				writer.append(',');
				writer.append(trend.getAcceptedAnswerCountSum().toString());
				writer.append(',');
				// writer.append(trend.getAcceptedAnswerCountAvg().toString());
				// writer.append(',');
				writer.append(trend.getOpenAnswerCountSum().toString());
				writer.append(',');
				// writer.append(trend.getOpenAnswerCountAvg().toString());
				// writer.append(',');
				writer.append(trend.getFavouriteCountSum().toString());
				writer.append(',');
				writer.append(trend.getFavouriteCountAvg().toString());
				writer.append(',');
				writer.append(trend.getScoreCountSum().toString());
				writer.append(',');
				writer.append(trend.getScoreCountPos().toString());
				writer.append(',');
				writer.append(trend.getScoreCountNeg().toString());
				writer.append(',');
				writer.append(trend.getViewCountSum().toString());
				writer.append(',');
				writer.append(trend.getViewCountAvg().toString());
				writer.append('\n');
			}

			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void addSelectedSynonymCandidates() {
		// evtl. auch getallTagsAndId
		Set<String> allTagNames = HelperDBHandler.getAllAndroidTagsAndId(6000).keySet();

		for (String tagName : allTagNames) {

			EList<Set<String>> selectedSynonymCandidatesForTag = HelperDBHandler
					.getSelectedSynonymCandidatesForTag(tagName);

			// filter and add
			Set<String> selectedSourceSynonymCandidatesForTag = selectedSynonymCandidatesForTag.get(0);
			Set<String> selectedTargetSynonymCandidatesForTag = selectedSynonymCandidatesForTag.get(1);

			// TEST
			// if (!tagName.startsWith("guice") && !tagName.startsWith("guice")
			// && !tagName.startsWith("guide") && !tagName.startsWith("guide")
			// && !tagName.startsWith("guile")
			// && !tagName.startsWith("guile")) {
			// continue;
			// }
			System.out.print(tagName + "(source): ");
			for (String synonymCandidate : selectedSourceSynonymCandidatesForTag) {
				System.out.print(synonymCandidate + " ");
				// TODO
				// addSynonymCandidates(synonymCandidate, tagName);
			}
			System.out.print(tagName + "(target): ");
			for (String synonymCandidate : selectedTargetSynonymCandidatesForTag) {
				System.out.print(synonymCandidate + " ");
				// TODO
				// addSynonymCandidates(tagName, synonymCandidate);
			}
			System.out.println("--");
			// collect all approved synonym pairs
		}
	}

	private void addApprovedSynonyms() {

		// source, target
		EList<SynonymCandidate> allTagsAndSynonymsFromDb = HelperDBHandler.getAllApprovedSynonymPairs(0, 5000);

		for (SynonymCandidate dto : allTagsAndSynonymsFromDb) {

			String source = dto.getTag();
			String target = dto.getSynonymTag();

			// TEST
			// if ((!source.startsWith("g") &&
			// !entry.getValue().startsWith("g"))) {
			// || (!source.startsWith("guide") &&
			// !entry.getValue().startsWith("guide"))
			// || (!source.startsWith("guile") &&
			// !entry.getValue().startsWith("guile"))) {
			// || (!source.startsWith("freetype") &&
			// !entry.getValue().startsWith("freetype"))
			// || (!source.startsWith("droidgap") &&
			// !entry.getValue().startsWith("droidgap")) ||
			// (!source.startsWith("color") &&
			// !entry.getValue().startsWith("color"))) {
			// continue;
			// }

			addSynonymCandidates(source, target);

		}

	}

	private void addSynonymCandidatesNEU(String source, String target) {
		Set<String> subList;
		// es gibt noch keine einträge
		if (!allGroupedTagNames.contains(source) && !allGroupedTagNames.contains(target)) {

			// füge zu allgemeiner liste hinzu
			allGroupedTagNames.add(source);
			allGroupedTagNames.add(target);

			// füge zu synonym groups hinzu
			subList = new HashSet<>();
			subList.add(source);
			synonymGroups.put(target, subList);

		}
		// bereits in synonymGroups aber tausch notwendig
		else if (synonymGroups.containsKey(source)) {

			// switch source and target

			Set<String> list = synonymGroups.get(source);
			// remove old
			synonymGroups.remove(source);

			// change
			list.add(source);
			list.remove(target);
			if (synonymGroups.containsKey(target)) {
				list.addAll(synonymGroups.get(target));
			}
			allGroupedTagNames.add(target);
			synonymGroups.put(target, list);

		}
		// bereits in synonymGroups kein tausch notwendig
		else if (synonymGroups.containsKey(target)) {
			Set<String> list = synonymGroups.get(target);
			list.add(source);
			allGroupedTagNames.add(source);
			synonymGroups.put(target, list);
		}

		// schon mal verwendet
		else if (allGroupedTagNames.contains(source)) {
			String key = getKeyOfGroupedTags(source);

			if (key.equals("-1")) {
				System.out.println("ERROR " + source + " " + target);
			} else {
				Set<String> list = synonymGroups.get(key);
				list.add(target);
				allGroupedTagNames.add(target);
				synonymGroups.put(key, list);
			}
		}
		// schon mal verwendet TODO check ob tausch noch notwendig?
		else if (allGroupedTagNames.contains(target)) {
			String key = getKeyOfGroupedTags(target);
			if (key.equals("-1")) {
				System.out.println("ERROR " + source + " " + target);
			} else {
				Set<String> list = synonymGroups.get(key);
				list.add(source);
				allGroupedTagNames.add(source);
				synonymGroups.put(key, list);
			}
		}
	}

	private void addSynonymCandidates(String source, String target) {

		// // TODO nur jetz - wieder weggeben
		// if (true) {
		// System.out.println(source + " --- " + target);
		// return;
		// }

		Set<String> subList;
		// es gibt noch keine einträge
		if (!allGroupedTagNames.contains(source) && !allGroupedTagNames.contains(target)) {

			// füge zu allgemeiner liste hinzu
			allGroupedTagNames.add(source);
			allGroupedTagNames.add(target);

			// füge zu synonym groups hinzu
			subList = new HashSet<>();
			subList.add(source);
			synonymGroups.put(target, subList);

		}
		// bereits in synonymGroups aber tausch notwendig
		else if (synonymGroups.containsKey(source)) {

			// switch source and target

			Set<String> list = synonymGroups.get(source);
			// remove old
			synonymGroups.remove(source);

			// change
			list.add(source);
			list.remove(target);
			if (synonymGroups.containsKey(target)) {
				list.addAll(synonymGroups.get(target));
			}
			allGroupedTagNames.add(target);
			synonymGroups.put(target, list);

		}
		// bereits in synonymGroups kein tausch notwendig
		else if (synonymGroups.containsKey(target)) {
			Set<String> list = synonymGroups.get(target);
			list.add(source);
			allGroupedTagNames.add(source);
			synonymGroups.put(target, list);
		}

		// schon mal verwendet
		else if (allGroupedTagNames.contains(source)) {
			String key = getKeyOfGroupedTags(source);

			if (key.equals("-1")) {
				System.out.println("ERROR " + source + " " + target);
			} else {
				Set<String> list = synonymGroups.get(key);
				list.add(target);
				allGroupedTagNames.add(target);
				synonymGroups.put(key, list);
			}
		}
		// schon mal verwendet TODO check ob tausch noch notwendig?
		else if (allGroupedTagNames.contains(target)) {
			String key = getKeyOfGroupedTags(target);
			if (key.equals("-1")) {
				System.out.println("ERROR " + source + " " + target);
			} else {
				Set<String> list = synonymGroups.get(key);
				list.add(source);
				allGroupedTagNames.add(source);
				synonymGroups.put(key, list);
			}
		}
	}

	private String getKeyOfGroupedTags(String tag) {

		for (Entry<String, Set<String>> entry : synonymGroups.entrySet()) {
			for (String tagName : entry.getValue()) {
				if (tag.equals(tagName)) {
					return entry.getKey();
				}
			}
		}

		return "-1";

	}
}
