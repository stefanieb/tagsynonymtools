package datahandling.handleData;

public class StackOverflowPost {

	public StackOverflowPost() {

	}

	public StackOverflowPost(Integer id, String body, String[] tags, String title) {
		super();
		this.id = id;
		this.body = body;
		this.tags = tags;
		this.title = title;
	}

	private Integer id;
	private String body;
	private String[] tags;
	private String title;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String[] getTags() {
		return tags;
	}

	public void setTags(String[] tags) {
		this.tags = tags;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
