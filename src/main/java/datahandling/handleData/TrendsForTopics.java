package datahandling.handleData;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import org.apache.commons.io.FileUtils;

import helper.HelperDBHandler;
import helper.TrendDto;
import tagsynonymprediction.utility.EArrayList;
import tagsynonymprediction.utility.EList;

public class TrendsForTopics {

	String time = System.currentTimeMillis() + "";
	// String path = "/Users/stefanie/Documents/workspace_mars/TagSynonymTools/src/main/resources/generatedData/";
	String path = "/Users/stefanie/Desktop/trends_tag/";
	String filename = "synonym-trends.csv";

	private Boolean SYNONYM = true; // toggle false means use normal tags

	public static void main(String[] args) throws IOException {

		System.out.println("start to find topics");
		TrendsForTopics tft = new TrendsForTopics();

		tft.startTrendsForTopics();
		System.out.println("Done.");

	}

	public TrendsForTopics() throws IOException {
		FileUtils.cleanDirectory(new File(path));
	}

	public void startTrendsForTopics() {
		EList<String> tagsToFindTrends = getMostXXXTopics();

		saveTrendsPerGroup(tagsToFindTrends);

	}

	private EList<String> getMostXXXTopics() {
		// select synonym, count(*) from post2synonyms group by synonym order by count(*) desc;
		System.out.println("TODOOOOOOOO");

		EList<String> returnList = new EArrayList<>();

		// TAG SYNONYM GROUPS
		if (SYNONYM) {

			returnList.add("layout");
			returnList.add("listview");
			returnList.add("eclipse");
			returnList.add("fragment");
			returnList.add("sqlite");
			returnList.add("android-intent");
		} else {

			// TAG GROUPS

			returnList.add("layout");
			returnList.add("listview");
			returnList.add("eclipse");
			returnList.add("fragment");
			returnList.add("sqlite");
			returnList.add("android-intent");

		}

		/*
		 * returnList.add("facebook"); returnList.add("service"); returnList.add("cordova"); returnList.add("image"); returnList.add("webview"); returnList.add("json");
		 * returnList.add("android-actionbar");
		 */

		return returnList;

	}

	private void saveTrendsPerGroup(EList<String> targetList) {

		// save groups
		setHeaderOfTrendsFile();

		// get trends
		for (String target : targetList) {

			// System.out.println((tags));
			// TODO switch
			EList<TrendDto> trends;
			if (SYNONYM) {
				trends = HelperDBHandler.getTrendsForGroupedSynonyms(target);
			} else {
				trends = HelperDBHandler.getTrendsForTags(target);
			}

			exportTrendsPerGroupToCsv(trends, target);

			exportTrendsPerGroupToCsvOtherFormat(trends, target);

		}
	}

	private void exportTrendsPerGroupToCsvOtherFormat(EList<TrendDto> trends, String target) {
		try {

			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
			FileWriter writer = new FileWriter(path + time + "trends_" + target + ".csv", true);

			writer.append("metrik");
			writer.append(",");

			writer.append("value");
			writer.append(",");
			writer.append("date");
			writer.append("\n");

			for (TrendDto trend : trends) {

				String date_ = sdf.format(trend.getDate());

				// postcount
				writer.append("postCount");
				writer.append(",");
				writer.append(trend.getPostCount().toString());
				writer.append(',');
				writer.append(date_);
				writer.append("\n");

				// answercount
				// writer.append("answerCountSum");
				// writer.append(",");
				// writer.append(trend.getAnswerCountSum().toString());
				// writer.append(',');
				// writer.append(date_);
				// writer.append('\n');

				// writer.append("answerCountAvg");
				// writer.append(",");
				// writer.append(trend.getAnswerCountAvg().toString());
				// writer.append(',');
				// writer.append(date_);
				// writer.append('\n');

				// commentcount
				// writer.append("commentCountSum");
				// writer.append(",");
				// writer.append(trend.getCommentCountSum().toString());
				// writer.append(',');
				// writer.append(date_);
				// writer.append('\n');
				// writer.append("commentCountAvg");
				// writer.append(",");
				// writer.append(trend.getCommentCountAvg().toString());
				// writer.append(',');
				// writer.append(date_);
				// writer.append('\n');

				// acceptedAnswersCount
				// writer.append("acceptedAnswersCountSum");
				// writer.append(",");
				// writer.append(trend.getAcceptedAnswerCountSum().toString());
				// writer.append(',');
				// writer.append(date_);
				// writer.append('\n');

				// // writer.append("acceptedAnswersCountAvg");
				// // writer.append(",");
				//// writer.append(trend.getAcceptedAnswerCountAvg().toString());
				//// writer.append(',');
				//// writer.append(date_);
				//// writer.append('\n');

				// openanswercount
				// writer.append("openAnswerCountSum");
				// writer.append(",");
				// writer.append(trend.getOpenAnswerCountSum().toString());
				// writer.append(',');
				// writer.append(date_);
				// writer.append('\n');

				// writer.append("openAnswerCountAvg");
				// writer.append(",");
				// writer.append(trend.getOpenAnswerCountAvg().toString());
				// writer.append(',');
				// writer.append(date_);
				// writer.append('\n');

				// favouriteCount
				// writer.append("favoriteCountSum");
				// writer.append(",");
				// writer.append(trend.getFavouriteCountSum().toString());
				// writer.append(',');
				// writer.append(date_);
				// writer.append('\n');

				// writer.append("favoriteCountAvg");
				// writer.append(",");
				// writer.append(trend.getFavouriteCountAvg().toString());
				// writer.append(',');
				// writer.append(date_);
				// writer.append('\n');

				// scorecount
				// writer.append("scoreCountSum");
				// writer.append(",");
				// writer.append(trend.getScoreCountSum().toString());
				// writer.append(',');
				// writer.append(date_);
				// writer.append('\n');
				//
				// writer.append("scoreCountPos");
				// writer.append(",");
				// writer.append(trend.getScoreCountPos().toString());
				// writer.append(',');
				// writer.append(date_);
				// writer.append('\n');
				//
				// writer.append("scoreCountNeg");
				// writer.append(",");
				// writer.append(trend.getScoreCountNeg().toString());
				// writer.append(',');
				// writer.append(date_);
				// writer.append('\n');

				// viewcount

				// writer.append("viewCountSum");
				// writer.append(",");
				// writer.append(trend.getViewCountSum().toString());
				// writer.append(',');
				// writer.append(date_);
				// writer.append('\n');

				// writer.append("viewCountAvg");
				// writer.append(",");
				// writer.append(trend.getViewCountAvg().toString());
				// writer.append(',');
				// writer.append(date_);
				// writer.append('\n');

			}

			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void exportTrendsPerGroupToCsv(EList<TrendDto> trends, String label) {

		try {
			FileWriter writer = new FileWriter(path + time + filename, true);

			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);

			for (TrendDto trend : trends) {
				writer.append(label);
				writer.append(',');
				String date_ = sdf.format(trend.getDate());
				writer.append(date_);
				writer.append(',');
				writer.append(trend.getPostCount().toString());
				writer.append(',');
				// writer.append(trend.getAnswerCountSum().toString());
				// writer.append(',');
				// writer.append(trend.getAnswerCountAvg().toString());
				// writer.append(',');
				// writer.append(trend.getCommentCountSum().toString());
				// writer.append(',');
				// writer.append(trend.getCommentCountAvg().toString());
				// writer.append(',');
				// writer.append(trend.getAcceptedAnswerCountSum().toString());
				// writer.append(',');

				// writer.append(trend.getAcceptedAnswerCountAvg().toString());
				// writer.append(',');
				// writer.append(trend.getOpenAnswerCountSum().toString());
				// writer.append(',');
				// writer.append(trend.getOpenAnswerCountAvg().toString());
				// writer.append(',');
				// writer.append(trend.getFavouriteCountSum().toString());
				// writer.append(',');
				// writer.append(trend.getFavouriteCountAvg().toString());
				// writer.append(',');
				// writer.append(trend.getScoreCountSum().toString());
				// writer.append(',');
				// writer.append(trend.getScoreCountPos().toString());
				// writer.append(',');
				// writer.append(trend.getScoreCountNeg().toString());
				// writer.append(',');
				writer.append(trend.getViewCountSum().toString());
				// writer.append(',');
				// writer.append(trend.getViewCountAvg().toString());
				writer.append('\n');
			}

			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void setHeaderOfTrendsFile() {

		FileWriter writer2;
		try {
			writer2 = new FileWriter(path + time + filename, true);
			writer2.append("category");
			writer2.append(",");
			writer2.append("date");
			writer2.append(",");
			writer2.append("postCount");
			writer2.append(",");
			writer2.append("viewCount");
			// writer2.append(",");
			// writer2.append("answerCount");
			// writer2.append(",");
			// writer2.append("commentCount");
			// writer2.append(",");
			// writer2.append("acceptedAnswersCount");
			// writer2.append(",");
			// writer2.append("openAnswerCount");
			// writer2.append(",");
			// writer2.append("favoriteCount");
			// writer2.append(",");
			// writer2.append("scoreCount");
			writer2.append("\n");
			writer2.flush();
			writer2.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
