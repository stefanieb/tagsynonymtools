package datahandling.handleData;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import tagsynonymprediction.models.SynonymCandidate;

public class GroupNeu {

	private Set<String> sourceList;

	private Set<String> targetList;

	private Set<Integer> ids;

	private Integer groupId;

	private Boolean createdApproved = false;

	public boolean isCreatedFromApprovedSynonyms() {
		return createdApproved;
	}

	public GroupNeu() {
		sourceList = new HashSet<>();
		targetList = new HashSet<>();
		ids = new HashSet<>();
	}

	public void setCreatedByApprovedSynonyms() {
		createdApproved = true;
	}

	public String getLabel() {

		return collectionItemsToString(targetList);
	}

	public Set<String> getSourceList() {
		return sourceList;
	}

	public Set<String> getTargetList() {
		return targetList;
	}

	public Set<Integer> getIdList() {
		return ids;
	}

	public void addSourcesToList(Set<String> list, Set<Integer> idList) {
		sourceList.addAll(list);
		ids.addAll(idList);

	}

	public void addSourceAndTargetWithId(String source, String target, Integer id) {
		sourceList.add(source);
		targetList.add(target);
		ids.add(id);
	}

	public void addSynonymCandidate(SynonymCandidate symCan) {
		sourceList.add(symCan.getTag());
		targetList.add(symCan.getSynonymTag());
		ids.add(symCan.getId());
	}

	public void addTargetsToList(Set<String> list, Set<Integer> idList) {
		targetList.addAll(list);
		ids.addAll(idList);
	}

	public void addSourceInfoDto(String source, Integer id) {
		if (!targetList.contains(source)) {
			sourceList.add(source);
		}
		ids.add(id);
	}

	public void addTargetInfoDto(String target, Integer id) {
		targetList.add(target);
		ids.add(id);
	}

	private <E> String collectionItemsToString(Collection<E> coll) {
		StringBuilder sb = new StringBuilder();
		for (Object oo : coll) {
			sb.append((oo.toString() + " "));
		}
		return sb.toString();
	}

	public void switchTargetToSourceAndAddNewTarget(String targetOld, String targetNew, Integer id) {
		sourceList.add(targetOld);
		sourceList.addAll(targetList);

		targetList = new HashSet<>();
		targetList.add(targetNew);
		ids.add(id);
	}

	public void switchTargetToSourceAndAddNewTarget(Set<String> targetOldList, String targetNew, Integer id) {
		sourceList.addAll(targetOldList);
		sourceList.addAll(targetList);

		targetList = new HashSet<>();
		targetList.add(targetNew);
		ids.add(id);
	}

	public void addToIdList(Integer id) {
		ids.add(id);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append("Group(" + groupId + "): " + getLabel() + "\n");
		sb.append("source-tags: " + collectionItemsToString(sourceList) + "\n");
		// sb.append("target-tags: " + collectionItemsToString(targetList) + "\n");
		sb.append("--- \n");

		return sb.toString();

	}

	public String getFirstSource() {
		return sourceList.iterator().next();
	}

	public String getFirstTarget() {
		return targetList.iterator().next();

	}

	public Integer getFirstId() {
		return ids.iterator().next();

	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

}
