package datahandling.handleData;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.google.common.collect.Table.Cell;

import helper.HelperDBHandler;
import helper.TrendDto;
import tagsynonymprediction.db.DBHandler;
import tagsynonymprediction.models.SynonymCandidate;
import tagsynonymprediction.models.Tag;
import tagsynonymprediction.utility.EArrayList;
import tagsynonymprediction.utility.EList;

public class TagsToGroups {

	private final Boolean ONLY_ANDROID = true;
	private final int FILTER_TOP = 58000;
	private Boolean SAVE_INTO_FILE = true;
	private Boolean PRINT_DELICATE_TAGS = false;
	private final double LIMIT = 0.60;

	public static void main(String args[]) {
		new TagsToGroups().startGroupingOfTags();
	}

	// liste aller bereits groupierten tags
	private Set<String> allGroupedTagNames;
	private HashMap<Integer, GroupNeu> synonymGroups;
	private Map<Integer, SynonymCandidate> synonymCandidateMap;
	private EList<SynonymCandidate> approvedSynonymPairs;
	private EList<SynonymCandidate> synonymCandidates;
	private Set<Integer> usedSynonymCandidateIds;
	// source target id
	private Table<String, String, Integer> table;
	private int groupIdCount = 0;
	private ImportantTopicDto topicPostcount = new ImportantTopicDto();
	private boolean approvedfinished = false;

	private String time;
	private final String path = "/Users/stefanie/Documents/workspace_mars/TagSynonymTools/src/main/resources/generatedData/";

	private SortedSet<String> listOfNotUsedTags;
	private boolean selectedTagsOnly = true;
	// private Version v;

	private String words;
	private String[] delicateTags;

	public void setSelectedTagsOnly(Boolean selectedTagsOnly) {
		this.selectedTagsOnly = selectedTagsOnly;
	}

	public void startGroupingOfTags() {
		// collect all synonym pairs for each tag and put them into a table

		init();

		// add all approved synonym pairs to synonymGroups8
		groupApprovedSynonyms();
		approvedfinished = true;

		// for (Entry<Integer, GroupNeu> entry : synonymGroups.entrySet()) {
		// System.out.println(entry.getValue().toString() + "\n");
		// }

		System.out.println("===============================================\n\n\n");

		Set<SynonymCandidate> synonymCandidatesSet = new HashSet<>();
		synonymCandidatesSet.addAll(synonymCandidates);
		Set<SynonymCandidate> remainingSynonymCandidates = assignSynonymCandidatesToGroups(synonymCandidatesSet, 1.0, LIMIT);

		// TEST print

		TreeMap<Integer, GroupNeu> tm = new TreeMap<>();
		tm.putAll(synonymGroups);

		for (Entry<Integer, GroupNeu> entry : tm.entrySet()) {
			System.out.println(entry.getKey() + ": " + entry.getValue().toString() + "\n");
		}

		if (SAVE_INTO_FILE) {
			time = System.currentTimeMillis() + "";
			saveAllGroupsToCsv(tm.keySet());
			// ?
			// getTrendsPerGroup(topicPostcount.getIdsOfMostImportantTopics());

		}

		System.out.println("finally");

		System.out.println(listOfNotUsedTags.toString());
		System.out.println("unused tags: " + listOfNotUsedTags.size());
		System.out.println(remainingSynonymCandidates.size());
		System.out.println(synonymGroups.size());
	}

	public void saveIntoFile(Boolean save) {
		this.SAVE_INTO_FILE = save;
	}

	private void getTrendsPerGroup(EList<Integer> groupIds) {

		// TODO handle android-listview + listview

		// save groups
		try {

			FileWriter writer = new FileWriter(path + time + "groupstop21.txt", true);

			setHeaderOfTrendsFile(path, time);

			// get trends
			for (Integer groupId : groupIds) {
				GroupNeu group = synonymGroups.get(groupId);

				Set<String> tags = group.getSourceList();
				tags.addAll(group.getTargetList());

				// System.out.println((tags));
				writer.append(collectionItemsToString(tags));
				writer.append("\n");

				EList<TrendDto> trends = HelperDBHandler.getTrendsForGroupedSynonyms(tags);

				exportTrendsPerGroupToCsv(trends, collectionItemsToString(group.getTargetList()), path, time);

			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void saveAllGroupsToCsv(Set<Integer> groupIds) {
		try {

			FileWriter writer = new FileWriter(path + time + "groups.txt", true);

			setHeaderOfTrendsFile(path, time);

			// get trends
			for (Integer groupId : groupIds) {
				GroupNeu group = synonymGroups.get(groupId);

				Set<String> tags = group.getTargetList();
				tags.addAll(group.getSourceList());

				// System.out.println((tags));
				writer.append(collectionItemsToString(tags));
				writer.append("\n");

				// EList<TrendDto> trends = HelperDBHandler.getTrendsForGroupedSynonyms(tags);

				// exportTrendsPerGroupToCsv(trends, collectionItemsToString(group.getTargetList()), path, time);

			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void addNewSynonymGroup(Integer synCandidateId, SynonymCandidate dto) {
		groupIdCount++;
		int groupId_ = groupIdCount;
		GroupNeu groupNeu = new GroupNeu();
		groupNeu.setGroupId(groupId_);
		groupNeu.addSourceAndTargetWithId(dto.getTag(), dto.getSynonymTag(), groupId_);
		updateDataObjects(dto, groupNeu);
	}

	private Set<SynonymCandidate> assignSynonymCandidatesToGroups(Set<SynonymCandidate> toIterate, double ranking, double rankingLimit) {
		Set<SynonymCandidate> notUsedSynonymCandidates = new HashSet<>();
		Set<SynonymCandidate> checkAgainSynonymCandidates = new HashSet<>();

		// 1 check
		notUsedSynonymCandidates = iterateSynonymCandidatesForExistingAndSameTarget(toIterate);

		if (ranking >= rankingLimit) {
			// es ist nichts weggekommen
			Set<SynonymCandidate> toRemove = new HashSet<>();
			for (SynonymCandidate sc : notUsedSynonymCandidates) {

				Set<SynonymCandidate> scSet = new HashSet<>();

				if (sc.getTag().equals("dynamic-pivot") || sc.getTag().equals("pivotal-crm")) {
					System.out.println("STOPP");
				}

				scSet.add(sc);

				// wenn nichts anderes gefunden worden ist, hinzufügen bei
				// entsprechendem ranking
				boolean found = true;
				if (iterateSynonymCandidatesForExistingAndSameTarget(scSet).size() > 0) {
					if (checkForOtherRelations(scSet).size() > 0) {
						if (sc.getStoredRanking() >= ranking) {
							addNewSynonymGroup(sc.getId(), sc);

						} else {
							// wurde nichts gefunden und aich ranking passt
							// nicht
							// - nicht entfernen
							found = false;
						}
					}
				}
				if (found) {
					toRemove.add(sc);
				}

			}
			notUsedSynonymCandidates.removeAll(toRemove);
			ranking = ranking - 0.01;

			checkAgainSynonymCandidates = iterateSynonymCandidates(notUsedSynonymCandidates, ranking, rankingLimit);
		}

		return checkAgainSynonymCandidates;

	}

	private Set<SynonymCandidate> iterateSynonymCandidatesForExistingAndSameTarget(Set<SynonymCandidate> toIterate) {
		Set<SynonymCandidate> notUsedSynonymCandidatesTarget = new HashSet<>();
		Set<SynonymCandidate> notUsedSynonymCandidatesBoth = new HashSet<>();
		Set<SynonymCandidate> notUsedSynonymCandidates = new HashSet<>();

		int oldSize = toIterate.size();
		int newSize = 0;
		notUsedSynonymCandidates = toIterate;
		while (oldSize > newSize) {
			notUsedSynonymCandidatesTarget = new HashSet<>();
			notUsedSynonymCandidatesBoth = new HashSet<>();
			oldSize = notUsedSynonymCandidates.size();
			notUsedSynonymCandidatesTarget.addAll(checkTargetExIsTargetNew(notUsedSynonymCandidates));
			notUsedSynonymCandidatesBoth.addAll(checkAlreadyBothInGroup(notUsedSynonymCandidatesTarget));

			newSize = notUsedSynonymCandidatesBoth.size();
			notUsedSynonymCandidates = new HashSet<SynonymCandidate>();
			notUsedSynonymCandidates.addAll(notUsedSynonymCandidatesBoth);

		}
		// gibt keine neuen mehr

		Set<SynonymCandidate> stillNotUsedSynonymCandidates = new HashSet<>();
		for (SynonymCandidate synCan : notUsedSynonymCandidates) {

			if (!checkAlreadyBothInGroupSingle(synCan)) {
				if (!checkTargetExIsTargetNewSingle(synCan)) {
					stillNotUsedSynonymCandidates.add(synCan);
				}
			}

		}

		return stillNotUsedSynonymCandidates;
	}

	private Set<SynonymCandidate> iterateSynonymCandidates(Set<SynonymCandidate> toIterate, double ranking, double rankingLimit) {
		synonymGroups.size();
		Set<SynonymCandidate> chainsForTopCategoriesLeft = new HashSet<>();
		Set<SynonymCandidate> chainsForTopCategoriesRight = new HashSet<>();
		Set<SynonymCandidate> chainsForTopCategoriesDown = new HashSet<>();

		Set<SynonymCandidate> chainsForTopCategoriesUp = new HashSet<>();

		// already exist group containing both
		// Exist SG: s € SL und t € TL

		chainsForTopCategoriesLeft = checkAlreadyBothInGroup(toIterate);
		chainsForTopCategoriesRight = checkTargetExIsTargetNew(chainsForTopCategoriesLeft);

		// check nachdem gleiche targets vlt waren ob jetzt gleiche gruppen sind
		chainsForTopCategoriesDown = checkAlreadyBothInGroup(chainsForTopCategoriesRight);
		if (chainsForTopCategoriesRight.size() > chainsForTopCategoriesDown.size()) {
			// if (toIterate.size() > chainsForTopCategoriesDown.size()) {
			// es ist was weggekommen -> nochmal schauen
			chainsForTopCategoriesLeft = checkAlreadyBothInGroup(chainsForTopCategoriesDown);

			chainsForTopCategoriesRight = checkTargetExIsTargetNew(chainsForTopCategoriesLeft);
			chainsForTopCategoriesDown = checkAlreadyBothInGroup(chainsForTopCategoriesRight);
			chainsForTopCategoriesUp = iterateSynonymCandidates(chainsForTopCategoriesDown, ranking, rankingLimit);
		} else {
			if (ranking >= rankingLimit) {
				// es ist nichts weggekommen
				Set<SynonymCandidate> toRemove = new HashSet<>();
				for (SynonymCandidate sc : chainsForTopCategoriesDown) {

					Set<SynonymCandidate> scSet = new HashSet<>();
					scSet.add(sc);

					// wenn nichts anderes gefunden worden ist, hinzufügen bei
					// entsprechendem ranking
					boolean found = true;
					if (checkAlreadyBothInGroup(scSet).size() > 0) {
						if (checkTargetExIsTargetNew(scSet).size() > 0) {
							if (checkForOtherRelations(scSet).size() > 0) {
								if (sc.getStoredRanking() >= ranking) {
									addNewSynonymGroup(sc.getId(), sc);

								} else {
									// wurde nichts gefunden und aich ranking
									// passt nicht
									// - nicht entfernen
									found = false;
								}
							}
						}
					}
					if (found) {
						toRemove.add(sc);
					}

				}
				chainsForTopCategoriesDown.removeAll(toRemove);
				ranking = ranking - 0.01;

				// System.out.println("chainsForTopCategoriesRight " +
				// chainsForTopCategoriesRight.size());
				// System.out.println("chainsForTopCategoriesDown " +
				// chainsForTopCategoriesDown.size());

				chainsForTopCategoriesUp = iterateSynonymCandidates(chainsForTopCategoriesDown, ranking, rankingLimit);
			}

		}

		return chainsForTopCategoriesUp;
	}

	private Boolean checkAlreadyBothInGroupSingle(SynonymCandidate synonymCandidate) {
		// achtung speziell for numbers

		String synonymWithoutNumbers = synonymCandidate.getSynonymTag().replaceAll("-?\\d\\.?-?\\d?", "");
		int groupId = getGroupBySourceTagPair(synonymCandidate.getTag(), synonymWithoutNumbers);

		if (groupId != -1) {
			addToSourcesOfExistingGroups(groupId, synonymCandidate);
			return true;
		}
		return false;
	}

	private Set<SynonymCandidate> checkAlreadyBothInGroup(Set<SynonymCandidate> setOfSynonymCandidates) {
		Set<SynonymCandidate> chainsForTopCategoriesLeft = new HashSet<>();

		for (SynonymCandidate synonymCandidate : setOfSynonymCandidates) {
			if (synonymCandidate != null) {
				int groupId = getGroupBySourceTagPair(synonymCandidate.getTag(), synonymCandidate.getSynonymTag());
				if (groupId != -1) {
					GroupNeu groupNeu = synonymGroups.get(groupId);
					groupNeu.addToIdList(groupId);
					SynonymCandidate sc1 = new SynonymCandidate(synonymCandidate.getTag(), groupNeu.getFirstTarget(), true);
					updateDataObjects(sc1, groupNeu);

					SynonymCandidate sc2 = new SynonymCandidate(synonymCandidate.getSynonymTag(), groupNeu.getFirstTarget(), true);
					updateDataObjects(sc2, groupNeu);
				} else {

					chainsForTopCategoriesLeft.add(synonymCandidate);
				}
			}
		}
		return chainsForTopCategoriesLeft;
	}

	private Set<SynonymCandidate> checkTargetExIsTargetNew(Set<SynonymCandidate> synonymCandidates) {

		Set<SynonymCandidate> returnSynonymCandidates = new HashSet<>();

		for (SynonymCandidate synonymCandidate : synonymCandidates) {
			int groupId = getIdForGroupByTarget(synonymCandidate.getSynonymTag());

			// targetEx = targetNew
			if (groupId != -1 && synonymGroups.containsKey(groupId)) {
				GroupNeu groupNeu = synonymGroups.get(groupId);
				groupNeu.addSourceInfoDto(synonymCandidate.getTag(), synonymCandidate.getId());
				updateDataObjects(synonymCandidate, groupNeu);

			} else {
				returnSynonymCandidates.add(synonymCandidate);
			}
		}
		return returnSynonymCandidates;
	}

	private Boolean checkTargetExIsTargetNewSingle(SynonymCandidate synonymCandidate) {

		String synonymWithoutNumbers = synonymCandidate.getSynonymTag().replaceAll("-?\\d\\.?-?\\d?", "");

		int groupId = getIdForGroupByTarget(synonymWithoutNumbers);

		// targetEx = targetNew
		if (groupId != -1 && synonymGroups.containsKey(groupId)) {
			addToSourcesOfExistingGroups(groupId, synonymCandidate);
			return true;
		}
		return false;
	}

	private Set<SynonymCandidate> checkForOtherRelations(Set<SynonymCandidate> setToIterate) {

		Set<SynonymCandidate> returnSet = new HashSet<>();

		for (SynonymCandidate synonymCandidate : setToIterate) {

			if (synonymCandidate.getTag().equals("pivotal-crm") && synonymCandidate.getSynonymTag().equals("crm")) {
				System.out.println("STOPP");
			}

			if (!checkTargetExIsSourceNew(synonymCandidate)) {
				if (!checkSourceExIsTargetNew(synonymCandidate)) {
					if (!checkSourceExIsSourceNew(synonymCandidate)) {
						returnSet.add(synonymCandidate);
					}
				}
			}
		}

		return returnSet;

	}

	private boolean checkSourceExIsSourceNew(SynonymCandidate synonymCandidate) {

		// check exists relation a-c new and a-b old?

		return false;
	}

	private SynonymCandidate getSynonymCandidateIdBySourceTarget(String source, Collection<String> targetList) {

		for (String target : targetList) {
			SynonymCandidate sc = HelperDBHandler.getInfoDtoForSourceTarget(source, target, LIMIT);
			if (sc != null) {
				return sc;
			}
		}
		return null;
	}

	private boolean checkSourceExIsTargetNew(SynonymCandidate synonymCandidate) {

		// sourceEx = targetNew
		EList<Integer> groupIds = getIdsForGroupBySource(synonymCandidate.getSynonymTag());
		boolean found = false;
		double maxRanking = -1.0;
		int groupIdMaxRanking = -1;

		for (Integer groupId : groupIds) {

			if (groupId != -1 && synonymGroups.containsKey(groupId)) {

				GroupNeu groupNeu = synonymGroups.get(groupId);
				// gibt es infodto der dreieck schließt?
				SynonymCandidate triangleCandidate = getSynonymCandidateIdBySourceTarget(synonymCandidate.getTag(), groupNeu.getTargetList());
				if (triangleCandidate != null && !triangleCandidate.getId().equals(synonymCandidate.getId())) {
					// check max
					if (triangleCandidate.getRanking() > maxRanking) {
						maxRanking = triangleCandidate.getRanking();
						groupIdMaxRanking = groupId;
					}

				}
			}
		}
		if (maxRanking != -1 && groupIdMaxRanking != -1) {
			GroupNeu groupNeu = synonymGroups.get(groupIdMaxRanking);
			groupNeu.addSourceInfoDto(synonymCandidate.getSynonymTag(), synonymCandidate.getId());
			groupNeu.addSourceInfoDto(synonymCandidate.getTag(), synonymCandidate.getId());
			updateDataObjectsSpecial(synonymCandidate, groupNeu.getFirstTarget(), groupNeu);
			found = true;
		}

		return found;
	}

	private void updateTableSwitch(int groupId, String targetNew, String targetOld) {

		List<Cell<String, String, Integer>> cellsToRemove = new ArrayList<>();
		for (Cell<String, String, Integer> cell : table.cellSet()) {
			if (cell.getValue().equals(groupId) && cell.getColumnKey().equals(targetOld)) {
				cellsToRemove.add(cell);
			}
		}

		for (Cell<String, String, Integer> cell : cellsToRemove) {
			table.remove(cell.getRowKey(), cell.getColumnKey());
			table.put(cell.getRowKey(), targetNew, groupId);
		}

		// check

		if (getIdForGroupByTarget(targetOld) != -1) {
			System.out.println("ERROR!!!");
		}
	}

	private boolean checkTargetExIsSourceNew(SynonymCandidate synonymCandidate) {

		// targetEx = sourceNew
		int groupId = getIdForGroupByTarget(synonymCandidate.getTag());

		if (groupId != -1 && synonymGroups.containsKey(groupId)) {
			GroupNeu groupNeu = synonymGroups.get(groupId);
			// gibt es synonymcandidate der dreieck schließt?
			List<Integer> synonymCandidateIdList = getSynonymCandidateForSourceAndTarget(groupNeu.getSourceList(), synonymCandidate.getSynonymTag());

			if (synonymCandidateIdList.size() == groupNeu.getSourceList().size()) {

				// if (synonymCandidateId != -1 && !synonymCandidateId.equals(synonymCandidate.getId())) {
				// switch source target
				// a->b, b->c --- es gibt auch a->c
				groupNeu.switchTargetToSourceAndAddNewTarget(synonymCandidate.getTag(), synonymCandidate.getSynonymTag(), synonymCandidate.getId());

				updateTableSwitch(groupNeu.getGroupId(), synonymCandidate.getSynonymTag(), synonymCandidate.getTag());
				// table entries mit groupId

				updateDataObjects(synonymCandidate, groupNeu);
				return true;

				// }
			} // else {

			// check ob alte gruppe approved ist

			// LuceneLevenshteinDistance lld = new LuceneLevenshteinDistance();
			//
			// if (groupNeu.isCreatedFromApprovedSynonyms()) {
			// if (synonymCandidate.getRanking() >= LIMIT && (synonymCandidate.getCat() != null && synonymCandidate.getCat().equals(SynonymCategory.SIMILARITY)
			// || (lld.getDistance(groupNeu.getFirstTarget(), synonymCandidate.getSynonymTag()) >= LIMIT))) {
			//
			// groupNeu.addSourceInfoDto(synonymCandidate.getSynonymTag(), synonymCandidate.getId());
			// updateDataObjects(synonymCandidate, groupNeu);
			//
			// } else {
			// return false;
			// }
			//
			// } else if (synonymCandidate.getRanking() >= LIMIT && groupNeu.getIdList().size() < 3) {
			// groupNeu.switchTargetToSourceAndAddNewTarget(synonymCandidate.getTag(), synonymCandidate.getSynonymTag(), synonymCandidate.getId());
			// updateDataObjects(synonymCandidate, groupNeu);
			// }
			//
			// else if (groupNeu.getIdList().size() > 3) {
			// addNewSynonymGroup(synonymCandidate.getId(), synonymCandidate);
			// } else {
			// ansonsten einfach hinzufügen
			// groupNeu.addSourceInfoDto(synonymCandidate.getSynonymTag(), synonymCandidate.getId());
			// groupNeu.addSourceInfoDto(synonymCandidate.getTag(), synonymCandidate.getId());
			// // ?
			// updateDataObjects(synonymCandidate, groupNeu);
		}
		// }

		// }
		return false;
	}

	private void updateDataObjects(SynonymCandidate dto, GroupNeu groupNeu) {

		synonymGroups.put(groupNeu.getGroupId(), groupNeu);
		table.put(dto.getTag(), dto.getSynonymTag(), groupNeu.getGroupId());
		listOfNotUsedTags.remove(dto.getTag());
		listOfNotUsedTags.remove(dto.getSynonymTag());

		if (PRINT_DELICATE_TAGS) {

			List<String> delicateTagList = Arrays.asList(delicateTags);
			if (approvedfinished && (delicateTagList.contains(dto.getTag()) || delicateTagList.contains(dto.getSynonymTag()))) {
				System.out.println("delicate tags: " + dto.getTag() + " " + dto.getSynonymTag());
				System.out.println(groupNeu.toString());
				System.out.println();
			}
		}

		// if (groupNeu.getGroupId().equals(465) || groupNeu.getGroupId().equals(466)) {
		// System.out.println("delicate tags: " + dto.getTag() + " " + dto.getSynonymTag());
		// System.out.println(groupNeu.toString());
		// System.out.println();
		// }

		if (listOfNotUsedTags.size() % 1000 == 0) {
			System.out.println(listOfNotUsedTags.size());
		}
	}

	private void updateDataObjectsSpecial(SynonymCandidate dto, String target, GroupNeu groupNeu) {

		synonymGroups.put(groupNeu.getGroupId(), groupNeu);
		table.put(dto.getTag(), target, groupNeu.getGroupId());
		table.put(dto.getSynonymTag(), target, groupNeu.getGroupId());
		listOfNotUsedTags.remove(dto.getTag());
		listOfNotUsedTags.remove(dto.getSynonymTag());

		if (listOfNotUsedTags.size() % 1000 == 0) {
			System.out.println(listOfNotUsedTags.size());
		}
	}

	private EList<Integer> getIdsForGroupBySource(String source) {

		EList<Integer> ids = new EArrayList<>();
		for (Table.Cell<String, String, Integer> cell : table.cellSet()) {
			if (cell.getRowKey().equals(source)) {
				ids.add(cell.getValue());
			}

		}
		return ids;
	}

	private void init() {

		allGroupedTagNames = new HashSet<>();
		synonymGroups = new HashMap<>();
		table = HashBasedTable.create();
		usedSynonymCandidateIds = new HashSet<>();
		// v = new Version();
		if (ONLY_ANDROID) {

			// synonymCandidates = HelperDBHandler.getSynonymCandidatesByMinimumRanking(LIMIT);
			if (selectedTagsOnly) {
				// applyFilter();
				// applySelection(FILTER_TOP);
			}
			synonymCandidates = HelperDBHandler.getSynonymCandidatesAndroidByMinimumRanking(LIMIT);
			// synonymCandidateMap = HelperDBHandler.getAllSynonymCandidatesAndroid();

			approvedSynonymPairs = HelperDBHandler.getAllApprovedSynonymPairs(0, 5000);
			// alles wissen das wir haben nutzen!!
			// approvedSynonymPairs = HelperDBHandler.getAllApprovedSynonymPairsForAndroid(5000);
			listOfNotUsedTags = new TreeSet<String>();
			listOfNotUsedTags.addAll(HelperDBHandler.getAllTagsForAndroid());
			for (SynonymCandidate sc : synonymCandidates) {
				if (!listOfNotUsedTags.contains(sc.getTag())) {
					listOfNotUsedTags.add(sc.getTag());
				}
				if (!listOfNotUsedTags.contains(sc.getTag())) {
					listOfNotUsedTags.add(sc.getSynonymTag());
				}

			}

		} else {
			// topRankingSynonymCandidates =
			// HelperDBHandler.getTopRankingSynonymCandidates();
			// chainsForTopCategories =
			// HelperDBHandler.getChainsForTopCategories();
			synonymCandidateMap = HelperDBHandler.getAllSynonymCandidates();

			approvedSynonymPairs = HelperDBHandler.getAllApprovedSynonymPairs(0, 5000);
		}

		// TODO remove
		if (selectedTagsOnly) {
			// applyFilter();
			// applySelection(FILTER_TOP);
		}
		words = "distributed-cache in-house-distribution data-distribution-service normal-distribution distribute distributed-filesystem distributed-system distributed-computing ad-hoc-distribution software-distribution distributed-transactions distributed-caching";
		// words += "distributed-cache in-house-distribution data-distribution-service normal-distribution distribute distributed-filesystem distributed-system
		// distributed-computing ad-hoc-distribution software-distribution distributed-transactions distributed-caching";
		delicateTags = words.split(" ");

	}

	private void applySelection(int amount) {
		synonymCandidates = synonymCandidates.getTopOfList(amount);
	}

	private void applyFilter() {

		String allTagsToInclude = "signedness feed-forward lazy-loading form-load dynamicpdf dragonfire-sdk data-persistence mongo-c-driver managed-extensions drupal-contact-form drupal-fivestar slow-load 2d-engine digital-logic dragenter driving-directions distinct-values timed system.drawing layout-manager yui-grids loading linked-tables dawg user-management time-travel random-seed default-programs double-checked lines release-management first-time dynamic-ui check-unit-testing custom-draw fibonacci-heap drupal-exposed-filter django-authentication let-binding activation-record drupal-file-api active-directory http-head linked-list 2d-3d-conversion load-time element-binding drag-and-drop directional-light time-series d real-time-strategy soft-real-time select-for-xml driving-distance select-n-plus-1 remote-desktop python-bindings disaster-recovery distributed-filesystem data-files pgp-desktop dynamic-websites code-signing aspect-oriented sql-manager dynamics-nav dot-command mastercard asset-management 10gen-csharp-driver android-input-method jboss-weld google-tag-manager long-click oracle-padding-exploit do-not-track bing forward-slash dd-belatedpng dd-wrt dbg urlread image-load data-access do-notation link-tag declared-property cpu-load webviewrunonuithread link-header create-directory dex-limit cpu-time data-caching default-parameters min-heap js-test-driver directed-acyclic-graphs download-manager delimited douglas-peucker select2 unix-time image-loading data-structures lift-record link-to link-local amazon-device-messaging dds facebook-public-feed-api content-management-system dropbox-php collection-select data-scrubbing numeric-keypad xor-drawing pad check-constraints invision-power-board cd-drive dynamics-crm bootstrap-select time-estimation drupal-batch java-time filter-driver memory-management dynamic-sql pay-per-click neomad directory-permissions google-desktop 3d-studio android-activityrecord batch-drawing heap-fragmentation data-recovery chunked heap-dump dynamic-class-loaders drupal-zen loading-image data-compression taphold bleno declarative-security drupal-form-submission load-path clipped devise-confirmable do-while perfect-forwarding group-by-time-interval html-heading dynamic-languages managed-directx jar-signing select-until google-breakpad forward-list drupal-themes window-managers dynamics-crm-4 recorder data-mining screen-record high-load code-behind heading 3d-model build-time read-unread device-context razor-grid divide-by-zero custom-binding jquery-lazyload time-wait dev-to-production device-manager account-management smartpart eager-loading distance audio-recording protected-mode contactless-smartcard 3ds drupal-roles url-link discrete-mathematics device-emulator-manager data-hiding binary-heap droidex exceed cd-burning drupal-render drag-to-select runcommand drupal-blocks datatype discussion-board drupal-content-types distributed-programming desktop-recording large-object-heap function-binding default-document jopenid contextual-binding using-directives desktop-wallpaper management-pack logstash-forwarder struts2-jquery-grid changed dynamic-data 3d-reconstruction dead-reckoning directory-traversal click-through keypad configuration-management wice-grid request-timed-out snap-to-grid minmax-heap time-hires declarative django-1.2 derived-table devise-async code-signing-certificate delay-load drag-short-listview gamesalad single-threaded boost-date-time multiple-records odbc-sql-server-driver a-records grit device-tree time-bomb significant-digits grip delphi-xe multiple-dispatch page-load-time html-select distribute color-blending select-menu appmethod distinct-on grib event-id matlab-load map-directions applaud subsonic-active-record clicking digital-ocean default-browser megaupload drop-table dbaas rooted-device anaglyph-3d double-submit-problem live-cd feeds azure-active-directory multiple-select-query column-defaults access-denied data-members data-url video-recording cocoa-bindings data-uri problem-steps-recorder daydream sanity-check drawertoggle selection-object drupal-taxonomy delete-directory development-environment data-entry xml-declaration database-link select-into-outfile sql-default-instance browser-link dynamic-tables drupal-hooks distributed-cache demos loaded devise-invitable right-justified devices event-dispatching name-binding preprocessor-directive drawingcache time-precision power-management method-declaration smart-device change-script desk-check direct-composition 2d-context-api chained-select drupal-menu angularjs-directive device-width compile-time-weaving zend-form-select dom-selection 3d-rendering dumpsys scala-dispatch drupal-fields select-case early-binding drupal-commerce management-studio drupal-rules data-integrity file-not-found mex-bindings application-management fast-forward click-framework default-implementation extensible-records select-tag git-detached-head d2xx drupal-field-api time-format factory-method static-linking distributed-caching package-managers derivative zero-pad drupal-domain-access disable-caching checked-exceptions domain-driven-design double-byte data-formats ringdroid activity-manager droid-fu data-access-layer quartz-2d gdirections null-check tsung-recorder django-select-related drupal-nodes iphone-softkeyboard dos-donts time-limiting error-checking system.drawing.imaging manager-app design-time-data dynamic-data-display double-elimination data-partitioning signed-applet wifi-direct rawcontactid directed-graph html-head dashlet multi-device-hybrid-apps rubber-band dropbox-api jquery-mobile-grid dot-matrix key-management defaulted-functions data-loss dot-notation data-interchange input-devices pre-signed-url windows-desktop-gadgets delayed-loading data-visualization css-grids digital-downloads nintendo-ds device data-stream kendo-grid gps-time method-dispatch default-scope device-emulation jquery-load osx-leopard compilation-time default-package data-manipulation 3d-secure google-cardboard http-chunked firm-real-time check-constraint draw late-static-binding ng-grid load-generator foreground-service select-xml remote-management resource-management chrome-web-driver data-binding deriving dynamic-dispatch drupal-feeds django-wsgi select-to-slider android-avd xml-dtd dev-phone objectify dummy-data drivers dynamic-analysis drupal-cache dynamic-url point-in-time time-complexity window-load data-acquisition drupal-form-validation wall-time dynamic-allocation zk-grid drag data-type-conversion data-loading angular-ui-grid objective-grid grid-computing django-ajax-selects kiicloud active-record-query state-management design-by-contract declare-styleable google-openid async-loading go-cd google-direction dynamic-text link-to-function derived-instances drupal-gmap documentsdirectory datastax-java-driver piracy-protection heat data-uri-scheme menuitem-selection head delphi-xe6 time-saving delphi-xe7 delphi-xe5 vb4android drupal-6 drupal-permissions drupal-5 code-signing-entitlements drupal-8 delphi-xe2 drupal-7 spring-loaded 34grid fault-tolerant-heap digital-certificate roulette-wheel-selection device-node wii-balanceboard process-management dinamico java2word property-binding single-sign-on clj-time extjs-grid database-management server-load dataform mvccontrib-grid j-security-check internal-link double-free zend-db-select dcmtk device-orientation app2sd distributed-objects digital-filter drupal-input-format protect-from-forgery dynamic-arrays class-loading time-management droidgap getpasswd drupal-alter hard-real-time ld-preload path-2d single-dispatch change-notification load-factor forgot-password grid-system change-data-capture 2-digit-year double-compare-and-swap custom-backend python-click programmatically-created password-protection data-paging desktop-application declarative-authorization data-extraction background-thread load-time-weaving selectable deferred-loading dynamic-splash-screen libpd deep-linking motorola-droid task-manager iosched select-string forward-compatibility 3d-engine mvxbind sigma-grid-control linux-device-driver dolby-audio-api data-link-layer variable-declaration durandal-navigation deep-copy desktop-integration objectdb drop-shadow weak-head-normal-form d-star tiled dynamic-columns desktop-shortcut digit derived-class device-name derived-types csrf-protection assembly-binding-redirect dreamservice code-management parent-child dual-monitor htc-dual-lens-sdk device-detection d-pad double-submit-prevention data-transfer dot-emacs android-download-manager rest-assured video-card directx-9 file-management non-well-formed source-control-bindings domain-model directed-identity utility-method double-click drupal-theming connection-manager select-statement objectal failed-to-load-viewstate compile-time-constant ng-bind-html disabled-control device-controller sign-extension dev-null 3d-modelling drupal-entities distributed-computing data-collection d-pointer dynamically-generated deep-learning selenium-grid file-read max-heap d1 d2 default-method telerik-grid declarative-services tiling canonical-link messageid managed-jscript delimited-text deep-web dotnet-httpclient adaptive-threshold location-based dailymotion-api near-real-time load-order universal-time default-copy-constructor 7digital rtmpd linkmovementmethod data-capture heap-memory bevelled graph-drawing time-and-attendance drupal-regions dynamics-crm-2011 windows-live-id heap-corruption data-synchronization virtual-device-manager linked-server data-modeling load-testing cuda-driver-api spotify-desktop django-piston apache-commons-fileupload android-background dynamic-loading dependency-management dollar-sign dot-product distributed-testing drupal-services dynamic-library design-principles time-trial dot42 appcloud design-time django-rest-framework htmlunit-driver logon-failed drupal-ajax change-management data-encryption desktop-sharing number-with-delimiter dds-format timed-events check-framework manager data-connections libcmtd managed-c++ dymamic spell-checking android-softkeyboard google-license-manager dual-sim load-balancing device-driver directory-structure roundedcorners-dropshadow null-terminated puid durandal-2.0 mx-record drupal-webform drupal-smart-ip grid-layout record-count desktop-search select-insert thinkpad jeet-grid dirtyrectangle owl-time scala-time records jquery-mobile-select davinci delphi-2006 3d-array pairing-heap diffie-hellman delete-record design-guidelines 3d-graphics protect digital-design android-keypad mate-desktop dynamic-variables managed-ews double-quotes double-underscore smartcard-reader bulk-load time-t dispatch-async dvi data-sharing clock sd-card ui-thread soft-heap jquery-click-event declarative-programming drupal-field-collection deep-cloneable double-dispatch disable-link dead-code drupal-views drupal-contextual-filters developer-payload change-tracking osx-snow-leopard unique-id copy-protection dolphin-browser default-constraint twilio-click-to-call linked-data event-binding dolby directions erlang-driver testdroid onutterancecompleted grand-central-dispatch insert-select android-package-managers protection dynamic-programming angularjs-ng-click feature-selection design-patterns subsonic-select dispatch-table 2d-games background-foreground heap-randomization real-time-systems multiple-select time-measurement data-consistency stereo-3d dynamic-folders heap-profiling real-time-text divide-and-conquer digital-signature drupal-views-relationship message-forwarding dynamic-memory-allocation easy-digital-downloads data-management selected django-views driver-signing digital-assets drives java-3d blending default-database data-storage default-value linking-errors magic-draw droidquery google-ad-manager dexopt default-arguments django-imagefield android-sdcard cpu-speed friendly-id google-directory-api managed-file drupal-spam device-instance-id denial-of-service smd text-based directory-browsing one-click-web-publishing default-selected link-checking loading-animation diskcache type-ahead android-handlerthread drop-down-menu late-binding crystal-space-3d xaml-binding defaults device-mapper home-directory forward wayland binomial-heap 2d sum-of-digits data-retrieval donut-chart chunks digital-compass dynamic-cast project-management xml-binding at-sign app-manager ember-select disparity-mapping mdsd dynamic-controls declare drupal-schema protected-resource dynamic-values dtd-parsing management-studio-express ejabberd-http-bind google-feed-api java-2d drupal-forms shiva3d drupal-comments language-binding double-splat sim-card django-tastypie developer-tools digital django-csrf distributed-system directory-listing database-managers forward-declaration assembly-signing managment 3d-texture data-migration selection drupal-templates inventory-management doby-grid selection-sort jquery-grid disable-app line-drawing chunked-encoding recording data-exchange direct-path voice-recording django-endless-pagination real-time-data data-protection dexmaker clicked delphi-6 delphi-7 dtn desktop-publishing vudroid delphi-prism timing jquery-ui-selectable static-binding dynamic-binding 4d-database select-query econnrefused dynamic-script-loading datamatrix demo-effects development-process date-format drupal-behaviors execution-time worker-thread drupal-db-api email-forwarding designtime-attributes credit-card droidparts grouped-collection-select load-data-infile double-buffering news-feed selection-border drupal-filefield 4d securid forward-engineer levenshtein-distance real-time dispatch twitter-feed app-id page-loading-message double-checked-locking protected-folders change-password hardware-id link-to-remote select-syscall bitwise-and web-based dial-up click-tracking block-device dynamically-loaded-xap 3d mraid google-desktop-search data-analysis right-click zend-feed blind disabled-input iphone-keypad beans-binding sender-id digital-analog-converter android-nsd analog-digital-converter expression-blend default-constructor double-precision double-pointer managed-code directories one-time-password drupal-search assembly-loading x11-forwarding public-method conditional-binding permission-denied deep-clone resource-loading time-select divider director drupal-commons 2d-vector datadroid dotted-line file-manager delimited-continuations drupal-modules mysql-select-db negative-lookahead reference-binding posix-select redeclaration draw-list dijit dynamic-list objectaid androguard gwt-dispatch delay-sign drupal-panels justinmind boost-thread 2.5d pre-packaged record-locking fortran-iso-c-binding device-admin well-formed self-signed select-into last-modified resource-id durability signed singly-linked-list tapandhold data-security cd-rom digest-authentication response-time color-management dynamic-content orientation-changes check-digit dynamic-linking mutated generic-method syndication-feed launch-time edirectory managed-bean drupal-boost desktop-background data-transfer-objects signing android-audiorecord ext-direct pre-3d virtual-desktop ms-word user-defined linked blend-sdk dataflex datetime-select text-driver dynamic-compilation multi-select visual-d editfield display-picture eval-after-load time-tracking stalled fixed-length-record aol-desktop data-conversion double-brace-initialize dot-operator 2d-array double-click-advertising real-time-clock heap-size drupal-multi-domain drupal-distributions real-time-updates drupal-jcarousel domain-name file-link direction forward-delete do-loops display-manager drupal-preprocess external-links depth-buffer declarations time-frequency dibs dl-dt-dd blend-2012 signed-assembly forward-reference mongo-scala-driver managed-property django-allauth android-2.3-gingerbread device-policy-manager drupal-fapi off-the-record-messaging heap-pollution click-counting findfragmentsbyid compile-time";
		Set<SynonymCandidate> filteredCandidates = new HashSet<>();

		Set<String> tags = new HashSet<>();
		tags.addAll(Arrays.asList(allTagsToInclude.split(" ")));

		// filter all synonym candidates
		for (SynonymCandidate sc : synonymCandidates) {
			if (tags.contains(sc.getTag()) || tags.contains(sc.getSynonymTag())) {
				filteredCandidates.add(sc);
			}
		}
		(synonymCandidates = new EArrayList<SynonymCandidate>()).addAll(filteredCandidates);

		// filter approved synonym pairs
		EList<SynonymCandidate> filteredApprovedCandidates = new EArrayList<>();
		for (SynonymCandidate sc : approvedSynonymPairs) {
			if (tags.contains(sc.getTag()) || tags.contains(sc.getSynonymTag())) {
				filteredApprovedCandidates.add(sc);
			}
		}
		approvedSynonymPairs = filteredApprovedCandidates;

		// filter list of tags
		listOfNotUsedTags.clear();
		listOfNotUsedTags.addAll(tags);

		// correct:
		// gnu-classpath - gnu
		//

	}

	private void setHeaderOfTrendsFile(String path, String time) throws IOException {
		FileWriter writer2 = new FileWriter(path + time + "trends.csv", true);

		writer2.append("category");
		writer2.append(",");
		writer2.append("date");
		writer2.append(",");
		writer2.append("postCount");
		writer2.append(",");
		writer2.append("answerCountSum");
		writer2.append(",");
		writer2.append("answerCountAvg");
		writer2.append(",");
		writer2.append("commentCountSum");
		writer2.append(",");
		writer2.append("commentCountAvg");
		writer2.append(",");
		writer2.append("acceptedAnswersCountSum");
		writer2.append(",");
		// writer2.append("acceptedAnswersCountAvg");
		// writer2.append(",");
		writer2.append("openAnswerCountSum");
		writer2.append(",");
		writer2.append("openAnswerCountAvg");
		writer2.append(",");
		writer2.append("favoriteCountSum");
		writer2.append(",");
		writer2.append("favoriteCountAvg");
		writer2.append(",");
		writer2.append("scoreCountSum");
		writer2.append(",");
		writer2.append("scoreCountPos");
		writer2.append(",");
		writer2.append("scoreCountNeg");
		writer2.append("\n");
		writer2.flush();
		writer2.close();
	}

	private <E> String collectionItemsToString(Collection<E> coll) {
		StringBuilder sb = new StringBuilder();
		for (Object oo : coll) {
			sb.append((oo.toString() + " "));
		}
		return sb.toString();
	}

	private void exportTrendsPerGroupToCsv(EList<TrendDto> trends, String label, String path, String time) {

		try {
			FileWriter writer = new FileWriter(path + time + "trends.csv", true);

			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

			for (TrendDto trend : trends) {
				writer.append(label);
				writer.append(',');
				String date_ = sdf.format(trend.getDate());
				writer.append(date_);
				writer.append(',');
				writer.append(trend.getPostCount().toString());
				writer.append(',');
				writer.append(trend.getAnswerCountSum().toString());
				writer.append(',');
				writer.append(trend.getAnswerCountAvg().toString());
				writer.append(',');
				writer.append(trend.getCommentCountSum().toString());
				writer.append(',');
				writer.append(trend.getCommentCountAvg().toString());
				writer.append(',');
				writer.append(trend.getAcceptedAnswerCountSum().toString());
				writer.append(',');
				// writer.append(trend.getAcceptedAnswerCountAvg().toString());
				// writer.append(',');
				writer.append(trend.getOpenAnswerCountSum().toString());
				writer.append(',');
				// writer.append(trend.getOpenAnswerCountAvg().toString());
				// writer.append(',');
				writer.append(trend.getFavouriteCountSum().toString());
				writer.append(',');
				writer.append(trend.getFavouriteCountAvg().toString());
				writer.append(',');
				writer.append(trend.getScoreCountSum().toString());
				writer.append(',');
				writer.append(trend.getScoreCountPos().toString());
				writer.append(',');
				writer.append(trend.getScoreCountNeg().toString());
				writer.append(',');
				writer.append(trend.getViewCountSum().toString());
				writer.append(',');
				writer.append(trend.getViewCountAvg().toString());
				writer.append('\n');
			}

			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void groupApprovedSynonyms() {

		EList<SynonymCandidate> versions = new EArrayList<>();
		for (SynonymCandidate dto : approvedSynonymPairs) {

			if (!addToSynonymGroups(dto)) {
				versions.add(dto);
			}
		}

		for (SynonymCandidate dto : versions) {
			secondAddingToSynonymGroups(dto);
		}

	}

	/**
	 * adds a synonym candidate to the synonym groups
	 *
	 * candidate dto
	 */
	private Boolean addToSynonymGroups(SynonymCandidate synonymCandidate) {
		// get groupId if any exists
		String synonym = synonymCandidate.getSynonymTag();
		Integer groupId = getIdForGroupByTarget(synonym);

		// no groupId exists
		if (groupId == -1) {

			// check ob schon beide vorhanden?
			Set<SynonymCandidate> synCanSet = new HashSet<>();
			synCanSet.add(synonymCandidate);
			if (checkAlreadyBothInGroup(synCanSet).size() != 0) {

				// check for version
				if (synonym.matches(".*\\d\\.?\\d*.*")) {
					return false;
				} else {
					addNewApprovedSynonymCandidate(synonymCandidate);
				}
			}

		} else {

			addToExistingGroup(groupId, synonymCandidate);
			// groupId exists

		}
		return true;

	}

	private int synonymCandidateId = 0;

	private Integer addNewApprovedSynonymCandidate(SynonymCandidate synCan, String synonym, Integer synonymCandidateId) {

		GroupNeu group = new GroupNeu();
		group.addSourceAndTargetWithId(synCan.getTag(), synonym, synonymCandidateId);
		groupIdCount++;
		group.setGroupId(groupIdCount);
		group.setCreatedByApprovedSynonyms();

		SynonymCandidate scanNeu = new SynonymCandidate(synCan.getTag(), synonym, synCan.isTagSource());
		scanNeu.setId(synonymCandidateId);

		updateDataObjects(scanNeu, group);
		return groupIdCount;
	}

	private Integer addNewApprovedSynonymCandidate(SynonymCandidate synCan) {
		return addNewApprovedSynonymCandidate(synCan, synCan.getSynonymTag(), synCan.getId());
	}

	private void secondAddingToSynonymGroups(SynonymCandidate synonymCandidate) {

		// zahlen entfernen
		// String synonym = synonymCandidate.getSynonymTag().replaceAll("\\d\\.x", "");

		String replacenemt = "";
		if (synonymCandidate.getSynonymTag().matches(".+-\\d+\\.?-?\\d?(-.+)")) {
			replacenemt = "-";
		}

		String synonym = synonymCandidate.getSynonymTag().replaceAll("-\\d+\\.?-?\\d?(-|$)", replacenemt);

		if (synonym.matches(".+-\\d\\.x")) {
			synonym = synonym.replaceAll("-\\d\\.x", "");
		}

		// dirty workaround - zu faul zum regexbasteln
		if (synonym.endsWith("-")) {
			synonym = synonym.substring(0, synonym.length() - 1);
		}

		if (synonymCandidate.getSynonymTag().length() > 1 && synonym.length() == 1) {
			synonym = synonymCandidate.getSynonymTag();
		}

		// finde gruppe mit target ohne zahlen?
		int groupId = getIdForGroupByTarget(synonym);

		if (groupId != -1) {
			addToSourcesOfExistingGroups(groupId, synonymCandidate);
		} else {
			// check ob es den tag sonst auch gibt
			Tag tagByName = DBHandler.getTagByName(synonym);
			if (tagByName.getId() != -1 || makesTagSense(synonym, synonymCandidate.getSynonymTag())) {
				groupId = addNewApprovedSynonymCandidate(synonymCandidate, synonym, synonymCandidateId--);

				addToSourcesOfExistingGroups(groupId, synonymCandidate);

			}
			// tag - zahlen gibt es ohnehin nicht, dann hinzufügen
			else {

				addNewApprovedSynonymCandidate(synonymCandidate);

			}

		}

	}

	private boolean makesTagSense(String synonym, String oldSynonym) {
		// System.out.println("synonym: " + synonym + " - macht das sinn? y,n (old: " + oldSynonym + ")");
		// Scanner in = new Scanner(System.in);
		// String sense = in.nextLine();
		// if (sense.equals("y")) {
		// return true;
		// }
		//
		// return false;
		return true;
	}

	private void addToSourcesOfExistingGroups(Integer groupId, SynonymCandidate synonymCandidate) {

		GroupNeu group = synonymGroups.get(groupId);

		group.addSourceInfoDto(synonymCandidate.getTag(), synonymCandidate.getId());
		group.addSourceInfoDto(synonymCandidate.getSynonymTag(), synonymCandidate.getId());

		synonymGroups.remove(group);
		// update data objects and add updated group again
		updateDataObjects(synonymCandidate, group);

	}

	private void addToExistingGroup(Integer groupId, SynonymCandidate synonymCandidate) {
		GroupNeu group = synonymGroups.get(groupId);
		// group.addSynonymCandidate(synonymCandidate);
		group.addSourceInfoDto(synonymCandidate.getTag(), synonymCandidate.getId());
		// remove existing group
		synonymGroups.remove(group);
		// update data objects and add updated group again
		updateDataObjects(synonymCandidate, group);

	}

	private Integer getIdForGroupByTarget(String target) {

		EList<Integer> ids = new EArrayList<>();
		for (Table.Cell<String, String, Integer> cell : table.cellSet()) {
			if (cell.getColumnKey().equals(target) && cell.getValue() != -1) {
				ids.add(cell.getValue());
			}
		}
		if (ids.size() > 0) {
			return ids.get(0);
		}

		return -1;
	}

	/**
	 * returns the id of the synonymcandidates for a defined target and a sourcelist
	 * 
	 * @param sourceList
	 * @param target
	 * @return
	 */
	private List<Integer> getSynonymCandidateForSourceAndTarget(Collection<String> sourceList, String target) {

		List<Integer> returnList = new ArrayList<>();
		for (String source : sourceList) {
			SynonymCandidate sc = HelperDBHandler.getInfoDtoForSourceTarget(source, target, LIMIT);
			if (sc != null) {
				returnList.add(sc.getId());
			}
		}
		return returnList;
	}

	private Integer getGroupBySourceTagPair(String source1, String source2) {

		for (Entry<Integer, GroupNeu> entry : synonymGroups.entrySet()) {

			boolean foundSource1 = false;
			boolean foundSource2 = false;

			for (String source : entry.getValue().getSourceList()) {
				if (source1.equals(source)) {
					foundSource1 = true;
				} else if (source2.equals(source)) {
					foundSource2 = true;
				}
				if (foundSource1 && foundSource2) {
					return entry.getKey();
				}
			}
		}

		return -1;
	}

	public String getPathToFile() {
		return path + time;
	}

}
