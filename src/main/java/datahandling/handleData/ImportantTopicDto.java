package datahandling.handleData;

import tagsynonymprediction.utility.EArrayList;
import tagsynonymprediction.utility.EList;

import java.util.Collections;



public class ImportantTopicDto {

	// groupId, count
	private EList<ImportantTopicEntry> entryList;

	public ImportantTopicDto() {
		entryList = new EArrayList<>();
	}

	public void addEntry(Integer groupId, Integer count) {
		entryList.add(new ImportantTopicEntry(groupId, count));
	}

	public EList<Integer> getIdsOfMostImportantTopics() {
		Collections.sort(entryList);
		EList<Integer> returnList = new EArrayList<>();
		for (ImportantTopicEntry entry : entryList.getTopOfList(21)) {
			returnList.add(entry.getGroupId());
		}

		return returnList;
	}

	private class ImportantTopicEntry implements Comparable<ImportantTopicEntry> {
		private Integer groupId;

		private Integer count;

		public ImportantTopicEntry(Integer groupId, Integer count) {
			this.groupId = groupId;
			this.count = count;
		}

		public Integer getGroupId() {
			return groupId;
		}

		public void setGroupId(Integer groupId) {
			this.groupId = groupId;
		}

		public Integer getCount() {
			return count;
		}

		public void setCount(Integer count) {
			this.count = count;
		}

		@Override
		public int compareTo(ImportantTopicEntry o) {

			return o.count - this.count;
		}

	}
}
