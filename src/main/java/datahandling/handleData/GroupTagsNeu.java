package datahandling.handleData;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.lucene.search.spell.LuceneLevenshteinDistance;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Multimap;
import com.google.common.collect.Table;

import helper.HelperDBHandler;
import helper.TrendDto;
import tagsynonymprediction.categories.maincat.Version;
import tagsynonymprediction.db.DBHandler;
import tagsynonymprediction.models.SynonymCandidate;
import tagsynonymprediction.models.SynonymCategory;
import tagsynonymprediction.models.Tag;
import tagsynonymprediction.utility.EArrayList;
import tagsynonymprediction.utility.EList;

public class GroupTagsNeu {

	private final Boolean ONLY_ANDROID = true;

	public static void main(String args[]) {
		new GroupTagsNeu().startGroupingOfTags();
	}

	// liste aller bereits groupierten tags
	private Set<String> allGroupedTagNames;
	private HashMap<Integer, GroupNeu> synonymGroups;
	private Map<Integer, SynonymCandidate> synonymCandidateMap;
	private EList<SynonymCandidate> approvedSynonymPairs;
	private Set<SynonymCandidate> synonymCandidates;
	private Set<Integer> usedSynonymCandidateIds;
	// source target id
	private Table<String, String, Integer> table;
	private int groupIdCount = 0;
	private ImportantTopicDto topicPostcount = new ImportantTopicDto();

	private Boolean saveIntoFile = false;
	private String time;
	private final String path = "/Users/stefanie/Documents/workspace_mars/TagSynonymTools/src/main/resources/generatedData";

	private Set<String> listOfNotUsedTags;
	private boolean selectedTagsOnly = true;
	private Version v;

	public void setSelectedTagsOnly(Boolean selectedTagsOnly) {
		this.selectedTagsOnly = selectedTagsOnly;
	}

	public void startGroupingOfTags() {
		// collect all synonym pairs for each tag and put them into a table

		init();

		// add all approved synonym pairs to synonymGroups8
		groupApprovedSynonyms();

		// get groups with only one source and target
		// Map<Integer, GroupNeu> singleGroups =
		// getGroupsWithOneSourceAndTarget();

		// TODO bottleneck!
		// connect groups
		// findBridgesBetweenGroups();

		// todo evtl ausschließen diese synonymcandidaten die bei den brücken
		// dazugekommen sind

		// Set<SynonymCandidate> remainingSynonymCandidates =
		// iterateSynonymCandidates(synonymCandidates, 1.0, 0.65);

		Set<SynonymCandidate> remainingSynonymCandidates = assignSynonymCandidatesToGroups(synonymCandidates, 1.0, 0.65);

		// tests(synonymGroups);

		// getGroupsWithOneSourceAndTarget();

		// add chains of the top categories
		// addChainsForTopCategories();

		// singleGroups = getGroupsWithOneSourceAndTarget();

		// // regroup
		// // regroup(singleGroups);
		//
		// // addTopRankingSynonymPairs(true);
		//
		// // addSynonymCandidatesToSynonymGroupTargets();
		//
		// // remainingSynonymCandidates =
		// getSynoymCandidatesForTagsNotCoveredAlready();

		// umschlichten

		// TODO check coverage of all tags
		// System.out.println("unused tags: " + listOfNotUsedTags.size());
		// Set<SynonymCandidate> uncoveredSynonymCandidates =
		// HelperDBHandler.getSynonymCandidatesForUnusedTags(listOfNotUsedTags);
		// iterateSynonymCandidates(uncoveredSynonymCandidates, 1.0, 0.44);

		// TEST print

		for (Entry<Integer, GroupNeu> entry : synonymGroups.entrySet()) {
			System.out.println(entry.getValue().toString() + "\n");
		}

		// add selected synonym candidates
		// addSelectedSynonymCandidates();
		//

		if (saveIntoFile) {
			time = System.currentTimeMillis() + "";
			saveAllGroupsToCsv(synonymGroups.keySet());
			// getTrendsPerGroup(topicPostcount.getIdsOfMostImportantTopics());

		}

		// selectMostImportantTopics();

		// exemplarisch für postcount

		System.out.println("finally");
		System.out.println("unused tags: " + listOfNotUsedTags.size());
		System.out.println(listOfNotUsedTags.toString());
		System.out.println(remainingSynonymCandidates.size());
		System.out.println(synonymGroups.size());
	}

	public void saveIntoFile(Boolean save) {
		this.saveIntoFile = save;
	}

	private void saveAllGroupsToCsv(Set<Integer> groupIds) {
		try {

			FileWriter writer = new FileWriter(path + time + "groups.txt", true);

			setHeaderOfTrendsFile(path, time);

			// get trends
			for (Integer groupId : groupIds) {
				GroupNeu group = synonymGroups.get(groupId);

				Set<String> tags = group.getTargetList();
				tags.addAll(group.getSourceList());

				// System.out.println((tags));
				writer.append(collectionItemsToString(tags));
				writer.append("\n");

				EList<TrendDto> trends = HelperDBHandler.getTrendsForGroupedSynonyms(tags);

				exportTrendsPerGroupToCsv(trends, collectionItemsToString(group.getTargetList()), path, time);

			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void findBridgesBetweenGroups() {
		Set<Integer> groupsToRemove = new HashSet<>();
		Multimap<SynonymCandidate, GroupNeu> mm = ArrayListMultimap.create();

		Multimap<Integer, String> tableUpdate = ArrayListMultimap.create();

		Collection<GroupNeu> groups = synonymGroups.values();

		// iteriere single group
		for (GroupNeu singleGroup : groups) {
			int firstGroupId = singleGroup.getGroupId();

			// iteriere alle gruppen
			for (GroupNeu secondGroup : synonymGroups.values()) {

				if (singleGroup != secondGroup) {

					// ranking >= 0.75
					SynonymCandidate targetCandidate = HelperDBHandler.getSynonymCandidateBySourceAndTag(singleGroup.getFirstTarget(), secondGroup.getFirstTarget());

					// check ob die beiden gruppen in einer source-target
					// verbindungn sind
					if (targetCandidate != null) {

						// handle singleGroup
						secondGroup.addSourceInfoDto(singleGroup.getFirstSource(), singleGroup.getFirstId());
						groupsToRemove.add(firstGroupId);

						// add targetCandidate
						secondGroup.addSynonymCandidate(targetCandidate);
						usedSynonymCandidateIds.add(targetCandidate.getId());

						// lösche gruppen
						groupsToRemove.add(firstGroupId);

						// aktualisiere table einträge
						tableUpdate.put(firstGroupId, secondGroup.getFirstTarget());

					}
					// else if((targetCandidate =
					// HelperDBHandler.getSynonymCandidateBySourceAndTag(singleGroup.getFirstTarget(),
					// secondGroup.getFirstTarget()))!= null)
					// // wenn sourcegroup != targetgroup und es eine brücke
					// gibt, dann wird diese brücke zum target
					// GroupNeu group = new GroupNeu();
					// group.addTargetInfoDto(targetCandidate.getSynonymTag(),
					// targetCandidate.getId());
					// group.addSourcesToList(singleGroup.getSourceList(),
					// singleGroup.getIdList());
					// group.addSourcesToList(secondGroup.getSourceList(),
					// secondGroup.getIdList());
					//
					// // aktualisiere table einträge
					// tableUpdate.put(firstGroupId,
					// targetCandidate.getSynonymTag());
					// tableUpdate.put(targetGroupId,
					// targetCandidate.getSynonymTag());
					//
					// // lösche gruppen
					// groupsToRemove.add(firstGroupId);
					// groupsToRemove.add(targetGroupId);
					// mm.put(targetCandidate, group);
					// // updateDataObjects(targetCandidate.getId(),
					// targetCandidate, idCount, group);
					// }
				}

			}

		}

		for (Integer groupId : groupsToRemove) {
			synonymGroups.remove(groupId);
		}

		for (Entry<Integer, String> e : tableUpdate.entries()) {

			Integer groupId = getIdForGroupByTarget(e.getValue());
			if (groupId == -1) {
				groupId = groupIdCount++;
			}
			// wenn nichts gefunden????
			updateTableId(e.getKey(), groupId);
		}

		for (Entry<SynonymCandidate, GroupNeu> e : mm.entries()) {

			GroupNeu gn = e.getValue();
			Integer groupId = getIdForGroupByTarget(e.getValue().getFirstTarget());
			gn.setGroupId(groupId);
			updateDataObjects(e.getKey(), e.getValue());
		}

	}

	/**
	 * @returns a map of groupid and groups that contain only one source and one target
	 */
	private Map<Integer, GroupNeu> getGroupsWithOneSourceAndTarget() {
		Map<Integer, GroupNeu> returnMap = new HashMap<>();

		for (Entry<Integer, GroupNeu> entry : synonymGroups.entrySet()) {
			GroupNeu group = entry.getValue();
			// group has only one id and only one target and source
			if (group.getIdList().size() == 1) {
				returnMap.put(entry.getKey(), group);

				// check
				if (group.getSourceList().size() > 1 || group.getTargetList().size() > 1) {
					throw new IllegalArgumentException("das darf nicht sein");
				}
			}
		}

		return returnMap;
	}

	private void addNewSynonymGroup(Integer synCandidateId, SynonymCandidate dto) {
		groupIdCount++;
		int groupId_ = groupIdCount;
		GroupNeu groupNeu = new GroupNeu();
		groupNeu.setGroupId(groupId_);
		groupNeu.addSourceAndTargetWithId(dto.getTag(), dto.getSynonymTag(), groupId_);
		updateDataObjects(dto, groupNeu);
	}

	// private void addChainsForTopCategories() {
	//
	// Set<SynonymCandidate> notFound =
	// iterateCandidateIdsForChains(chainsForTopCategories, false);
	//
	// notFound = iterateCandidateIdsForChains(notFound, false);
	// notFound = iterateCandidateIdsForChains(notFound, true);
	// System.out.println(notFound.size());
	//
	// }

	private Set<SynonymCandidate> assignSynonymCandidatesToGroups(Set<SynonymCandidate> toIterate, double ranking, double rankingLimit) {
		Set<SynonymCandidate> notUsedSynonymCandidates = new HashSet<>();
		Set<SynonymCandidate> checkAgainSynonymCandidates = new HashSet<>();

		// 1 check
		notUsedSynonymCandidates = iterateSynonymCandidatesForExistingAndSameTarget(toIterate);

		if (ranking >= rankingLimit) {
			// es ist nichts weggekommen
			Set<SynonymCandidate> toRemove = new HashSet<>();
			for (SynonymCandidate sc : notUsedSynonymCandidates) {

				Set<SynonymCandidate> scSet = new HashSet<>();

				scSet.add(sc);

				// wenn nichts anderes gefunden worden ist, hinzufügen bei
				// entsprechendem ranking
				boolean found = true;
				if (iterateSynonymCandidatesForExistingAndSameTarget(scSet).size() > 0) {
					if (checkForOtherRelations(scSet).size() > 0) {
						if (sc.getStoredRanking() >= ranking) {
							addNewSynonymGroup(sc.getId(), sc);

						} else {
							// wurde nichts gefunden und aich ranking passt
							// nicht
							// - nicht entfernen
							found = false;
						}
					}
				}
				if (found) {
					toRemove.add(sc);
				}

			}
			notUsedSynonymCandidates.removeAll(toRemove);
			ranking = ranking - 0.01;

			checkAgainSynonymCandidates = iterateSynonymCandidates(notUsedSynonymCandidates, ranking, rankingLimit);
		}

		return checkAgainSynonymCandidates;

	}

	private Set<SynonymCandidate> iterateSynonymCandidatesForExistingAndSameTarget(Set<SynonymCandidate> toIterate) {
		Set<SynonymCandidate> notUsedSynonymCandidatesTarget = new HashSet<>();
		Set<SynonymCandidate> notUsedSynonymCandidatesBoth = new HashSet<>();
		Set<SynonymCandidate> notUsedSynonymCandidates = new HashSet<>();

		int oldSize = toIterate.size();
		int newSize = 0;
		notUsedSynonymCandidates = toIterate;
		while (oldSize > newSize) {
			notUsedSynonymCandidatesTarget = new HashSet<>();
			notUsedSynonymCandidatesBoth = new HashSet<>();
			oldSize = notUsedSynonymCandidates.size();
			notUsedSynonymCandidatesTarget.addAll(checkTargetExIsTargetNew(notUsedSynonymCandidates));
			notUsedSynonymCandidatesBoth.addAll(checkAlreadyBothInGroup(notUsedSynonymCandidatesTarget));

			newSize = notUsedSynonymCandidatesBoth.size();
			notUsedSynonymCandidates = new HashSet();
			notUsedSynonymCandidates.addAll(notUsedSynonymCandidatesBoth);

		}
		// gibt keine neuen mehr

		Set<SynonymCandidate> stillNotUsedSynonymCandidates = new HashSet<>();
		for (SynonymCandidate synCan : notUsedSynonymCandidates) {

			if (!checkAlreadyBothInGroupSingle(synCan)) {
				if (!checkTargetExIsTargetNewSingle(synCan)) {
					stillNotUsedSynonymCandidates.add(synCan);
				}
			}

		}

		return stillNotUsedSynonymCandidates;
	}

	private Set<SynonymCandidate> iterateSynonymCandidates(Set<SynonymCandidate> toIterate, double ranking, double rankingLimit) {
		synonymGroups.size();
		Set<SynonymCandidate> chainsForTopCategoriesLeft = new HashSet<>();
		Set<SynonymCandidate> chainsForTopCategoriesRight = new HashSet<>();
		Set<SynonymCandidate> chainsForTopCategoriesDown = new HashSet<>();

		Set<SynonymCandidate> chainsForTopCategoriesUp = new HashSet<>();

		// already exist group containing both
		// Exist SG: s € SL und t € TL

		chainsForTopCategoriesLeft = checkAlreadyBothInGroup(toIterate);
		chainsForTopCategoriesRight = checkTargetExIsTargetNew(chainsForTopCategoriesLeft);

		// check nachdem gleiche targets vlt waren ob jetzt gleiche gruppen sind
		chainsForTopCategoriesDown = checkAlreadyBothInGroup(chainsForTopCategoriesRight);
		if (chainsForTopCategoriesRight.size() > chainsForTopCategoriesDown.size()) {
			// if (toIterate.size() > chainsForTopCategoriesDown.size()) {
			// es ist was weggekommen -> nochmal schauen
			chainsForTopCategoriesLeft = checkAlreadyBothInGroup(chainsForTopCategoriesDown);

			chainsForTopCategoriesRight = checkTargetExIsTargetNew(chainsForTopCategoriesLeft);
			chainsForTopCategoriesDown = checkAlreadyBothInGroup(chainsForTopCategoriesRight);
			chainsForTopCategoriesUp = iterateSynonymCandidates(chainsForTopCategoriesDown, ranking, rankingLimit);
		} else {
			if (ranking >= rankingLimit) {
				// es ist nichts weggekommen
				Set<SynonymCandidate> toRemove = new HashSet<>();
				for (SynonymCandidate sc : chainsForTopCategoriesDown) {

					Set<SynonymCandidate> scSet = new HashSet<>();
					scSet.add(sc);

					// wenn nichts anderes gefunden worden ist, hinzufügen bei
					// entsprechendem ranking
					boolean found = true;
					if (checkAlreadyBothInGroup(scSet).size() > 0) {
						if (checkTargetExIsTargetNew(scSet).size() > 0) {
							if (checkForOtherRelations(scSet).size() > 0) {
								if (sc.getStoredRanking() >= ranking) {
									addNewSynonymGroup(sc.getId(), sc);

								} else {
									// wurde nichts gefunden und aich ranking
									// passt nicht
									// - nicht entfernen
									found = false;
								}
							}
						}
					}
					if (found) {
						toRemove.add(sc);
					}

				}
				chainsForTopCategoriesDown.removeAll(toRemove);
				ranking = ranking - 0.01;

				// System.out.println("chainsForTopCategoriesRight " +
				// chainsForTopCategoriesRight.size());
				// System.out.println("chainsForTopCategoriesDown " +
				// chainsForTopCategoriesDown.size());

				chainsForTopCategoriesUp = iterateSynonymCandidates(chainsForTopCategoriesDown, ranking, rankingLimit);
			}

		}

		return chainsForTopCategoriesUp;
	}

	private Boolean checkAlreadyBothInGroupSingle(SynonymCandidate synonymCandidate) {
		// achtung speziell for numbers

		String synonymWithoutNumbers = synonymCandidate.getSynonymTag().replaceAll("-?\\d\\.?-?\\d?", "");
		int groupId = getGroupBySourceTagPair(synonymCandidate.getTag(), synonymWithoutNumbers);

		if (groupId != -1) {
			addToSourcesOfExistingGroups(groupId, synonymCandidate);
			return true;
		}
		return false;
	}

	private Set<SynonymCandidate> checkAlreadyBothInGroup(Set<SynonymCandidate> setOfSynonymCandidates) {
		Set<SynonymCandidate> chainsForTopCategoriesLeft = new HashSet<>();

		for (SynonymCandidate synonymCandidate : setOfSynonymCandidates) {
			if (synonymCandidate != null) {
				int groupId = getGroupBySourceTagPair(synonymCandidate.getTag(), synonymCandidate.getSynonymTag());
				if (groupId != -1) {
					GroupNeu groupNeu = synonymGroups.get(groupId);
					groupNeu.addToIdList(groupId);
					SynonymCandidate sc1 = new SynonymCandidate(synonymCandidate.getTag(), groupNeu.getFirstTarget(), true);
					updateDataObjects(sc1, groupNeu);

					SynonymCandidate sc2 = new SynonymCandidate(synonymCandidate.getSynonymTag(), groupNeu.getFirstTarget(), true);
					updateDataObjects(sc2, groupNeu);
				} else {

					chainsForTopCategoriesLeft.add(synonymCandidate);
				}
			}
		}
		return chainsForTopCategoriesLeft;
	}

	private Set<SynonymCandidate> checkTargetExIsTargetNew(Set<SynonymCandidate> synonymCandidates) {

		Set<SynonymCandidate> returnSynonymCandidates = new HashSet<>();

		for (SynonymCandidate synonymCandidate : synonymCandidates) {
			int groupId = getIdForGroupByTarget(synonymCandidate.getSynonymTag());

			// targetEx = targetNew
			if (groupId != -1 && synonymGroups.containsKey(groupId)) {
				GroupNeu groupNeu = synonymGroups.get(groupId);
				groupNeu.addSourceInfoDto(synonymCandidate.getTag(), synonymCandidate.getId());
				updateDataObjects(synonymCandidate, groupNeu);

			} else {
				returnSynonymCandidates.add(synonymCandidate);
			}
		}
		return returnSynonymCandidates;
	}

	private Boolean checkTargetExIsTargetNewSingle(SynonymCandidate synonymCandidate) {

		String synonymWithoutNumbers = synonymCandidate.getSynonymTag().replaceAll("-?\\d\\.?-?\\d?", "");

		int groupId = getIdForGroupByTarget(synonymWithoutNumbers);

		// targetEx = targetNew
		if (groupId != -1 && synonymGroups.containsKey(groupId)) {
			addToSourcesOfExistingGroups(groupId, synonymCandidate);
			return true;
		}
		return false;
	}

	private Set<SynonymCandidate> checkForOtherRelations(Set<SynonymCandidate> setToIterate) {

		Set<SynonymCandidate> returnSet = new HashSet<>();

		for (SynonymCandidate synonymCandidate : setToIterate) {

			// targetEx = sourceNew
			int groupId = getIdForGroupByTarget(synonymCandidate.getTag());

			if (!checkTargetExIsSourceNew(synonymCandidate, groupId)) {
				// checkSourceExIsTargetNew(synonymCandidate, groupId);
				returnSet.add(synonymCandidate);
			}
		}

		return returnSet;

	}

	// private Set<SynonymCandidate> checkSourceExIsTargetNew(SynonymCandidate
	// synonymCandidate, Integer groupId) {
	// Set<SynonymCandidate> chainsForTopCategoriesDown = new HashSet<>();
	// Set<SynonymCandidate> chainsForTopCategoriesUp = new HashSet<>();
	//
	// // sourceEx = targetNew
	// EList<Integer> groupIds =
	// getIdsForGroupBySource(synonymCandidate.getSynonymTag());
	// boolean found = false;
	// for (Integer gId : groupIds) {
	//
	// if (groupId != -1 && synonymGroups.containsKey(gId)) {
	//
	// GroupNeu groupNeu = synonymGroups.get(gId);
	// // gibt es infodto der dreieck schließt?
	// Integer triangleId = getInfoDtoForSourceTarget(synonymCandidate.getTag(),
	// groupNeu.getTargetList());
	// if (triangleId != -1 && !triangleId.equals(synonymCandidate.getId())) {
	// groupNeu.addSourceInfoDto(synonymCandidate.getSynonymTag(),
	// synonymCandidate.getId());
	// groupNeu.addSourceInfoDto(synonymCandidate.getTag(),
	// synonymCandidate.getId());
	// updateDataObjects(synonymCandidate, groupNeu);
	// found = true;
	// break;
	// }
	// }
	// }
	// chainsForTopCategoriesDown = setToIterate;
	// if (found) {
	//
	// chainsForTopCategoriesDown.remove(synonymCandidate);
	// break;
	//
	// } else {
	//
	// groupIds = getIdsForGroupBySource(synonymCandidate.getTag());
	//
	// // sourceEx = sourceNew
	// // New a-b Ex: a-c -> ? b-c? oder c-b?
	// for (Integer grId : groupIds) {
	// if (groupId != -1 && synonymGroups.containsKey(groupId)) {
	//
	// GroupNeu groupNeu = synonymGroups.get(grId);
	// // b-c?
	// Integer dtoId =
	// getInfoDtoForSourceTarget(synonymCandidate.getSynonymTag(),
	// groupNeu.getTargetList());
	//
	// if (dtoId != -1) {
	// groupNeu.addSourceInfoDto(synonymCandidate.getSynonymTag(),
	// synonymCandidate.getId());
	// groupNeu.addSourceInfoDto(synonymCandidate.getTag(),
	// synonymCandidate.getId());
	// updateDataObjects(synonymCandidate, groupNeu);
	// found = true;
	// break;
	// } else if ((dtoId =
	// getSynonymCandidateForSourceAndTarget(groupNeu.getTargetList(),
	// synonymCandidate.getSynonymTag())) != -1) {
	//
	// // c-b?
	// groupNeu.switchTargetToSourceAndAddNewTarget(groupNeu.getTargetList(),
	// synonymCandidate.getSynonymTag(),
	// synonymCandidate.getId());
	// updateDataObjects(synonymCandidate, groupNeu);
	// found = true;
	// break;
	//
	// }
	//
	// }
	//
	// }
	// chainsForTopCategoriesUp = chainsForTopCategoriesDown;
	//
	// if (found) {
	// chainsForTopCategoriesUp.remove(synonymCandidate);
	// break;
	//
	// }
	//
	// }
	// return chainsForTopCategoriesUp;
	// }

	private Set<SynonymCandidate> checkForOtherRelationsOLD(Set<SynonymCandidate> setToIterate) {
		Set<SynonymCandidate> chainsForTopCategoriesDown = new HashSet<>();
		Set<SynonymCandidate> chainsForTopCategoriesUp = new HashSet<>();

		for (SynonymCandidate synonymCandidate : setToIterate) {

			// targetEx = sourceNew
			int groupId = getIdForGroupByTarget(synonymCandidate.getTag());

			if (groupId != -1 && synonymGroups.containsKey(groupId)) {
				GroupNeu groupNeu = synonymGroups.get(groupId);
				// gibt es synonymcandidate der dreieck schließt?
				Integer synonymCandidateId = getSynonymCandidateForSourceAndTarget(groupNeu.getSourceList(), synonymCandidate.getSynonymTag());

				if (synonymCandidateId != -1 && !synonymCandidateId.equals(synonymCandidate.getId())) {
					// switch source target
					// a->b, b->c --- es gibt auch a->c
					groupNeu.switchTargetToSourceAndAddNewTarget(synonymCandidate.getTag(), synonymCandidate.getTag(), synonymCandidate.getId());
					updateDataObjects(synonymCandidate, groupNeu);

				} else {

					// check ob alte gruppe approved ist

					LuceneLevenshteinDistance lld = new LuceneLevenshteinDistance();

					if (groupNeu.isCreatedFromApprovedSynonyms()) {
						if (synonymCandidate.getRanking() > 0.65 && (synonymCandidate.getCat() != null && synonymCandidate.getCat().equals(SynonymCategory.SIMILARITY)
								|| (lld.getDistance(groupNeu.getFirstTarget(), synonymCandidate.getSynonymTag()) > 0.65))) {

							groupNeu.addSourceInfoDto(synonymCandidate.getSynonymTag(), synonymCandidate.getId());
							updateDataObjects(synonymCandidate, groupNeu);

						}

					} else if (synonymCandidate.getRanking() > 0.65 && groupNeu.getIdList().size() < 3) {
						groupNeu.switchTargetToSourceAndAddNewTarget(synonymCandidate.getTag(), synonymCandidate.getSynonymTag(), synonymCandidate.getId());
						updateDataObjects(synonymCandidate, groupNeu);
					}

					else if (groupNeu.getIdList().size() > 3) {
						addNewSynonymGroup(synonymCandidate.getId(), synonymCandidate);
					} else {
						// ansonsten einfach hinzufügen
						groupNeu.addSourceInfoDto(synonymCandidate.getSynonymTag(), synonymCandidate.getId());
						groupNeu.addSourceInfoDto(synonymCandidate.getTag(), synonymCandidate.getId());
						// ?
						updateDataObjects(synonymCandidate, groupNeu);
					}
				}

			} else {
				// sourceEx = targetNew
				EList<Integer> groupIds = getIdsForGroupBySource(synonymCandidate.getSynonymTag());
				boolean found = false;
				for (Integer gId : groupIds) {

					if (groupId != -1 && synonymGroups.containsKey(gId)) {

						GroupNeu groupNeu = synonymGroups.get(gId);
						// gibt es infodto der dreieck schließt?
						Integer triangleId = getInfoDtoForSourceTarget(synonymCandidate.getTag(), groupNeu.getTargetList());
						if (triangleId != -1 && !triangleId.equals(synonymCandidate.getId())) {
							groupNeu.addSourceInfoDto(synonymCandidate.getSynonymTag(), synonymCandidate.getId());
							groupNeu.addSourceInfoDto(synonymCandidate.getTag(), synonymCandidate.getId());
							updateDataObjects(synonymCandidate, groupNeu);
							found = true;
							break;
						}
					}
				}
				chainsForTopCategoriesDown = setToIterate;
				if (found) {

					chainsForTopCategoriesDown.remove(synonymCandidate);
					break;

				} else {

					groupIds = getIdsForGroupBySource(synonymCandidate.getTag());

					// sourceEx = sourceNew
					// New a-b Ex: a-c -> ? b-c? oder c-b?
					for (Integer grId : groupIds) {
						if (groupId != -1 && synonymGroups.containsKey(groupId)) {

							GroupNeu groupNeu = synonymGroups.get(grId);
							// b-c?
							Integer dtoId = getInfoDtoForSourceTarget(synonymCandidate.getSynonymTag(), groupNeu.getTargetList());

							if (dtoId != -1) {
								groupNeu.addSourceInfoDto(synonymCandidate.getSynonymTag(), synonymCandidate.getId());
								groupNeu.addSourceInfoDto(synonymCandidate.getTag(), synonymCandidate.getId());
								updateDataObjects(synonymCandidate, groupNeu);
								found = true;
								break;
							} else if ((dtoId = getSynonymCandidateForSourceAndTarget(groupNeu.getTargetList(), synonymCandidate.getSynonymTag())) != -1) {

								// c-b?
								groupNeu.switchTargetToSourceAndAddNewTarget(groupNeu.getTargetList(), synonymCandidate.getSynonymTag(), synonymCandidate.getId());
								updateDataObjects(synonymCandidate, groupNeu);
								found = true;
								break;

							}

						}

					}
					chainsForTopCategoriesUp = chainsForTopCategoriesDown;

					if (found) {
						chainsForTopCategoriesUp.remove(synonymCandidate);
						break;

					}
				}

			}
		}

		return chainsForTopCategoriesUp;

	}

	private boolean checkTargetExIsSourceNew(SynonymCandidate synonymCandidate, Integer groupId) {

		if (groupId != -1 && synonymGroups.containsKey(groupId)) {
			GroupNeu groupNeu = synonymGroups.get(groupId);
			// gibt es synonymcandidate der dreieck schließt?
			Integer synonymCandidateId = getSynonymCandidateForSourceAndTarget(groupNeu.getSourceList(), synonymCandidate.getSynonymTag());

			if (synonymCandidateId != -1 && !synonymCandidateId.equals(synonymCandidate.getId())) {
				// switch source target
				// a->b, b->c --- es gibt auch a->c
				groupNeu.switchTargetToSourceAndAddNewTarget(synonymCandidate.getTag(), synonymCandidate.getTag(), synonymCandidate.getId());
				updateDataObjects(synonymCandidate, groupNeu);

			} else {

				// check ob alte gruppe approved ist

				LuceneLevenshteinDistance lld = new LuceneLevenshteinDistance();

				if (groupNeu.isCreatedFromApprovedSynonyms()) {
					if (synonymCandidate.getRanking() > 0.65 && (synonymCandidate.getCat() != null && synonymCandidate.getCat().equals(SynonymCategory.SIMILARITY)
							|| (lld.getDistance(groupNeu.getFirstTarget(), synonymCandidate.getSynonymTag()) > 0.65))) {

						groupNeu.addSourceInfoDto(synonymCandidate.getSynonymTag(), synonymCandidate.getId());
						updateDataObjects(synonymCandidate, groupNeu);

					}

				} else if (synonymCandidate.getRanking() > 0.65 && groupNeu.getIdList().size() < 3) {
					groupNeu.switchTargetToSourceAndAddNewTarget(synonymCandidate.getTag(), synonymCandidate.getSynonymTag(), synonymCandidate.getId());
					updateDataObjects(synonymCandidate, groupNeu);
				}

				else if (groupNeu.getIdList().size() > 3) {
					addNewSynonymGroup(synonymCandidate.getId(), synonymCandidate);
				} else {
					// ansonsten einfach hinzufügen
					groupNeu.addSourceInfoDto(synonymCandidate.getSynonymTag(), synonymCandidate.getId());
					groupNeu.addSourceInfoDto(synonymCandidate.getTag(), synonymCandidate.getId());
					// ?
					updateDataObjects(synonymCandidate, groupNeu);
				}
			}

			return true;
		}
		return false;
	}

	private void addSubGroupToGroup(Integer idSuperGroup, Integer idSubGroup) {
		if (idSuperGroup == idSubGroup) {
			return;
		}
		GroupNeu superGroup = synonymGroups.get(idSuperGroup);
		GroupNeu subGroup = synonymGroups.get(idSubGroup);

		superGroup.addSourcesToList(subGroup.getSourceList(), subGroup.getIdList());
		superGroup.addSourcesToList(subGroup.getTargetList(), subGroup.getIdList());

	}

	private void updateDataObjects(SynonymCandidate dto, GroupNeu groupNeu) {
		synonymGroups.put(groupNeu.getGroupId(), groupNeu);
		table.put(dto.getTag(), dto.getSynonymTag(), groupNeu.getGroupId());
		listOfNotUsedTags.remove(dto.getTag());
		listOfNotUsedTags.remove(dto.getSynonymTag());

	}

	private void removePutPut(Integer synCandidateId, SynonymCandidate dto, int groupId, GroupNeu groupNeu) {
		// synonymCandidateMap.remove(synCandidateId);
		synonymGroups.put(groupId, groupNeu);
		table.put(dto.getTag(), dto.getSynonymTag(), groupId);
		listOfNotUsedTags.remove(dto.getTag());
		listOfNotUsedTags.remove(dto.getSynonymTag());
	}

	private void init() {

		allGroupedTagNames = new HashSet<>();
		synonymGroups = new HashMap<>();
		table = HashBasedTable.create();
		usedSynonymCandidateIds = new HashSet<>();
		v = new Version();
		if (ONLY_ANDROID) {
			(synonymCandidates = new HashSet<SynonymCandidate>()).addAll(HelperDBHandler.getSynonymCandidatesByMinimumRanking(0.65));
			// topRankingSynonymCandidates =
			// HelperDBHandler.getTopRankingSynonymCandidatesAndroid();
			// synonymCandidateMap =
			// HelperDBHandler.getAllSynonymCandidatesAndroid();

			approvedSynonymPairs = HelperDBHandler.getAllApprovedSynonymPairsForAndroid(5000);

			listOfNotUsedTags = HelperDBHandler.getAllTagsForAndroid();
			for (SynonymCandidate sc : synonymCandidates) {
				listOfNotUsedTags.add(sc.getTag());
				listOfNotUsedTags.add(sc.getSynonymTag());

			}

		} else {
			// topRankingSynonymCandidates =
			// HelperDBHandler.getTopRankingSynonymCandidates();
			// chainsForTopCategories =
			// HelperDBHandler.getChainsForTopCategories();
			synonymCandidateMap = HelperDBHandler.getAllSynonymCandidates();

			approvedSynonymPairs = HelperDBHandler.getAllApprovedSynonymPairs(0, 5000);
		}

		// TODO remove
		if (selectedTagsOnly) {
			applyFilter();
		}
	}

	private void applyFilter() {

		String allTagsToInclude = "libssl xmlbeans-maven-plugin sql-server-2012 sql-server-openxml system-tray xmllist gm-xmlhttprequest xmlnodelist jenkins-plugins xml-builder post-build sql-server-2005 scales-xml scala-xml scxml sql-server-2000 google-api-php-client sql-server-2008 xmlhttprequest-states xml-drawable login-config.xml xmldiff app-secret missing-data xml-database for-xml xml-entities ccxml jquery-mobile-button application.xml xml-sitemap apache-commons-scxml ejb-jar.xml return-type xmlport xmlconvert xml-import dex-limit xmltodict for-xml-explicit for-xml-path libsvm unreachable-code voicexml f#-scripting xmltextreader xml-signature xmlvend web.xml xmldatasource rapidxml xmladapter oracle-xml-db-repository xmlformview nsxmlnode semantics xmlschemaset xmltype xmlseclibs xml-nil xml-formatting xml-dsig siri-xml xml-declaration serverxmlhttp xmltable closedxml manifest.xml xml-binding xmlreader xmlstreamwriter xmltext pyxml vtd-xml openxml-sdk xml-rpc.net zombie-process xml.modify geoxml3 fxml xml-namespaces server.xml xfl microxml mini-xml xmldsig berkeley-db-xml nsxmlparserdelegate xml.etree latexml kissxml xml-file oracle-xml-db system.xml xmlwriter qxmlstreamreader qtxml android-xml xmlspy select-for-xml xmlstore innerxml pugixml fetchxml xml-dml xml-document-transform qbxml xmlmapper xmlmassupdate xml-documentation xmlstreamer xll xml-validation xmlrpclib xml-attribute export-to-xml symlink xmlhttp parsexml persistence.xml xml-encryption xms xml-twig xmm xmp haxml xml-conduit xmlbeans xmi xmllint smooth-numbers xmlattributecollection xml-1.1 similarity cxml xmlsec1 simulate nsxmlparsererrordomain xml-libxml xmlanyelement xmltextwriter kxml2 chilkat-xml xmlsec musicxml hbmxml xmlstreamwriter2 sqlxml xmlnode xml-deserialization xmlsocket sxml xmlcatalog wbxml sample xmlblaster simplexmlrpcserver xmldataprovider xmlhttprequest-level2 readxml xmlexception xmlencoder nsxmlelement swixml jquery-xml xmltask xmlhelper xmlservice mxml hibernate.cfg.xml xmldocument fxmlloader xml-visualizer msxml msxml4 msxml3 nativexml xmllistcollection dbms-xmlgen msxml6 xamarin.android kxml msxml5 xml-layout anti-xml context.xml writexml xmlindex oxygenxml zend-xmlrpc xml-schema-collection qdbusxml2cpp xml-generation libxml-ruby xml-spreadsheet ixmldomnode select-xml lxml.html xmlhttp-vba binary-xml som linq-to-xml signedxml xml-dtd symbolic-computation mxmlc xml-swf-charts xml-rpc princexml xamarin.mac layout-xml xml-column gbxml luaxml nxml xml-transform tinyxml++ libxml libxml2 xmlconfigurator samsung-mobile symbolic-math xmlworker magento-layout-xml semaphore build.xml txmldocument xmllite xmlpullparser xmltransient gdataxml xmlunit xamarin.ios xmlserializer plugin.xml xmos xml-literals vxml ixmldomdocument sitemap.xml xmlroot inline-xml crossdomain.xml smart-pointers xmlpoke xmlslurper xul libxml-js searching-xml xmlinclude xmlns xml-encoding xmlstarlet xbl clientaccesspolicy.xml ebxml smack nsxmldocument xmlrpcclient symfony-sonata updatexml xmla touchxml xmldataset xmldom xmldog xmldoc rexml xmlupdate tinyxml fasterxml xmlstreamreader geoxml xml-comments omnixml sml outerxml xml-simple pubxml javax.xml simple-xml-serialization nsxmlparser ixmlserializable ixmldomelement xml-configuration xmlignore nsxml zend-config-xml xml-editor tbxml security-constraint dynamic-sql spring-social-twitter google-fit-sdk air-android open-intents conflicting-libraries noise-generator helix-server belongs-to opus temp-tables lookup-tables vsts fastmember adsense-api pass-by-value apk-expansion-files no-cache create-table filenet-content-engine samba opengl-es-2.0 time-precision android-xmlpullparser namespace-organisation inline-namespaces p-namespace global-namespace xnu xna-3.0 xnet xname xnamath struts2-namespace xnanimation xnat xna-math-library unnamed-namespace cocos2d-x-for-xna zend-session-namespace javascript-namespaces my-namespace recurring-events image-generation select-tag sklearn scaleanimation xcode-playground scim sqlselect scaml google-image-search palm-pre domain-driven-design ion droid-fu mongodb-java multi-device-hybrid-apps archive-tar facebook-c#-sdk error-log code-access-security visual-web-developer-2010 ui-automation jquery-mobile-collapsible google-data read-write android-lru-cache data-type-conversion data-uri-scheme opencore simple-framework custom-data-attribute code-efficiency hibernate-search build-gradle url-encoding text-styling here-api overlay-view data-paging domain-model selectors-api two-column-layout zimbra dailymotion-api pdf-reader openjms io-socket cfsocket svsocket xsocket jsocket qsslsocket usocket grails-domain-class layer-list opengl-es-lighting android-4.2-jelly-bean third-party-code smartcard-reader core-text opera-mini operator-precedence c++-cli color-codes code-analysis private-class project-organization opacity video-tracking samsung-touchwiz session-timeout pci-compliance line-drawing labels opengl-es-3.0 worker-thread particle-system ios6-maps opengl-2.0 gwt-rpc multiple-definition-error text-formatting connection-leaks android-multiple-users get-request code-coverage document-storage post-request linked merging-data adobe-native-extensions gradle-release-plugin apache-commons-logging application-server code-assist a-star alternate-stylesheets text-size rest-client runtime-error kendo-tabstrip thread-priority in-app-purchase blogger-dynamic-views pls-00323 titled-border systemevent table-rename texas-instruments web-config web-security facebook-php-sdk google-schemas http-post data-recovery spring-roo osx-mavericks fortify web-testing libusb app-bundle html-parser lossy-compression content-encoding terminal-ide mathcontext styledtext perl-context tinytext operationcontext transformable ambientcontext headertext spcontext boost-context xgettext selectedtext richeditabletext susy-next jquery-context xtuple ruby-ext requirejs-text cordys-opentext xdt-transform tab-delimited-text instance-transform writealltext freetext initial-context eaglcontext jquery-textext gwt-edittext cgbitmapcontext requestcontext grouped-text transformation config-transformation graphicscontext ucontext static-text xtunit shakespeare-text transform-origin erpnext domaincontext xtrachart html-to-text executioncontext django-context event-context android-pagetransformer subtext dtcoretext device-context enhanced-rich-text pbxt emptydatatext ntext externalcontext viewcontext weboperationcontext principalcontext transform-feedback dbext homogenous-transformation ajaxcontext ssis-data-transformations betwixt real-time-text requirements.txt sessioncontext webkitaudiocontext attachmate-extra wavelet-transform mylyn-wikitext emftext geom-text xtrf opentext bson-ext drawingcontext xt cliext openglcontext burrows-wheeler-transform postgres-ext web-config-transform matrix-transform structured-text wikitext cgpdfcontext libqxt ydn-db-fulltext operational-transform shellext pagecontext security-context richedittext javax.swing.text xtratreelist c#-edittext ynchronizationontext actioncontext php-gettext normalize-text tableservicescontext jmxtrans xtype android-droidtext fedext longtext facescontext managedobjectcontext controllercontext formatted-text owlnext appendtext testcontext webcontext cross-context xtea dynamic-text audiocontext xtraeditors geoext c++-edittext helpcontext large-text threadcontext oracle-text nsanimationcontext xtradb nsmanageobjectcontext scalar-context export-to-text vertical-text parse-transform main-activity star mesh-network amazon-web-services paperclip-validation in-operator text-recognition text-alignment build.gradle sql-view outgoing-mail html-select android-l event-id google-analytics-api access-denied usb-mass-storage libvlc csip-simple web-scripting text-search common-code look-and-feel where-in persistent-connection jquery-ui-map sql-triggers power-management lxml machine-learning openxml pom.xml simplexml http-status-codes video-streaming win-universal-app build-system dom-manipulation openjdk parserole android-library data-partitioning ssms playback-rate application-close custom-attribute input-type-file internal-link data-extraction code-organization 3d-engine data-link-layer fs list-separator dx ed dm portable-applications worklight-adapters robolectric-gradle-plugin em rpclib coco2d-x ssml here-maps android-multiple-apk streaming-flv-video google-apps-script qcar-sdk ids ar avatar-generation mqseries m2eclipse mcc mxm mac-address mcml microsoft-appstore microsoft-sync-framework mockups maml mac-os-x-server mkmapkit microsoft-dynamics ida timed-events touch-punch freopen custom-build-step linq-to-sql save-image mx-record apache-commons-io pe pc smali ln ls json-parsing data-sharing mobile-os dead-code ime update-site firedac string-table membership-provider data-management handpoint-sdk xml-serialization libvpx text-indent text-based remove-if opml google-document-viewer ms-access xml-parsing xml-schema vr google-api-nodejs-client app-config tabbed-view server-variables smart-tv custom-data-type paypal-rest-sdk hybrid-mobile-app android-x86 multi-layer object-pascal bits-per-pixel error-suppression autocad-plugin flickr-api opengl-es-1.1 app-id boost-log pt ps http-unit run-configuration bump-mapping last-modified method-signature auto-update web-reference android-json-rpc foxit-reader full-text-search crosswalk-runtime android-layout-weight paypal-express options-menu web-services-enhancements help-system ruby-on-rails-3.1 ruby-on-rails-3.2 error-codes error-handling virtual-tour scala-collections general-network-error android-compat-lib sql-server-ce clipboard-interaction batch-updates image-file android-parser do-while pls-00103 live-update single-page-application jms-serializer qtserial android-4.3-jelly-bean azure-web-roles code-signing-certificate single-threaded exe libtool if-condition xs xsd-1.1 winsxs sxs xslt-tools subsonic-simplerepository xsp2 ember-simple-auth xsp4 xslt-3.0 xsd2code xsb xsitype xsi xsbt-web-plugin xsl-variable xshd xslkey xslt-extension simple-authentication simple-html-dom xsl-stylesheet xspace sts-simple-templatesystem android-simple-facebook perl-xs simple-mvvm xsbt xsltc hana-xs simple-openni simple-push-api simplex-noise xsl-choose simple-machines-forum xsom xscale xsdobjectgen xspf xsbt-proguard-plugin ibm-wxs simple-el xsql simple-form-for simple-web-token xsl-grouping xsltforms famo.us yahoo-maps vs-android gmail-api google-plus build-target server-push memory-allocation xamlreader mxbean data-access-layer github-for-mac relative-date form-submit thinktecture-ident-server google-play-services topaz-signatures real-datatype open-session-in-view yandex-maps dtd-parsing xdmcp scriptresource.axd spring-xd axd xdr xdp xdv xdt xdgutils xdebug-profiler xdoclet xdoc xda xdocreport xdist xxd xdr-schema xdotool xdomainrequest native-code ems node-modules titanium-android options iguana-ui samsung-mobile-sdk object-to-string greendao-generator canny-operator output-file unhandled-exception aws-elastictranscoder locked-files static-data crosstool-ng custom-search-provider pdf-rendering google-drive-realtime-api soap-client rapidshare rapidsql java-ee-6 berkeley-db-je google-api-console mathematical-notation xmpppy non-well-formed web-api up-navigation multi-table oma-dm spring-el php-openssl dollar-sign code-formatting android-scrollable-tabs music-player type-conversion custom-lists wificonfiguration libpng date-difference scala-ide socket.io-1.0 accessible value-of email-threading ng-view device-mapper x86-64 xmlhttprequest android-custom-attributes android-tap-and-pay sql-date-functions firewall-access pkg-file directory-listing fast-app-switching aspectj-maven-plugin http-get qt-jambi varying panorama-control wikipedia-api luhn smslib geo-schema html-escape-characters spring-data-jpa unity-web-player openal circular-list oauth-provider internal-server-error variable-naming real-time-updates firing sql-azure layout-animation os.path java-io system-font dragonfire-sdk systems-programming servicestack-text mms-gateway url-scheme load-time ide-customization anonymous-types android-app-ops selenium-ide bins inspect-element package-name qt-creator windows-media-services openfl edit-in-place concurrent-collections http-server rapid-prototyping sts-springsourcetoolsuite form-validation tabular-form libcrypto log-viewer file-writing documentation-generation software-update google-data-api fragment-tab-host libreoffice-base amazon-rds appengine-maven-plugin youtube-v3-api openni opentk scrollable-table code-templates custom-overlay array-initialize linkedin request-headers opensl cos linuxmint facebook-unity-sdk facebook-app-center hpx data-persistence pex-and-moles pexcept tpersistent sqlworkflowpersistencese persistjs polyglot-persistance persistent-storage innobackupex gpx akka-persistence persisted-column persistence-manager pxsourcelist nspersistentstore persistentmanager ipersistfile upx object-persistence fsharpx 500px persist node-persist rpx pxe nspersistentdocument jspx persistent-set pex persistence-ignorance persistent-object-store writeablebitmapex upnpx appx imapx datapersistance persistence-unit guice-persist data-formats gaps-in-visuals thread-exceptions simile python-requests google-code-hosting dropbox-api source-maps remember-me custom-attributes google-finance-api ndk-gdb indoor-positioning-system object-oriented-database data-acquisition google-shopping-api password-confirmation photo-upload zbar-sdk xmpphp jquery-transit outline-view arduino-ide systemexit class-loading sql-delete openexr intel-galileo android-youtube-api code-management adaptive-design silent-installer delete-row name-attribute xaml-binding class-attribute xa65 xamgeographicmap alexa for-attribute xamlpad xadisk xamdatachart jquery-attributes xaml-designer xajax xamdatagrid passthrough-attributes xamlx conditional-attribute xapian authorize-attribute xamarin.auth compare-attribute function-attributes virtual-attribute xamlwriter entity-attribute-value designtime-attributes xattr xaml-serialization xact xaml-2009 xact-abort android-textattributes xaml-tools xamgrid xaction dynamic-attributes xauth assembly-attributes property-attribute rendered-attribute xamairn alt-attribute xamarin.social meta-attribute xacml2 xacml3 xaml-resources xaudio2 update-attributes update-attribute file-attributes xacml class-attributes xar xades4j xamlparseexception vertex-attributes attribute-exchange xaf configuration-files windows-store-apps starling-framework javax.mail sql-server-express data-modeling jquery-ui-touch-punch drupal-services json-simple xmetal vimeo-api unique-index map-edit column-aggregation series name-lookup soap-extension google-tasks memory-leaks satellite-image amazon-ses xembed ntvdm.exe al.exe perl2exe xenocode xep-0198 expression-encoder-sdk picaxe mb-convert-encoding exen rc.exe gui2exe http-accept-encoding run-length-encoding windows-media-encoder httpcfg.exe expression-encoder-4 rs.exe xendesktop zigzag-encoding xerces2-j ipxe xetex range-encoding cfurl-encoding soap-rpc-encoded xenu logstash-logback-encoder sgen.exe no-response finite-state-machine google-eclipse-plugin parse-error http-host remote-execution amazon-sqs bing-api applicationcraft binary-diff programmable-completion android-open-accessory server-communication payment cyclic-reference access-log morse-code system.io.packaging expat-parser doc data-protection text-extraction serial sender-id google-fusion-tables ia-32 garbage-collection va-list apple-push-notifications google-drive-api mail-sender text-to-speech der des xrm xri xrc xrdp jax-rpc xrds json-rpc xrange jpeg-xr lxr xregexp xrandr advertisement-server smooth-streaming-player str-replace map-projections opengl-3 application-security static-vs-non-static jenkins-cli system-shutdown dynamic-ui web-crawler custom-draw legal logo logcat lcds local-variables lexical-analysis like-operator l2-cache safari-web-inspector rcs image-load modified-date texture-mapping raw ras code-search-engine non-scrolling view-hierarchy rx-java ros roi code-behind setup-wizard opentok exc-bad-access document-store access-point linaro semantic-web xming ruby-on-rails node-webkit touchdb touchatag touch-location layout-gravity pseudo-element stack-trace google-reporting-api ria access-token google-geocoding-api logfile-analysis touch-event feature-tracking non-deterministic nested-views export-to-csv method-declaration dns-sd iterable junit-ee custom-renderer ms-office letter-spacing tree-left-right in-call sam external-application mcmc raw-data jquery-mobile-panel live-sdk lock-in bottom-up design-by-contract corrupt-data graphic-design pin-it-sdk java.lang.class cross-domain opcode accessibility-api jxcore pycxx sjax foxx d2xx javacaps jaxm jaxl cljx google-ajax jasmine-ajax jxmapkit jaxb-episode javax.time javax.speech eldarion-ajax htmlcxx ic-ajax netrexx sajax jxmultisplitpane jxtree jxtaskpane sijax xxtea hijax node-set lync task-manager procedural-music azure-android-sdk lynx windows-explorer right-to-left pop-up sin maven-archetype cocos2d-android saving-data openssh build-settings ignorelist sencha-touch-2 language-name dynamic-loading static-code-analysis sds sip-server system-sounds sql-injection .doc data-connections iphone-web name-conflict stateless-session-bean soc http-equiv code-separation application-design gamepad-api drupal-views google-tasks-api socialize-sdk error-code test-project frustum http-post-vars cross-compiling voice-recognition ios-ui-automation smp sms operating-system special-keys http-proxy storage-access-framework smb smd java-websocket character-limit banner-ads git-pull custom-event jquery-ui-datepicker producer-consumer libgit2 stack-corruption chunked-encoding ssd real-time-data google-cloud-sql ext.list ssp parsepy prism-2 parse-ez parse.js parseexcel paraccel sum web-based imagemagick-convert nat-traversal voice-detection document-root dynamic-list server-side-validation data-transfer-objects java-native-interface currency-exchange-rates jquery-mobile-pageshow stario-sdk ui-testing ws-client saml-2.0 mime-types google-api-python-client librsvg google-chrome-os grails-plugin hibernation linked-list sql-manager phantom-reference data-caching script-tag pac iterative pam parserelation task-queue http-error smime system-properties man-in-the-middle steam-web-api projects-and-solutions pen pem secure-coding http-live-streaming facebook-sdk-3.14.x google-client request-timed-out simplejmx simplejson simplepie simpletip simplecov simplegeo simpledom simpletype simplecart simplepager sample-rate simplex simplemodal simpletest sample-size simplecv simplify soomla array-map dojox.mobile orient-db android-maps-extensions triangle-generation live-cd live-streaming date-of-birth ppm facebook-java-api text-align pmd contact-list closed-captions konami-code android-sdk-tools storm-gen streaming-video web-frameworks strip-tags l18n data-loss code-structure create-view has-many pwm sslexception data-binding password-retrieval galaxy-tab iframe-app cross-platform side-by-side qr-code android-mms jquery-mobile-listview facebook-requests iserializable tilt-sensor clients parsing-error line-count audio-converter rtsp-client actionbarsherlock-map file-type windows-phone-8-sdk inet-socket pdf-form web-services cron-task linear optional-parameters microsoft-media-server data-collection clickable-image berkeley-db libjpeg event-stream gradle-eclipse google-drive-android-api facebook-app-settings network-service rendering-engine code-conversion extension-methods format-conversion enhanced-for-loop training-data sql-server-2008-r2 google-http-client wcf-web-api samsung-knox telephone-api jquery-validation-engine connection-reset texture-packing network-scanner server-sent-events code-migration google-apps-marketplace nook-tablet service-reference visual-studio-express serial-number client-authentication wlan google-street-view cordova-chrome-app runtime-environment binary-tree date-format eclipse-classpath mvvm-light zend-framework-mvc cimg windows-applications maven-jar-plugin class-names country-codes outlook-calendar bus-error custom-error-pages apache-commons-lang eyecon-plugin program-counter if-modified-since itunes-store size-t android-backup-service openpgp google-weather-api system-information simulink data-files code-editor parse4j recent-file-list touch-feedback http-put access-control bing-maps pox parent-pom pomelo pomm hippomocks nas blackberry-android opentype text-processing google-sites date-range libev application-lifecycle google-product-search xpsdocument libmagic libxl llblgenpro libxslt libsox libgmail libdbi libumem libgomp libyaml libming llblgen libnds libffi http-streaming android-sdk-2.2 android-sdk-2.1 android-sdk-2.3 asp.net-apicontroller cocoa-touch dynamic-data dead-reckoning libav x3dom xmodem map-directions task-parallel-library drop-table system-testing build-environment youtube-api box-view-api xlsxwriter mqlwrite writers htmlwriter writer http-status-code-505 http-monitor http-status-code-500 application-framework error-checking parser-generator google-play-store text-decorations server-error libv8 foreground-service libuv mod-rewrite location-client layout-editor system-administration google-maps-timezone compound-drawables clojure-java-interop app-inventor jquery-ui-sortable pie-chart root-access website-payment-pro singleton-methods app-store indexed-image webservices-client sts-securitytokenservice libpd linux-toolchain processing-ide pkg-config overflow-menu android-maps-utils text-database context-switching multiple-languages pausing-execution out-of-memory ode location-based duplicate-data ci-server windows-live-id typed android-maps-v2 alter-table code-smell text-editor xampp cross-threading ipod-touch unix-domain-sockets facebook-android-sdk cache-manifest wsimport open-source null-terminated app-engine-ndb ab-testing playing-cards name-value nokia-maps google-voice-search youtube-data-api opensaml database-update web-worker google-search-appliance in-memory eclipse-builder data-consistency layout-optimization duplicate-entry vaadin-touchkit zumero google-directory-api unreal-development-kit remote-access pixel-ratio data-retrieval facebook-ads-api insert-update source-code blank-space apple-tv pdf-writer progressive-enhancement android-style-tabhost client-server visual-web-developer css-text-overflow music21 http-status-code-413 http-status-code-415 http-status-code-411 cocos2d-js http-status-code-406 google-maps http-status-code-404 document-classification http-status-code-405 http-status-code-403 http-status-code-400 http-status-code-401 opa opc one-to-many http-redirect level-of-detail web-audio ng-animate authorize.net http-basic-authentication anonymous-inner-class windows-phone-store google-voice stored-procedures expansion-files sthttprequest em-http-request http-request winhttprequest clienthttprequest httprequest afhttprequestoperation oma same-origin-policy facebook-access-token memory-access opengl-es sql-parametrized-query expert-system web-controls transport-stream data-access lorem-ipsum sim-toolkit master-slave lan data-scrubbing libzip opencmis freetts linker-error android-music-player twitter-client json-deserialization promotion-code library-interposition dev-to-production web-deployment maintaining-code google-charts-api http-1.1 opennlp mean-square-error log-rotation sql-order-by data-url data-uri dynamic-tables iphone-standalone-web-app on-the-fly http-status-code-301 http-status-code-302 http-status-code-304 facebook-javascript-sdk data-integrity http-status-code-307 use-case lso libyuv lte dynamic-data-display code-review exist-db web-content self-updating merge-conflict-resolution data-interchange shadow-dom close-application openstack web-applications foreign-data-wrapper query-string sharpen-tool parental-control lomo-effect gdata-api screen-readers onsen-ui lex windows-services qt-mobility androidplot binary-data internal-storage html5builder air-native-extension html-rendering android-embedded-api ws-discovery app-data youtube.net-api lis media-type graphical-layout-editor data-transfer long-integer android-sdk-plugin twitter-api network-traffic sound-api crash-log openmap openmax web.py access-violation challenge-response external-sorting read-eval-print-loop sql-parser ebook-reader facebook-events payment-services unique-id sbt-android-plugin outlook-web-app custom-view paypal-adaptive-payments http-status-code-201 ibeacon-android device-instance-id custom-application mpl point-in-polygon app-manager empty-string ejabberd-http-bind date-calculation luabind google-text-to-speech collaborative-editing web-hosting disable-app android-mapping printing-web-page xsl-fo public-key-encryption libiconv interrupted-exception packet-capture data-analysis io-redirection gwt-mosaic code-cleanup icloud-api scan-build graphic-effects pre-packaged micro-optimization mlt spaces system-preferences symbol data-conversion forms-authentication domain-name web-scraping json-encode post-update test-runner zoom incomplete-type motodev-studio visual-studio-online web2.0 code-duplication image-loading step-by-step command-line-tool regional-settings lazy-initialization end-of-life service-application dynamic-class-loaders rails-api system-setting client-certificates google-app-engine data-hiding physics-engine code-first ws-security transitive-dependency view-source nested-lists wcf-endpoint application-cache nested-statement thin-client cache-expiration page-load-time html-encode function-overriding android-task google-http-java-client string-externalization couchdb-lite api-design documents z-index multiple-accounts post-build-event opengl-shader-builder team-foundation-service pos-tagger greatest-n-per-group uncaught-exception device-emulation google-custom-search system.exit android-sdk-1.6 xmpp core-data static-initializer backup-sqldatabase rating-system application-shutdown server-response sources menuitem-selection http-etag single-sign-on desktop-application code-quality power-off html-form system-clock device-name f#-data divx video-card cvx error-logging ads-api file-read code-size grape-api time-and-attendance arcgis-server web-browser load-testing thread-dump inner-classes n-gram mongo-jackson-mapper cwac-endless select-insert web-analytics http-token-authentication sd-card ui-thread source-tree http-via-header non-static code-comments email-address ajax-request unchecked-exception dynamic-memory-allocation fortify-source spatial-index http-headers postal-code javax.activation electronic-signature route-provider nokia-s40 breadth-first-search file-encodings sdl-ttf ingress-game azure-mobile-services lossless-compression libx264 error-reporting database-schema tinyurl google-maps-sdk-ios css-text-shadow google-static-maps firefox-os android-service-binding magento-admin google-maps-engine nsdocument code-documentation ihtmldocument2 phpdocumenter cfdocument htmldocument xdocument user-documentation reference-application application-variables symbols opendata sql-update google-maps-api-3 google-maps-api-2 self-signed ubuntu-server bad-request facebook-social-plugins home-server ti-dsp is-empty jquery-mobile-dialog response.addheader hierarchical-data application-name 9-bit-serial java-custom-serialization native-activity launch-configuration sqlite-net-extensions azure-notificationhub modelattribute engine.io code-completion web-storage wso2-emm html-lists sbt-android installed-applications dxva remote-validation msi-validation invalid-object-name html5-validation xv6 asp.net-mvc-2-validation xvfb request-validation validation-controls event-validation data-validation nhibernate-validator fluentvalidation-2.0 argument-validation client-side-validation kendo-validator codeigniter-validation asp.net-mvc-validation ui-validate validates-uniqueness-of grails-validation cross-validation gwt-validation model-validation struts-validation invalid-url valid-html cache-invalidation client-validation spring-validator validate-request foolproof-validation castle-validators validates-associated drupal-form-validation ice-validation invalid-postback xval validation-application-bl xvalue invalid-argument custom-validators zend-validate url-validation xvcg html-validation respect-validation invalid-characters knockout-validation css-validator receipt-validation django-validation validating-event windows-server-2008-r2 snapshot-view square-wire sphero-api google-docs raw-sockets qexception cexception onexception madexcept node-xmpp build-dependencies activity-state open-graph-beta jar-signing square-root linear-interpolation couchdb-lucene terminal-emulator line-numbers camera-roll drag-to-select pass-by-reference team-explorer i18n-gem file-get-contents parser-combinators tiny dynamics-nav dynamic-binding tinyint dnn7 dnn5 dynamically-generated-co denali dnd tuning tinymvc jquery-mobile-loader route-me ms-access-2007 ms-access-2010 file-extension flex-mobile third-party-api content-type app-startup empty-list mathematical-expressions premultiplied-alpha pixel-manipulation asihttprequest sql-like sp-executesql api-key materialized-path-pattern multi-level ios-bluetooth ean-13 eclipse-rap exchange-server auto-build html-datalist custom-type cycle-tile shared-data xoom html-form-post matrix-multiplication server-load iphone-sdk-3.0 serial-port google-oauth-java-client particle-swarm memory-mapping http-delete contact-form tinyalsa facebook-likebox uv-mapping windows-client true-type-fonts facebook-app-requests google-spreadsheet-api svg-android pos-tagging render-to-string libcmtd import-libraries windows-server-2008 windows-server-2012 google-code thermal-printer android-developer-api android-app-indexing page-curl inter-process-communicat opcodes type-ahead text-parsing multi-touch application-start table-relationships build-error decimal-format native-methods textreader application-state database-managers code-reuse application-size bi-lingual xalan data-exchange simulation symfony2 general-purpose-registers xom code-view date-conversion persistent-data one-time-password opengl-to-opengles media-source text-rendering singly-linked-list freelancer.com-api gmaven-plugin gradle-tomcat-plugin wordpress-plugin myreviewplugin exec-maven-plugin jbehave-plugin maven-nar-plugin resharper-plugins google-go-idea-plugin hibernate3-maven-plugin build-pipeline-plugin plugin-pattern searchable-plugin gae-eclipse-plugin blackberry-eclipse-plugin jaxb2-maven-plugin cxf-codegen-plugin maven-wagon-plugin gedit-plugin clojure-maven-plugin maven-gae-plugin maven-invoker-plugin gravity-forms-plugin grails-resources-plugin symfony-plugins maven-bundle-plugin gwt-maven-plugin struts2-json-plugin sfauthorizationpluginview hudson-plugin-batch-task gkplugin ibplugin mongoose-plugins maven-pdf-plugin versions-maven-plugin google-plugin-eclipse maven-jaxb2-plugin eclipse-plugin-dev ftplugin swagger-maven-plugin maven-dependency-plugin jasmine-maven-plugin visual-sourcesafe-plugin maven-scala-plugin tycho-surefire-plugin maven-resources-plugin maven-ear-plugin jasperreports-jsf-plugin buildnumber-maven-plugin android-emulator-plugin hudson-plugins sbt-plugin maven-jetty-plugin knockout-es5-plugin plugin-architecture silverlight-plugin hadoop-plugins struts2-codebehind-plugin maven-deploy-plugin grunt-plugins netbeans-plugins cordova-plugins zend-controller-plugin grails-tomcat-plugin jira-plugin maven-replacer-plugin redmine-plugins struts2-jquery-plugin spotlight-plugin bazaar-plugins maven-shade-plugin maven-changes-plugin sbt-aws-plugin maven-plugin maven-source-plugin maven-enforcer-plugin gradle-custom-plugin ruby-on-rails-plugins cargo-maven2-plugin cycle-plugin vim-plugin maven-scr-plugin spring-plugin phonegap-plugins idea-plugin knockout-mapping-plugin rpm-maven-plugin jbehave-maven-plugin maven-webstart-plugin jaxws-maven-plugin jmeter-plugins cakephp-search-plugin struts2-jfreechart-plugin maven-tomcat-plugin eclipse-plugin jflex-maven-plugin plugin-system jmeter-maven-plugin maven-antrun-plugin maven-clean-plugin maven-cobertura-plugin next-generation-plugin jooq-sbt-plugin maven-plugin-development cxf-xjc-plugin weblogic-maven-plugin maven-javadoc-plugin elasticsearch-plugin grails-plugin-rabbitmq jquery-ui-plugins maven-eclipse-plugin struts2-tiles-plugin phonegap-pushplugin jquery-webcam-plugin beanstalk-maven-plugin android-maven-plugin native-maven-plugin jsonplugin knockout-viewmodel-plugin jquery-upload-file-plugin maven-assembly-plugin struts2-actionflow-plugin scala-maven-plugin struts2-convention-plugin maven-site-plugin sublime-text-plugin struts2-junit-plugin atlassian-plugin-sdk maven-war-plugin dart-eclipse-plugin maven-failsafe-plugin jacoco-maven-plugin browser-plugin jquery-forms-plugin play-plugins-redis maven-glassfish-plugin maven-surefire-plugin openjpa-maven-plugin build-helper-maven-plugin maven-scm-plugin maven-install-plugin ie-plugins maven-release-plugin google-earth-plugin eye-tracking transfer-encoding multiple-entries dom-traversal phone-state-listener jay-parser-generator php-parser tsql-parser python-parsley dom-parser xpathdocument hachoir-parser css-parsing xpinc android-pullparser xps-generation parse-recdescent sparse-matrix xpages charniak-parser ruby-parser url-parsing xperf gold-parser xproc uu-parsinglib sparse-array apache-regexp earley-parser tlbexp xpress jmxmp jaxp string-parsing xpathquery xps xpathnodeiterator email-parsing xpi xpo query-parser xpc xpages-extlib xpath-api xmppframework xpc-target parse-android-sdk datetime-parsing xpressive command-line-parser parse-url windows-xp xpcshell sjxp xpcom parse-combinator xpce xpsviewer qregexp xpath-3.0 xpdf xpointer xpath-2.0 xpath-1.0 parse-framework xmpp4r xpages-ssjs xppageselector sparse-columns parse-tree html-parsing sjsxp xpdo most-vexing-parse command-line-parsing exp xp-cmdshell pdf-parsing xptable gxp sparse-checkout xpathnavigator xp-theme jericho-html-parser xp-mode xpand php-parse-error right-align tips-and-tricks line-intersection webcenter-sites max-size constant-expression request-cancelling web-inspector web-search units-of-measurement xamarin.mobile model-view-controller root-certificate salesforce-ios-sdk online-radio google-maps-markers bean-io google-drive-sdk port-scanning unboundid-ldap-sdk fatal-error unit-testing web-ide adobe-reader identity-map audio-streaming user-mode-linux file-move app-themes build-time divide-by-zero date-parsing incremental-build context-switch facebook-batch-request linkage zen-coding test-data multiple-views camera-overlay jquery-ui-slider sanity-check type-declaration razor-declarative-helpers declarative-security using-declarative devel-declare openfire zend-rest wordpress-plugin-dev azure-storage-tables mobile-analytics iphone-web-app svn-client application-management coordinate-systems farsi software-tools facebook-audience-network spring-integration data-visualization nintendo-ds web-component system-calls http-chunked steam-condenser memory-mapped-files windows-embedded youtube-javascript-api publish-actions openurl text-coloring async-loading starter-kits startup-error open-with self-closing event-capturing on-screen-keyboard merge-file google-api-client yahoo-api rest-assured wcf-client paypal-api market-share call-control load-order vungle-ads dds-format android-ide mod-python dual-sim youtube-iframe-api event-delegation setup.exe file.exists security-testing location-aware jquery-terminal jquery-plugins add-on multiple-select model-view voice-comparison django-views sql-server offline-mode weather-api code-formatter location-services event-bus large-file-upload fully-qualified-naming login-attempts character-encoding add-in find-occurrences dynamically-loaded-xap analog-digital-converter reporting-services session-state-server labview custom-contextmenu purchase-order mobile-country-code well-formed square-tape webservice-client chrome-for-android http-protocols openvpn image-stabilization lync-2010 color-space lync-2013 web-site-project large-file-support server-side angular-ui-bootstrap email-attachments fortumo amazon-mobile-analytics libfaac smtp lines google-apps maven-ant-tasks turn-off database-table pre-commit content-management-system webresource.axd binary-deserialization url-design fixed-header-tables exit-code background-drawable aide-ide tag-soup text-files read-unread speed-test ad-hoc-distribution source-sets nest-api links input-buffer android-log using-directives chrome-web-store gradle-android-test-plugi request google-profiles-api asp.net-web-api inertial-navigation building google-provisioning-api generated-code facebook-java-sdk http-accept-language http-authentication bing-maps-api ssh-tunnel credit-card-track-data as3-api freerdp news-ticker salesforce-service-cloud input-language smil html-email code-snippets google-places-api good-dynamics large-data voicemail type-2-dimension serializable facebook-sdk-4.0 echo-cancellation continuous-testing html-frames http-request-parameters listview-adapter at-command cross-correlation code-readability dummy-data openwrt canonical-name single-table-inheritance pdf-annotations smpp code-habits nested-generics spacing outer-classes alcatel-ot class-reference random-access http-upload motorola-droid mail-server remote-client processing.org ui-patterns mobile-application processing-efficiency many-to-many delimited-text adaptive-threshold python-c-api menu-items facebook-authorization azure-media-services redundant-code java-binding tic-tac-toe ray-picking date-arithmetic file-exists non-termination exception-handling sencha-touch-2.2 sencha-touch-2.1 sencha-touch-2.3 github-api prefix-operator http-content-length amazon-product-api sencha-touch sencha-command application-data project-settings error-console object-serialization summary chrome-app-developer-tool option pull-request kendo-ui raspberry-pi sim-card primitive-types in-clause background-application distributed-system facebook-graph-api multipart-form ion-koush facebook-apps read-data boost-serialization symbian standard-icons class-extensions up-button lang page-loading-message jquery-ui-dialog named-pipes google-app-engine-java third-party-controls release-builds large-data-volumes variable-length-arguments neural-network-tuning background-music facebook-sdk-3.0 facebook-sdk-3.1 jquery-ui single-instance scala-java-interop drupal-schema xss-prevention prismatic-schema xslt-1.0 information-schema xsockets.net xsp xss dynamic-schema schema-compare xslcompiledtransform xslt-2.0 xsd-validation angular-schema-form db-schema payment-schemas xssf schema-design xslt-grouping canonical-schema multiple-schema xserver xstream clj-schema snowflake-schema schema-migration solr-schema xsd.exe biztalk-schemas star-schema video-thumbnails python-docx cxfrs crossdomain-request.js rexx pow.cx pcx licenses.licx phpdocx cross-domain-policy crx log4cxx mscomct2.ocx cxx-destruct cxf-client livedocx libpqxx cxxtest novacode-docx pscx bluetooth-socket legacy-code sencha-cmd5 idle-timer android-wear-data-api asp.net-identity-2 flash-media-server rational-test-workbench dom-events long-click render-to-texture reverse-ajax cpu-load cloud-code mutual-exclusion data-structures nexus-one code-generation code-translation postgres-xl application-level-proxy top-level high-level type-level-computation rubyxl high-level-architecture apiaxle stack-level stxxl xla warning-level xlsb jexl xlsread low-level-code xlw xlslib row-level-security low-level-io compatibility-level first-level-cache cisco-axl xlet xlconnect access-levels gxl xlutils xlispstat xlocale second-level-cache stagexl pclxl readable rtsj readelf openbsd webgl-extensions up-casting generic-collections tablet-pc software-quality window-soft-input-mode data-dictionary google-api-dotnet-client http-compression visual-studio-lightswitch frege graphical-interaction access-point-name soap-serialization android-os-handler smaato delayed-execution yui-datasource domaindatasource json-schema-validator libm foreign-collection libs generic-list libz this-pointer tightly-coupled-code object-object-mapping fast-forward tab-completion ruby-on-rails-3 ruby-on-rails-4 command-line-interface sampling libc language-interoperability step-into remote-management firebug-lite group-membership line tabbed-interface html5-apps service-accounts smspdu ui-guidelines binary-serialization fire-tv average-precision forall ui-virtualization gdata-java-client dolby-audio-api service-not-available key-value-store optimus opencsv couchbase-lite html-agility-pack windows-media-server if-statement loopback-address data-capture remote-server layout-inflater data-synchronization php-socket palm-os ruby-serialport data-serialization software-serial serial-processing active-model-serializers serial-communication node-serialport wcf-serialization django-serializer virtual-serial-port wmi-service eclipse-tptp relative-addressing type-erasure observer-pattern clock-synchronization low-level-api openldap cross-reference database-tuning-advisor database-events database-diagramming database-com-sdk refactoring-databases database-create vertexdata progress-database database-independent database-theory database-template database-comparison database-com database-driven database-programming system-databases database-installation non-relational-database advantage-database-server database-dump main-memory-database document-based-database database-diagram database-tools database-cloning portable-database in-memory-database database-users drop-database default-database transactional-database database-fragmentation ef-database-first database-edition database-scripts database-caching location-provider package-explorer sbt-idea pass-data base-url css-line-height java-3d linking-errors form-data include-path code-injection logmx coldfusion-mx vsixmanifest argmax mxit betamax sqlmx wimax negamax maxthon maxent maxtotalconnections mx4j fmx daq-mx maxby appxmanifest blitzmax mxhr configuration-profile java-2d corrupted-state-exception hardware-id application-singleton many-to-one lwuit-layouts memory-layout page-layout bundle-layout ios-autolayout layout-engine jquery-layout xlc xlsm jquery-ui-layout asp.net-mvc-layout null-layout-manager code-layout xlink form-layout qt-layout graph-layout layout-page layout-extraction dxl backbone-layout-manager project-layout zend-layout xliff standard-layout object-layout gwt-tablayoutpanel pattern-layout streaming-radio visual-studio-debugging clx relx lxrt clustalx rdlx dlx app-launcher mpeg2-ts google-mirror-api xcelsius xcode3.1 threaded-comments xceed-datagrid xclip nxc acts-as-commentable xcode6-beta6 xcode6-beta7 xcode6-beta4 xcode6-beta5 drupal-comments xcode-bots xcode6gm xcode5.0.1 xcache postgres-xc xcode-workspace comment-conventions xcode-project xctool xcode-organizer django-comments xcode-service xcdatamodel xcode-tools xcore xcode-template conditional-comments xcode-server xcasset xcarchive lxc xcos xcode3.2.3 xcode4.6.2 xc16 xcode4.6.3 xcsnapshots xcode3to4 top-command xcrun xcode-scheme xcf xcb xc8 xceed block-comments xcharts xchat2 xcconfig xcode4.4 xcode4.1 pull-to-refresh html-content-extraction key-value-coding bean-validation asp.net-webpages export-to-excel built-in html-input file-io exceed vrml fgrep code-sharing for-loop snap-to-grid custom-adapter re-engineering visual-studio-2008 backup-agent unit-conversion data-entry alloy-ui matlab-toolbox visual-studio-2012 visual-studio-2013 visual-studio-2010 message-loop bitwise-or command-line-arguments python-cffi modern-ui intellij-plugin yandex-api wcf-service user-settings sample-data jersey-client naming greenrobot-eventbus iphone-maps flash-cc data-manipulation network-state contact-form-7 external-js angular-ui-router libexif simd opera rounding-error bus llvm-3.0 sensitive-data wii-balanceboard common-library box-api opera-mobile effective-java openx uses mediawiki-api error-recovery complex-data-types user-data web-sql lua-table pseudo-class r-tree light-sensor text-width facebook-stream-story linegraph ane lens spotify-app html-entities android-host-api osx-mountain-lion typecasting-operator thread-synchronization system-error rational-rsa merchant-account port-number libmms closed-source web-database unordered-map open-graph-protocol sdl-image c-libraries date-comparison yahoo-weather-api navigation-style libpcap data-storage google-api-java-client acc google-ad-manager denial-of-service acs application-error stress-testing ui-design disparity-mapping sgmlreader xhtmlrenderer nexus-s localsocket c-ares jaunt-api game-ai web-traffic asp.net-web-api-routing google-cse applicationmanager proxy-server text-justify network-share adwords-api-v201109 android-speech-api cross-browser type-hinting static-initialization linkedin-j ase listview-selector store-data lucene-index ape event-receiver open-mobile-api query-builder gwt-url-builder typebuilder build-process productbuild javabuilders teambuilding selenium-builder msbuild-batching module-build dojo-build report-builder2.0 powerbuilder.net buildmanager fbx xbuild msbuildcommunitytasks objectbuilder dbx cocosbuilder vbx msbuild-wpp c++builder-xe4 pbx c++builder-xe5 c++builder-xe2 c++builder-xe3 pybuilder c++builder-xe6 c++builder-xe7 pre-build-event telerik-appbuilder sqlcommandbuilder buildpack powerbuilder-conversion jbuilder nightly-build jsonbuilder build-tools phonegap-build flash-builder4.5 reportbuilder msbuild-4.0 liferay-service-builder predicatebuilder parallel-builds c++builder-xe wxformbuilder ocamlbuild jibx boost-build build-chain c++builder-2009 c++builder-2006 c++builder-2007 buildroot pdebuild product-builder buildmaster c++builder-2010 buildhive build-automation interface-builder msbuild-itemgroup gprbuild assemblybuilder flex-builder-3 sublime-build build-controller visual-build-professional buildengine extension-builder3 cljsbuild uribuilder flash-builder build-triggers joomla-community-builder promoted-builds boost.build jhbuild build-agent finalbuilder ckbuilder expressionbuilder shake-build-system sqlbuilder flashbuilder4 msbuild-task builtins msbuild-api buildforge gtkbuilder html-treebuilder msbuildextensionpack batch-build formbuilder buildapp dailybuilds string-building input-builders debug-build jenkins-job-builder buildr-extension buildr msbuild-buildengine sbt-buildinfo build-management swingbuilder microsoft.build buildbot grunt-build-control pkgbuild buildaction msbuild-propertygroup javascript-build c++builder64 quickbuild teambuild2010 gui-builder uibuilder buildconfig antbuilder powerbuilder-pfc kbuild buildprovider buildfarm build-pipeline build-server jsbuilder httpbuilder rebuild vcbuild msbuild-target build-definition buildpath build-rules luntbuild wspbuilder buildconfiguration builder-pattern nbuilder couchdbx incredibuild ndk-build tagbuilder platform-builder markupbuilder spritebuilder redquerybuilder team-build fusionpbx halbuilder form-builder team-build2010 powerbuilder-build-deploy radbuilder pabx build-events ocp-build build-script buildingblocks c++builder-6 c++builder-5 ebuild custom-build pbuilder build-process-template msbuild-projectreference restclientbuilder build-numbers oracle-warehouse-builder streamingmarkupbuilder prebuild coldfusionbuilder cross-fade google-reader api-doc service-provider xamarin tool-uml cross-application native-executable email-client facebook-rest-api message-type search-form formats slow-load 2d-engine encryption-symmetric can-bus drag-and-drop simulator jquery-ui-accordion always-on-top multipartform-data processing-instruction mailing-list sql-limit inflate-exception vnc-server ar.drone time-estimation lejos-nxj loading-image data-compression event-log data-mining linkify libharu universal-image-loader team-explorer-everywhere cmd.exe http-caching point-clouds apple-touch-icon application-settings node-request qt-quick sql-drop twitter-streaming-api segment-io google-form navigation-list maven-compiler-plugin calabash-android exception-notifier data-stream file-io-permissions percent-encoding syntax-error test-suite compiler-errors id3-tag openfeint code-conventions application-restart security-by-obscurity launching-application xbox-music zooming window.open custom-events htc-dual-lens-sdk object-pooling google-docs-api saml pin-code automatic-updates es5-shim html-table xmonad libgcj google-music in-app-billing french java-web-start exchange-server-2010 message-listener rss-reader smooth-streaming feature-extraction ftp-client open-nfc mercurial-subrepos friendly-id tf.exe activex-exe font-editor version-editor vshost.exe xenomai in-place-editor xmemcached javascript-editor yi-editor wp-editor formula-editor sn.exe jquery-datatables-editor gui-editor xeround tofixed mvc-editor-templates winexe resource-editor service-config-editor w3wp.exe chrome-dev-editor kendo-editor tangible-t4-editor xenforo css-editor property-editor lwuit-resource-editor yui-editor content-editor midas-editor mxe icon-editor e-texteditor telerik-editor editor-zone django-ckeditor google-maps-mobile file-access providers vorbis jquery-ui-timepicker http-response-codes lwuit-form jasper-plugin couchbase-view top-down scheduled-tasks voice-recording periodic-task client-applications android-async-http system-variable rich-text-editor non-printing-characters java-util-logging dial-up code-inspection typed-arrays return-code database-server location-based-service coding-style bing-maps-controls resource-id file-ownership data-security overlapping-matches commercial-application ms-word socket-timeout-exception ucs csplit acs-serviceidentity iodocs cstringio css-gcpm csound getsystemmetrics localytics oracle-analytics ancs practical-common-lisp filtered-statistics csqldataprovider appdynamics systemdynamics secs wpf-graphics cscfg riak-cs css-to-pdf cheminformatics ansi-common-lisp azspcs astrophysics cssadapter csdl pygraphics cstdint mcs denotational-semantics zelle-graphics csso csharpoptparse acm.graphics yahoo-analytics django-statistics ghc-generics csvde ergoemacs diff-lcs cssedit css-variables sitecore-dynamics css-grids csb synopsys-vcs mds-cs system.graphics libwcs cocoanetics countly-analytics css-friendly cstdio csstidy cshrc quality-metrics ownership-semantics jscs ruby-basics csx codahale-metrics texmacs apache-commons-dateutils uibarmetrics playnomics spring-data-commons angulartics csrss mathics lowest-common-ancestor rabbitvcs cssom openwebanalytics pgraphics common-access-card css-hack swift-generics apache-portable-runtime flurry-analytics lccs openacs pecs worklight-analytics simics read-the-docs apache-spark-sql email-analytics apache-commons-jci psychoacoustics relaxed-atomics cybernetics cspack apache-wink-spring csvtools css-rem cvcs cs-script dmcs date-formatting libvirt http-parameters pocketsphinx-android google-api world-map lazy-loading google-search-api opengraph android-lint cross-process android-cts wcf-data-services service-discovery smooth http-head time-series code-signing spring-android visual-c#-express-2010 tone-generator js-test-driver names sql-insert ws-trust video-encoding access-modifiers obscured-view multiple-tables user-defined-types cross-compile background-service configuration-management natural-sort facebook-as3-api bsd-sockets nextgen-gallery writing time-bomb network-security seam eclipse-memory-analyzer rooted-device facebook-ios-sdk data-members pdf-extraction ubuntu-touch file-not-found java-client firefox-addon-sdk web-clips checked-exceptions object-detection nativecss unix-socket eclipse-mat multiple-apk samsung-smart-tv asp.net-web-api2 serializer libssh2 auto-close console-application echo-server http-digest on-duplicate-key fbml android-custom-view canon-sdk google-plus-one tizen-web-app json-lib type-mismatch pax-web ld-preload programmatically-created apache-commons-net facebook-field-expansion lib-nfc titanium-proxy channel-api config-files qt-necessitas in-house-distribution user-testing zend-gdata file-listing expensive-resources string-to-datetime jax-ws touchmove ftp-server camera-view seam-carving cross-domain-proxy loose-coupling rational-performance-test apple-maps data-encryption facebook-actionscript-api storing-data jquery-mobile-popup owl-api http-referer speech-to-text instruction-set inner-join app-update client-side terms-of-use lint application-icon openerp-7 uses-feature square-oss media-library matlab-coder windows-ce trademark-symbol opensuse windows-server function-signature type-signature signature-files google-chrome-app pebble-sdk java-compiler-api frapi program-transformation multi-dimensional-scaling google-feed-api html-post memory-alignment container-file dbi-profile short-filenames ear-file file-globs llvm-filecheck xfn file-reference-url drupal-file-api welcome-file lua-loadfile distributed-filesystem ithit-ajax-file-browser restricted-profiles feature-file valums-file-uploader war-files file-find single-file zend-db-profiler apache-commons-fileupload print-to-file filenet-process-engine filesystem-browser file-diffs django-file-upload filenet-bpf mysql-loadfile xfdl file-icons filenet-workplace xfdf file-import mvc-mini-profiler long-filenames rack-mini-profiler xflr5 virtual-file dof-file help-files zend-file common-files compound-file sql-server-profiler ant-loadfile mercurial-bigfiles ini-files file-generation war-filedeployment jquery-file-upload android-expansion-files downloading-website-files filesystem-access makefile-project txf asp.net-mvc-file-upload remote-file-inclusion file-in-use file-dependancy managed-file multiple-file-upload bootstrap-file-upload select-into-outfile pdw-file-browser xfermode bluetooth-profile seafile-server po-file java-ee-web-profile file-inclusion file-moving roaming-profile source-file-organization file-traversal multiple-makefiles load-data-infile filenet-panagon roxy-fileman all-files drupal-filefield xforms-betterform temporary-asp.net-files arduino-makefile railsmini-profiler is-uploaded-file eclipse-project-file angular-file-upload shared-file file-connection core-file filesystem-events map-files sln-file jquery-fileupload-rails fixed-length-file django-filer rackspace-cloudfiles gnu-fileutils sharepoint-userprofile ignore-files system-profiler nhibernate-profiler archive-file .net-client-profile file-link log-files filenet-image-services reliable-secure-profile data-migration jquery-ui-draggable wake-on-lan facebook-graph-api-v2.0 select-query custom-exceptions tizen-wearable-sdk view-helpers in-app bitwise-and ydn-db updatedate nativecompilation worklight-server sony-camera-api proxy-classes polar-coordinates launch apex-code memory-limit figures office365-apps google-maps-android-api-2 google-maps-android-api-1 jax-rs project-documentation libtiff touch-events libxml libxml2 maven serverxmlhttp server.xml sqlxml simplexmlrpcserver sql-server-2012 system.xml system-tray xmlhttprequest plugin.xml jenkins-plugins post-build build.xml sql-server-2005 string-conversion sql-server-2000 clientaccesspolicy.xml rapidxml xmlrpcclient sql-server-2008 state-diagram drawing like-operator firemonkey-fm3 maven-wagon-plugin application.xml xmlmapper xml-database xmldataprovider missing-data xmldatasource gdataxml xmldataset firebug entity cartesian-coordinates jquery-xml annotation-processing jar xmltype return-type xmlindex cpu-time facebook-oauth xmlencoder vsx voicexml vs-extensibility voiceover android-xml lxml xml simulation openxml xampp pom.xml xml-serialization ssms xmpp xml-schema simplexml xml-parsing sms symfony2 twig security-constraint xmlsec scxml dynamic-sql drawable xmllist gm-xmlhttprequest xmlnodelist openxml-sdk operator-keyword google-breakpad repositories space android-xmlpullparser open-intents xmlrpclib conflicting-libraries xml-binding noise-generator xml-formatting helix-server belongs-to touchxml xmltodict xmlstore xmlconfigurator javamail xmltable facet fastercsv vsts2010 fasterxml facets faceted-search dojox.charting libxml-ruby xml-file nsxmlnode xmlignore xmlnode ixmldomnode create-table filenet-content-engine calling-convention xml-namespaces samba qt5.2 qt5.3 qt5.1 custom-object loader loaded gstreamer fetchxml xmlsocket opengl-es-2.0 xmltext xml-rpc.net xmlserializer xmlslurper xml-simple xmlseclibs xmlbeans select-xml writexml innerxml persistence.xml xml-attribute xml-encoding xmltextwriter geoxml searching-xml ixmlserializable xml-spreadsheet ixmldomelement xmlwriter ixmldomdocument xmlvend princexml nsxmlparserdelegate signedxml xml-sitemap xmlservice context.xml xmlupdate inline-xml hibernate.cfg.xml xml-declaration xmlattributecollection nsxmldocument sitemap.xml xmltextreader zend-xmlrpc xmlstarlet ebxml ejb-jar.xml xmlhttprequest-level2 updatexml manifest.xml xml-visualizer xml-comments nsxmlparsererrordomain xmlhttprequest-states web.xml txmldocument xmlanyelement xmlstreamwriter xmlformview closedxml xml.etree geoxml3 fxmlloader nsxmlelement xmlstreamreader xml-documentation dbms-xmlgen xmltransient xmlpullparser parsexml latexml xml-encryption xmlstreamer nativexml outerxml xmlschemaset xml-generation xmladapter xmlblaster xmlhelper xmllistcollection qxmlstreamreader nsxmlparser xmldocument xmlexception scales-xml xml-entities xml-deserialization xml-drawable xmlpoke rexml xmlmassupdate readxml xmllite xmlstreamwriter2 xmlinclude xmlconvert oxygenxml xmlreader xmlsec1 xmlworker xml-literals xml-editor xml-builder xml-signature xmlhttp pyxml xml.modify fxml microxml xmldsig kissxml qtxml xna-4.0 xmlspy pugixml qbxml xna haxml xmlport xmllint cxml kxml2 musicxml hbmxml sxml xmlcatalog wbxml swixml xmltask mxml msxml msxml4 msxml3 msxml6 kxml msxml5 qdbusxml2cpp lxml.html mxmlc xml-rpc luaxml gbxml nxml tinyxml++ ccxml xnamespace xmlunit vxml xmlroot crossdomain.xml xmlns xmla xmldom xmldog xmldoc tinyxml omnixml pubxml javax.xml xmldiff nsxml tbxml recurring-events image-generation xml-twig org-mode select-tag sql-management-studio sql-order-by sql-ce screen-scraping schema scheduling saxparser scrapy-spider sqlmembershipprovider sql-syntax socketio sql-server-2008-r2 sxs sigkill scroll sql-like sccm xquery-update xcode-storyboard scrolling sax script# hsqldb xcode6-beta4 xquery sql-server-express scikit-learn scope sketchup xcode scss security scrapy sqlite scom scripting xcode6 sql-match-all sequence-points scrapyd sql-server-ce-4 stxxl socket.io segmentation-fault sockets sql xcode4.5 scheduled-tasks xcode4 sql-server sql-loader palm-pre false-positive ion xml-configuration xml-validation ternary-operator repository oracle-adf-mobile helper switch-case scientific-computing cakephp-2.1 error-log draw tabbing resource-management ui-automation venn-diagram read-write blackberry-simulator asp.net-mvc-5 asp.net-mvc-4 asp.net-mvc-3 delphi-xe6 delphi-xe7 delphi-xe5 delphi-xe2 simple-framework asp.net-mvc-2 code-efficiency hibernate-search build-gradle url-encoding text-styling jquery-1.9 here-api data-paging selectable packet-loss magnet-uri domain-model selectors-api xml-column magento-layout-xml xml-layout zimbra editing pdf-reader simulate symlink sml winsockets similarity touches visualization opengl-es-lighting berkeley-db-xml export-to-xml sql-server-openxml localizable.strings liferay-6 android-emulator-plugin xmlbeans-maven-plugin third-party-code core-text vb.net sdk3.0 opera-mini multi-column custom-component java.util.date facebook-fanpage scala-2.10 c++-cli content-length color-codes netbeans-plugins htmltext tcp-ip generator maven-plugin string-literals qt4.7 qt4.8 vtd-xml builder temporary-files phonegap-plugins hammer.js session-timeout figure uiapplicationdelegate microchip android-loader pci-compliance line-drawing gui-builder labels generated opengl-es-3.0 particle-system ios6-maps iterate httprequest recycle-bin container-classes gwt-rpc multiple-definition-error text-formatting get-request code-coverage document-storage post-request linq-to-xml linker merging-data friend apache-commons-logging login-config.xml application-server compiler-options text-size runtime-error class-hierarchy thread-priority carriage-return jquery-chosen domdocument in-app-purchase mini-xml binary-xml pls-00323 html5-audio texas-instruments web-config music google-schemas http-post data-recovery osx-mavericks for-xml openoffice-calc encryption-asymmetric scaling libusb font-face html-parser conditional-operator xamarin.mac xml-libxml content-encoding terminal-ide xml-transform xtable servicestack-text monad-transformers kext transform responsetext android-context dbcontext gwt-ext gettext email-ext delimited-text coordinate-transformation synchronizationcontext catransform3d cgaffinetransform sublimetext genfromtxt xtext pdftotext plaintext cgaffinetransformscale innertext settext texttransform speech-to-text i18next xtend jqtransform httpcontext xterm richtext web.config-transform cgcontext fulltext rotatetransform datacontext css-transforms transformgroup asp.net-vnext xml-nil nxt xtify xts next colortransform objectcontext xtk rendertransform drawtext bundletransformer catransform3drotate itext xtragrid robots.txt text layouttransform restructuredtext xslcompiledtransform xtrareport android-edittext hxt resulttransformer extra xml-dtd nsaffinetransform imagettftext catransformlayer nsvaluetransformer uigraphicscontext gxt hough-transform transformer nsmanagedobjectcontext gridextra applicationcontext msitransform transformtovisual affinetransform scaletransform context main-activity stat in-operator text-recognition build.gradle sql-view outgoing-mail jquery-mobile multiple-databases html-select select-menu android-l usb-mass-storage libvlc text-search common-code where-in java-6 java-8 java-7 persistent-connection xml-conduit sql-triggers urbanairship.com machine-learning http-status-codes xmlhttp-vba build-system facebook-login android-library data-partitioning nvd3.js geotools settings.bundle shortest-path inversion-of-control selectlist writefile asynchronous-processing smack internal-link data-extraction code-organization 3d-engine gd fs java.util.concurrent binding urlstream android-download-manager du dx pixel-perfect xml-dml portable-applications em ca matrix-inverse ssml here-maps multiple-instances backup-strategies cp collect ice ar av ide migration macros max microsoft mocking microsoft-metro makefile mkmapview make timed-events navigation-drawer custom-build-step linq-to-sql save-image realm apache-commons-io rotational-matrices chinese-locale pe pc pi smali ln ls border-layout build-automation data-sharing mv mobile-os xml-import oracle-adf ime update-site int multiple-files ini ld lg file-manipulation bin-packing data-management ir il io text-indent text-based xt google-document-viewer ms-access load vi app-config tabbed-view server-variables smart-tv paypal-rest-sdk hybrid-mobile-app android-x86 multi-layer pull bits-per-pixel su error-suppression autocad-plugin flickr-api so rapidminer opengl-es-1.1 sh android-date rm rc app-id qt boost-log provider pt ps http-unit batch-file bump-mapping weak-references method-signature generic-method importance css-border-image magnific-popup web-reference for-xml-path foxit-reader full-text-search android-layout-weight options-menu web-services-enhancements undefined-reference zend-config-xml simple-xml-serialization xml-schema-collection apache-commons-scxml inline ruby-on-rails-3.2 error-codes scala-collections settings android-compat-lib libxml-js selectionmodel sql-server-ce geographical-information nexmo programming-languages freeze android-parser glm-math do-while pls-00103 live-update single-page-application build automated-tests encoding azure-web-roles ews code-signing-certificate blas single-threaded exe libtool dnsimple simplehttpserver simpleaudioengine simplecart xss-prevention simplecursortreeadapter simpletip pysimplesoap simplegeo xslt-1.0 xsockets.net simplepie amazon-simpledb simplemembership xsp xss xslt simplemodal xsd simplesamlphp simplification simplecv xsl-fo simplecursoradapter xslt-2.0 simpledialog xsd-validation simplecaptcha simplejdbcinsert simple.odata simpledateformat paysimple simpleadapter simplyvbunit xssf firebasesimplelogin simple-injector xslt-grouping simplejmx simpledom simpleworkerrequest simplex simpletest xserver simplify xstream simplejson cmsmadesimple simplyscroll simplepager simple.data simple-form simpletabcontrol simplecov xsd.exe simplerepository simpletype simplebutton famo.us yahoo-maps sentiment-analysis vs-android google-plus varchar reverse-engineering utf8-decode server-push memory-allocation eps github-for-mac graph-visualization form-submit systemtime thinktecture-ident-server database-normalization topaz-signatures datetime-format yandex-maps xml-dsig xml-1.1 semantics xdebug webresource.axd zombie-process xdocument sample samsung-mobile symbolic-math semaphore smart-pointers symfony-sonata ems runtime.exec samsung-mobile-sdk object-to-string google-direction circular-reference state crosstool-ng ejb indy resource-file eye-detection naming-conventions url-design soap-client payment-processing berkeley-db-je gwt-mvp gis md5-file gin parent-child settings.settings conditional-compilation line-breaks non-well-formed geolocation extreme-programming latency simultaneous-calls near-real-time geo spring-el datetime-generation browser-addons dollar-sign code-formatting jquery-isotope phonegap-build third-party import fetch include sqlite-net variant custom-lists interface-builder wificonfiguration libpng scala-ide two-columns search-dialog email-threading ng-view x86-64 midi-instrument sql-date-functions declare aspectj-maven-plugin http-get interpolate explicit-intent panorama-control machine-instruction art-runtime frp views jquery-slider google-datastore smslib geo-schema html-escape-characters plugins updating suppress-warnings openal circular-list internal-server-error sql-azure layout-animation os.path java-io loading opencl login-page plugin-pattern opencv mms-gateway jquery-mask db2 anonymous-types public database-backups java-ee selenium-ide mobile-safari apache bins dbm backbone-events openid items opengl android-build windows-media-services edit-in-place bind openmp concurrent-collections http-server cts facebook-fql geometry-path sts-springsourcetoolsuite form-validation audio-playing jagged-arrays tabular-form jquery-lazyload log-viewer file-writing porting discussion-board documentation-generation java-me fragment-tab-host libreoffice-base anti-patterns user-agent code-templates cli ember.js try-with-resources request-headers com cos time-format android-file oracle-apex pexpect apex persistence persistent data-formats openerp gaps-in-visuals thread-exceptions simile system.out password-recovery python-requests google-fusion-tables g1gc google-chartwrapper google-reader google-chrome-app google-closure-library google-charts-api google-drive-sdk google-search-api google-search-appliance googlemock google-places cxf google-places-api google-talk c++0x cocos2d-iphone google-chrome-os googletest google-account google-adwords google-drive quickblox google-app-engine google-cloud-storage google-cloud-messaging cql google-apps-script google-maps-android-api-2 google-calendar google-code gqlquery google-search google-maps-api-3 google-cast google-api google-nativeclient ccnet-config gawk google-spreadsheet google-docs cookies google-website-optimizer google-play gquery cakephp google-play-games gcm checkmark chicken-scheme google-street-view cocos2d google-experiments google-refine cqrs google-maps gql google-docs-api checkbox google-maps-markers google-closure-compiler google-geochart google-chrome google-visualization google-closure cctray ccrc partial-page-refresh buddy.com source-maps remember-me ccl custom-attributes ndk-gdb sprite-kit indoor-positioning-system cep local-storage data-acquisition constructor-chaining database-abstraction password-confirmation photo-upload arduino-ide image-formats class-loading sql-delete maven-central intel-galileo ear ratio oxygene silent-installer delete-row imports selector state-machines database-versioning dynamically-generated xa testcaseattribute nonserializedattribute attributeerror readonlyattribute routeattribute actionfilterattribute displayattribute getcustomattributes frombodyattribute attributerouting.net nsmutableattributedstring xalan assemblyversionattribute attributeusage addattribute setattribute authorizeattribute nsattributedstring gitattributes attributes nsattributedescription ohattributedlabel serializableattribute validationattribute xamarin.forms xargs methodimplattribute nested-attributes attributerouting tttattributedlabel xaml xamarin xap columnattribute getattribute filterattribute xattribute xamarin-studio configuration-files maven-gae-plugin canonical-link apache-kafka javax.mail data-modeling json-simple som xamarin.ios symbolic-computation relative-path maven-resources-plugin series alphabetical-sort geomap restful-architecture soap-extension memory-leaks satellite-image template-engine iterator cocos2d-x amazon-ses tf.exe file-encodings activex-exe church-encoding xeon-phi urlencode asciiencoding aspnet-regiis.exe vshost.exe flashmedialiveencoder xen sendmail.exe xenomai nsstringencoding xmemcached uuencode jpegbitmapencoder character-encoding expression-encoder formencode sn.exe xemacs schtasks.exe html.encode xerces scala-xml xeround xenapp bitmapencoder haxe xerces-c pxe setup.exe c++builder-xe shinemp3encoder encodeuricomponent winexe svcutil.exe w3wp.exe mencoder zencoder autoencoder wsdl.exe uriencoding xenforo oracle-xe cl.exe encode xelement xelatex delphi-xe mxe huffman-encoding py2exe html-encode cmd.exe boxen json-encode google-eclipse-plugin parse-error cloud-storage selected binary-compatibility layout http-host window-position remote-execution amazon-sqs dom file-storage bing-api dos apache-axis binary-diff catalog object-recognition string-search signalr.client dms server-communication access-log selection system.io.packaging expat-parser doc data-protection login dtd language-translation repository-design sencha-cmd text-extraction double-buffering stateful serial sender-id expression-blend architectural-patterns ia-32 garbage-collection google-drive-api finite-automata language-translator mail-sender asynchronous-postback text-to-speech der des csharpcodeprovider powerpc protorpc fsharpchart sunrpc rpc fsharpcodeprovider icsharpcode sharpcompress rpcl erpconnect rpclib dex advertisement-server readability connection-timeout multi-select map-projections url-redirection net.p2p click-counting diff zend-framework build-process dynamic-ui boost-spirit worklight-runtime gae-eclipse-plugin localization locking logging log4net safari-web-inspector rcs string-formatting windows-runtime java.util.calendar texture-mapping ras code-search-engine wcf-binding file-security non-scrolling search-box element ros boost-build roi exc-bad-access document-store rms access-point openshift semantic-web ruby-on-rails touchesmoved layout-gravity pseudo-element stack-trace adapter ria side-effects delegates video-recording graceful-degradation logfile-analysis touch-event non-deterministic export-to-csv url-parameters webkit-transform currency-formatting helpers facebook-opengraph iterable ms-office letter-spacing cocos2d-x-2.x android-view in-call android-service three.js nodelist microsoft-sync-framework mkmapkit microsoft-dynamics m2eclipse mac-address javax.imageio raw-data anti-piracy live-sdk lock-in android-maps corrupt-data boot-animation background-image power-state windows-xp-sp3 windows-xp-sp2 for-xml-explicit pin-it-sdk java.lang.class cross-domain protect-from-forgery path-separator asp.net-ajax jaxb2 jxl javax.swing.timer javax.sound.midi jaxb2-maven-plugin jax-rpc rexx xajax jaxws-maven-plugin jxls jax-ws microsoft-ajax ajax jaxb2-basics reverse-ajax jaxp telerik-ajax jaxb jax-ws-customization jxtreetable dajax mockjax drupal-ajax jxpath log4cxx jxta asp.net-mvc-ajax jaxbelement jxtable jaxer libpqxx unobtrusive-ajax mathjax jquery-mobile-ajax xxd jax-rs pjax node-set formula android-source view openssl right-to-left parameter-passing color-channel pop-up file-download charts maven-archetype cocos2d-android saving-data sdk search-suggestion android-selector compact-database build-settings control-c ruby set static-code-analysis sip-server sed sql-injection .doc build-tools spy data-connections composite-component gwt-bootstrap sliding-window project-reference eclipse-plugin-dev sos stateless-session-bean fat-binaries jquery-autocomplete http-equiv char error-code select-for-xml xml-document-transform iis-express com.sun.net.httpserver cross-compiling scalable system.data.sqlite smp http-proxy smb custom-cursor swf project-planning banner-ads git-pull custom-event serialization travis-ci sys document producer-consumer stack-corruption chunked-encoding google-cloud-sql ext.list processes precision pax-exam parallels parsley.js parser-generator parse-url process parsec processing.org processing parse-tree prism-4 processid prism lotus-domino sum sup email-validation web-based nat-traversal document-root tar objective-c++ tap server-side-validation raw-input eclipse-wtp currency-exchange-rates jquery-masonry file-location writetofile parsing iron.io apns-php ui-testing levels ws-client saml-2.0 layout-xml javax.crypto grails-plugin gui-designer file-association linked-list sql-manager pda oracle-sqldeveloper relational-database image-gallery script-tag scala-2.8 pac bar-chart iterative android-simple-facebook android-layout http-error smime zend-studio tinymce pen secure-coding http-live-streaming iteration array-map dojox.mobile orient-db twitter-oauth embedded-browser pid cruisecontrol.net live-streaming collections fortran closed-captions android-sdk-tools jmeter-plugins storm-gen usb-debugging apache-commons-httpclient pts strip-tags soft-keyboard format-string data-loss has-many conditional-formatting buildconfiguration installation-path ndk-build data-binding background-audio phonegap-pushplugin galaxy-tab mapping standards-compliance multidimensional-array side-by-side import-contacts nativeapplication jquery-mobile-listview facebook-requests serializable tilt-sensor parsing-error store line-count actionbarsherlock-map scriptresource.axd spring-xd xdt-transform xdomainrequest file-type pdf-form cron-task double-click readline zend-acl push.js data-collection berkeley-db libjpeg geometric-arc fuzzy-search absolute-path event-stream gradle-eclipse package-private network-service rendering-engine code-conversion extension-methods enhanced-for-loop training-data ip-geolocation jodconverter class-library hudson-plugins eclipse-juno tools.jar post-parameter network-scanner android-internet server-sent-events ejb-3.0 directory-browsing nook-tablet apache-tika service-reference background-subtraction visual-studio-express serial-number ado.net css-transitions jboss-arquillian gdata runtime-environment binary-tree date-format pattern-recognition real-time mvvm-light haml zend-framework-mvc card.io cimg indexing haxm maven-jar-plugin country-codes floating-point-precision dataprovider bus-error signed magento apache-commons-lang eyecon-plugin kendo-chart program-counter if-modified-since java-api system.drawing jquery-chaining itunes-store native size-t android-backup-service nfs system-information simulink code-editor string-concatenation root eclipse-emf-ecore gae-datastore key-bindings http-put rational bing-maps nat nas blackberry-android ssl-certificate flash-builder4.5 text-processing phpseclib google-sites formatted android-adapter xpsdocument formatter libsvm libgdx http-streaming asp.net-apicontroller file-search cocoa-touch string-comparison dynamic-data apache-mina dead-reckoning click-through map-directions task-parallel-library system-testing event-tracking build-environment url-rewriting hibernate database-metadata https sharepoint-workflow handlebars.js error-checking convex-hull apache-commons-dbutils master-pages google-code-hosting server-error typing jquery-knob layout-editor encrypted app-inventor pie-chart website-payment-pro browser-plugin singleton-methods event-listeners parcelable password-protection special-characters hidden-files sts-securitytokenservice nil linux-toolchain home-automation auto-indent pkg-config overflow-menu assisted-inject properties-file video-editing multiple-languages out-of-memory ode duplicate-data ci-server windows-live-id types text-editor cross-threading ipod-touch file-recovery objective-c facebook-android-sdk open-source null-terminated firefox thread-sleep app-engine-ndb cpu-cores ab-testing playing-cards asp.net-mvc syntactic-sugar nokia-maps database-update jain-sip cursor-position in-memory candlestick-chart data-consistency java.util.logging layout-optimization duplicate-entry google-directory-api remote-access pixel-ratio data-retrieval ratchet-2 source-code blank-space python-imaging-library forward-declaration pdf-generation google-buzz index spreadsheet osi client-server msiexec mysql mysql-python cocos2d-js document-classification out one-to-many http-redirect oop orm ng-animate 2.5d authorize.net encryption html5-appcache depth-buffer oma crypt memory-access release-management opengl-es sql-parametrized-query normal-distribution expert-system base client sqlhelper transport-stream data-access lorem-ipsum separation-of-concerns sim-toolkit master-slave browser-automation lan data-scrubbing google-earth source parse.com linker-error xamarin.android forums promotion-code visual-inheritance library-interposition virtual dev-to-production taps pretty-print apache-commons-math http-1.1 visual-sourcesafe portable-class-library lazy-evaluation asp.net-membership namespaces svn-externals amazon-payments fastclick.js task eofexception mean-square-error state-saving log-rotation inline-images data-url data-uri development-environment on-the-fly cfml facebook-javascript-sdk data-integrity lto selectbox cascade-classifier lua lsp lte code-review exist-db javax.script query-string library-path thread-safety parental-control port onsen-ui lex content-negotiation piracy-protection binary-data internal-storage ws-discovery app-data file-format jquery-datatables lis custom-headers e-commerce paint.net graphical-layout-editor data-transfer map mat long-integer network-traffic crash-log accessibility web.py generics external-sorting elliptic-curve android-application auto-increment luasocket formview flex-charting sql-parser ebook-reader geos unique-id magento-1.7 magento-1.6 magento-1.4 sbt-android-plugin git-merge paypal-adaptive-payments apache-commons-codec fastscroll cognos-10 ibeacon-android session-cookies mpl delegation point-in-polygon donut-chart apache-commons-compress chromium-embedded android-version empty-string google-text-to-speech collaborative-editing processing.js documentation offline-caching inline-editing jquery-pagination public-key-encryption gantt-chart selectall mvs interrupted-exception packet-capture svn-checkout streamreader data-analysis session-state automated-testing io-redirection code-cleanup graphic-effects outlook-express pre-packaged min tabs mlt symbol image-scaling data-conversion forms-authentication software-engineering mms mmo post-update test-runner low-level zoom incomplete-type motodev-studio blackberry-eclipse-plugin ports updated extending-classes image-loading step-by-step enquire.js updates lazy-initialization end-of-life service-application dynamic-class-loaders client-certificates core-telephony physics-engine ws-security protobuf-net nested-statement cache-expiration function-overriding string-externalization local-files cytoscape.js documents multiple-accounts opencart post-build-event declaration number-formatting dbms pos-tagger device-emulation windows-xp system.exit stream visual-effects core-data static-initializer dbus server-response menuitem-selection http-etag single-sign-on bubble-sort virus shell-exec encoder vertical-text event-listener android-tabs google-earth-plugin window-resize fest html-form system-clock fft video video-streaming future vxworks void-pointers avx clear-cache apache-license batch-insert error-logging group-by ads-api box2d-iphone rule-engine file-read user-preferences google-login arcgis-server web-browser edit n-gram w3c-validation cwac-endless select-insert header-files sd-card ui-thread source-tree non-static code-comments ajax-request frequency-analysis keyboard-layout dynamic-memory-allocation spatial-index http-headers postal-code electronic-signature miglayout nokia-s40 greatest-common-divisor ansi sdl-ttf azure-mobile-services error-reporting database-schema css-text-shadow r.java-file firefox-os boost-asio magento-admin basic-authentication string.format parallel-processing config external-process x11-forwarding symbols sql-update self-signed ubuntu-server bad-request facebook-social-plugins home-server little-endian hexagonal-tiles jquery-jscrollpane ti-dsp is-empty voice hierarchical-data binaries application-name linux netdatacontractserializer pyserial xs ruby-serialport csip-simple xsd-1.1 winsxs data-serialization xsocket siri-xml deserialization xslt-tools subsonic-simplerepository binary-deserialization xsp2 ember-simple-auth xsp4 xslt-3.0 oracle-xml-db qtserialport xsd2code serialscroll xsb software-serial xsitype xsi xsbt-web-plugin sharpserializer xsl-variable xslkey orsserialport serial-processing notserializableexception xaml-serialization chilkat-xml qextserialport simple-authentication active-model-serializers simple-html-dom xsl-stylesheet libserial anti-xml perl-xs usbserial simple-mvvm jsonserializer json-deserialization qtserial datacontractjsonserialize xml-swf-charts xsltc serializearray jmsserializerbundle hana-xs iserializable simple-openni jms-serializer node-serialport simplex-noise wcf-serialization xsl-choose simple-machines-forum django-serializer xsom virtual-serial-port xscale xsdobjectgen xspf serializationbinder ideserializationcallback nsjsonserialization simple-el datacontractserializer javascriptserializer serialversionuid simple-form-for xsl-grouping xsltforms azure-notificationhub engine.io frames code-completion entity-framework sqlexception creative-commons validationframework ivalidatableobject fluentvalidation rangevalidator hibernate-validator validationrule unobtrusive-validation requestvalidationmode validationgroup invalidate maskededitvalidator validatelongrange livevalidation input-validation reactivevalidatedobject backbone.validation.js validationsummary jqbootstrapvalidation validationerror validity.js requiredfieldvalidator validationrules customvalidator invalidation validationmessage eventvalidation validateset validform qvalidator comparevalidator invalidoperationexception invalidargumentexception invalidprogramexception programming-paradigms jvm-arguments raw-sockets except webexception exception noexcept ioexception nsexception comexception node-xmpp build-dependencies transformation visual-c++ nightly-build graph-layout open-graph-beta maven-dependency-plugin jar-signing linear-interpolation java-server mapper terminal-emulator line-numbers drag-to-select stacked-chart iris-recognition team-explorer i18n-gem parser-combinators validating dynamics-ax dnn dns download dynamics-ax-2009 dynamics-crm dynamic-arrays dynamic-linq diagnostics dynamic-allocation path validation vert.x jquery-mobile-loader route-me jquery-animate ms-access-2007 ms-access-2010 file-extension flex-mobile third-party-api content-type calculated-columns matrix-transform method-invocation angularjs-directive filepicker.io pixel-manipulation asihttprequest sp-executesql eclipse-rcp materialized-path-pattern multi-level ean-13 procedural-programming mule-studio eclipse-rap stackoverflow.com exchange-server user-presence html-datalist custom-type xoom html-form-post congestion-control inlining matrix-multiplication server-load mediaelement.js serial-port argument-passing image-editing system.printing memory-mapping http-delete google-books contact-form facebook-likebox select-statement android-manifest breakpoint-sass uv-mapping angularjs-scope inline-block file-uri frequency export pos-tagging render-to-string import-libraries windows-server-2008 windows-server-2012 video.js thermal-printer interpolation wcf-rest page-curl inter-process-communicat text-parsing build-error application-state code-reuse bi-lingual data-exchange eclipse-plugin xmp converters file-conversion xom printer-properties json-rpc one-time-password force.com dotted-line opengl-to-opengles media-source text-rendering android-settings rpx libvpx gpx jspx eye-tracking transfer-encoding multiple-entries signing tryparse urlparse gpars pycparser javaparser jparsec xpath domparser argparse parseint parsefloat feedparser parsefacebookutils parse.js parslet parse4j parsley ciscoconfparse csharpoptparse parserole ddmathparser sparse psychparser parsererror parsekit linkparser parserelation petitparser parsecontrol parsedown saxparseexception parseexcel mp4parser logparser fparsec fileparse configparser jstyleparser optparse textfieldparser pyparsing parsepy parseexception optionparser reparsepoint xamlparseexception fileparsing iterparse attoparsec html5-animation line-intersection webcenter-sites release-mode max-size layout-manager constant-expression request-cancelling magic-numbers web-search laravel-4 units-of-measurement xamarin.mobile online-radio forecasting bean-io computation-theory port-scanning label unboundid-ldap-sdk fatal-error unit-testing file-permissions main database-testing pathing identity-map user-mode-linux custom-titlebar build-time date-parsing incremental-build dllimport piracy-prevention zen-coding scale object-graph test-data scala crash-reports visual-glitch sanity-check email-notifications undeclared-identifier axd xdr xdv declarative-services declared-property qtdeclarative implicit-declaration using-declaration redeclare function-declaration xdoclet redeclaration declarative-programming declarative xdocreport declarations declarative-authorization xdotool zend-rest wordpress-plugin-dev azure-storage-tables integration-testing git-bash unix-timestamp svn-client git-bare embedded-resource software-tools libcurl spring-integration data-visualization nintendo-ds system-calls http-chunked restful-authentication steam-condenser vertex youtube-javascript-api formatting home-button binary-search varnish startup-error build-script open-with self-closing on-screen-keyboard self-extracting maps using php-parser query-builder file-rename rest-assured paypal-api zend-framework2 streaming web market-share secret-key copy-files load-order apache-httpcomponents console.log powershell-remoting fragment landscape-portrait git-branch dual-sim android-fragments microblogging scroll-position file.exists security-testing location-aware nodes dolphin-browser atlassian-sourcetree jquery-plugins add-on multiple-select offline-mode forward event-bus known-issues google-cloud-console login-attempts string-parsing flat-file credit-card network-analysis double-checked-locking add-in find-occurrences static-class model-binding format-specifiers reporting-services file session-state-server labview custom-contextmenu apache-commons production-environment search-path mobile-country-code well-formed page-refresh orientation-changes chrome-for-android http-protocols lync-2010 lync-2013 server-side .net maven-release-plugin outer the-little-schemer smtp microphone lines lexical-analysis maven-ant-tasks android-debug-bridge infinite-loop backbone.js database-table pre-commit var knockout.js content-management-system vba audio-player google-plugin-eclipse htmlunit exit-code shapedrawable drawablegamecomponent transitiondrawable colordrawable bitmapdrawable layerdrawable rippledrawable statelistdrawable android-drawable animationdrawable multiple-columns sprite-sheet image-editor tag-soup key-events nest-api activity-indicator request asp.net-web-api inertial-navigation building default-browser google-provisioning-api asp.net-mvc-routing generated-code facebook-java-sdk multifile-uploader http-authentication elements entities bing-maps-api ssh-tunnel use fractions news-ticker salesforce-service-cloud input-language information-visualization smil limited-user stacked-area-chart modifier large-data setting file-monitoring unknown-publisher continuous-testing last.fm listview-adapter cwac-touchlist at-command online-game cross-correlation dummy-data import-module state-management single-table-inheritance scaleanimation format smpp code-habits foreach nested-generics asp.net path-2d command-line ssh-keys event-bubbling random-access http-upload motorola-droid unsigned-char variable-declaration compiler-optimization heartbleed-bug auto-generate static-classes mail-server ui-patterns mobile-application many-to-many menu-items azure-media-services login-script game-maker apache-commons-fileupload password-policy systemtap path-finding forking grid-layout attr file-exists non-termination http-content-length amazon-product-api feature-selection sencha-touch sencha-command application-data non-ascii-chars geonames object-serialization javascript dataset summary char-array pull-request kendo-ui user-registration data sim-card date in-clause background-application multipart-form linear-algebra ion-koush facebook-apps read-data aspect-ratio boost-serialization symbian standard-icons global-state listings class-extensions complexity-theory tor .net-4.0 meta-inf tie file-locking hardware-interface third-party-controls cyclomatic-complexity variable-length-arguments file-upload neural-network-tuning querydsl jquery-ui single-instance getschema schema.rb schemacrawler jsonschema schemaless getschematable schematron schemaspy jsonschema2pojo schemabinding schemaexport schema.yml schemagen domexception ascx ocx cx-freeze docx c++-cx cx-oracle user-defined legacy-code node.js tex javax.sound.sampled sencha-cmd5 hosts-file idle-timer google-cloud-endpoints java-gstreamer java-bytecode-asm asp.net-identity-2 flash-media-server ignore axes dom-events cpu-registers .net-3.5 render-to-texture nested-array cloud-code data-structures nexus-one firefox-addon inputstreamreader code-generation code-translation xlwt xlib xls xlsx xll xlc xlsm openpyxl xlink xlrd isolation-level dxl xlsxwriter xliff level retina-display readlines rtc rtf realurl return-value-optimization readdir rotateanimation routes rotation rad-controls redirect readfile readonly fread background-attachment generic-collections tablet-pc binary software-quality window-soft-input-mode data-dictionary curl-commandline writer screen-capture http-compression soap-serialization android-os-handler delayed-execution objectdatasource sqldatasource linqdatasource datasource json-schema-validator virtual-path jquery-selectors foreign-collection libs generic-list custom-cell system tightly-coupled-code worker object-object-mapping key-generator fast-forward tab-completion indexes command-line-interface contextmenu sampling libc language-interoperability workflow-rehosting step-into remote-management firebug-lite database-connection indexed validator line indexer tabbed-interface sgml html5-apps auto-populate service-accounts declare-styleable column-width maven-assembly-plugin ui-guidelines binary-serialization ui-virtualization back-stack battery-saver wordpress-plugin dynamic-columns trigger.io vertica apache-commons-vfs gitlab-ci class-template couchbase-lite html-agility-pack windows-media-server .net-2.0 if-statement loopback-address agent remote-server quick-search layout-inflater data-synchronization short-circuiting palm-os serializer eclipse-tptp spell-checking apache-felix type-erasure cocos2d-html5 observer-pattern streamwriter low-level-api android-2.2-froyo database-link qsqldatabase database-mirroring database-cleaner database-concurrency database-partitioning database-security database-replication databasescripting multi-database multivalue-database getwritabledatabase database-first database-integrity 4d-database database-cluster django-database database-engine insight.database database-indexes document-database distributed-database databasepublishingwizard database-tuning database-deadlocks database-migration database-project database-design legacy-database cross-database database-server local-database databasedotcom-gem opendatabase database-cursor database-mail database-permissions protein-database graph-databases sysdatabases database-connectivity database-optimization temporal-database database-modeling yapdatabase database-deployment database-agnostic database-trigger database update-all updatepanel updatemodel sbt-idea pass-data base-url binarywriter java-3d linking-errors form-data include-path binaryfiles minimax maximization varcharmax maximize-window dmx edmx minmax spring-jmx max-heap max-flow tmx maxlength maxmind tweenmax openmax maximize edismax lmax maximo maxima maxreceivedmessagesize jmx varbinarymax maxstringcontentlength mx-record nidaqmx mxunit manifest maxscript asmx max-path mxgraph mx maxrequestlength dismax max-msp-jitter manifest.cache manifest.mf 3dsmax maxdate tokumx mmx gnu-make java-2d embedded-jetty common latex corrupted-state-exception app.yaml restlet-2.0 sitemap execution-time sfml bug-reporting back-button bookmarks big-o wbem big-omega many-to-one noise-reduction language-agnostic service absolutelayout force-layout layoutkind.explicit cardlayout layouttransition boxlayout flkautolayout elasticlayout nsautolayout sublayout nslayoutconstraint docklayoutpanel nestedlayout drawerlayout uicollectionviewlayout android-linearlayout gridbaglayout android-relativelayout layoutpanels layoutsubviews qgridlayout nslayoutmanager layoutparams viewdidlayoutsubviews qvboxlayout android-tablelayout dijit.layout android-gridlayout springlayout framelayout swiperefreshlayout relativelayout viewlayout grouplayout flowlayout structlayout autolayout haslayout flowlayoutpanel slidingpanelayout tablelayout tablelayoutpanel qlayout apache-stringutils glx memory-layout page-layout bundle-layout ios-autolayout xamlx jquery-layout liquid-layout code-layout form-layout qt-layout lxc parallax project-layout zend-layout lxr standard-layout mailx gwt-tablayoutpanel pattern-layout dhtmlx visual-editor assistive-technology delete-file mpeg2-ts notification-area google-mirror-api xcode3.2 facebook-comments comments xcode-instruments commenting xctest xcopy nerdcommenter autocommenting foscommentbundle xcode5 xcodebuild xcode5.1 xcode4.6 xcode4.3 xcode4.2 compile-time eclipse-emf pull-to-refresh file-transfer html-content-extraction bin camera-calibration inplace-editing twitter-bootstrap json.net bean-validation user-profile fwrite sign asp.net-webpages export-to-excel maven-3 maven-2 built-in forge selenium-webdriver logical-operators input-button-image d3.js verify configure audio-processing html-input file-io cgi-bin floating-accuracy vc6 vectorization viewcontroller factory vk fix vector factory-pattern for-loop snap-to-grid appstore-approval xinclude uiinclude jspinclude .class-file cordova-plugins flash-builder selectors database-administration re-engineering google-chrome-extension backup-agent data-entry alloy-ui matlab-toolbox siri message-loop poco-libraries bitwise-or command-line-arguments object-files tcpsocket python-cffi modern-ui intellij-plugin cas cat sample-data cal greenrobot-eventbus chilkat iphone-maps data-manipulation issue-tracking network-state contact-form-7 external-js serial-communication apache-httpclient-4.x keyboard-input simd rounding-error zend-auth bus llvm-3.0 sensitive-data large-text common-library varargs object-reference box-api business-intelligence opera-mobile generic-programming effective-java openx uses mediawiki-api error-recovery proxy-authentication user-data web-sql array-initialization pseudo-class explicit database-restore r-tree google-dfp sheet configuration writable css-overflow any ant ane commonsware-cwac lens amr file-descriptor html-entities java.library.path osx-mountain-lion file-browser thread-synchronization multi-user system-error rational-rsa merchant-account port-number search-engine web-database password-encryption multiple-inheritance database-relations unordered-map acceptance-testing open-graph-protocol sdl-image navigation-style digital-signature libpcap express-checkout ads acl data-storage ada acs application-error stress-testing ui-design disparity-mapping embedded-database job-scheduling nexus-s c-ares logged tabbed asp.net-web-api-routing websocket proxy-server forum network-share flood-fill type-hinting forth static-initialization fork coldfusion-10 art ase lucene-index ape event-receiver api apt windowbuilder tfsbuild rpmbuild msbuild powerbuilder processbuilder flexbuilder scenebuilder c++builder buildout stringbuilder bxslider reportbuilder3.0 osx-gatekeeper android-stlport api-doc smartclient agents-jade forms email-client att search-form eclipse-cdt formats 2d-engine encryption-symmetric css-position touch intel-xdk simulator always-on-top multipartform-data stemming mailing-list database-locking executable-jar browser-support download-manager sql-limit printer-control-language inflate-exception generic-class vnc-server ar.drone time-estimation cpu-cache lejos-nxj loading-image data-compression event-log data-mining image-stitching apply innerhtml core-location apple multiple-users universal-image-loader http-caching point-clouds cakephp-1.3 input-file apple-touch-icon application-settings node-request file-structure cloud-hosting shallow-copy login-control sql-drop user-permissions jquery-validate segment-io chars google-form system.drawing.imaging maven-compiler-plugin project-files java.lang.linkageerror socks exception-notifier data-stream file-io-permissions facebook-friends percent-encoding syntax-error google-bigquery time-complexity inline-assembly test-suite compiler-errors id3-tag exporting css-box-shadow code-conventions fractals oracle launching-application custom-controls path-variables xbox-music post-processing free search zooming project-properties apache-poi window.open editor custom-events file-management converter game-engine configurable schema.org artificial-intelligence micr saml bluetooth-sco es5-shim live-templates in-app-billing mutable cache-control database-performance exchange-server-2010 message-listener feature-extraction barcode-scanner jquery webpage-rendering visualizer radar-chart two-factor-authentication base-class open-nfc sencha-architect mercurial-subrepos apache-pig applicationpage friendly-id wymeditor page-editor cleditor nsihtmleditor gwt-editors editortemplates uivideoeditorcontroller fckeditor roweditor htmleditorextender dart-editor uitypeeditor editorformodel html-editor collectioneditor ckeditor.net propertyeditor htmleditorkit nspredicateeditor tablecelleditor wmd-editor editorgridpanel atom-editor fixed itemeditor jeditorpane cuteeditor tinyeditor aloha-editor nsruleeditor ace-editor editorfor epiceditor xtraeditors celleditorlistener mercury-editor ckeditor radeditor mixed hex-editors fieldeditor treetablecelleditor file-access google-compute-engine android-ui sails.js lwuit-form jasper-plugin javax.comm positioning transient rich-text-editor visual-studio build-server non-printing-characters openlayers html table file-manager position location-based-service sony-xperia file-ownership data-security response-time overlapping-matches embedded-linux ms-word socket-timeout-exception common-crawl cstring cscope website-metrics csproj common.logging apache-commons-dbcp least-common-ancestor apache-celix csom fontmetrics css-expressions apache2-module cssresource momentics wcs css-parsing numerics apache-config apache-commons-exec ilnumerics css-float css-paged-media commondomain css-menu apache-camel csrf-protection apache-shindig apache-pivot cscript bioinformatics wikimedia-commons scriptcs dot-emacs game-physics ropemacs amazon-ecs google-analytics apache-dbi carbon-emacs apachebench logistics usage-statistics commonsware puppetlabs-apache turtle-graphics apache-torque biometrics quartz-graphics kinematics system.diagnostics apache-crunch css-border-radius ncommon apache-regexp common-lisp commonspot common-dialog common-controls ocs common-service-locator apache-ace universal-analytics common-test metrics heuristics lcs raster-graphics ipcs code-statistics commonj apache-tailer apache-commons-cli css-frameworks apache-chemistry css-sprites gnu-common-lisp csvhelper apache-commons-collection apache2 aesthetics economics kissmetrics css-text-overflow adobe-analytics formal-semantics specs cs4 cs5 intrinsics commoncrypto cs3 css3 apache-forrest good-dynamics acoustics apache-cloudstack statistics jcs uikit-dynamics fedora-commons css-tables whmcs apache-1.3 csslint apache-bloodhound apache-tiles video-codecs common-tasks rhino-commons csrf csg css-reset csc apache-tomee sharepoint2010-bcs htdocs apache-whirr csh successor-arithmetics electronics qualtrics csquery css-content apache-ode apache-lucy azure-diagnostics common-table-expression apache-cayenne css-filters analytics drupal-commons css-specificity apache-abdera combinatorics web-analytics apache-commons-daemon csv csp css csr pymacs physics apache-commons-digester css-keyframes phonetics core-graphics discrete-mathematics mbcs aquamacs computer-forensics computational-linguistics cs-cart characteristics ergonomics genetics apache2.2 csplitterwnd apache2.4 windows-azure-diagnostics gmcs mnemonics bcs robotics uidynamics commonjs linguistics profilecommon common-files darcs apache-modules csla rapache css-shapes apache-nms fluid-dynamics infragistics apache-roller diacritics apache-vysper css-animations css3pie inverse-kinematics apache-twill bulletphysics cscore apache-stanbol apache-fop apache-karaf apache-chainsaw apache-commons-pool azure-acs performanceanalytics csv-import gemspecs italics ethics cics orbital-mechanics apache-commons-config code-metrics vector-graphics emacs css-line-height apache-spark css-validator graphics apache-commons-email open-generics web-statistics bbv.common.eventbroker move-semantics cs193p pvcs css-selectors dvcs table-statistics apache-tuscany youtube-analytics apacheds crashlytics date-formatting file-sharing world-map yahoo-search data-persistence android-lint android-cts visual-artifacts smooth initial-scale http-head time-series facebook-authentication anti-cheat code-signing complex-numbers js-test-driver sql-insert ws-trust video-encoding intermec cluster-computing android.mk apache-commons-beanutils http pdf.js stlport keyboard-events bit-shift cross-compile scalability background-service configuration-management html-parsing natural-sort shopping-cart bsd-sockets nextgen-gallery writing python-3.x network-security seam byte-order-mark fluid-layout eclipse-memory-analyzer data-members states underscore.js binary-reproducibility object-database firefox-addon-sdk checked-exceptions select object-detection procedural-generation facebook-sharer multiple-apk samsung-smart-tv asp.net-web-api2 tree echo-server http-digest on-duplicate-key background-size google-plus-one composite-controls database-management json-lib oracle-fusion-middleware android-contextmenu ld-preload internet-explorer programmatically-created apache-commons-net merge-replication parcel config-files apache-wink in-house-distribution url-parsing zend-gdata expensive-resources string-to-datetime ftp-server headless-browser seam-carving outlook.com pre-build-event internet apple-maps class-constructors data-encryption storing-data http-referer keyword-search repository-pattern terms-of-use linq lint key-value-observing media-library windows-ce trademark-symbol list windows-server signaturepad signature gpsignaturefile signaturetool signatures program-transformation gemfile google-feed-api memory-alignment filesort rprofile libsndfile gzipfile filesystemobject allusersprofile multifile qfiledialog nsfilehandle nsfileversion filemtime xfs sqlprofiler xfa system.io.fileinfo filehandle filezilla spfile .cs-file filevisitor defaultfilemonitor filestream sequencefile sqlfilestream jfilechooser fileobserver mvcminiprofiler file-listing profiler fileresult profiles mat-file large-files filewalker shapefile fileapi infile filenames tfilestream bakefile file-copying filebuf filecompare isolatedstoragefile nsfilecoordinator filepicker qfile maven-profiles zipfile shfileoperation pdb-files ipersistfile filefield fileconveyor fileupdate cffile httpfilecollection staticfilehandler boost-filesystem nsfilewrapper getfileversion extaudiofile getmodulefilename filemaker fileappender dxf specfiles execfile cprofile filesavepicker inifile filesize dotfiles fileset django-staticfiles xfce text-files xfbml cfilefind filesystems getopenfilename nsfilepresenter clrprofiler fileoutputstream nsfilemanager copyfile filesplitting fileinput spring-profiles hfile sendfile filepattern filetree swapfile cloudfiles randomaccessfile asyncfileupload filestructure berksfile fileloadexception xforms datafile filemq filestreams provisioning-profile file-attributes safefilehandle filedialog m-file program-files publish-profiles savefiledialog fileiopermission metafile outfile file-properties profile-provider fileshare luafilesystem extaudiofileread fileserver filesystemwatcher fileopenpicker dsofile cfile helpfile django-profiles pagefile filepath rollingfilesink filehash transmitfile vagrantfile httppostedfilebase rakefile createfile utl-file downloadfileasync xfire .bash-profile virtualfilesystem filehandler lockfile nosuchfileexception storagefile getfiles hyperfilesql fileslurp filelock findinfiles java.nio.file splfileobject filenet file-organization filedrop.js logfiles sqlprofileprovider resource-files filereferencelist appendfile procfile assemblyfileversion android-fileprovider fileversioninfo qfileinfo file-handling gemfile.lock cacheprofile filereader filesysteminfo qfilesystemmodel file-comparison urldownloadtofile filenet-p8 filefilter tmpfile response.transmitfile filereference tempfile httppostedfile logfile profile fileopendialog into-outfile filetime avcapturemoviefileoutput cmultifileupload uilaunchimagefile fileitem oprofile static-files cfiledialog filestreamresult tagfile filehelpers initwithcontentsoffile openfiledialog asp.net-profiles policyfiles html5-filesystem color-profile django-filebrowser .profile file.readalllines filemap fileutils filenotfoundexception tarfile rollingfileappender controlfile fileinfo postfile filelist movefileex unexpectendoffile filechannel jprofiler filecontentresult file-mapping filetable filebrowse filemerge modulefile dumpfile fileinputstream ddfilelogger filewriter x-sendfile filegroup cakefile filechooser qfilesystemwatcher file-pointer gemfile.apk pffile fromfile downloadfile file-processing data-migration android-search wake-on-lan select-query frame javax.swing.text custom-exceptions in-app bitwise-and ydn-db worklight-server sony-camera-api crystal-reports proxy-classes polar-coordinates version http-request memory-limit android-maven-plugin figures font-awesome comparison-operators hardware-acceleration undo-redo project-documentation maven-install-plugin touch-events ";
		Set<SynonymCandidate> filteredCandidates = new HashSet<>();

		Set<String> tags = new HashSet<>();
		tags.addAll(Arrays.asList(allTagsToInclude.split(" ")));

		// filter all synonym candidates
		for (SynonymCandidate sc : synonymCandidates) {
			if (tags.contains(sc.getTag()) || tags.contains(sc.getSynonymTag())) {
				filteredCandidates.add(sc);
			}
		}
		synonymCandidates = filteredCandidates;

		// filter approved synonym pairs
		EList<SynonymCandidate> filteredApprovedCandidates = new EArrayList<>();
		for (SynonymCandidate sc : approvedSynonymPairs) {
			if (tags.contains(sc.getTag()) || tags.contains(sc.getSynonymTag())) {
				filteredApprovedCandidates.add(sc);
			}
		}
		approvedSynonymPairs = filteredApprovedCandidates;

		// filter list of tags
		listOfNotUsedTags.clear();
		listOfNotUsedTags.addAll(tags);

		// correct:
		// gnu-classpath - gnu
		//

	}

	private void addImportantTopics(Integer groupId, TrendDto importantTrend) {

		topicPostcount.addEntry(groupId, importantTrend.getPostCount());

	}

	private void setHeaderOfTrendsFile(String path, String time) throws IOException {
		FileWriter writer2 = new FileWriter(path + time + "trends.csv", true);

		writer2.append("category");
		writer2.append(",");
		writer2.append("date");
		writer2.append(",");
		writer2.append("postCount");
		writer2.append(",");
		writer2.append("answerCountSum");
		writer2.append(",");
		writer2.append("answerCountAvg");
		writer2.append(",");
		writer2.append("commentCountSum");
		writer2.append(",");
		writer2.append("commentCountAvg");
		writer2.append(",");
		writer2.append("acceptedAnswersCountSum");
		writer2.append(",");
		// writer2.append("acceptedAnswersCountAvg");
		// writer2.append(",");
		writer2.append("openAnswerCountSum");
		writer2.append(",");
		writer2.append("openAnswerCountAvg");
		writer2.append(",");
		writer2.append("favoriteCountSum");
		writer2.append(",");
		writer2.append("favoriteCountAvg");
		writer2.append(",");
		writer2.append("scoreCountSum");
		writer2.append(",");
		writer2.append("scoreCountPos");
		writer2.append(",");
		writer2.append("scoreCountNeg");
		writer2.append("\n");
		writer2.flush();
		writer2.close();
	}

	private <E> String collectionItemsToString(Collection<E> coll) {
		StringBuilder sb = new StringBuilder();
		for (Object oo : coll) {
			sb.append((oo.toString() + " "));
		}
		return sb.toString();
	}

	private void exportTrendsPerGroupToCsv(EList<TrendDto> trends, String label, String path, String time) {

		try {
			FileWriter writer = new FileWriter(path + time + "trends.csv", true);

			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

			for (TrendDto trend : trends) {
				writer.append(label);
				writer.append(',');
				String date_ = sdf.format(trend.getDate());
				writer.append(date_);
				writer.append(',');
				writer.append(trend.getPostCount().toString());
				writer.append(',');
				writer.append(trend.getAnswerCountSum().toString());
				writer.append(',');
				writer.append(trend.getAnswerCountAvg().toString());
				writer.append(',');
				writer.append(trend.getCommentCountSum().toString());
				writer.append(',');
				writer.append(trend.getCommentCountAvg().toString());
				writer.append(',');
				writer.append(trend.getAcceptedAnswerCountSum().toString());
				writer.append(',');
				// writer.append(trend.getAcceptedAnswerCountAvg().toString());
				// writer.append(',');
				writer.append(trend.getOpenAnswerCountSum().toString());
				writer.append(',');
				// writer.append(trend.getOpenAnswerCountAvg().toString());
				// writer.append(',');
				writer.append(trend.getFavouriteCountSum().toString());
				writer.append(',');
				writer.append(trend.getFavouriteCountAvg().toString());
				writer.append(',');
				writer.append(trend.getScoreCountSum().toString());
				writer.append(',');
				writer.append(trend.getScoreCountPos().toString());
				writer.append(',');
				writer.append(trend.getScoreCountNeg().toString());
				writer.append(',');
				writer.append(trend.getViewCountSum().toString());
				writer.append(',');
				writer.append(trend.getViewCountAvg().toString());
				writer.append('\n');
			}

			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void groupApprovedSynonyms() {

		EList<SynonymCandidate> versions = new EArrayList<>();
		for (SynonymCandidate dto : approvedSynonymPairs) {

			if (!addToSynonymGroups(dto)) {
				versions.add(dto);
			}
		}

		for (SynonymCandidate dto : versions) {
			secondAddingToSynonymGroups(dto);
		}

	}

	/**
	 * adds a synonym candidate to the synonym groups
	 *
	 * candidate dto
	 */
	private Boolean addToSynonymGroups(SynonymCandidate synonymCandidate) {
		// get groupId if any exists
		String synonym = synonymCandidate.getSynonymTag();
		Integer groupId = getIdForGroupByTarget(synonym);

		// no groupId exists
		if (groupId == -1) {

			// check ob schon beide vorhanden?
			Set<SynonymCandidate> synCanSet = new HashSet<>();
			synCanSet.add(synonymCandidate);
			if (checkAlreadyBothInGroup(synCanSet).size() != 0) {

				// check for version
				if (synonym.matches(".*\\d\\.?\\d*.*")) {
					return false;
				} else {
					addNewApprovedSynonymCandidate(synonymCandidate);
				}
			}

		} else {

			addToExistingGroup(groupId, synonymCandidate);
			// groupId exists

		}
		return true;

	}

	private int synonymCandidateId = 0;

	private Integer addNewApprovedSynonymCandidate(SynonymCandidate synCan, String synonym, Integer synonymCandidateId) {

		GroupNeu group = new GroupNeu();
		group.addSourceAndTargetWithId(synCan.getTag(), synonym, synonymCandidateId);
		groupIdCount++;
		group.setGroupId(groupIdCount);
		group.setCreatedByApprovedSynonyms();

		SynonymCandidate scanNeu = new SynonymCandidate(synCan.getTag(), synonym, synCan.isTagSource());
		scanNeu.setId(synonymCandidateId);

		updateDataObjects(scanNeu, group);
		return groupIdCount;
	}

	private Integer addNewApprovedSynonymCandidate(SynonymCandidate synCan) {
		return addNewApprovedSynonymCandidate(synCan, synCan.getSynonymTag(), synCan.getId());
	}

	private void secondAddingToSynonymGroups(SynonymCandidate synonymCandidate) {

		// zahlen entfernen
		String synonym = synonymCandidate.getSynonymTag().replaceAll("-?\\d\\.?-?\\d?", "");

		// finde gruppe mit target ohne zahlen?
		int groupId = getIdForGroupByTarget(synonym);

		if (groupId != -1) {
			addToSourcesOfExistingGroups(groupId, synonymCandidate);
		} else {

			Tag tagByName = DBHandler.getTagByName(synonym);
			if (tagByName.getId() != -1) {
				groupId = addNewApprovedSynonymCandidate(synonymCandidate, synonym, synonymCandidateId--);

				addToSourcesOfExistingGroups(groupId, synonymCandidate);

			}
			// tag - zahlen gibt es ohnehin nicht, dann hinzufügen
			else {
				addNewApprovedSynonymCandidate(synonymCandidate);
			}

		}

	}

	private void addToSourcesOfExistingGroups(Integer groupId, SynonymCandidate synonymCandidate) {

		GroupNeu group = synonymGroups.get(groupId);

		group.addSourceInfoDto(synonymCandidate.getTag(), synonymCandidate.getId());
		group.addSourceInfoDto(synonymCandidate.getSynonymTag(), synonymCandidate.getId());

		synonymGroups.remove(group);
		// update data objects and add updated group again
		updateDataObjects(synonymCandidate, group);

	}

	private void addToExistingGroup(Integer groupId, SynonymCandidate synonymCandidate) {
		GroupNeu group = synonymGroups.get(groupId);
		// group.addSynonymCandidate(synonymCandidate);
		group.addSourceInfoDto(synonymCandidate.getTag(), synonymCandidate.getId());
		// remove existing group
		synonymGroups.remove(group);
		// update data objects and add updated group again
		updateDataObjects(synonymCandidate, group);

	}

	private Integer getIdForGroupByTarget(String target) {

		EList<Integer> ids = new EArrayList<>();
		for (Table.Cell<String, String, Integer> cell : table.cellSet()) {
			if (cell.getColumnKey().equals(target) && cell.getValue() != -1) {
				ids.add(cell.getValue());
			}
		}
		if (ids.size() > 0) {
			return ids.get(0);
		}

		return -1;
	}

	private void updateTableId(Integer oldId, Integer newId) {
		EList<String> rowsToUpdate = new EArrayList<>();
		EList<String> columnsToUpdate = new EArrayList<>();
		for (Table.Cell<String, String, Integer> cell : table.cellSet()) {
			if (cell.getValue() != oldId) {
				rowsToUpdate.add(cell.getRowKey());
				columnsToUpdate.add(cell.getColumnKey());
			}
		}

		// update table
		for (int i = 0; i < rowsToUpdate.size(); i++) {
			table.put(rowsToUpdate.get(i), columnsToUpdate.get(i), newId);
		}

	}

	private EList<Integer> getIdsForGroupBySource(String source) {

		EList<Integer> ids = new EArrayList<>();
		for (Table.Cell<String, String, Integer> cell : table.cellSet()) {
			if (cell.getRowKey().equals(source)) {
				ids.add(cell.getValue());
			}

		}
		return ids;
	}

	// private void addSynonymCandidates(String source, String target) {
	//
	// // // TODO nur jetz - wieder weggeben
	// // if (true) {
	// // System.out.println(source + " --- " + target);
	// // return;
	// // }
	//
	// Set<String> subList;
	// // es gibt noch keine einträge
	// if (!allGroupedTagNames.contains(source) &&
	// !allGroupedTagNames.contains(target)) {
	//
	// // füge zu allgemeiner liste hinzu
	// allGroupedTagNames.add(source);
	// allGroupedTagNames.add(target);
	//
	// // füge zu synonym groups hinzu
	// subList = new HashSet<>();
	// subList.add(source);
	// synonymGroups.put(target, subList);
	//
	// }
	// // bereits in synonymGroups aber tausch notwendig
	// else if (synonymGroups.containsKey(source)) {
	//
	// // switch source and target
	//
	// Set<String> list = synonymGroups.get(source);
	// // remove old
	// synonymGroups.remove(source);
	//
	// // change
	// list.add(source);
	// list.remove(target);
	// if (synonymGroups.containsKey(target)) {
	// list.addAll(synonymGroups.get(target));
	// }
	// allGroupedTagNames.add(target);
	// synonymGroups.put(target, list);
	//
	// }
	// // bereits in synonymGroups kein tausch notwendig
	// else if (synonymGroups.containsKey(target)) {
	// Set<String> list = synonymGroups.get(target);
	// list.add(source);
	// allGroupedTagNames.add(source);
	// synonymGroups.put(target, list);
	// }
	//
	// // schon mal verwendet
	// else if (allGroupedTagNames.contains(source)) {
	// String key = getKeyOfGroupedTags(source);
	//
	// if (key.equals("-1")) {
	// System.out.println("ERROR " + source + " " + target);
	// } else {
	// Set<String> list = synonymGroups.get(key);
	// list.add(target);
	// allGroupedTagNames.add(target);
	// synonymGroups.put(key, list);
	// }
	// }
	// // schon mal verwendet TODO check ob tausch noch notwendig?
	// else if (allGroupedTagNames.contains(target)) {
	// String key = getKeyOfGroupedTags(target);
	// if (key.equals("-1")) {
	// System.out.println("ERROR " + source + " " + target);
	// } else {
	// Set<String> list = synonymGroups.get(key);
	// list.add(source);
	// allGroupedTagNames.add(source);
	// synonymGroups.put(key, list);
	// }
	// }
	// }

	// private String getKeyOfGroupedTags(String tag) {
	//
	// for (Entry<String, Set<String>> entry : synonymGroups.entrySet()) {
	// for (String tagName : entry.getValue()) {
	// if (tag.equals(tagName)) {
	// return entry.getKey();
	// }
	// }
	// }
	//
	// return "-1";
	//
	// }

	// private Set<Integer> getIdsOfUnusedTags() {
	// EList<Integer> allTagIds = HelperDBHandler.getAndroidTagIds();
	//
	// EList<Integer> usedTagIds = new EArrayList<>();
	//
	// for (GroupNeu group : synonymGroups.values()) {
	//
	// for (String tag : group.getSourceList()) {
	// int id = HelperDBHandler.getTagByName(tag).getId();
	// if (id != -1) {
	// usedTagIds.add(id);
	// }
	// }
	//
	// for (String tag : group.getTargetList()) {
	// int id = HelperDBHandler.getTagByName(tag).getId();
	// if (id != -1) {
	// usedTagIds.add(id);
	// }
	// }
	//
	// }
	//
	// SetView<Integer> intersection = Sets.intersection(new
	// HashSet<Integer>(allTagIds), new HashSet<Integer>(usedTagIds));
	// Set<Integer> returnSet = new HashSet<>();
	// intersection.copyInto(returnSet);
	// return returnSet;
	// }

	private Integer getInfoDtoForSourceTarget(String source, Collection<String> targetList) {

		for (String target : targetList) {
			Integer id = HelperDBHandler.getInfoDtoForSourceTarget(source, target, 0.6).getId();
			if (id != -1) {
				return id;
			}
		}
		return -1;
	}

	/**
	 * returns the id of the synonymcandidates for a defined target and a sourcelist
	 * 
	 * @param sourceList
	 * @param target
	 * @return
	 */
	private Integer getSynonymCandidateForSourceAndTarget(Collection<String> sourceList, String target) {

		for (String source : sourceList) {
			Integer id = HelperDBHandler.getInfoDtoForSourceTarget(source, target, 0.6).getId();
			if (id != -1) {
				return id;
			}
		}
		return -1;
	}

	// private void addSynonymCandidates(String source, String target) {
	//
	// // // TODO nur jetz - wieder weggeben
	// // if (true) {
	// // System.out.println(source + " --- " + target);
	// // return;
	// // }
	//
	// Set<String> subList;
	// // es gibt noch keine einträge
	// if (!allGroupedTagNames.contains(source) &&
	// !allGroupedTagNames.contains(target)) {
	//
	// // füge zu allgemeiner liste hinzu
	// allGroupedTagNames.add(source);
	// allGroupedTagNames.add(target);
	//
	// // füge zu synonym groups hinzu
	// subList = new HashSet<>();
	// subList.add(source);
	// synonymGroups.put(target, subList);
	//
	// }
	// // bereits in synonymGroups aber tausch notwendig
	// else if (synonymGroups.containsKey(source)) {
	//
	// // switch source and target
	//
	// Set<String> list = synonymGroups.get(source);
	// // remove old
	// synonymGroups.remove(source);
	//
	// // change
	// list.add(source);
	// list.remove(target);
	// if (synonymGroups.containsKey(target)) {
	// list.addAll(synonymGroups.get(target));
	// }
	// allGroupedTagNames.add(target);
	// synonymGroups.put(target, list);
	//
	// }
	// // bereits in synonymGroups kein tausch notwendig
	// else if (synonymGroups.containsKey(target)) {
	// Set<String> list = synonymGroups.get(target);
	// list.add(source);
	// allGroupedTagNames.add(source);
	// synonymGroups.put(target, list);
	// }
	//
	// // schon mal verwendet
	// else if (allGroupedTagNames.contains(source)) {
	// String key = getKeyOfGroupedTags(source);
	//
	// if (key.equals("-1")) {
	// System.out.println("ERROR " + source + " " + target);
	// } else {
	// Set<String> list = synonymGroups.get(key);
	// list.add(target);
	// allGroupedTagNames.add(target);
	// synonymGroups.put(key, list);
	// }
	// }
	// // schon mal verwendet TODO check ob tausch noch notwendig?
	// else if (allGroupedTagNames.contains(target)) {
	// String key = getKeyOfGroupedTags(target);
	// if (key.equals("-1")) {
	// System.out.println("ERROR " + source + " " + target);
	// } else {
	// Set<String> list = synonymGroups.get(key);
	// list.add(source);
	// allGroupedTagNames.add(source);
	// synonymGroups.put(key, list);
	// }
	// }
	// }

	// private String getKeyOfGroupedTags(String tag) {
	//
	// for (Entry<String, Set<String>> entry : synonymGroups.entrySet()) {
	// for (String tagName : entry.getValue()) {
	// if (tag.equals(tagName)) {
	// return entry.getKey();
	// }
	// }
	// }
	//
	// return "-1";
	//
	// }

	private Integer getGroupBySourceTagPair(String source1, String source2) {

		for (Entry<Integer, GroupNeu> entry : synonymGroups.entrySet()) {

			boolean foundSource1 = false;
			boolean foundSource2 = false;

			for (String source : entry.getValue().getSourceList()) {
				if (source1.equals(source)) {
					foundSource1 = true;
				} else if (source2.equals(source)) {
					foundSource2 = true;
				}
				if (foundSource1 && foundSource2) {
					return entry.getKey();
				}
			}
		}

		return -1;
	}

	private void addSelectedSynonymCandidates() {
		// evtl. auch getallTagsAndId
		Set<String> allTagNames = HelperDBHandler.getAllAndroidTagsAndId(150000).keySet();
		Set<String> allTagNamesBackup = allTagNames;

		for (String tagName : allTagNames) {

			EList<Set<SynonymCandidate>> selectedSynonymCandidatesForTag = HelperDBHandler.getSelectedSynonymCandidatesAsInfoDtosForTag(tagName);

			// filter and add
			Set<SynonymCandidate> selectedSourceSynonymCandidatesForTag = selectedSynonymCandidatesForTag.get(0);
			Set<SynonymCandidate> selectedTargetSynonymCandidatesForTag = selectedSynonymCandidatesForTag.get(1);

			Group tagGroup = new Group();
			EList<SynonymCandidate> sources = new EArrayList<>();
			EList<SynonymCandidate> targets = new EArrayList<>();

			// add sources
			for (SynonymCandidate synonymCandidate : selectedSourceSynonymCandidatesForTag) {

				sources.add(synonymCandidate);
			}

			// add targetsynonyms
			for (SynonymCandidate synonymCandidate : selectedTargetSynonymCandidatesForTag) {
				targets.add(synonymCandidate);

				// target -> other sources
				if (synonymCandidate.getCat().ordinal() < 6 && synonymCandidate.getStoredRanking() > 0.6) {
					sources.addAll(HelperDBHandler.getSourceTagsAsInfoDtosForTag(synonymCandidate.getSynonymTag()));
				}
			}

			tagGroup.addSourceInfoDtoList(sources);
			tagGroup.addTargetInfoDtoList(targets);

		}
	}

	private void selectMostImportantTopics() {
		for (Entry<Integer, GroupNeu> entry : synonymGroups.entrySet()) {

			GroupNeu group = entry.getValue();
			Set<String> tags = group.getSourceList();
			tags.addAll(group.getTargetList());

			// System.out.println((tags));

			TrendDto importantTrend = HelperDBHandler.getImportanceOfTopic(tags).get(0);
			addImportantTopics(entry.getKey(), importantTrend);

			// exportTrendsPerGroupToCsv(trends,
			// collectionItemsToString(group.getTargetList()), path, time);

		}
	}

	private void getTrendsPerGroup(EList<Integer> groupIds) {

		// TODO handle android-listview + listview

		// save groups
		try {

			FileWriter writer = new FileWriter(path + time + "groupstop21.txt", true);

			setHeaderOfTrendsFile(path, time);

			// get trends
			for (Integer groupId : groupIds) {
				GroupNeu group = synonymGroups.get(groupId);

				Set<String> tags = group.getSourceList();
				tags.addAll(group.getTargetList());

				// System.out.println((tags));
				writer.append(collectionItemsToString(tags));
				writer.append("\n");

				EList<TrendDto> trends = HelperDBHandler.getTrendsForGroupedSynonyms(tags);

				exportTrendsPerGroupToCsv(trends, collectionItemsToString(group.getTargetList()), path, time);

			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void addSynonymCandidatesToSynonymGroupTargets() {

		for (GroupNeu group : synonymGroups.values()) {

			for (String target : group.getTargetList()) {
				Map<Integer, SynonymCandidate> allSynonymCandidatesByTarget = HelperDBHandler.getAllSynonymCandidatesByTarget(target);
				for (SynonymCandidate dto : allSynonymCandidatesByTarget.values()) {
					if (synonymCandidateMap.containsKey(dto.getId())) {

						Integer id = getIdForGroupByTarget(target);
						synonymGroups.get(id).addSourceInfoDto(dto.getTag(), dto.getId());
						synonymCandidateMap.remove(dto.getId());
						table.put(dto.getTag(), dto.getSynonymTag(), id);
					}
				}
			}
		}

	}

	private Set<Integer> iterateCandidateIdsForChains(Set<Integer> chainsForTopCategories, boolean lastRound) {

		Set<Integer> notFound = new HashSet<>();
		for (Integer synCandidateId : chainsForTopCategories) {
			SynonymCandidate dto = synonymCandidateMap.get(synCandidateId);
			if (dto != null) {
				String source = dto.getTag();
				String target = dto.getSynonymTag();
				GroupNeu groupNeu;

				// already exist group containing both
				int groupId = getGroupBySourceTagPair(source, target);
				if (groupId != -1) {
					groupNeu = synonymGroups.get(groupId);
					groupNeu.addToIdList(groupId);
					updateDataObjects(dto, groupNeu);
				} else {
					// EList<Integer> groupIds = getIdsForGroupByTarget(target);

					groupId = getIdForGroupByTarget(target);

					// targetEx = targetNew
					if (groupId != -1 && synonymGroups.containsKey(groupId)) {
						groupNeu = synonymGroups.get(groupId);
						groupNeu.addSourceInfoDto(source, dto.getId());
						updateDataObjects(dto, groupNeu);

					} else {
						// targetEx = sourceNew
						groupId = getIdForGroupByTarget(source);

						if (groupId != -1 && synonymGroups.containsKey(groupId)) {
							groupNeu = synonymGroups.get(groupId);

							// groupNeu.addTargetInfoDto(target, dto.getId());

							Integer dtoId = getSynonymCandidateForSourceAndTarget(groupNeu.getSourceList(), target);
							if (dtoId != -1) {
								// switch source target
								// a->b, b->c --- es gibt auch a->c
								groupNeu.switchTargetToSourceAndAddNewTarget(source, target, dto.getId());

							} else {
								// ansonsten einfach hinzufügen
								groupNeu.addSourceInfoDto(target, dto.getId());
								groupNeu.addSourceInfoDto(source, dto.getId());

							}
							updateDataObjects(dto, groupNeu);
						} else {
							// sourceEx = targetNew
							EList<Integer> groupIds = getIdsForGroupBySource(target);

							boolean found = false;
							for (Integer groupId_ : groupIds) {

								if (groupId_ != -1 && synonymGroups.containsKey(groupId_)) {

									groupNeu = synonymGroups.get(groupId_);
									// gibt es infodto der dreieck schließt?
									Integer dtoId = getInfoDtoForSourceTarget(source, groupNeu.getTargetList());
									if (dtoId != -1) {
										groupNeu.addSourceInfoDto(target, dto.getId());
										groupNeu.addSourceInfoDto(source, dto.getId());
										found = true;
										updateDataObjects(dto, groupNeu);

									} else {

										// TODO sanity check, gleiche wie oben
										// ansonsten einfach hinzufügen
										groupNeu.addSourceInfoDto(target, dto.getId());
										groupNeu.addSourceInfoDto(source, dto.getId());
										updateDataObjects(dto, groupNeu);
									}

									// groupNeu.addSourceInfoDto(target,
									// dto.getId());
									// found = true;
									// updateDataObjects(synCandidateId, dto,
									// groupId_, groupNeu);
								}

							}
							if (!found) {

								// sourceEx = sourceNew

								if (!lastRound) {
									EList<Integer> groupIds2 = getIdsForGroupBySource(source);
									for (Integer groupId_ : groupIds2) {
										if (groupId_ != -1 && synonymGroups.containsKey(groupId_)) {

											groupNeu = synonymGroups.get(groupId_);
											Integer dtoId = getInfoDtoForSourceTarget(target, groupNeu.getTargetList());

											if (dtoId != -1) {
												groupNeu.addSourceInfoDto(target, dto.getId());
												groupNeu.addSourceInfoDto(source, dto.getId());
												updateDataObjects(dto, groupNeu);
											} else {
												dtoId = getSynonymCandidateForSourceAndTarget(groupNeu.getTargetList(), target);
												if (dtoId != -1) {
													groupNeu.switchTargetToSourceAndAddNewTarget(source, target, dto.getId());
													updateDataObjects(dto, groupNeu);
												} else {
													notFound.add(synCandidateId);
													continue;
												}
											}

											// groupNeu =
											// synonymGroups.get(groupId_);
											//
											// Integer targetIsSource =
											// getInfoDtoForSourceTarget(target,
											// groupNeu.getTargetList());

										}

										// if (groupId_ != -1 &&
										// synonymGroups.containsKey(groupId_)
										// && !lastRound) {
										// // TODO
										// notFound.add(synCandidateId);
										// continue;
										// }
										// else {

										// }
									}
								} else {

									addNewSynonymGroup(synCandidateId, dto);
								}

							}

							// if (groupId != -1 &&
							// synonymGroups.containsKey(groupId)) {
							// groupNeu = synonymGroups.get(groupId);
							// groupNeu.addSourceInfoDto(target, dto.getId());
							// } else {
							// groupId = getIdForGroupBySource(source);
							//
							// if (groupId != -1 &&
							// synonymGroups.containsKey(groupId)) {
							// notFound.add(synCandidateId);
							// continue;
							// } else {
							// if (lastRound) {
							// idCount++;
							// groupId = idCount;
							// groupNeu = new GroupNeu();
							// groupNeu.addSourceAndTargetWithId(source, target,
							// groupId);
							// } else {
							// notFound.add(synCandidateId);
							// continue;
							// }
							//
							// }
							// }
						}

					}
				}

			}
		}

		return notFound;
	}

	private Set<Integer> getAllUsedSynonymPairIds() {
		Set<Integer> returnSet = new HashSet<>();
		for (GroupNeu gn : synonymGroups.values()) {
			returnSet.addAll(gn.getIdList());
		}
		return returnSet;
	}

	private Integer getInfoDtoForSourceTarget(String source, String target) {
		return HelperDBHandler.getInfoDtoForSourceTarget(source, target, 0.6).getId();
	}

	private Set<Integer> iterateCandidateIdsForSameTargets(SynonymCandidate dto) {
		Set<Integer> notFound = new HashSet<>();
		if (dto != null) {
			String source = dto.getTag();
			String target = dto.getSynonymTag();

			int groupId = getIdForGroupByTarget(target);
			GroupNeu groupNeu;
			// targetEx = targetNew
			if (groupId != -1 && synonymGroups.containsKey(groupId)) {
				groupNeu = synonymGroups.get(groupId);
				groupNeu.addSourceInfoDto(source, dto.getId());
				updateDataObjects(dto, groupNeu);

			} else {
				notFound.add(dto.getId());
			}
		}

		return notFound;
	}

	private Set<Integer> addExistingTargetMatchesSource(SynonymCandidate dto) {
		Set<Integer> notFound = new HashSet<>();
		if (dto != null) {
			String source = dto.getTag();
			String target = dto.getSynonymTag();

			int groupId = getIdForGroupByTarget(source);
			GroupNeu groupNeu;
			if (groupId != -1 && synonymGroups.containsKey(groupId)) {
				groupNeu = synonymGroups.get(groupId);
				groupNeu.addTargetInfoDto(target, dto.getId());
				updateDataObjects(dto, groupNeu);
			} else {
				notFound.add(dto.getId());
			}
		}

		return notFound;
	}

	private Set<Integer> iterateCandidateIdsForRanking(Set<Integer> chainsForTopCategories, boolean lastRound) {
		Set<Integer> notFound = new HashSet<>();
		for (Integer synCandidateId : chainsForTopCategories) {
			SynonymCandidate dto = synonymCandidateMap.get(synCandidateId);
			if (dto != null) {
				String source = dto.getTag();
				String target = dto.getSynonymTag();

				// EList<Integer> groupIds = getIdsForGroupByTarget(target);

				int groupId = getIdForGroupByTarget(target);
				GroupNeu groupNeu;
				// targetEx = targetNew
				if (groupId != -1 && synonymGroups.containsKey(groupId)) {
					groupNeu = synonymGroups.get(groupId);
					groupNeu.addSourceInfoDto(source, dto.getId());
					updateDataObjects(dto, groupNeu);

				} else {
					// targetEx = sourceNew
					groupId = getIdForGroupByTarget(source);
					if (groupId != -1 && synonymGroups.containsKey(groupId)) {
						groupNeu = synonymGroups.get(groupId);
						groupNeu.addTargetInfoDto(target, dto.getId());
						updateDataObjects(dto, groupNeu);
					} else {
						// sourceEx = targetNew
						EList<Integer> groupIds = getIdsForGroupBySource(target);

						boolean found = false;
						for (Integer groupId_ : groupIds) {

							if (groupId_ != -1 && synonymGroups.containsKey(groupId_)) {
								groupNeu = synonymGroups.get(groupId_);
								groupNeu.addSourceInfoDto(target, dto.getId());
								found = true;
								updateDataObjects(dto, groupNeu);
							}

						}
						if (!found) {

							// sourceEx = sourceNew

							if (!lastRound) {
								EList<Integer> groupIds2 = getIdsForGroupBySource(source);
								for (Integer groupId_ : groupIds2) {
									if (groupId_ != -1 && synonymGroups.containsKey(groupId_)) {
										notFound.add(synCandidateId);
										continue;
									}

									// if (groupId_ != -1 &&
									// synonymGroups.containsKey(groupId_) &&
									// !lastRound) {
									// // TODO
									// notFound.add(synCandidateId);
									// continue;
									// }
									// else {

									// }
								}
							} else {
								addNewSynonymGroup(synCandidateId, dto);
							}

						}

					}

				}
			}
		}
		return notFound;

	}

	// private Set<SynonymCandidate> getSynonymCandidatesNotCoveredAlready() {
	// Set<Integer> idsOfUnusedTags = getIdsOfUnusedTags();
	// Set<SynonymCandidate> synonymCandidates = new HashSet<>();
	//
	// for (Integer i : idsOfUnusedTags) {
	// // find synonym candidates
	// Set<SynonymCandidate> dtoSet =
	// HelperDBHandler.getSynonymBySourceOrTargetId(i);
	// for (SynonymCandidate dto : dtoSet) {
	// synonymCandidates.add(dto);
	// }
	//
	// }
	// return synonymCandidates;
	//
	// }

	private void regroup(Map<Integer, GroupNeu> singleGroups) {
		Set<Integer> idsToRemove = new HashSet<Integer>();
		for (Entry<Integer, GroupNeu> entry : singleGroups.entrySet()) {

			String subTarget = entry.getValue().getFirstTarget();

			EList<Integer> idsSuperGroup = getIdsForGroupBySource(subTarget);

			int maxSourceSize = -1;
			int idSuperGroup = -1;

			// suche gruppe mit den meissten sources
			for (int i = 0; i < idsSuperGroup.size(); i++) {
				GroupNeu superGroup = synonymGroups.get(idsSuperGroup.get(i));

				if (superGroup.getSourceList().size() > maxSourceSize) {
					maxSourceSize = superGroup.getSourceList().size();
					idSuperGroup = idsSuperGroup.get(i);
				}

			}
			// füge gruppe in supergruppe ein
			if (idSuperGroup != -1) {
				Integer idSubGroup = entry.getKey();
				addSubGroupToGroup(idSuperGroup, idSubGroup);
				idsToRemove.add(idSubGroup);
			}
		}
		// entferne alte gruppen
		for (Integer rem : idsToRemove) {
			synonymGroups.remove(rem);
		}

	}

	private Integer getInfoDtoForSourceListAndTargetList(Collection<String> sourceList, Collection<String> targetList) {
		for (String target : targetList) {
			for (String source : sourceList) {
				Integer id = HelperDBHandler.getInfoDtoForSourceTarget(source, target, 0.6).getId();
				if (id != -1) {
					return id;
				}
			}
		}
		return -1;
	}

	private Integer getGroupByTargetTagPair(String source1, String source2) {

		for (Entry<Integer, GroupNeu> entry : synonymGroups.entrySet()) {

			boolean foundSource1 = false;
			boolean foundSource2 = false;

			for (String source : entry.getValue().getSourceList()) {
				if (source1.equals(source)) {
					foundSource1 = true;
				} else if (source2.equals(source)) {
					foundSource2 = true;
				}
				if (foundSource1 && foundSource2) {
					return entry.getKey();
				}
			}
		}

		return -1;
	}

	public String getPathToFile() {
		return path + time;
	}

}
