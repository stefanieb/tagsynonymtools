package datahandling.handleData;

import tagsynonymprediction.models.SynonymCandidate;
import tagsynonymprediction.utility.EArrayList;
import tagsynonymprediction.utility.EList;

import java.util.Collection;


public class Group {

	private EList<SynonymCandidate> sourceList;

	private EList<SynonymCandidate> targetList;

	public Group() {
		sourceList = new EArrayList<>();
		targetList = new EArrayList<>();
	}

	public String getLabel() {

		return collectionItemsToString(targetList);
	}

	public EList<SynonymCandidate> getSourceList() {
		return sourceList;
	}

	public void setSourceList(EList<SynonymCandidate> sourceList) {
		this.sourceList = sourceList;
	}

	public EList<SynonymCandidate> getTargetList() {
		return targetList;
	}

	public void setTargetList(EList<SynonymCandidate> targetList) {
		this.targetList = targetList;
	}

	public void addSourceInfoDtoList(EList<SynonymCandidate> dtoList) {
		sourceList.addAll(dtoList);

	}

	public void addTargetInfoDtoList(EList<SynonymCandidate> dtoList) {
		targetList.addAll(dtoList);
	}

	public void addSourceInfoDto(SynonymCandidate dto) {
		sourceList.add(dto);

	}

	public void addTargetInfoDto(SynonymCandidate dto) {
		targetList.add(dto);
	}

	private <E> String collectionItemsToString(Collection<E> coll) {
		StringBuilder sb = new StringBuilder();
		for (Object oo : coll) {
			sb.append((oo.toString() + " "));
		}
		return sb.toString();
	}

	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append("LABEL: " + getLabel() + "\n");
		sb.append("source-tags: " + collectionItemsToString(sourceList) + "\n");
		sb.append("target-tags: " + collectionItemsToString(targetList) + "\n");
		sb.append("--- \n");

		return sb.toString();

	}

}
