package datahandling.handleData;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.lucene.search.spell.LevensteinDistance;

public class GroupsToDB {

	/**
	 * PIPELINE: GroupTagsNeu GroupsToDB mit Pfad Post2Synonym TrendsForTopic
	 * 
	 */

	private String url = "jdbc:mysql://localhost:3306/";
	private String dbName = "SO_Nov2014";
	// private static String dbName = "AndroidOnly";
	private String driver = "com.mysql.jdbc.Driver";
	private String userName = "root";
	private String password = "";

	private String space = " ,'";

	public void saveSynonymGroupsToDB(HashMap<Integer, GroupNeu> synonymGroups) {

		for (Entry<Integer, GroupNeu> entry : synonymGroups.entrySet()) {
			Integer groupId = entry.getKey();
			Integer existingGroupId;
			if ((existingGroupId = getExistingGroupId(entry.getValue().getFirstTarget())) != -1) {
				groupId = existingGroupId;
			}
			String values = prepareValues(groupId, entry.getValue());

			saveGroup(values);
		}

	}

	public Integer getExistingGroupId(String target) {
		Integer groupId = -1;
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select distinct(groupId) as 'groupId' from synonymgroups where target = '" + target + "'");

			while (tags_db.next()) {

				groupId = tags_db.getInt("groupId");

			}

			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return groupId;
	}

	public void startSaveGroupsToDB(String pathToFile) {
		deleteExistingGroups();
		saveGroupsFromFile(pathToFile + "groups.txt");
	}

	public static void main(String args[]) {
		GroupsToDB g = new GroupsToDB();

		g.deleteExistingGroups();
		g.saveGroupsFromFile("/Users/stefanie/Documents/workspace_mars/TagSynonymTools/src/main/resources/generatedData/generatedData1436225470573groups.txt");
		System.out.println("switches: " + g.switchcounter);
		// int i = 5;
		// GroupNeu synonymGroup = new GroupNeu();
		// synonymGroup.addSourceAndTargetWithId("source1", "target1", 1);
		// synonymGroup.addSourceInfoDto("source2", 2);
		// synonymGroup.addSourceInfoDto("source3", 3);
		// synonymGroup.addSourceInfoDto("source4", 4);
		//
		// String s = g.prepareValues(i, synonymGroup);
		// System.out.println(s);
		// g.saveGroup(s);
		// System.out.println("saved");
	}

	public String prepareValues(Integer groupId, GroupNeu synonymGroup) {

		StringBuilder sb = new StringBuilder();

		for (String target : synonymGroup.getTargetList()) {

			for (String source : synonymGroup.getSourceList()) {

				if (source.equals(target)) {
					continue;
				} else {
					sb.append(" (");
					sb.append(groupId);
					sb.append(space);
					sb.append(source);
					sb.append("'");
					sb.append(space);
					sb.append(target);
					sb.append("'");
					sb.append("),");
				}
			}
		}

		return sb.substring(0, sb.length() - 1);

	}

	private void deleteExistingGroups() {
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			st.executeUpdate("delete from synonymgroups");

			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void saveGroup(String values) {

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);

			Statement st = conn.createStatement();
			String query = "insert into synonymgroups (groupId, source, target) " + "values " + values + "";

			// System.out.println(query);
			st.executeUpdate(query);
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void saveGroupsFromFile(String filename) {

		int idCount = 1;
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
				idCount = splitAndSaveGroup(idCount, sCurrentLine);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public int splitAndSaveGroup(int idCount, String sCurrentLine) {
		StringBuilder sb = new StringBuilder();
		String[] splits = sCurrentLine.split(" ");
		List<String> splitList = new ArrayList<>();
		splitList.addAll(Arrays.asList(splits));

		if (!hasRightTarget(splitList)) {
			splitList = evaluateRightTarget(splitList);
		}

		Integer groupId = getExistingGroupId(splitList.get(0));
		if (groupId == -1) {
			groupId = idCount++;
		}

		String target = splitList.get(0);

		for (int i = 1; i < splits.length; i++) {
			sb.append(" (");
			sb.append(groupId);
			sb.append(space);
			sb.append(splits[i]);
			sb.append("'");
			sb.append(space);
			sb.append(target);
			sb.append("'");
			sb.append("),");

		}

		saveGroup(sb.substring(0, sb.length() - 1));
		return idCount;
	}

	private List<String> evaluateRightTarget(List<String> tags) {
		Map<String, Integer> splitTagCounter = new HashMap<>();

		// ermittle verteilung der subtags

		for (String tag : tags) {
			String[] splits = tag.split("-");
			for (String split : splits) {
				if (!splitTagCounter.containsKey(split)) {
					splitTagCounter.put(split, 1);

				} else {
					int count = splitTagCounter.get(split);
					splitTagCounter.put(split, ++count);
				}

			}
		}

		// subtag der am öftesten vorkommt
		String largestMap = getLargestMap(splitTagCounter);
		if (tags.contains(largestMap)) {
			tags = setTagToTarget(largestMap, tags);
		} else {
			// sanity check for groups
			tags = applySanityCheckForGroups(tags);
		}

		return tags;
	}

	public List<String> setTagToTarget(String newTarget, List<String> tags) {

		List<String> returnList = new ArrayList<>();
		returnList.addAll(tags);

		for (int i = 0; i < tags.size(); i++) {
			String tag = tags.get(i);
			if (tag.equals(newTarget)) {

				return switchListEntries(returnList, 0, i);

			}
		}

		return returnList;

	}

	/**
	 * switches entries of list with index1 und index2
	 * 
	 * @param tags
	 * @param index1
	 * @param index2
	 * @return
	 */
	public List<String> switchListEntries(List<String> tags, int index1, int index2) {

		List<String> returnList = new ArrayList<>();
		returnList.addAll(tags);

		if (index1 == index2) {
			return tags;
		}

		String newIndex2 = returnList.get(index1);
		String newIndex1 = returnList.get(index2);
		returnList.set(index1, newIndex1);
		returnList.set(index2, newIndex2);

		return returnList;

	}

	public String getLargestMap(Map<String, Integer> map) {
		int maxValue = -1;
		String maxKey = "";

		for (Entry<String, Integer> entry : map.entrySet()) {

			if (maxValue < entry.getValue()) {
				maxValue = entry.getValue();
				maxKey = entry.getKey();
			}
		}
		return maxKey;

	}

	public boolean hasRightTarget(List<String> splits) {

		String target = splits.get(0);

		int targetCounter = 0;
		for (int i = 1; i < splits.size(); i++) {
			if (splits.get(i).contains(target)) {
				targetCounter++;
			}
		}

		if (targetCounter >= ((splits.size() - 1.0) / 4.0 * 3.0)) {
			return true;
		}

		return false;
	}

	private int switchcounter = 0;

	/**
	 * sanity check for target
	 * 
	 * @param splits
	 * @return
	 */
	private List<String> applySanityCheckForGroups(List<String> splits) {

		String target = splits.get(0);
		int maxCount = getPostCountForTag(target);

		for (int i = 1; i < splits.size(); i++) {

			Integer count_;
			if ((count_ = getPostCountForTag(splits.get(i))) > maxCount) {
				// if(splits[i].matches("\\.*\\d\\.?-?\\d?"))

				LevensteinDistance ld = new LevensteinDistance();
				if (isSourceTarget(splits.get(0), splits.get(i)) || (ld.getDistance(splits.get(0), splits.get(i)) > 0.65 || splits.get(i).contains(splits.get(0)))) {
					maxCount = count_;
					String oldTarget = target;

					target = splits.get(i);

					splits.set(0, target);
					splits.set(i, oldTarget);

					switchcounter++;
				}

				// System.out.println(target + " <--- " + oldTarget);

			}
		}

		return splits;
	}

	private boolean isSourceTarget(String source, String target) {

		boolean isSourceTarget = false;
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select source, target from synonym_candidates where source = '" + source + "' and target = '" + target + "' and ranking > 0.55");

			String s = "";
			String t = "";
			while (tags_db.next()) {

				s = tags_db.getString("source");
				t = tags_db.getString("target");

			}

			if (s.equals(source) && t.equals(target)) {
				isSourceTarget = true;
			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return isSourceTarget;
	}

	private Integer getPostCountForTag(String tagname) {

		Integer count = -1;

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery("select Count from Tags where tagname like '" + tagname + "'");

			while (tags_db.next()) {

				count = tags_db.getInt("Count");

			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}

}
