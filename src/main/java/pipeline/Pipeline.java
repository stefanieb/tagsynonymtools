package pipeline;

import java.io.IOException;

import datahandling.handleData.GroupTagsNeu;
import datahandling.handleData.TrendsForTopics;
import helper.Post2Tags;
import helper.Post2TagsInARow;
import helper.TagSplit;
import tagsynonymprediction.main.TagSynonymPrediction;

public class Pipeline {

	public static void main(String[] args) throws IOException {

		// Preprocessing
		// preproccesing();

		// Synonym Prediction
		// synonymPrediction();
		// System.out.println("FINISHED: tag synonym prediction");

		// Grouping of Synonyms
		// String pathToFile = groupingOfSynonyms();
		// System.out.println("patToFile: " + pathToFile);

		// String pathToFile = "/Users/stefanie/Documents/workspace_mars/TagSynonymTools/src/main/resources/generatedData/1444819200928";

		// Save Groups To DB
		// GroupsToDB g2db = new GroupsToDB();
		// g2db.startSaveGroupsToDB(pathToFile);
		// System.out.println("FINISHED: save groups");

		// Save Post To Synonym
		// Post2Synonym p2s = new Post2Synonym();
		// p2s.startPost2Synonym();
		// System.out.println("FINISHED: post to synonyms");

		// Trends For Topic
		TrendsForTopics tft = new TrendsForTopics();
		tft.startTrendsForTopics();
		System.out.println("FINISHED: trends for topic");

	}

	private static String groupingOfSynonyms() {
		GroupTagsNeu gtn = new GroupTagsNeu();
		gtn.saveIntoFile(true);
		gtn.setSelectedTagsOnly(false);
		gtn.startGroupingOfTags();
		String pathToFile = gtn.getPathToFile();
		return pathToFile;
	}

	private static void synonymPrediction() {
		TagSynonymPrediction tsp = new TagSynonymPrediction();
		tsp.saveIntoDB(true);
		tsp.setPrintResults(false);
		tsp.setUseTestTags(false);
		tsp.startTagPrediction();
	}

	private static void preproccesing() {
		Post2Tags p2t = new Post2Tags();
		p2t.startPost2Tags();

		Post2TagsInARow p2tiar = new Post2TagsInARow();
		p2tiar.startPost2TagInARow();

		TagSplit ts = new TagSplit();
		ts.initSplitAndStemmedTags();
		ts.calculateSimilarityForEachTag();
		ts.calculateMetaphone();
	}

}
