package datahandling.handleData;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestGroupsToDB {

	private GroupsToDB gtdb;

	@Before
	public void init() {
		gtdb = new GroupsToDB();
	}

	@Test
	public void testCheckForRightTargetRight() {

		List<String> splits = new ArrayList<>();

		splits.add(".net");
		splits.add("asp.net");
		splits.add(".net-4.6");
		splits.add("testen-.net");
		splits.add("asp");

		Assert.assertTrue("right target should be set", gtdb.hasRightTarget(splits));

	}

	@Test
	public void testCheckForRightTargetFalse() {

		List<String> splits = new ArrayList<>();

		splits.add("asp.net");
		splits.add(".net");
		splits.add("net-4.6");
		splits.add("testen-.net");

		Assert.assertFalse("failure, should be false", gtdb.hasRightTarget(splits));

	}

	@Test
	public void testSwitchListEntries() {

		int index1 = 0;
		int index2 = 1;

		List<String> testList = new ArrayList<>();
		testList.add("a1");
		testList.add("a2");
		testList.add("a3");
		testList.add("a4");

		List<String> returnList = gtdb.switchListEntries(testList, index1, index2);
		Assert.assertEquals("Strings sollten vertauscht sein", returnList.get(index1), testList.get(index2));
		Assert.assertEquals("Strings sollten vertauscht sein", returnList.get(index2), testList.get(index1));

	}

	@Test
	public void testSwitchListEntriesSameIndex() {

		int index1 = 1;
		int index2 = 1;

		List<String> testList = new ArrayList<>();
		testList.add("a1");
		testList.add("a2");
		testList.add("a3");
		testList.add("a4");

		List<String> returnList = gtdb.switchListEntries(testList, index1, index2);
		Assert.assertEquals("Strings sollten vertauscht sein", returnList.get(index1), testList.get(index2));
		Assert.assertEquals("Strings sollten vertauscht sein", returnList.get(index2), testList.get(index1));

	}

	@Test
	public void blub() {
		String groupData = "google-maps-android-api-2 apple-maps google-maps-android-api-1 world-map ios6-maps android-mapping mapping-resources memory-mapped-files google-maps-api-3 google-maps-api-2 google-maps-sdk-ios message-map android-maps-extensions map-force map-edit uv-mapping actionbarsherlock-map plotgooglemaps rgooglemaps google-maps-api bump-mapping jasper-reports-map mappings mapping-by-code here-maps google-maps-timezone google-static-maps object-object-mapping gmaps map-projections mapped-drive disparity-mapping yandex-maps android-maps enum-map bing-maps karnaugh-map source-maps range-map ordered-map array-map unordered-map maps bing-maps-api google-maps google-maps-markers identity-map";

		gtdb.splitAndSaveGroup(1, groupData);
	}
}
