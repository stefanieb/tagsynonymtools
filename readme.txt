### TSST ###
Start TSST from command line with: java -cp TSST-improved.jar tagsynonymprediction.main.Main <tagname> <number of suggestions> <dbname>

### Preprocessor ###
Start Preprocessor with:  java -cp preprocessor.jar helper.Preprocessor <optional argument>
To start only one preprocessing step, start preprocessor with argument 'split_stem', 'similarity', or 'metaphone'

The name of the database is SO_Nov2014. The tags to process should be in a table named Tags with the columns ID (int(11) and TagName(varchar(45)).


